//
//  UIView+Addons.m
//  Tamaranga
//
//  Created by Maks on 6/4/16.
//  Copyright © 2016 Maks. All rights reserved.
//

#define kCustomGreyColor [UIColor colorWithRed:(218.f/255) green:(218.f/255) blue:(218.f/255) alpha:1]

#import "UIView+Addons.h"

@implementation UIView (Addons)

@dynamic bottomBorderWidth, topBorderWidth;

- (void)setBottomBorderWidth:(CGFloat)bottomBorderWidth {
    
    UIView *bottomBorderLine = [[UIView alloc]initWithFrame:CGRectMake(0.0f, CGRectGetHeight(self.frame) - bottomBorderWidth, CGRectGetWidth(self.frame), bottomBorderWidth)];
    bottomBorderLine.backgroundColor = kCustomGreyColor;
    //bottomBorderLine.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    bottomBorderLine.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:bottomBorderLine];
    [self setConstraintsToBottomBorderLine:bottomBorderLine];
}

- (void)setTopBorderWidth:(CGFloat)topBorderWidth {
    
    UIView *topBorderLine = [[UIView alloc]initWithFrame:CGRectMake(0.0f, 0.0f, CGRectGetWidth(self.frame), topBorderWidth)];
    topBorderLine.backgroundColor = kCustomGreyColor;
    topBorderLine.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    [self addSubview:topBorderLine];
}

- (void)setConstraintsToBottomBorderLine:(UIView *)bottomBorderLine {
    NSLayoutConstraint *bottom = [NSLayoutConstraint constraintWithItem:bottomBorderLine
                                                              attribute:NSLayoutAttributeBottom
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:self
                                                              attribute:NSLayoutAttributeBottom
                                                             multiplier:1
                                                               constant:0];
    
    NSLayoutConstraint *trailing = [NSLayoutConstraint constraintWithItem:bottomBorderLine
                                                              attribute:NSLayoutAttributeTrailing
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:self
                                                              attribute:NSLayoutAttributeTrailing
                                                             multiplier:1
                                                               constant:0];
    
    NSLayoutConstraint *leading = [NSLayoutConstraint constraintWithItem:bottomBorderLine
                                                                attribute:NSLayoutAttributeLeading
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:self
                                                                attribute:NSLayoutAttributeLeading
                                                               multiplier:1
                                                                 constant:0];
    
    NSLayoutConstraint *height = [NSLayoutConstraint constraintWithItem:bottomBorderLine
                                                               attribute:NSLayoutAttributeHeight
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:nil
                                                               attribute:NSLayoutAttributeNotAnAttribute
                                                              multiplier:1
                                                                constant:CGRectGetHeight(bottomBorderLine.frame)];
    [bottomBorderLine addConstraint:height];
    [self addConstraints:@[bottom, trailing, leading]];
}

@end
