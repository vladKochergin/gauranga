//
//  AppDelegate.m
//  Gauranga
//
//  Created by yvp on 2/7/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import "AppDelegate.h"
#import <RamblerTyphoonUtils/AssemblyCollector.h>
#import "SlideNavigationController.h"
#import "SlideMenu.h"
#import <UserNotifications/UserNotifications.h>
#import "DataManager.h"
#import "PushNotificationHandler.h"

@interface AppDelegate () <UNUserNotificationCenterDelegate>

@property (nonatomic, strong) PushNotificationHandler *pushNotificationHandler;

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:[[NSBundle mainBundle].infoDictionary objectForKey:@"UIMainStoryboardFile"] bundle:[NSBundle mainBundle]];
    UIViewController *leftMenu = (SlideMenu *)[storyboard
                                                      instantiateViewControllerWithIdentifier: @"LeftMenuViewController"];
    
    [[SlideNavigationController sharedInstance].navigationBar setBarTintColor:[UIColor mainColor]];
    [SlideNavigationController sharedInstance].leftMenu = leftMenu;
    [SlideNavigationController sharedInstance].menuRevealAnimationDuration = .18;
    
    if (![[[NSUserDefaults standardUserDefaults] objectForKey:@"firstLaunching"] boolValue]) {
        [[DataManager sharedInstance] delUserProfile];
        [[WebService sharedService] delAToken];
        [[NSUserDefaults standardUserDefaults] setObject:@(1) forKey:@"firstLaunching"];
    }
    self.pushNotificationHandler = [PushNotificationHandler new];

    [self registerForPushNotifications];
    
    return YES;
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(nonnull NSDictionary *)userInfo fetchCompletionHandler:(nonnull void (^)(UIBackgroundFetchResult))completionHandler {
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSString *token = [[deviceToken description] stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    [[WebService sharedService] saveAPNSToken:token];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"%@", error.localizedDescription);
}

- (NSArray *)initialAssemblies {
    RamblerInitialAssemblyCollector *collector = [RamblerInitialAssemblyCollector new];
    return [collector collectInitialAssemblyClasses];
}

#warning ПРОВЕРИТЬ

- (void)registerForPushNotifications { ///////Проверить на ios 9...
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    center.delegate = self;
    [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error) {
        if (granted) {
            [self getNotificationSettings];
        }
    }];
}

- (void)getNotificationSettings {
    [[UNUserNotificationCenter currentNotificationCenter] getNotificationSettingsWithCompletionHandler:^(UNNotificationSettings * _Nonnull settings) {
        if (settings.authorizationStatus == UNAuthorizationStatusAuthorized) {
            [[UIApplication sharedApplication] registerForRemoteNotifications];
        }
    }];
}

#pragma mark - UNUserNotificationCenterDelegate

- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler {
    NSDictionary *userInfo = response.notification.request.content.userInfo;
    [self.pushNotificationHandler handlePushWithUserInfo:userInfo rootViewController:(SlideNavigationController *)self.window.rootViewController];
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler {
    NSDictionary *userInfo = notification.request.content.userInfo;
    [self.pushNotificationHandler showPushWithUserInfo:userInfo withCompletionHandler:^(UNNotificationPresentationOptions options) {
        completionHandler(options);
    }];
}

- (void)applicationWillResignActive:(UIApplication *)application {
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
}

- (void)applicationWillTerminate:(UIApplication *)application {
}

@end
