//
//  PushNotificationHandler.h
//  Gauranga
//
//  Created by a on 8/3/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SlideNavigationController.h"

#import <UserNotifications/UserNotifications.h>

@interface PushNotificationHandler : NSObject

- (void)handlePushWithUserInfo:(NSDictionary *)userInfo rootViewController:(SlideNavigationController *)slideNavigationController;
- (void)showPushWithUserInfo:(NSDictionary *)userInfo withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler;

@end
