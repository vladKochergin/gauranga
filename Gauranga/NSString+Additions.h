//
//  NSString+Additions.h
//  Gauranga
//
//  Created by egor on 6/18/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Additions)

- (BOOL)isEmpty;

@end
