//
//  RequestErrorHandler.h
//  Gauranga
//
//  Created by a on 8/4/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RequestErrorHandler : NSObject

+ (void)handleError:(NSError *)error;

@end
