//
//  DynamicPropsRangeModel.m
//  Gauranga
//
//  Created by egor on 6/13/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import "DynamicPropsRangeModel.h"

@implementation DynamicPropsRangeModel

+ (EKObjectMapping *)objectMapping{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapKeyPath:@"search_range_user" toProperty:@"searchRangeUser"];
        [mapping mapKeyPath:@"search_ranges" toProperty:@"searchRanges"];
        [mapping mapKeyPath:@"start" toProperty:@"start"];
        [mapping mapKeyPath:@"step" toProperty:@"step"];
        [mapping mapKeyPath:@"end" toProperty:@"end"];
    }];
}

+ (instancetype)dynamicPropertyRangeWithDictionary:(NSDictionary *)JSONresponse{
    return [DynamicPropsRangeModel objectWithProperties:JSONresponse];
}

@end
