//
//  DynamicPropsRangeModel.h
//  Gauranga
//
//  Created by egor on 6/13/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import <EasyMapping/EasyMapping.h>
#import <Foundation/Foundation.h>

@interface DynamicPropsRangeModel : EKObjectModel

@property (nonatomic) double searchRangeUser;
@property (nonatomic) double start;
@property (nonatomic) double step;
@property (nonatomic) double end;
@property (nonatomic, strong) NSArray *searchRanges;

+ (instancetype)dynamicPropertyRangeWithDictionary:(NSDictionary *)JSONresponse;

@end
