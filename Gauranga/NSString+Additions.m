//
//  NSString+Additions.m
//  Gauranga
//
//  Created by egor on 6/18/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import "NSString+Additions.h"

@implementation NSString (Additions)

- (BOOL)isEmpty {
    if(self.length == 0) {
        return YES;
    }
    if(![[self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length]) {
        return YES;
    }
    return NO;
}

@end
