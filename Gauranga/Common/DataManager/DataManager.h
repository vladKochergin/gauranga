//
//  DataManager.h
//  Gauranga
//
//  Created by yvp on 2/9/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CurrencyModel;

@interface DataManager : NSObject

+ (instancetype)sharedInstance;

/*User Profile*/
- (void)saveUserProfile:(id)userProfile;
- (id)getUserProfile;
- (void)delUserProfile;

/*User Profile*/
- (void)saveCurrency:(id)currency;
- (NSDictionary*)getCurrency;

@end
