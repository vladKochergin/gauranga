//
//  DataManager.m
//  Gauranga
//
//  Created by yvp on 2/9/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import "DataManager.h"
#import "FDKeychain.h"

@interface DataManager()

@end

@implementation DataManager

+ (instancetype)sharedInstance{
    static DataManager *dataManager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dataManager = [DataManager new];
    });
    
    return dataManager;
}

#pragma mark - UserProfile

- (void)saveUserProfile:(id)userProfile{
    NSError *error = nil;
    [FDKeychain saveItem: userProfile
                  forKey: @"userProfile"
              forService: @"Gauranga"
                   error: &error];
}

- (id)getUserProfile{
    NSError *error = nil;
    id userProfile = [FDKeychain itemForKey:@"userProfile"
                                 forService:@"Gauranga"
                                      error:&error];
    return userProfile;
}

- (void)delUserProfile{
    NSError *error = nil;
    [FDKeychain deleteItemForKey:@"userProfile"
                      forService:@"Gauranga"
                           error:&error];
}

#pragma mark - Currency

- (void)saveCurrency:(id)currency{
    NSError *error = nil;
    [FDKeychain saveItem: currency
                  forKey: @"currency"
              forService: @"Gauranga"
                   error: &error];
}

- (NSDictionary *)getCurrency{
    NSError *error = nil;
    id currency = [FDKeychain itemForKey:@"currency"
                                 forService:@"Gauranga"
                                      error:&error];
    return currency;
}

@end
