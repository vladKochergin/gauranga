//
//  UIColor.m
//  Gauranga
//
//  Created by yvp on 4/7/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import "UIColor+CustomColor.h"

@implementation UIColor (CustomColor)

+ (UIColor *)mainColor{
    return [UIColor colorWithRed:56.0f/255.0f green:171.0f/255.0f blue:222.0f/255.0f alpha:1];
}

@end
