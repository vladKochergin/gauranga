//
//  UIColor.h
//  Gauranga
//
//  Created by yvp on 4/7/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (CustomColor)

+ (UIColor *)mainColor;

@end
