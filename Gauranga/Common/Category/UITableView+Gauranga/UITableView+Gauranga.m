//
//  UITableView+Gauranga.m
//  Gauranga
//
//  Created by yvp on 2/7/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import "UITableView+Gauranga.h"

@implementation UITableView (Gauranga)

- (void)reloadDataWithCheck{
    [self reloadData];
    [self checkDataInTable];
    [self hiddenRefreshControl];
}

#pragma mark RefreshControl

- (void)hiddenRefreshControl{
    for(UIView *view in self.subviews){
        if([view isKindOfClass:[UIRefreshControl class]])
            [view performSelector:@selector(endRefreshing)];
    }
    //[self checkDataInTable];
}

- (BOOL)isSpinnerShow{
    for(UIRefreshControl *view in self.subviews){
        if([view isKindOfClass:[UIRefreshControl class]])
            return view.isRefreshing;
    }
    return NO;
}

- (void)showRefreshControl{
    if([[self.subviews lastObject] respondsToSelector:@selector(beginRefreshing)])
        [[self.subviews lastObject] performSelector:@selector(beginRefreshing)];
}

- (void)addRefreshControlWithAction:(SEL)sel view:(UIViewController *)view{
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:view action:sel forControlEvents:UIControlEventValueChanged];
    refreshControl.layer.zPosition = -1;
    [self addSubview:refreshControl];
}

#pragma mark - PagingSpinner

- (void)showPagingSpinner{
   UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, [UIScreen mainScreen].bounds.size.width, 40.0)];
    [footerView setRestorationIdentifier:@"PagSpinner"];
    
    UIActivityIndicatorView * actInd = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    actInd.tag = 10;
    actInd.frame = CGRectMake([UIScreen mainScreen].bounds.size.width/2, 5.0, 20.0, 20.0);
    actInd.hidesWhenStopped = YES;
    [actInd startAnimating];
    [footerView addSubview:actInd];
    self.tableFooterView = footerView;
}

- (void)hiddenPagingSpinner{
    for(UIView *view in self.subviews){
        if([view isKindOfClass:[UIView class]])
            if([view.restorationIdentifier isEqualToString:@"PagSpinner"])
                [view removeFromSuperview];
    }
}

#pragma mark - Check

- (void)checkDataInTable{
    if(![[self indexPathsForVisibleRows] count]){
        UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height)];
        noDataLabel.backgroundColor  = [UIColor whiteColor];
        noDataLabel.font = [UIFont systemFontOfSize:25];
        noDataLabel.text             = @"Нет данных...";
        noDataLabel.textColor        = [UIColor blackColor];
        noDataLabel.textAlignment    = NSTextAlignmentCenter;
        noDataLabel.tag = 999;
        [self addSubview:noDataLabel];
    }else{
        for(UIView *view in self.subviews)
            if(view.tag==999)
                [view removeFromSuperview];
    }
}

@end
