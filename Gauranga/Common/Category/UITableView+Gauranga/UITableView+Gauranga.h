//
//  UITableView+Gauranga.h
//  Gauranga
//
//  Created by yvp on 2/7/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableView (Gauranga)<UITableViewDelegate>

//@property(nonatomic, strong) UIRefreshControl *refreshControl;

- (void)reloadDataWithCheck;

- (void)addRefreshControlWithAction:(SEL)sel view:(UIViewController *)view;
- (void)showRefreshControl;
- (void)hiddenRefreshControl;

- (void)showPagingSpinner;
- (void)hiddenPagingSpinner;

- (BOOL)isSpinnerShow;

@end
