//
//  UICollectionView+RefreshControl.h
//  Gauranga
//
//  Created by yvp on 3/1/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UICollectionView (RefreshControl) <UICollectionViewDelegate>

- (void)reloadDataWithCheck;

- (void)addRefreshControlWithAction:(SEL)sel view:(UIViewController *)view;
- (void)showRefreshControl;
- (void)hiddenRefreshControl;

- (void)showPagingSpinner;
- (void)hiddenPagingSpinner;

- (BOOL)isSpinnerShow;

@end
