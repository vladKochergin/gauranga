//
//  UICollectionView+RefreshControl.m
//  Gauranga
//
//  Created by yvp on 3/1/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import "UICollectionView+RefreshControl.h"

@implementation UICollectionView (RefreshControl)

- (void)reloadDataWithCheck{
    [self reloadData];
    [self checkDataInTable];
    [self hiddenRefreshControl];
}

#pragma mark RefreshControl

- (void)hiddenRefreshControl{
    for(UIView *view in self.subviews){
        if([view isKindOfClass:[UIRefreshControl class]])
            [view performSelector:@selector(endRefreshing)];
    }
    [self checkDataInTable];
}

- (BOOL)isSpinnerShow{
    for(UIRefreshControl *view in self.subviews){
        if([view isKindOfClass:[UIRefreshControl class]])
            return view.isRefreshing;
    }
    return NO;
}

- (void)showRefreshControl{
    if([[self.subviews lastObject] respondsToSelector:@selector(beginRefreshing)])
        [[self.subviews lastObject] performSelector:@selector(beginRefreshing)];
}

- (void)addRefreshControlWithAction:(SEL)sel view:(UIViewController *)view{
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:view action:sel forControlEvents:UIControlEventValueChanged];
    refreshControl.layer.zPosition = -1;
    [self addSubview:refreshControl];
}

#pragma mark - PagingSpinner

- (void)showPagingSpinner{
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 40.0)];
    [footerView setRestorationIdentifier:@"PagSpinner"];
    
    UIActivityIndicatorView * actInd = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    actInd.tag = 10;
    actInd.frame = CGRectMake(150.0, 5.0, 20.0, 20.0);
    actInd.center = CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
    actInd.hidesWhenStopped = YES;
    [actInd startAnimating];
    [footerView addSubview:actInd];
    [self addSubview:footerView];
}

- (void)hiddenPagingSpinner{
    for(UIView *view in self.subviews){
        if([view isKindOfClass:[UIView class]])
            if([view.restorationIdentifier isEqualToString:@"PagSpinner"])
                [view removeFromSuperview];
    }
}

#pragma mark - Check

-(void) checkDataInTable{
    if(![self numberOfItemsInSection:0]){
        UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height)];
        noDataLabel.backgroundColor  = [UIColor whiteColor];
        noDataLabel.font = [UIFont systemFontOfSize:25];
        noDataLabel.text             = @"Нет данных...";
        noDataLabel.textColor        = [UIColor blackColor];
        noDataLabel.textAlignment    = NSTextAlignmentCenter;
        noDataLabel.tag = 999;
        [self addSubview:noDataLabel];
    }else{
        for(UIView *view in self.subviews)
            if(view.tag==999)
                [view removeFromSuperview];
    }
}

@end
