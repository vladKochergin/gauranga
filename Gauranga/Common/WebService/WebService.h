//
//  WebService.h
//  Gauranga
//
//  Created by yvp on 2/7/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

typedef void(^WebServiceCompletionBlock)(NSError *error, NSArray *array, NSDictionary *dictionary);

@interface WebService : AFHTTPSessionManager

/* Core Methods */
+ (WebService *)sharedService;

/*Login*/
- (void)loginWitnEmail:(NSString *)email password:(NSString *)password completion:(WebServiceCompletionBlock)completion;

/*Registration*/
- (void)signUpWitnEmail:(NSString *)email password:(NSString *)password completion:(WebServiceCompletionBlock)completion;

/*SignOut*/
 
- (void)signOutWithCompletion:(WebServiceCompletionBlock)completion;

/*Catalog*/
- (void)getCatalogWithDictionaryParam:(NSDictionary *)param userId:(NSInteger)userId completion:(WebServiceCompletionBlock)completion; //PZDC
- (void)getCategoryWithLVL:(NSInteger)lvl pID:(NSInteger)pID completion:(WebServiceCompletionBlock)completion;
- (void)getRegionWithPid:(NSInteger)pID completion:(WebServiceCompletionBlock)completion;
- (void)addToFavoriteWithItemId:(NSInteger)itemId action:(NSString *)action completion:(WebServiceCompletionBlock)completion;
- (void)getFavoritesWithUserId:(NSInteger)userId page:(NSInteger)page completion:(WebServiceCompletionBlock)completion;
- (void)editProfileWithName:(NSString *)name phone:(NSString *)phone image:(NSString *)image completion:(WebServiceCompletionBlock)completion;
- (void)updateStatusAdvertising:(NSInteger )adID status:(NSString *)status
                     completion:(WebServiceCompletionBlock)completion;
/*Add Advertising*/
- (void)getDynamicPropertyWithCategoryId:(NSString *)categoryId completion:(WebServiceCompletionBlock)completion;
- (void)sendAdvertising:(NSDictionary *)param completion:(WebServiceCompletionBlock)completion;

/*Detail*/
- (void)getDetailAdvertisingWithId:(NSInteger)advId completion:(WebServiceCompletionBlock)completion;
- (void)sendReportWithId:(NSInteger)reportId message:(NSString *)message advId:(NSInteger)advId completion:(WebServiceCompletionBlock)completion;
- (void)updateAdvertising:(NSDictionary *)param completion:(WebServiceCompletionBlock)completion;


/*Chat*/
- (void)getDialogList:(WebServiceCompletionBlock)completion;
- (void)getMessageListWitInterlocutor:(NSInteger)interlocutor page:(NSInteger)page completion:(WebServiceCompletionBlock)completion;
- (void)sendMessageWithItemId:(NSInteger)itemId message:(NSString *)message completion:(WebServiceCompletionBlock)completion;
- (void)sendMessageWithpartnerId:(NSInteger)partnerId message:(NSString *)message completion:(WebServiceCompletionBlock)completion;

/*Additional Method*/
- (void)delAToken;
- (BOOL)checkToken;

- (void)saveAPNSToken:(NSString *)token;
- (void)deleteAPNSToken;
- (BOOL)checkAPNSToken;

@end
