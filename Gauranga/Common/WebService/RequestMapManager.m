//
//  RequestMapManager.m
//  Gauranga
//
//  Created by yvp on 2/7/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import "RequestMapManager.h"
#import "WebService.h"
#import "DataManager.h"
#import "RequestErrorHandler.h"

/*Models*/
#import "CatalogModel.h"
#import "CategoryModel.h"
#import "UserModel.h"
#import "CurrencyModel.h"
#import "DynamicPropertyModel.h"
#import "RegionModel.h"
#import "DetailAdvertisingModel.h"
#import "dopParamModel.h"
#import "MailModel.h"
#import "DialogModel.h"
#import "KVMessageModel.h"

@implementation RequestMapManager

+ (void)getCatalogWithDictionaryParam:(NSDictionary *)param userId:(NSInteger)userId completion:(RequestCompletion)completion{
    [[WebService sharedService] getCatalogWithDictionaryParam:param userId:userId completion:^(NSError *error, NSArray *array, NSDictionary *dictionary) {
        NSMutableArray *itemArray = [NSMutableArray new];
        NSMutableDictionary *curDic = [NSMutableDictionary new];
        
        if(!error){
            for(NSDictionary *dic in [dictionary objectForKey:@"currency_list"]){
                CurrencyModel *curModel = [CurrencyModel currencyWithDictionary:dic];
                [curDic setValue:curModel forKey:[NSString stringWithFormat:@"%li",curModel.currencyId]];
            }
            [[DataManager sharedInstance] saveCurrency:curDic];
            for(NSDictionary *dic in [[dictionary objectForKey:@"items_list"] objectForKey:@"rows"]){
                [itemArray addObject:[CatalogModel catalogWithDictionary:dic]];
            }
        }
        completion(itemArray, nil, nil);
    }];
}

+ (void)getCategoryWithLVL:(NSInteger)lvl pID:(NSInteger)pID completion:(RequestCompletion)completion{
    [[WebService sharedService] getCategoryWithLVL:lvl pID:pID completion:^(NSError *error, NSArray *array, NSDictionary *dictionary) {
        if(error) {
            //[MBProgressHUD hideHUDForView:[WireFrame getView].view animated:YES];
            [RequestErrorHandler handleError:error];
            return;
        }
        NSMutableArray *categoryArray = [NSMutableArray new];
        if(!error){
            for(NSDictionary *dic in [[dictionary objectForKey:@"categ_list"] objectForKey:@"rows"]){
                CategoryModel *model = [CategoryModel categoryWithDictionary:dic];
                model.imagePath = [[dictionary objectForKey:@"categ_list"] objectForKey:@"img_path"];
                [categoryArray addObject:model];
            }
        }
        completion(categoryArray, nil, nil);
    }];
    
}

+ (void)loginWitnEmail:(NSString *)email password:(NSString *)password completion:(RequestCompletion)completion{
    [[WebService sharedService] loginWitnEmail:email password:password completion:^(NSError *error, NSArray *array, NSDictionary *dictionary) {
        if(error) {
            //[MBProgressHUD hideHUDForView:[WireFrame getView].view animated:YES];
            [RequestErrorHandler handleError:error];
            return;
        }
        UserModel *model;
        if(!error){
            model = [UserModel userWithDictionary:[dictionary objectForKey:@"user_info"]];
            [[DataManager sharedInstance] saveUserProfile:model];
        }
        completion(nil, nil, model);
    }];
}

+ (void)signUpWitnEmail:(NSString *)email password:(NSString *)password completion:(RequestCompletion)completion{
    [[WebService sharedService] signUpWitnEmail:email password:password completion:^(NSError *error, NSArray *array, NSDictionary *dictionary) {
        if(error) {
            //[MBProgressHUD hideHUDForView:[WireFrame getView].view animated:YES];
            [RequestErrorHandler handleError:error];
            return;
        }
        UserModel *model;
        if(!error){
            model = [UserModel userWithDictionary:[dictionary objectForKey:@"user_info"]];
            [[DataManager sharedInstance] saveUserProfile:model];
        }
        completion(nil, nil, model);
    }];
}


+ (void)getFavoritesWithUserId:(NSInteger)userId page:(NSInteger)page completion:(RequestCompletion)completion{
    [[WebService sharedService] getFavoritesWithUserId:userId page:page completion:^(NSError *error, NSArray *array, NSDictionary *dictionary) {
        NSMutableArray *itemArray = [NSMutableArray new];
        if(!error){
            for(NSDictionary *dic in [[dictionary objectForKey:@"items_list"] objectForKey:@"rows"]){
                [itemArray addObject:[CatalogModel catalogWithDictionary:dic]];
            }
        }
        completion(itemArray, nil, nil);
    }];
}

+ (void)editProfileWithName:(NSString *)name phone:(NSString *)phone image:(NSString *)image completion:(RequestCompletion)completion{
    [[WebService sharedService] editProfileWithName:name phone:phone image:image completion:^(NSError *error, NSArray *array, NSDictionary *dictionary) {
        if(error) {
            //[MBProgressHUD hideHUDForView:[WireFrame getView].view animated:YES];
            [RequestErrorHandler handleError:error];
            return;
        }
        UserModel *model;
        if(!error){
            model = [UserModel userWithDictionary:[dictionary objectForKey:@"user_info"]];
            [[DataManager sharedInstance] saveUserProfile:model];
        }
        completion(nil, nil, model);
    }];
}

+ (void)getDynamicPropertyWithCategoryId:(NSString *)categoryId completion:(RequestCompletion)completion{
    [[WebService sharedService] getDynamicPropertyWithCategoryId:categoryId completion:^(NSError *error, NSArray *array, NSDictionary *dictionary) {
        if(error) {
            //[MBProgressHUD hideHUDForView:[WireFrame getView].view animated:YES];
            [RequestErrorHandler handleError:error];
            return;
        }
        NSMutableArray *dynamicPropertyArray = [NSMutableArray new];
        if(!error){
            for(NSDictionary *dic in [dictionary objectForKey:@"add_info"]){
                DynamicPropertyModel *model = [DynamicPropertyModel dynamicPropertyWithDictionary:dic];
                NSMutableArray *listArray = [NSMutableArray new];
                
                for(NSDictionary *dicP in model.list){
                    [listArray addObject:[dicP objectForKey:@"n"]];
                }
                model.list = listArray;
                [dynamicPropertyArray addObject:model];
                [model updateList];
            }
        }
        completion(dynamicPropertyArray, nil, nil);
    }];
}

+ (void)getRegionWithPid:(NSInteger)pID completion:(RequestCompletion)completion{
    [[WebService sharedService] getRegionWithPid:pID completion:^(NSError *error, NSArray *array, NSDictionary *dictionary) {
        if(error) {
            //[MBProgressHUD hideHUDForView:[WireFrame getView].view animated:YES];
            [RequestErrorHandler handleError:error];
            return;
        }
        NSMutableArray *regionArray = [NSMutableArray new];
        if(!error){
            if ([[dictionary objectForKey:@"region_list"] objectForKey:@"rows"])
                for(NSDictionary *dic in [[dictionary objectForKey:@"region_list"] objectForKey:@"rows"]){
                    RegionModel *model = [RegionModel regionWithDictionary:dic];
                    [regionArray addObject:model];
                }
        }
        completion(regionArray, nil, nil);
    }];
}

+ (void)getDetailAdvertisingWithId:(NSInteger)advId completion:(RequestCompletion)completion{
    [[WebService sharedService] getDetailAdvertisingWithId:advId completion:^(NSError *error, NSArray *array, NSDictionary *dictionary) {
        if(error) {
            //[MBProgressHUD hideHUDForView:[WireFrame getView].view animated:YES];
            [RequestErrorHandler handleError:error];
            return;
        }
        DetailAdvertisingModel *model = [DetailAdvertisingModel detailWithDictionary:[dictionary objectForKey:@"item_info"]];
        model.user = [UserModel objectWithProperties:[[dictionary objectForKey:@"item_info"] objectForKey:@"user"]];
        
        NSMutableArray *categories = [NSMutableArray new];
        if([[[dictionary objectForKey:@"item_info"] objectForKey:@"cat_list"] count]){
            for(NSDictionary *dic in [[dictionary objectForKey:@"item_info"] objectForKey:@"cat_list"]){
                CategoryModel *category = [CategoryModel objectWithProperties:dic];
                [categories addObject:category];
            }
        }
        model.categories = categories;
        
        NSMutableArray *dopParamArray = [NSMutableArray new];
        if([[[dictionary objectForKey:@"item_info"] objectForKey:@"cat_props"] count]) {
            for (NSString *key in [[[dictionary objectForKey:@"item_info"] objectForKey:@"cat_props"] allKeys]) {
                NSDictionary *dic = [[[dictionary objectForKey:@"item_info"] objectForKey:@"cat_props"] objectForKey:key];
                [dopParamArray addObject:[dopParamModel dopParamWithDictionary:dic]];
            }
        }
        
        NSMutableArray *dynamicPropertyArray = [NSMutableArray new];
        if([[[dictionary objectForKey:@"item_info"] objectForKey:@"cat_dyn_info"] count]) {
            for (NSDictionary *dic in [[dictionary objectForKey:@"item_info"] objectForKey:@"cat_dyn_info"]) {
                DynamicPropertyModel *model = [DynamicPropertyModel dynamicPropertyWithDictionary:dic];
                NSMutableArray *listArray = [NSMutableArray new];
                for(NSDictionary *dicP in model.list){
                    [listArray addObject:[dicP objectForKey:@"n"]];
                }
                model.list = listArray;
                [dynamicPropertyArray addObject:model];
            }
        }
        
        for (DynamicPropertyModel *dynModel in dynamicPropertyArray) {
            for (dopParamModel *dopModel in dopParamArray) {
                if ([dynModel.title isEqualToString:dopModel.title]) {
                    dynModel.value = dopModel.value;
                }
            }
        }
        
        NSMutableArray *imageArray = [NSMutableArray new];
        if([[[dictionary objectForKey:@"item_info"] objectForKey:@"images"] count]){
            for(NSDictionary *dic in [[dictionary objectForKey:@"item_info"] objectForKey:@"images"]){
                NSString *url = [dic objectForKey:@"path"];
                [imageArray addObject:url];
            }
        }
        
        NSMutableArray *similarArray = [NSMutableArray new];
        for(NSDictionary *dic in [[[dictionary objectForKey:@"item_info"] objectForKey:@"similar"] objectForKey:@"rows"]){
            [similarArray addObject:[CatalogModel catalogWithDictionary:dic]];
        }
        
        NSMutableArray *phones = [NSMutableArray new];
        for(NSDictionary *dic in [[dictionary objectForKey:@"item_info"] objectForKey:@"phones"]){
            [phones addObject:[dic objectForKey:@"v"]];
        }
        
        model.dynamicPropertyArray = dynamicPropertyArray;
        model.similarAdv = similarArray;
        model.images = imageArray;
        model.dopParam = dopParamArray;
        model.phones = phones;
        
        completion(nil, nil, model);
    }];
}

+ (void)getDialogList:(RequestCompletion)completion{
    [[WebService sharedService] getDialogList:^(NSError *error, NSArray *array, NSDictionary *dictionary) {
        if(error) {
            //[MBProgressHUD hideHUDForView:[WireFrame getView].view animated:YES];
            [RequestErrorHandler handleError:error];
            return;
        }
        NSMutableArray *dialogArray = [NSMutableArray new];
        for(NSDictionary *dic in [dictionary objectForKey:@"chats_list"]){
            [dialogArray addObject:[DialogModel dialogWithDictionary:dic]];
        }
        completion(dialogArray, nil, nil);
    }];
}

+ (void)getMessageListWitInterlocutor:(NSInteger)interlocutor page:(NSInteger)page completion:(RequestCompletion)completion{
    [[WebService sharedService] getMessageListWitInterlocutor:interlocutor page:page completion:^(NSError *error, NSArray *array, NSDictionary *dictionary) {
        if(error) {
            //[MBProgressHUD hideHUDForView:[WireFrame getView].view animated:YES];
            [RequestErrorHandler handleError:error];
            return;
        }
        NSMutableArray *messageArray = [NSMutableArray new];
        for(NSDictionary *dic in [dictionary objectForKey:@"chats_list"]){
            [messageArray addObject:[KVMessageModel messageWithDictionary:dic]];
        }
        completion(messageArray, nil, nil);
    }];
}

+ (void)updateStatusAdvertising:(NSInteger )adID status:(NSString *)status
                     completion:(RequestCompletion)completion {
    [[WebService sharedService] updateStatusAdvertising:adID status:status
                                             completion:^(NSError *error, NSArray *array, NSDictionary *dictionary) {
                                                 if(error) {
                                                     //[MBProgressHUD hideHUDForView:[WireFrame getView].view animated:YES];
                                                     [RequestErrorHandler handleError:error];
                                                     return;
                                                 }
                                                 [KVAlert alertWithTitle:@"Изменение статуса объявления" message:@"Готово" preferredStyle:UIAlertControllerStyleAlert completion:nil];
                                             }];
}

+ (void)updateAdvertising:(NSDictionary *)params completion:(RequestCompletion)completion{
    [[WebService sharedService] updateAdvertising:params completion:^(NSError *error, NSArray *array, NSDictionary *dictionary) {
        if(error){
            [RequestErrorHandler handleError:error];
            return;
        }
        NSDictionary *dic = [dictionary objectForKey:@"item"];
        CatalogModel *catalogModel = [CatalogModel catalogWithDictionary:dic];
        DetailAdvertisingModel *detailModel = [DetailAdvertisingModel detailWithDictionary:dic];
        
        NSDictionary *dicModels = @{
                                    @"CatalogModel" : catalogModel,
                                    @"DetailModel" : detailModel
                                    };
        
        completion(nil, dicModels, nil);
    }];
}

@end
