//
//  RequestMapManager.h
//  Gauranga
//
//  Created by yvp on 2/7/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^RequestCompletion)(NSArray *array, NSDictionary *dictionary, id object);

@interface RequestMapManager : NSObject

+ (void)getCatalogWithDictionaryParam:(NSDictionary *)param userId:(NSInteger)userId completion:(RequestCompletion)completion;
+ (void)getCategoryWithLVL:(NSInteger)lvl pID:(NSInteger)pID completion:(RequestCompletion)completion;
+ (void)loginWitnEmail:(NSString *)email password:(NSString *)password completion:(RequestCompletion)completion;
+ (void)signUpWitnEmail:(NSString *)email password:(NSString *)password completion:(RequestCompletion)completion;
+ (void)getFavoritesWithUserId:(NSInteger)userId page:(NSInteger)page completion:(RequestCompletion)completion;
+ (void)editProfileWithName:(NSString *)name phone:(NSString *)phone image:(NSString *)image completion:(RequestCompletion)completion;
+ (void)getDynamicPropertyWithCategoryId:(NSString *)categoryId completion:(RequestCompletion)completion;
+ (void)getRegionWithPid:(NSInteger)pID completion:(RequestCompletion)completion;
+ (void)getDetailAdvertisingWithId:(NSInteger)advId completion:(RequestCompletion)completion;
+ (void)getDialogList:(RequestCompletion)completion;
+ (void)getMessageListWitInterlocutor:(NSInteger)interlocutor page:(NSInteger)page completion:(RequestCompletion)completion;
+ (void)updateStatusAdvertising:(NSInteger )adID status:(NSString *)status completion:(RequestCompletion)completion;
+ (void)updateAdvertising:(NSDictionary *)params completion:(RequestCompletion)completion;

@end
