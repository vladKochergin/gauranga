//
//  WebService.m
//  Gauranga
//
//  Created by yvp on 2/7/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import "WebService.h"
#import "FDKeychain.h"

#import "DataManager.h"
#import "UserModel.h"

@interface WebService() {
    NSString *loginToken;
    NSString *APNSToken;
}

@end

@implementation WebService

+ (WebService *)sharedService{
    static WebService *service = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        //NSURL *baseURL = [NSURL URLWithString:@"https://obyava.org/api/"];
        NSURL *baseURL = [NSURL URLWithString:@"http://gauranga22.iskytest.com/api/"];
        service = [[WebService alloc]initWithBaseURL:baseURL];
        service.requestSerializer = [AFJSONRequestSerializer serializer];
        [service checkToken];
    });
    
    return service;
}

#pragma mark - Login

- (void)loginWitnEmail:(NSString *)email password:(NSString *)password completion:(WebServiceCompletionBlock)completion {
    NSMutableDictionary *parameters = [@{
                                         @"login" : email,
                                         @"passw" : password
                                         } mutableCopy];
    if (APNSToken.length) {
        [parameters setObject:APNSToken forKey:@"appleregid"];
    }
    
    NSDictionary *requestBody = @{
                                  @"user_login":parameters
                                  };
    [self POST:@"user" parameters:requestBody progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if(![self checkStatus:responseObject])
            [self saveToken:[responseObject objectForKey:@"SESID"]];
        completion([self checkStatus:responseObject], nil, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(error, nil, nil);
    }];
}

#pragma mark - Registration

- (void)signUpWitnEmail:(NSString *)email password:(NSString *)password completion:(WebServiceCompletionBlock)completion{
    NSDictionary *parameters = @{
                                 @"user_register": @{
                                         @"email" : email,
                                         @"password" : password
                                         }
                                 };
    
    [self POST:@"user" parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if(![self checkStatus:responseObject])
            [self saveToken:[responseObject objectForKey:@"SESID"]];
        completion([self checkStatus:responseObject], nil, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(error, nil, nil);
    }];
}

#pragma mark - SignOut

- (void)signOutWithCompletion:(WebServiceCompletionBlock)completion {
    NSDictionary *parameters = @{
                                 @"SESID":loginToken
                                 };
    [self POST:@"user" parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        completion([self checkStatus:responseObject], nil, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(error, nil, nil);
    }];
}

#pragma mark - Catalog

- (void)getCatalogWithDictionaryParam:(NSDictionary *)param userId:(NSInteger)userId completion:(WebServiceCompletionBlock)completion{
    
    NSMutableDictionary *parameters = [@{
                                         @"items_list" : [param mutableCopy]
                                         } mutableCopy];
    if(loginToken){
        [[parameters objectForKey:@"items_list"] setValue:[NSString stringWithFormat:@"%li",(long)userId] forKey:@"user_id:addfav"];
        [parameters setObject:loginToken forKey:@"SESID"];
    }
    [self POST:@"bbs" parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion([self checkStatus:responseObject], nil, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(error, nil, nil);
    }];
}

- (void)getCategoryWithLVL:(NSInteger)lvl pID:(NSInteger)pID completion:(WebServiceCompletionBlock)completion{
    NSMutableDictionary *parameters = [NSMutableDictionary new];
    parameters = [@{@"categ_list" : [@{
                                       @"lvl:max" : [NSString stringWithFormat:@"%li", (long)lvl],
                                       } mutableCopy]
                    } mutableCopy];
    if(pID){
        [[parameters objectForKey:@"categ_list"] setValue:[NSString stringWithFormat:@"%li", (long)pID] forKey:@"pid"];
    }
    
    [self POST:@"bbs" parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion([self checkStatus:responseObject], nil, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(error, nil, nil);
    }];
}

- (void)getRegionWithPid:(NSInteger)pID completion:(WebServiceCompletionBlock)completion{
    NSDictionary *parameters = @{@"region_list" :@{
                                         @"pid" : [NSString stringWithFormat:@"%li", (long)pID],
                                         }
                                 };
    //    if(pID){
    //        [[parameters objectForKey:@"categ_list"] setValue:[NSString stringWithFormat:@"%li", (long)pID] forKey:@"pid"];
    //    }
    
    [self POST:@"bbs" parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion([self checkStatus:responseObject], nil, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(error, nil, nil);
    }];
}

- (void)addToFavoriteWithItemId:(NSInteger)itemId action:(NSString *)action completion:(WebServiceCompletionBlock)completion{
    NSDictionary *parameters = @{
                                 @"SESID" : loginToken,
                                 @"fav_update" : @{
                                         action : @[[NSString stringWithFormat:@"%li", (long)itemId]]
                                         }
                                 };
    
    [self POST:@"bbs" parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion([self checkStatus:responseObject], nil, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(error, nil, nil);
    }];
}

- (void)getFavoritesWithUserId:(NSInteger)userId page:(NSInteger)page completion:(WebServiceCompletionBlock)completion{
    NSDictionary *parameters =@{
                                @"SESID" : loginToken,
                                @"items_list" :
                                    @{
                                        @"page" : [NSString stringWithFormat:@"%li", (long)page],
                                        @"onpage" : @"10i",
                                        @"sort" : @"publicated_less",
                                        @"user_id:myfav" : [NSString stringWithFormat:@"%li",(long)userId]
                                        }
                                };
    
    [self POST:@"bbs" parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion([self checkStatus:responseObject], nil, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(error, nil, nil);
    }];
}

- (void)editProfileWithName:(NSString *)name phone:(NSString *)phone image:(NSString *)image completion:(WebServiceCompletionBlock)completion{
    NSDictionary *parameters =@{
                                @"SESID" : loginToken,
                                @"edit_info" :
                                    @{
                                        @"name" : name,
                                        @"phone_number" : phone,
                                        @"avatar" : image
                                        }
                                };
    
    [self POST:@"user" parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion([self checkStatus:responseObject], nil, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(error, nil, nil);
    }];
}

- (void)updateStatusAdvertising:(NSInteger )adID
                         status:(NSString *)status completion:(WebServiceCompletionBlock)completion {
    NSDictionary *parameters =@{
                                @"SESID" : loginToken,
                                @"item_change_status" : @{
                                        @"id" : [NSString stringWithFormat:@"%li",(long)adID],
                                        @"status" : status
                                        }
                                };
    
    [self POST:@"bbs" parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion([self checkStatus:responseObject], nil, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(error, nil, nil);
    }];
}

#pragma mark - Add Advertising

- (void)getDynamicPropertyWithCategoryId:(NSString *)categoryId completion:(WebServiceCompletionBlock)completion{
    NSMutableDictionary *parameters = [@{
                                @"get_cat_dyns" : @{
                                        @"id" : categoryId
                                        }
                                } mutableCopy];
    
    if(loginToken){
        [parameters setValue:loginToken forKey:@"SESID"];
    }
    
    [self POST:@"cats" parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion([self checkStatus:responseObject], nil, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(error, nil, nil);
    }];
}

- (void)sendAdvertising:(NSDictionary *)param completion:(WebServiceCompletionBlock)completion{
    NSDictionary *parameters =@{
                                @"SESID" : loginToken,
                                @"item_add" : param
                                };
    
    [self POST:@"bbs" parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion([self checkStatus:responseObject], nil, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(error, nil, nil);
    }];
}

#pragma mark - Detatil

- (void)getDetailAdvertisingWithId:(NSInteger)advId completion:(WebServiceCompletionBlock)completion{
    NSMutableDictionary *parameters = [@{
                                @"item_info" : @{
                                        @"id" : [NSString stringWithFormat:@"%li",(long)advId],
                                        @"similar" : @"1",
                                        @"need_cat_dyn_info": @"1"
                                        }
                                } mutableCopy];
    if(loginToken){
        [parameters setValue:loginToken forKey:@"SESID"];
    }
    
    [self POST:@"bbs" parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion([self checkStatus:responseObject], nil, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(error, nil, nil);
    }];
}

- (void)sendReportWithId:(NSInteger)reportId message:(NSString *)message advId:(NSInteger)advId completion:(WebServiceCompletionBlock)completion{
    NSDictionary *parameters =@{
                                @"SESID" : loginToken,
                                @"item_claim" : @{
                                        @"item_id" : [NSString stringWithFormat:@"%li",(long)advId],
                                        @"reason" : [NSString stringWithFormat:@"%li",(long)reportId],
                                        @"message" : message
                                        }
                                };
    
    [self POST:@"bbs" parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion([self checkStatus:responseObject], nil, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(error, nil, nil);
    }];
}

#pragma mark - Chat

- (void)getDialogList:(WebServiceCompletionBlock)completion{
    NSDictionary *parameters =@{
                                @"SESID" : loginToken,
                                @"chats_list" : @{
                                        @"page" : @"0",
                                        @"onpage" : @"100" //[NSString stringWithFormat:@"%li",(long)userId],
                                        }
                                };
    
    [self POST:@"user" parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion([self checkStatus:responseObject], nil, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(error, nil, nil);
    }];
}

- (void)getMessageListWitInterlocutor:(NSInteger)interlocutor page:(NSInteger)page completion:(WebServiceCompletionBlock)completion{
    NSDictionary *parameters =@{
                                @"SESID" : loginToken,
                                @"chat_messages" : @{
                                        @"interlocutor" : [NSString stringWithFormat:@"%li",(long)interlocutor],
                                        @"page" : [NSString stringWithFormat:@"%li",(long)page],
                                        @"onpage" : @"25"
                                        }
                                };
    
    [self POST:@"user" parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion([self checkStatus:responseObject], nil, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(error, nil, nil);
    }];
}

- (void)sendMessageWithpartnerId:(NSInteger)partnerId message:(NSString *)message completion:(WebServiceCompletionBlock)completion{
    NSDictionary *parameters =@{
                                @"SESID" : loginToken,
                                @"intmail_add" : @{
                                        @"interlocutor" : [NSString stringWithFormat:@"%li",(long)partnerId],
                                        @"message" : message
                                        }
                                };
    
    [self POST:@"user" parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion([self checkStatus:responseObject], nil, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(error, nil, nil);
    }];

}

- (void)sendMessageWithItemId:(NSInteger)itemId message:(NSString *)message completion:(WebServiceCompletionBlock)completion{
    NSDictionary *parameters =@{
                                @"SESID" : loginToken,
                                @"intmail_add" : @{
                                        @"item_id" : [NSString stringWithFormat:@"%li",(long)itemId],
                                        @"message" : message
                                        }
                                };
    
    [self POST:@"user" parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion([self checkStatus:responseObject], nil, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(error, nil, nil);
    }];
}

- (void)updateAdvertising:(NSDictionary *)param completion:(WebServiceCompletionBlock)completion{
    NSDictionary *parameters =@{
                                @"SESID" : loginToken,
                                @"item_update" : param
                                };
    
    [self POST:@"bbs" parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion([self checkStatus:responseObject], nil, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(error, nil, nil);
    }];
}

#pragma mark - Additional Method

- (void)saveToken:(NSString *)token {
    loginToken = token;
    NSError *error = nil;
    [FDKeychain saveItem: token
                  forKey: @"token"
              forService: @"Gauranga"
                   error: &error];
}

- (void)delAToken {
    NSError *error = nil;
    
    [FDKeychain deleteItemForKey: @"token"
                      forService: @"Gauranga"
                           error: &error];
}

- (BOOL)checkToken{
    NSError *error = nil;
    loginToken = [FDKeychain itemForKey:@"token"
                             forService:@"Gauranga"
                                  error:&error];
    if (loginToken) {
        return YES;
    }
    else {
        return NO;
    }
}

#pragma mark - APNSToken

- (void)saveAPNSToken:(NSString *)token {
    APNSToken = token;
    NSError *error = nil;
    [FDKeychain saveItem: token
                  forKey: @"APNStoken"
              forService: @"Gauranga"
                   error: &error];
}

- (void)deleteAPNSToken {
    NSError *error = nil;
    
    [FDKeychain deleteItemForKey: @"APNStoken"
                      forService: @"Gauranga"
                           error: &error];
}

- (BOOL)checkAPNSToken{
    NSError *error = nil;
    APNSToken = [FDKeychain itemForKey:@"APNStoken"
                             forService:@"Gauranga"
                                  error:&error];
    if (APNSToken) {
        return YES;
    }
    else {
        return NO;
    }
}

- (NSError *)checkStatus:(NSDictionary *)response{
    if (![[response objectForKey:@"RESULT"] integerValue] && [[response allKeys] containsObject:@"MESSAGE"]) {
        NSDictionary *messageDict = [[response objectForKey:@"MESSAGE"] objectAtIndex:0];
        NSString *stringError = [messageDict objectForKey:@"msg"];
        NSMutableDictionary *dicError = [@{
                                  @"message" : [messageDict objectForKey:@"msg"]
                                  } mutableCopy];
        if ([[messageDict allKeys] containsObject:@"left"] &&
            [[messageDict allKeys] containsObject:@"dtime"]) {
            [dicError  setObject:messageDict[@"left"] forKey:@"left"];
            [dicError  setObject:messageDict[@"dtime"] forKey:@"dtime"];
        }
        if([stringError isEqualToString:@"NO_SESSION"]){
            [[DataManager sharedInstance] delUserProfile];
            [[WebService sharedService] delAToken];
            [KVAlert alertWithTitle:@"ERROR" message:@"Перезайдите в аккаунт" preferredStyle:UIAlertControllerStyleAlert completion:^{}];
        }
        NSError *error = [[NSError alloc]initWithDomain:@"status" code:0 userInfo:dicError];
        return error;
    }
    return nil;
}

@end
