//
//  CityNavigationBar.h
//  Gauranga
//
//  Created by yvp on 2/8/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CityNavigationBar : UIView

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@end
