//
//  UIImagePicker.h
//  Gauranga
//
//  Created by yvp on 2/15/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImagePicker : UIView

@property (nonatomic, strong) UIPageControl *pageControl;

- (void)setDataSource:(NSArray *)dataSource selectedIndex:(NSInteger)selectedIndex;

@end
