//
//  UIImagePicker.m
//  Gauranga
//
//  Created by yvp on 2/15/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//
#import "UIImagePicker.h"
@import ImageSlideshow;

@interface UIImagePicker()<UIScrollViewDelegate>

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) NSMutableArray *images;
@property (nonatomic, strong) ImageSlideshow *imageSlider;

@end

@implementation UIImagePicker

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self setupImageSlider];
    }
    
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self){
        [self setupImageSlider];
    }
    
    return self;
}

#pragma mark - Setup

- (void)setupImageSlider{
    CGRect imageSliderFrame = self.frame;
    imageSliderFrame.size.width = CGRectGetWidth([UIScreen mainScreen].bounds);
    self.imageSlider = [[ImageSlideshow alloc] initWithFrame:imageSliderFrame];
    
    [self.imageSlider.pageControl setCurrentPageIndicatorTintColor:[UIColor colorWithWhite:1 alpha:0.5]];
    [self.imageSlider.pageControl setPageIndicatorTintColor:[UIColor whiteColor]];
    [self.imageSlider setContentScaleMode:UIViewContentModeScaleAspectFill];
    [self.imageSlider addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showFullScreen)]];
    
    [self addSubview:self.imageSlider];
}

#pragma mark - DataSource

- (void)setDataSource:(NSArray *)dataSource selectedIndex:(NSInteger)selectedIndex{
    NSMutableArray *imageArray = [NSMutableArray new];
    for(NSString *urlString in dataSource){
        NSURL *url = [NSURL URLWithString:urlString];
        KingfisherSource *kingSource = [[KingfisherSource alloc] initWithUrl:url placeholder:[UIImage imageNamed:@"avatarPlaceholder"]];
        [imageArray addObject:kingSource];
    }
    
    [self.imageSlider setImageInputs:imageArray];
}

- (void)showFullScreen{
    [self.imageSlider presentFullScreenControllerFrom:[WireFrame getView]];
}

//- (void)setDataSource:(NSArray *)dataSource selectedIndex:(NSInteger)selectedIndex{
//    [self.scrollView removeFromSuperview];
//    self.images = [NSMutableArray new];
//    self.scrollView = [[UIScrollView alloc] initWithFrame:self.frame];
//    
//    if(self.frame.size.height > 500){
//        CGRect newFrame = [UIScreen mainScreen].bounds;
//        newFrame.size.height -= 20;
//        self.scrollView.frame = newFrame;
//    }
//    [self.scrollView setDelegate:self];
//    
//    int x = 0;
//    self.scrollView.pagingEnabled = YES;
//    for (int i=0; i<[dataSource count]; i++){
//        NSURL *url = [NSURL URLWithString:[dataSource objectAtIndex:i]];
//        UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.scrollView.frame.size.width, self.scrollView.frame.size.height)];
//        [img sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"avatarPlaceholder"] options:SDWebImageCacheMemoryOnly];
//        [img setContentMode:UIViewContentModeScaleAspectFill];
//        [img setTag:i];
//        [self.scrollView setBouncesZoom:YES];
//        
//        UIScrollView *pageScrollView = [self uiScrollForImage:img xPos:x];
//        [self.scrollView addSubview:pageScrollView];
//        x += self.scrollView.frame.size.width;
//    }
//    self.scrollView.contentSize = CGSizeMake(x, self.scrollView.frame.size.height);
//    [self.scrollView setContentOffset:CGPointMake(self.scrollView.frame.size.width * selectedIndex, 0)];
//    self.scrollView.showsHorizontalScrollIndicator = NO;
//    [self addSubview:self.scrollView];
//    [self addPageControl];
//    self.pageControl.numberOfPages = [dataSource count];
//    [self.pageControl setCurrentPage:selectedIndex];
//}
//- (void)addPageControl{
//
//    CGRect pageFrame = CGRectMake(0, CGRectGetHeight(self.scrollView.frame)-40, CGRectGetWidth(self.frame), 40);
//    self.pageControl = [[UIPageControl alloc] initWithFrame:pageFrame];
//    [self.pageControl setPageIndicatorTintColor:[UIColor colorWithWhite:1 alpha:0.5]];
//    [self.pageControl setCurrentPageIndicatorTintColor:[UIColor whiteColor]];
//    [self.pageControl setBackgroundColor:[UIColor colorWithRed:1.0f/255.0f green:1.0f/255.0f blue:1.0f/255.0f alpha:0.5f]];
//    [self addSubview:self.pageControl];
//}
//- (UIScrollView *)uiScrollForImage:(UIImageView *)image xPos:(int)xPos{
//    UIScrollView *pageScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(xPos, 0, CGRectGetWidth(self.scrollView.frame), CGRectGetHeight(self.scrollView.frame))];
//    pageScrollView.minimumZoomScale = 1.0f;
//    pageScrollView.maximumZoomScale = 2.0f;
//    pageScrollView.zoomScale = 1.0f;
//    pageScrollView.contentSize = image.bounds.size;
//    pageScrollView.delegate = self;
//    pageScrollView.showsHorizontalScrollIndicator = NO;
//    pageScrollView.showsVerticalScrollIndicator = NO;
//    
//    if(self.frame.size.height > 500){
//        [image setContentMode:UIViewContentModeScaleAspectFit];
//    }
//    
//    [pageScrollView addSubview:image];
//    return pageScrollView;
//}
//- (void) scrollViewDidScroll:(UIScrollView *)scrollView {
//    int newPage = (int)(scrollView.contentOffset.x/self.scrollView.frame.size.width);
//    self.pageControl.currentPage = newPage;
//}
//- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
//    return [[scrollView viewWithTag:self.pageControl.currentPage].subviews lastObject];
//}

@end
