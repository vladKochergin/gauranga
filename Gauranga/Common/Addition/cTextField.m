//
//  cTextField.m
//  Tamaranga
//
//  Created by yvp on 2/2/17.
//  Copyright © 2017 Maks. All rights reserved.
//

#import "cTextField.h"

#define kCustomGreyColor [UIColor colorWithRed:(218.f/255) green:(218.f/255) blue:(218.f/255) alpha:1]

@interface cTextField ()

@property (strong, nonatomic) UILabel *placeholderLabel;

@end

@implementation cTextField

- (id)initWithFrame:(CGRect)aRect{
    if ((self = [super initWithFrame:aRect])) {
        [self setupTextField];
    }
    return self;
}

- (id)initWithCoder:(NSCoder*)coder{
    if ((self = [super initWithCoder:coder])) {
        [self setupTextField];
    }
    return self;
}

- (void)setupTextField{
    [self setFloatingLabelActiveTextColor:[UIColor mainColor]];
    [self setFloatingLabelTextColor:[UIColor mainColor]];
    [self addTarget:self action:@selector(textFieldAction) forControlEvents:UIControlEventEditingDidBegin];
    [self addTarget:self action:@selector(textFieldDeselect) forControlEvents:UIControlEventEditingDidEnd];
}

- (void)textFieldAction{
    UILabel *view = [self.subviews objectAtIndex:1];
    if ([NSStringFromClass(view.class) isEqualToString:NSStringFromClass([UIView class])]) {
        [view setBackgroundColor:[UIColor mainColor]];
    }
    else {
        return;
    }
}

- (void)textFieldDeselect{
    UILabel *view = [self.subviews objectAtIndex:1];
    if ([NSStringFromClass(view.class) isEqualToString:NSStringFromClass([UIView class])]) {
        [view setBackgroundColor:kCustomGreyColor];
    }
    else {
        return;
    }
}

@end
