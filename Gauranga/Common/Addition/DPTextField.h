//
//  DPTextField.h
//  Gauranga
//
//  Created by yvp on 2/10/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "cTextField.h"

@interface DPTextField : cTextField <UIPickerViewDelegate, UIPickerViewDataSource>

@property (strong, nonatomic) NSArray *dataSource;
@property (nonatomic) NSInteger selectedIndex;

@end
