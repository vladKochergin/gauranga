//
//  Validation.h
//  Gauranga
//
//  Created by yvp on 2/9/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Validation : NSObject

+ (BOOL)isValidEmail:(NSString *)checkString;

@end
