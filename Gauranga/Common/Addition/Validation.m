//
//  Validation.m
//  Gauranga
//
//  Created by yvp on 2/9/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import "Validation.h"

@implementation Validation

+ (BOOL)isValidEmail:(NSString *)checkString {
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

+ (BOOL)isNonWordSymbolsInString:(NSString *)checkString {
    NSString *myRegex = @"[А-ЯЁ-ёа-яA-Za-z_]*";
    NSPredicate *myTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", myRegex];
    NSString *string = checkString;
    return ![myTest evaluateWithObject:string];
}

//+ (BOOL)isOnlyNonWordSymbolsInString:(NSString *)checkString {
//    NSCharacterSet *alphaSet = [NSCharacterSet letterCharacterSet];
//    return ![[checkString stringByTrimmingCharactersInSet:alphaSet] isEmpty];
//}

//+ (BOOL)allTextValid:(UIView *)contentView {
//    
//    return [self searchSubviews:contentView];
//    
//}

#pragma mark - Private methods

//+ (BOOL)searchSubviews:(UIView *)view {
//    BOOL result = [self validation:view];;
//    if (view.subviews.count != 0) {
//        for (UIView * aView in view.subviews) {
//            [self searchSubviews:aView];
//        }
//    }
//    
//    return result;
//}

//+ (BOOL)validation:(UIView *)contentView {
//    NSString *findedFieldPlaceholder = nil;
//    BOOL isMoreThanOneFieldFinded = NO;
//    BOOL isValid;
//    
//    UITextField *findedTextField = nil;
//    
//    for (UIView *view in contentView.subviews) {
//        
//        if ([view.class isSubclassOfClass:[UITextField class]]) {
//            NSString *placeholder = [(UITextField *)view placeholder];
//            NSString *textFieldText = [(UITextField *)view text];
//            if ([self isNonWordSymbolsInString:textFieldText]) {
//                findedTextField = (UITextField *)view;
//                if (findedFieldPlaceholder == nil) {
//                    findedFieldPlaceholder = placeholder;
//                } else {
//                    isMoreThanOneFieldFinded = YES;
//                }
//                
//            }
//        }
//        
//    }
//    
//    if (findedFieldPlaceholder != nil){
//        [MSAlertFactory createAlertWithTitle:@"Ошибка" message:@"Введены недопустимые символы" animated:YES];
//    }
//    
//    if (findedFieldPlaceholder == nil) {
//        isValid = YES;
//    }
//    
//    return isValid;
//}

@end
