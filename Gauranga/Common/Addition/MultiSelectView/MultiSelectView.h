//
//  MultiSelectView.h
//  Gauranga
//
//  Created by yvp on 3/3/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MultiSelectView : JVFloatLabeledTextField

@property (nonatomic, strong) NSMutableArray *selectItemsID;
@property (nonatomic, strong) NSMutableArray *selectItemsString;

- (void)setDataSource:(NSArray *)data;
- (void)setText;
@end
