//
//  MultiSelectView.m
//  Gauranga
//
//  Created by yvp on 3/3/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import "MultiSelectView.h"

@interface MultiSelectView()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSArray *dataSource;

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UIToolbar *toolBar;
@property (nonatomic, strong) UIView *mainView;

//@property (nonatomic, strong) NSMutableArray *selectItemsID;
//@property (nonatomic, strong) NSMutableArray *selectItemsString;

@end

@implementation MultiSelectView

- (id)initWithFrame:(CGRect)aRect
{
    if ((self = [super initWithFrame:aRect])) {
        [self setupTextField];
    }
    return self;
}

- (id)initWithCoder:(NSCoder*)coder
{
    if ((self = [super initWithCoder:coder])) {
        [self setupTextField];
    }
    return self;
}

#pragma mark - Init

- (void)setupTextField{
    self.selectItemsString = [NSMutableArray new];
    self.selectItemsID = [NSMutableArray new];
    
    self.mainView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 0)];
    
    [self initToolBar];
    [self initTableView];
    
    [self setInputView:self.mainView];
    
}

- (void)initToolBar{
    self.toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.mainView.frame.size.width, 50)];
    
    UIBarButtonItem *items = [[UIBarButtonItem alloc] initWithTitle:@"Готово" style:UIBarButtonItemStyleDone target:self action:@selector(doneButtonPressed)];
    [self.toolBar setItems:@[items]];
    [self.mainView addSubview:self.toolBar];
}

- (void)initTableView{
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 50, self.mainView.frame.size.width, 0)];
    
    [self.tableView setDataSource:self];
    [self.tableView setDelegate:self];
    [self.tableView setAllowsMultipleSelection:YES];
    [self.tableView setScrollEnabled:NO];
    //[self.tableView reloadData];
    [self.mainView addSubview:self.tableView];
}

- (void)setDataSource:(NSArray *)data{
    _dataSource = data;
    CGFloat mainHight = [self.dataSource count]*44+50;
    
    if(mainHight > SCREEN_HEIGHT){
        mainHight = SCREEN_HEIGHT - 50;
        [self.tableView setScrollEnabled:YES];
    }
    
    self.mainView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, mainHight);
    self.tableView.frame = CGRectMake(0, 50, [UIScreen mainScreen].bounds.size.width, mainHight-50);
    [self.tableView reloadData];
}

#pragma mark - UITableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.dataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [UITableViewCell new];
    cell.textLabel.text = [self.dataSource objectAtIndex:indexPath.row];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    if([self.selectItemsID containsObject:[NSString stringWithFormat:@"%li", indexPath.row]]){
        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
        [tableView selectRowAtIndexPath:indexPath animated:YES  scrollPosition:UITableViewScrollPositionBottom];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    [self.selectItemsID addObject:[NSString stringWithFormat:@"%li", (long)indexPath.row]];
    [self.selectItemsString addObject:cell.textLabel.text];
    
    [self setText];
    
    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [cell setAccessoryType:UITableViewCellAccessoryNone];
    
    [self.selectItemsID removeObject:[NSString stringWithFormat:@"%li", (long)indexPath.row]];
    [self.selectItemsString removeObject:cell.textLabel.text];
    
    [self setText];
}

#pragma mark - Action

- (void)doneButtonPressed{
    [self endEditing:YES];
}

#pragma mark - Additional

- (void)setText{
    NSMutableString *newText = [NSMutableString new];
    for(NSString *text in self.selectItemsString){
        [newText appendString:[NSString stringWithFormat:@"%@, ", text]];
    }
    self.text = newText;
}

@end
