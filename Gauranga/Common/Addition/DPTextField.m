//
//  DPTextField.m
//  Gauranga
//
//  Created by yvp on 2/10/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import "DPTextField.h"

@interface DPTextField()

@property(nonatomic, strong) UIView *ViewForValuePicker;
@property(nonatomic, strong) UIToolbar *toolBar;
@property(nonatomic, strong) UIPickerView *valuePicker;
@property(nonatomic, strong) UILabel *placeholderLabel;
@end

@implementation DPTextField

- (id)initWithFrame:(CGRect)aRect{
    if ((self = [super initWithFrame:aRect])) {
        [self setupTextField];
    }
    return self;
}

- (id)initWithCoder:(NSCoder*)coder{
    if ((self = [super initWithCoder:coder])) {
        [self setupTextField];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setupTextField{
    [super setupTextField];
    [self addPicker];
    //[self addPlaceholderLabel];
}

//- (void)addPlaceholderLabel{
//    
//    UILabel *placeholderLabel;
//    
//    if (!self.text.length) {
//        placeholderLabel = [[UILabel alloc]initWithFrame:self.bounds];
//        placeholderLabel.alpha = 0;
//    } else {
//        placeholderLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(self.bounds) + 2, CGRectGetMinY(self.bounds), CGRectGetWidth(self.frame) / 2, 7)];
//        placeholderLabel.alpha = 1;
//    }
//    self.placeholderLabel = placeholderLabel;
//    
//    placeholderLabel.font = [UIFont systemFontOfSize:10];
//    
//    [placeholderLabel setTextColor:[UIColor mainColor]];
//    [self addSubview:placeholderLabel];
//}

//-(void)setText:(NSString *)text{
//    [super setText:text];
//    if (text.length) {
//        self.placeholderLabel.text = self.placeholder;
//        self.placeholderLabel.frame = CGRectMake(CGRectGetMinX(self.bounds) + 2, CGRectGetMinY(self.bounds), CGRectGetWidth(self.frame) / 2, 7);
//        self.placeholderLabel.alpha = 1;
//    }
//}

#pragma mark - UIPickerView

- (void)addPicker{
    CGRect size = [UIScreen mainScreen].bounds;
    
    self.ViewForValuePicker = [[UIView alloc]initWithFrame:CGRectMake(0, size.size.height-266, size.size.width, 266)];
    
    
    self.toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.ViewForValuePicker.frame.size.width, 44)];
    
    UIBarButtonItem *btn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(selectRowInPickerView)];

    [self.toolBar setItems:[NSArray arrayWithObject:btn]];
    [self.ViewForValuePicker addSubview:self.toolBar];
    
    self.valuePicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 44, self.toolBar.frame.size.width, 216)];
    self.valuePicker.delegate=self;
    self.valuePicker.dataSource=self;
    self.valuePicker.showsSelectionIndicator=YES;
    
    [self.ViewForValuePicker addSubview:self.valuePicker];
    
    [self setInputView:self.ViewForValuePicker];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component {
    return  [self.dataSource count];
}
- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [self.dataSource objectAtIndex:row];
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    self.selectedIndex = row;
    self.text = [self.dataSource objectAtIndex:row];
    if (!self.text.length) {
        [UIView animateWithDuration:0.3 animations:^{
            self.placeholderLabel.frame = self.bounds;
            [self.placeholderLabel setAlpha:0];
        }];
    }
}
- (void)selectRowInPickerView{
    self.text = [self.dataSource objectAtIndex:self.selectedIndex];
    [self resignFirstResponder];
}

@end
