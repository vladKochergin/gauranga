//
//  cTextField.h
//  Tamaranga
//
//  Created by yvp on 2/2/17.
//  Copyright © 2017 Maks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface cTextField : JVFloatLabeledTextField

- (void)setupTextField;

@end
