//
//  WireFrame.m
//  Gauranga
//
//  Created by yvp on 2/9/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import "WireFrame.h"

@implementation WireFrame

+ (UIViewController*)getView{
    UIViewController *presentedViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    UIViewController *topView;
    
    if([presentedViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController *navigationController = (UINavigationController *)presentedViewController;
        topView = [navigationController.viewControllers lastObject];
    }
    
    while(presentedViewController.presentedViewController){
        presentedViewController = presentedViewController.presentedViewController;
        topView = presentedViewController;
    }
    
    if([topView isKindOfClass:[UINavigationController class]])
        for(UIViewController *view in presentedViewController.childViewControllers){
            topView = view;
        }
    
    return topView ? topView : presentedViewController;
}

+ (UIViewController *)topViewController: (UIViewController *) controller{
    BOOL isPresenting = NO;
    do {
        // this path is called only on iOS 6+, so -presentedViewController is fine here.
        UIViewController *presented = [controller presentedViewController];
        isPresenting = presented != nil;
        if(presented != nil) {
            controller = presented;
        }
        
    } while (isPresenting);
    
    return controller;
}

@end
