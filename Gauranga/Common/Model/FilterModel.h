//
//  FilterModel.h
//  Gauranga
//
//  Created by yvp on 2/21/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FilterModel : NSObject

@property (nonatomic, strong) NSDictionary *category;
@property (nonatomic, strong) NSDictionary *city;
@property (nonatomic, strong) NSArray *dynamicproperty;
@property (nonatomic, strong) NSMutableDictionary *dynamicPropertyFields;

@end
