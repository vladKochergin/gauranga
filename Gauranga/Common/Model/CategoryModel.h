//
//  CategoryModel.h
//  Gauranga
//
//  Created by yvp on 2/8/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <EasyMapping/EasyMapping.h>

@interface CategoryModel : EKObjectModel

@property (nonatomic) NSInteger categoryID;
@property (nonatomic) NSInteger parentCategoryID;
@property (nonatomic, strong) NSString *image;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *latTitle;
@property (nonatomic) NSInteger itemsCount;
@property (nonatomic) NSInteger subCategoryCount;
@property (nonatomic) NSInteger levelNesting;
@property (nonatomic, strong) NSString *imagePath;
@property (nonatomic) BOOL isParentCategory;

+ (instancetype)categoryWithDictionary:(NSDictionary *)JSONresponse;

@end
