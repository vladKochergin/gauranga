//
//  UserModel.m
//  Gauranga
//
//  Created by yvp on 2/9/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import "UserModel.h"

@implementation UserModel

+ (EKObjectMapping *)objectMapping{
    
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapKeyPath:@"id" toProperty:@"userId"];
        [mapping mapKeyPath:@"user_id_ex" toProperty:@"userIDex"];
        [mapping mapKeyPath:@"admin" toProperty:@"admin"];
        [mapping mapKeyPath:@"member" toProperty:@"member"];
        [mapping mapKeyPath:@"login" toProperty:@"login"];
        [mapping mapKeyPath:@"email" toProperty:@"email"];
        [mapping mapKeyPath:@"phone_number" toProperty:@"phoneNumber"];
        [mapping mapKeyPath:@"balance" toProperty:@"balance"];
        [mapping mapKeyPath:@"balance_spent" toProperty:@"balanceSpent"];
        [mapping mapKeyPath:@"reg1_country" toProperty:@"country"];
        [mapping mapKeyPath:@"reg2_region" toProperty:@"region"];
        [mapping mapKeyPath:@"reg3_city" toProperty:@"city"];
        [mapping mapKeyPath:@"region_id" toProperty:@"regionID"];
        [mapping mapKeyPath:@"name" toProperty:@"name"];
        [mapping mapKeyPath:@"surname" toProperty:@"surname"];
        [mapping mapKeyPath:@"avatar" toProperty:@"avatar"];
        [mapping mapKeyPath:@"avatar_crop" toProperty:@"avatarCrop"];
        [mapping mapKeyPath:@"blocked" toProperty:@"blocked"];
        [mapping mapKeyPath:@"blocked_reason" toProperty:@"blockedReason"];
        [mapping mapKeyPath:@"birthdate" toProperty:@"birthdate"];
        [mapping mapKeyPath:@"sex" toProperty:@"sex"];
        [mapping mapKeyPath:@"about" toProperty:@"about"];
        [mapping mapKeyPath:@"skype" toProperty:@"skype"];
        [mapping mapKeyPath:@"icq" toProperty:@"icq"];
        [mapping mapKeyPath:@"site" toProperty:@"site"];
        [mapping mapKeyPath:@"im_noreply" toProperty:@"imNoreply"];
        [mapping mapKeyPath:@"enotify" toProperty:@"enotify"];
        [mapping mapKeyPath:@"social_id" toProperty:@"socialID"];
        [mapping mapKeyPath:@"internalmail_new" toProperty:@"internalMailNew"];
        [mapping mapKeyPath:@"password" toProperty:@"password"];
        [mapping mapKeyPath:@"currency" toProperty:@"currency"];
    }];
}

+ (instancetype)userWithDictionary:(NSDictionary *)JSONresponse{
    return [UserModel objectWithProperties:JSONresponse];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        
        self.userId = [decoder decodeIntegerForKey:@"ID"];
        self.userIDex = [decoder decodeIntegerForKey:@"userIDex"];
        self.admin = [decoder decodeObjectForKey:@"admin"];
        self.member = [decoder decodeObjectForKey:@"member"];
        self.login = [decoder decodeObjectForKey:@"login"];
        self.email = [decoder decodeObjectForKey:@"email"];
        self.phoneNumber = [decoder decodeObjectForKey:@"phoneNumber"];
        self.balance = [decoder decodeDoubleForKey:@"balance"];
        self.balanceSpent = [decoder decodeDoubleForKey:@"balanceSpent"];
        self.country = [decoder decodeObjectForKey:@"country"];
        self.region = [decoder decodeObjectForKey:@"region"];
        self.city = [decoder decodeObjectForKey:@"city"];
        self.regionID = [decoder decodeIntegerForKey:@"regionID"];
        self.name = [decoder decodeObjectForKey:@"name"];
        self.surname = [decoder decodeObjectForKey:@"surname"];
        self.avatar = [decoder decodeObjectForKey:@"avatar"];
        self.avatarCrop = [decoder decodeObjectForKey:@"avatarCrop"];
        self.blocked = [decoder decodeObjectForKey:@"blocked"];
        self.blockedReason = [decoder decodeObjectForKey:@"blockedReason"];
        self.birthdate = [decoder decodeObjectForKey:@"birthdate"];
        self.sex = [decoder decodeObjectForKey:@"sex"];
        self.about = [decoder decodeObjectForKey:@"about"];
        self.skype = [decoder decodeObjectForKey:@"skype"];
        self.icq = [decoder decodeObjectForKey:@"icq"];
        self.site = [decoder decodeObjectForKey:@"site"];
        self.imNoreply = [decoder decodeObjectForKey:@"imNoreply"];
        self.enotify = [decoder decodeObjectForKey:@"enotify"];
        self.socialID = [decoder decodeIntegerForKey:@"socialID"];
        self.internalMailNew = [decoder decodeObjectForKey:@"internalMailNew"];
        self.password = [decoder decodeObjectForKey:@"password"];
        self.currency = [decoder decodeObjectForKey:@"currency"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder*)aCoder{
    [aCoder encodeInteger:self.userId forKey:@"ID"];
    [aCoder encodeInteger:self.userIDex forKey:@"userIDex"];
    [aCoder encodeObject:self.admin forKey:@"admin"];
    [aCoder encodeObject:self.member forKey:@"member"];
    [aCoder encodeObject:self.login forKey:@"login"];
    [aCoder encodeObject:self.email forKey:@"email"];
    [aCoder encodeObject:self.phoneNumber forKey:@"phoneNumber"];
    [aCoder encodeDouble:self.balance forKey:@"balance"];
    [aCoder encodeDouble:self.balanceSpent forKey:@"balanceSpent"];
    [aCoder encodeObject:self.country forKey:@"country"];
    [aCoder encodeObject:self.region forKey:@"region"];
    [aCoder encodeObject:self.city forKey:@"city"];
    [aCoder encodeInteger:self.regionID forKey:@"regionID"];
    [aCoder encodeObject:self.name forKey:@"name"];
    [aCoder encodeObject:self.surname forKey:@"surname"];
    [aCoder encodeObject:self.avatar forKey:@"avatar"];
    [aCoder encodeObject:self.avatarCrop forKey:@"avatarCrop"];
    [aCoder encodeObject:self.blocked forKey:@"blocked"];
    [aCoder encodeObject:self.blockedReason forKey:@"blockedReason"];
    [aCoder encodeObject:self.birthdate forKey:@"birthdate"];
    [aCoder encodeObject:self.sex forKey:@"sex"];
    [aCoder encodeObject:self.about forKey:@"about"];
    [aCoder encodeObject:self.skype forKey:@"skype"];
    [aCoder encodeObject:self.icq forKey:@"icq"];
    [aCoder encodeObject:self.site forKey:@"site"];
    [aCoder encodeObject:self.imNoreply forKey:@"imNoreply"];
    [aCoder encodeObject:self.enotify forKey:@"enotify"];
    [aCoder encodeInteger:self.socialID forKey:@"socialID"];
    [aCoder encodeObject:self.internalMailNew forKey:@"internalMailNew"];
    [aCoder encodeObject:self.password forKey:@"password"];
    [aCoder encodeObject:self.currency forKey:@"currency"];
}

@end
