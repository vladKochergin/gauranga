//
//  CurrencyModel.m
//  Gauranga
//
//  Created by yvp on 2/10/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import "CurrencyModel.h"

@implementation CurrencyModel

+ (EKObjectMapping *)objectMapping{
    
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapKeyPath:@"id" toProperty:@"currencyId"];
        [mapping mapKeyPath:@"rate" toProperty:@"rate"];
        [mapping mapKeyPath:@"title" toProperty:@"title"];
        [mapping mapKeyPath:@"title_short" toProperty:@"titleShort"];
    }];
}


+ (instancetype)currencyWithDictionary:(NSDictionary *)JSONresponse{
    return [CurrencyModel objectWithProperties:JSONresponse];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        self.currencyId = [decoder decodeIntegerForKey:@"currencyId"];
        self.rate = [decoder decodeDoubleForKey:@"rate"];
        self.title = [decoder decodeObjectForKey:@"title"];
        self.titleShort = [decoder decodeObjectForKey:@"titleShort"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder*)aCoder{
    [aCoder encodeInteger:self.currencyId forKey:@"currencyId"];
    [aCoder encodeDouble:self.rate forKey:@"rate"];
    [aCoder encodeObject:self.title forKey:@"title"];
    [aCoder encodeObject:self.titleShort forKey:@"titleShort"];
}

+ (NSString *)priceInFormat:(double)price{
    const NSInteger dotEtln = 3;
    NSMutableString *priceStr = [NSMutableString stringWithFormat:@"%li", (long)price];
    NSInteger dotCur = [priceStr length];
    
    do {
        dotCur -= dotEtln;
        if(dotCur > 0){
            [priceStr insertString:@" " atIndex:dotCur];
        }
        
    } while (dotCur>dotEtln);
    
    return priceStr;
}


@end
