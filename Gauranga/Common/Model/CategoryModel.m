//
//  CategoryModel.m
//  Gauranga
//
//  Created by yvp on 2/8/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import "CategoryModel.h"

@implementation CategoryModel

+ (EKObjectMapping *)objectMapping{
    
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapKeyPath:@"id" toProperty:@"categoryID"];
        [mapping mapKeyPath:@"pid" toProperty:@"parentCategoryID"];
        [mapping mapKeyPath:@"i" toProperty:@"image"];
        [mapping mapKeyPath:@"t" toProperty:@"title"];
        [mapping mapKeyPath:@"k" toProperty:@"latTitle"];
        [mapping mapKeyPath:@"items" toProperty:@"itemsCount"];
        [mapping mapKeyPath:@"subs" toProperty:@"subCategoryCount"];
        [mapping mapKeyPath:@"lvl" toProperty:@"levelNesting"];
        [mapping mapKeyPath:@"img_path" toProperty:@"imagePath"];
    }];
}

+ (instancetype)categoryWithDictionary:(NSDictionary *)JSONresponse{
    return [CategoryModel objectWithProperties:JSONresponse];
}


@end
