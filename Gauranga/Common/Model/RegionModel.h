//
//  RegionModel.h
//  Gauranga
//
//  Created by yvp on 2/14/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <EasyMapping/EasyMapping.h>

@interface RegionModel : EKObjectModel

@property (nonatomic) NSInteger regionID;
@property (nonatomic) NSInteger parentID;
@property (nonatomic) NSInteger countryID;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *coordinates;
@property (nonatomic) BOOL isNotCity;

+ (instancetype)regionWithDictionary:(NSDictionary *)JSONresponse;

@end
