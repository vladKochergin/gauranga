//
//  dopParamModel.m
//  Gauranga
//
//  Created by yvp on 2/15/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import "dopParamModel.h"

@implementation dopParamModel

+ (EKObjectMapping *)objectMapping{
    
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapKeyPath:@"title" toProperty:@"title"];
        [mapping mapKeyPath:@"v" toProperty:@"value"];
        [mapping mapKeyPath:@"description" toProperty:@"valueType"];
    }];
}

+ (instancetype)dopParamWithDictionary:(NSDictionary *)JSONresponse{
    return [dopParamModel objectWithProperties:JSONresponse];
}

@end
