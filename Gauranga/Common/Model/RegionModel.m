//
//  RegionModel.m
//  Gauranga
//
//  Created by yvp on 2/14/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import "RegionModel.h"

@implementation RegionModel

+ (EKObjectMapping *)objectMapping{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapKeyPath:@"id" toProperty:@"regionID"];
        [mapping mapKeyPath:@"pid" toProperty:@"parentID"];
        [mapping mapKeyPath:@"c" toProperty:@"countryID"];
        [mapping mapKeyPath:@"t" toProperty:@"title"];
        [mapping mapKeyPath:@"yc" toProperty:@"coordinates"];
    }];
}

+ (instancetype)regionWithDictionary:(NSDictionary *)JSONresponse{
    return [RegionModel objectWithProperties:JSONresponse];
}

@end
