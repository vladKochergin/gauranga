//
//  MailModel.m
//  Gauranga
//
//  Created by yvp on 2/27/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import "MailModel.h"

@implementation MailModel

+ (EKObjectMapping *)objectMapping{
    
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapKeyPath:@"attach" toProperty:@"attach"];
        [mapping mapKeyPath:@"author" toProperty:@"author"];
        [mapping mapKeyPath:@"created" toProperty:@"date"];
        
        [mapping mapKeyPath:@"id" toProperty:@"mailId"];
        [mapping mapKeyPath:@"item_id" toProperty:@"itemId"];
        [mapping mapKeyPath:@"message" toProperty:@"message"];
        [mapping mapKeyPath:@"readed" toProperty:@"readed"];
        [mapping mapKeyPath:@"recipient" toProperty:@"recipientId"];
    }];
}

+ (instancetype)mailWithDictionary:(NSDictionary *)JSONresponse{
    return [MailModel objectWithProperties:JSONresponse];
}

@end
