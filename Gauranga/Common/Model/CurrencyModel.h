//
//  CurrencyModel.h
//  Gauranga
//
//  Created by yvp on 2/10/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <EasyMapping/EasyMapping.h>

@interface CurrencyModel : EKObjectModel

@property(nonatomic) NSInteger currencyId;
@property(nonatomic) double rate;
@property(nonatomic, strong) NSString *title;
@property(nonatomic, strong) NSString *titleShort;

+ (instancetype)currencyWithDictionary:(NSDictionary *)JSONresponse;

+ (NSString *)priceInFormat:(double)price;

@end
