//
//  dopParamModel.h
//  Gauranga
//
//  Created by yvp on 2/15/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <EasyMapping/EasyMapping.h>

@interface dopParamModel : EKObjectModel

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *value;
@property (nonatomic, strong) NSString *valueType;

+ (instancetype)dopParamWithDictionary:(NSDictionary *)JSONresponse;

@end
