//
//  UserModel.h
//  Gauranga
//
//  Created by yvp on 2/9/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <EasyMapping/EasyMapping.h>

@interface UserModel : EKObjectModel

@property (nonatomic) NSInteger userId;
@property (nonatomic) NSInteger userIDex;
@property (nonatomic, strong) NSString *admin;
@property (nonatomic, strong) NSString *member;
@property (nonatomic, strong) NSString *login;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *phoneNumber;
@property (nonatomic) double balance;
@property (nonatomic) double balanceSpent;
@property (nonatomic, strong) NSString *country;
@property (nonatomic, strong) NSString *region;
@property (nonatomic, strong) NSString *city;
@property (nonatomic) NSInteger regionID;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *surname;
@property (nonatomic, strong) NSString *avatar;
@property (nonatomic, strong) NSString *avatarCrop;
@property (nonatomic, strong) NSString *blocked;
@property (nonatomic, strong) NSString *blockedReason;
@property (nonatomic, strong) NSString *birthdate;
@property (nonatomic, strong) NSString *sex;
@property (nonatomic, strong) NSString *about;
@property (nonatomic, strong) NSString *skype;
@property (nonatomic, strong) NSString *icq;
@property (nonatomic, strong) NSString *site;
@property (nonatomic, strong) NSString *imNoreply;
@property (nonatomic, strong) NSString *enotify;
@property (nonatomic) NSInteger socialID;
@property (nonatomic, strong) NSString *internalMailNew;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSNumber *currency;

+ (instancetype)userWithDictionary:(NSDictionary *)JSONresponse;

@end
