//
//  MailModel.h
//  Gauranga
//
//  Created by yvp on 2/27/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <EasyMapping/EasyMapping.h>

@interface MailModel : EKObjectModel

@property (nonatomic, strong) NSArray *attach;
@property (nonatomic) NSInteger author;
@property (nonatomic, strong) NSString *date;
@property (nonatomic) NSInteger mailId;
@property (nonatomic) NSInteger itemId;
@property (nonatomic, strong) NSString *message;
@property (nonatomic, strong) NSString *readed;
@property (nonatomic) NSInteger *recipientId;

+ (instancetype)mailWithDictionary:(NSDictionary *)JSONresponse;

@end
