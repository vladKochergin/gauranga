//
//  CatalogModel.h
//  Gauranga
//
//  Created by yvp on 2/7/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <EasyMapping/EasyMapping.h>

@interface CatalogModel : EKObjectModel

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *date;
@property (nonatomic) NSInteger itemId;
@property (nonatomic) NSInteger categoryID;
@property (nonatomic, strong) NSString *categoryName;
@property (nonatomic, strong) NSString *imageSmall;
@property (nonatomic, strong) NSString *imageMedium;
@property (nonatomic, strong) NSString *price;
@property (nonatomic) double latitude;
@property (nonatomic) double longitude;
@property (nonatomic) NSInteger curency;
@property (nonatomic) BOOL isFavorite;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *priceDisplay;

+(instancetype)catalogWithDictionary:(NSDictionary *)JSONresponse;
-(void)copyTo:(CatalogModel *)catalog;

@end
