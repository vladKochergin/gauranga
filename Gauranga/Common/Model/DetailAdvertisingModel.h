//
//  DetailAdvertisingModel.h
//  Gauranga
//
//  Created by yvp on 2/15/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <EasyMapping/EasyMapping.h>

@class UserModel;
@class DynamicPropertyModel;
@class CategoryModel;

@interface DetailAdvertisingModel : EKObjectModel

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *date;
@property (nonatomic) NSInteger cityId;
@property (nonatomic) NSInteger advId;
@property (nonatomic) NSInteger categoryID;
@property (nonatomic) NSInteger status;
@property (nonatomic, strong) NSString *categoryName;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *imageSmall;
@property (nonatomic, strong) NSString *imageMedium;
@property (nonatomic, strong) NSString *price;
@property (nonatomic, strong) NSString *descriptionItem;
@property (nonatomic) NSInteger curency;
@property (nonatomic) BOOL isFavorite;
@property (nonatomic) double latitude;
@property (nonatomic) double longitude;
@property (nonatomic, strong) NSArray *images;
//user
@property (nonatomic, strong) UserModel *user;
@property (nonatomic) NSInteger userID;
@property (nonatomic, strong) NSString *userIP;
//views
@property (nonatomic) NSInteger viewsToday;
@property (nonatomic) NSInteger viewsTotal;

@property (nonatomic, strong) NSArray *dopParam;
@property (nonatomic, strong) NSArray *similarAdv;
@property (nonatomic, strong) NSArray *phones;
//groups
//@property (nonatomic, copy) NSArray <MSPhotoInfo *> *images;
//@property (nonatomic, copy) MSUserInfo *user;

//@property (nonatomic, copy) NSArray <MSAd *> *similar;
@property (nonatomic, copy) NSArray <DynamicPropertyModel *> *dynamicPropertyArray;
@property (nonatomic, copy) NSArray <CategoryModel *> *categories;
//@property (nonatomic, copy) NSArray <MSCategoryProperty *> *properties;


+ (instancetype)detailWithDictionary:(NSDictionary *)JSONresponse;
- (void)copyTo:(DetailAdvertisingModel *)model;

@end
