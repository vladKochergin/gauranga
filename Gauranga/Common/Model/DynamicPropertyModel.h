//
//  DynamicPropertyModel.h
//  Gauranga
//
//  Created by yvp on 2/13/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <EasyMapping/EasyMapping.h>
#import "DynamicPropsRangeModel.h"

@interface DynamicPropertyModel : EKObjectModel

@property (nonatomic) NSInteger dynPropID;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *value;
@property (nonatomic, copy) NSArray *list;
@property (nonatomic, strong) DynamicPropsRangeModel *extra;

+ (instancetype)dynamicPropertyWithDictionary:(NSDictionary *)JSONresponse;

- (void)updateList;

@end
