//
//  DialogModel.m
//  Gauranga
//
//  Created by yvp on 4/4/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import "DialogModel.h"

@implementation DialogModel

+ (EKObjectMapping *)objectMapping{
    
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapKeyPath:@"interlocutor_id" toProperty:@"interlocutorId"];
        [mapping mapKeyPath:@"avatar" toProperty:@"avatarUrl"];
        [mapping mapKeyPath:@"last_message_date" toProperty:@"lastMessageDate"];
        [mapping mapKeyPath:@"last_message_id" toProperty:@"lastMessageId"];
        [mapping mapKeyPath:@"message" toProperty:@"message"];
        [mapping mapKeyPath:@"messages_new" toProperty:@"messageNew"];
        [mapping mapKeyPath:@"messages_total" toProperty:@"messageTotal"];
        [mapping mapKeyPath:@"shop_id" toProperty:@"shopId"];
        [mapping mapKeyPath:@"shoptitle" toProperty:@"shopTitle"];
        [mapping mapKeyPath:@"user_id" toProperty:@"userId"];
        [mapping mapKeyPath:@"username" toProperty:@"userName"];
        [mapping mapKeyPath:@"userlogin" toProperty:@"userLogin"];
        [mapping mapKeyPath:@"lastmy" toProperty:@"lastSenderID"];
    }];
}

+ (instancetype)dialogWithDictionary:(NSDictionary *)JSONresponse{
    return [DialogModel objectWithProperties:JSONresponse];
}

@end
