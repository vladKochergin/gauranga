//
//  DialogModel.h
//  Gauranga
//
//  Created by yvp on 4/4/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <EasyMapping/EasyMapping.h>

@interface DialogModel : EKObjectModel

@property (nonatomic) NSInteger interlocutorId;
@property (nonatomic) NSInteger lastSenderID;

@property (nonatomic, strong) NSString *avatarUrl;
@property (nonatomic, strong) NSString *lastMessageDate;
@property (nonatomic, strong) NSString *lastMessageId;
@property (nonatomic, strong) NSString *message;
@property (nonatomic, strong) NSString *messageNew;
@property (nonatomic, strong) NSString *messageTotal;
@property (nonatomic, strong) NSString *shopId;
@property (nonatomic, strong) NSString *shopTitle;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *userLogin;

+ (instancetype)dialogWithDictionary:(NSDictionary *)JSONresponse;

@end
