//
//  DetailAdvertisingModel.m
//  Gauranga
//
//  Created by yvp on 2/15/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import "DetailAdvertisingModel.h"

@implementation DetailAdvertisingModel

+ (EKObjectMapping *)objectMapping{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapKeyPath:@"title" toProperty:@"title"];
        [mapping mapKeyPath:@"city_title" toProperty:@"city"];
        [mapping mapKeyPath:@"img_s" toProperty:@"imageSmall"];
        [mapping mapKeyPath:@"img_m" toProperty:@"imageMedium"];
        [mapping mapKeyPath:@"publicated" toProperty:@"date"];
        [mapping mapKeyPath:@"price" toProperty:@"price"];
        [mapping mapKeyPath:@"id" toProperty:@"advId"];
        [mapping mapKeyPath:@"cat_title" toProperty:@"categoryName"];
        [mapping mapKeyPath:@"cat_id" toProperty:@"categoryID"];
        [mapping mapKeyPath:@"addr_lat" toProperty:@"latitude"];
        [mapping mapKeyPath:@"addr_lon" toProperty:@"longitude"];
        [mapping mapKeyPath:@"user_id" toProperty:@"userID"];
        [mapping mapKeyPath:@"user_ip" toProperty:@"userIP"];
        [mapping mapKeyPath:@"views_today" toProperty:@"viewsToday"];
        [mapping mapKeyPath:@"views_total" toProperty:@"viewsTotal"];
        [mapping mapKeyPath:@"descr" toProperty:@"descriptionItem"];
        [mapping mapKeyPath:@"price_curr" toProperty:@"curency"];
        [mapping mapKeyPath:@"fav" toProperty:@"isFavorite"];
        [mapping mapKeyPath:@"name" toProperty:@"userName"];
        [mapping mapKeyPath:@"city_id" toProperty:@"cityId"];
        [mapping mapKeyPath:@"status" toProperty:@"status"];
    }];
}

+ (instancetype)detailWithDictionary:(NSDictionary *)JSONresponse{
    return [DetailAdvertisingModel objectWithProperties:JSONresponse];
}

- (void)copyTo:(DetailAdvertisingModel *)model{
    model.title = self.title;
    model.city = self.city;
    model.imageSmall = self.imageSmall;
    model.imageMedium = self.imageMedium;
    model.date = self.date;
    model.price = self.price;
    model.advId = self.advId;
    model.categoryName = self.categoryName;
    model.latitude = self.latitude;
    model.longitude = self.longitude;
    model.userID = self.userID;
    model.userIP = self.userIP;
    model.viewsToday = self.viewsToday;
    model.viewsTotal = self.viewsTotal;
    model.descriptionItem = self.descriptionItem;
    model.curency = self.curency;
    model.isFavorite = self.isFavorite;
    model.userName = self.userName;
    model.cityId = self.cityId;
    model.status = self.status;
}

@end
