//
//  DynamicPropertyModel.m
//  Gauranga
//
//  Created by yvp on 2/13/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import "DynamicPropertyModel.h"

@implementation DynamicPropertyModel

+ (EKObjectMapping *)objectMapping{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapKeyPath:@"id" toProperty:@"dynPropID"];
        [mapping mapKeyPath:@"t" toProperty:@"title"];
        [mapping mapKeyPath:@"type" toProperty:@"type"];
        [mapping mapKeyPath:@"val" toProperty:@"value"];
        [mapping mapKeyPath:@"list" toProperty:@"list"];
        [mapping hasOne:[DynamicPropsRangeModel class] forKeyPath:@"extra"];
    }];
}


+ (instancetype)dynamicPropertyWithDictionary:(NSDictionary *)JSONresponse {
    return [DynamicPropertyModel objectWithProperties:JSONresponse];
}

- (void)updateList {
    if (self.type.integerValue == 11) {
        NSMutableArray *list = [NSMutableArray new];
        for (double i = self.extra.start; i < self.extra.end; i += self.extra.step) {
            int intValueI = i;
            if ((i - intValueI) > 0) {
                [list addObject:[NSString stringWithFormat:@"%f", i]];
            }
            else {
                [list addObject:[NSString stringWithFormat:@"%d", intValueI]];
            }
        }
        self.list = list;
    }
}

@end
