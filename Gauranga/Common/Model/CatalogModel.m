//
//  CatalogModel.m
//  Gauranga
//
//  Created by yvp on 2/7/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import "CatalogModel.h"

@implementation CatalogModel

+ (EKObjectMapping *)objectMapping{
    
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapKeyPath:@"title" toProperty:@"title"];
        [mapping mapKeyPath:@"city_title" toProperty:@"city"];
        [mapping mapKeyPath:@"publicated" toProperty:@"date"];
        [mapping mapKeyPath:@"id" toProperty:@"itemId"];
        [mapping mapKeyPath:@"cat_id" toProperty:@"categoryID"];
        [mapping mapKeyPath:@"cat_title" toProperty:@"categoryName"];
        [mapping mapKeyPath:@"img_s" toProperty:@"imageSmall"];
        [mapping mapKeyPath:@"img_m" toProperty:@"imageMedium"];
        [mapping mapKeyPath:@"price" toProperty:@"price"];
        [mapping mapKeyPath:@"lat" toProperty:@"latitude"];
        [mapping mapKeyPath:@"lon" toProperty:@"longitude"];
        [mapping mapKeyPath:@"price_curr" toProperty:@"curency"];
        [mapping mapKeyPath:@"fav" toProperty:@"isFavorite"];
        [mapping mapKeyPath:@"status" toProperty:@"status"];
        [mapping mapKeyPath:@"item_price_display" toProperty:@"priceDisplay"];
    }];
}

+ (instancetype)catalogWithDictionary:(NSDictionary *)JSONresponse{
    return [CatalogModel objectWithProperties:JSONresponse];
}

-(void)copyTo:(CatalogModel *)catalog{
    catalog.title = self.title;
    catalog.city = self.city;
    catalog.date = self.date;
    catalog.itemId = self.itemId;
    catalog.categoryID = self.categoryID;
    catalog.categoryName = self.categoryName;
    catalog.imageSmall = self.imageSmall;
    catalog.imageMedium = self.imageMedium;
    catalog.price = self.price;
    catalog.latitude = self.latitude;
    catalog.longitude = self.longitude;
    catalog.curency = self.curency;
    catalog.isFavorite = self.isFavorite;
    catalog.status = self.status;
    catalog.priceDisplay = self.priceDisplay;
}

@end
