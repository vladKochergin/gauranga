//
//  CityNavigationBar.m
//  Gauranga
//
//  Created by yvp on 2/8/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import "CityNavigationBar.h"

@implementation CityNavigationBar

- (instancetype)init{
    if (self = [super init]) {
        NSArray* nibViews = [[NSBundle mainBundle] loadNibNamed:@"CityNavigationBar"
                                                          owner:self
                                                        options:nil];
        
        self = [nibViews objectAtIndex:0];
        
        self.frame = CGRectMake(-30, 0, 150, 44);
        
        for (UIView *subView in self.searchBar.subviews)
        {
            for (UIView *secondLevelSubview in subView.subviews){
                if ([secondLevelSubview isKindOfClass:[UITextField class]])
                {
                    UITextField *searchBarTextField = (UITextField *)secondLevelSubview;
                    searchBarTextField.textColor = [UIColor whiteColor];
                    break;
                }
            }
        }
        
        self.searchBar.backgroundImage = [UIImage new];
        UIImageView *image = [UIImageView new];
        image.image = [image.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        image.image = [UIImage imageNamed:@"searchMenu"];
        
        [self.searchBar setImage:image.image
           forSearchBarIcon:UISearchBarIconSearch
                      state:UIControlStateNormal];
        
        [[UILabel appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]] setTextColor:[UIColor colorWithWhite:1 alpha:0.4]];
        [[UITextField appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]] setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.4]];
    }
    [self setupColor];
    return self;
}

#pragma mark - Additions 

- (void)setupColor{
    [self.searchBar setBarTintColor:[UIColor mainColor]];
    [self.searchBar setBackgroundColor:[UIColor mainColor]];
}

@end
