//
//  PushNotificationHandler.m
//  Gauranga
//
//  Created by a on 8/3/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SafariServices/SafariServices.h>
#import <UIKit/UIKit.h>

#import "PushNotificationHandler.h"
#import "ChatViewController.h"
#import "DialogViewController.h"

@interface PushNotificationHandler () <SFSafariViewControllerDelegate>

@end

@implementation PushNotificationHandler

- (void)handlePushWithUserInfo:(NSDictionary *)userInfo rootViewController:(SlideNavigationController *)slideNavigationController{
    NSDictionary *aps = userInfo[@"aps"];
    
    if ([[aps objectForKey:@"category"] isEqualToString:@"NEW_MESSAGE"]) {
        
        if ([[self topViewController] isKindOfClass:[DialogViewController class]]) {
            return;
        }
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier: @"dialogViewController"];
        [slideNavigationController pushViewController:vc animated:NO];
        return;
    }
    if ([[aps objectForKey:@"category"] isEqualToString:@"AD"]) {
        NSDictionary *adParams = [aps objectForKey:@"alert"];
        
        NSMutableArray *buttonsArray = [NSMutableArray new];
        NSURL *invalidURL = [NSURL URLWithString:[adParams objectForKey:@"uri"]];
        if (invalidURL && invalidURL.scheme && invalidURL.host) {
            [buttonsArray addObject:@"Перейти"];
        }
        [KVAlert alertWithTitle:[adParams objectForKey:@"title"] message:[adParams objectForKey:@"body"] buttons:buttonsArray preferredStyle:UIAlertControllerStyleAlert completion:^(NSUInteger buttonPresed) {
            switch (buttonPresed) {
                case 0:{
                    NSURL *url = [NSURL URLWithString:[adParams objectForKey:@"uri"]];
                    SFSafariViewController *svc = [[SFSafariViewController alloc] initWithURL:url];
                    svc.delegate = self;
                    [[self topViewController] presentViewController:svc animated:YES completion:nil];
                    break;
                }
            }
        }];
    }
}

- (void)showPushWithUserInfo:(NSDictionary *)userInfo withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
    NSDictionary *aps = userInfo[@"aps"];
    
    if ([[aps objectForKey:@"category"] isEqualToString:@"NEW_MESSAGE"]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ReceiveNewMessageNotification" object:nil];
        if ([[self topViewController] isKindOfClass:[ChatViewController class]]) {
            return;
        }
    }
    completionHandler(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge);
}

#pragma mark - Private Methods

- (UIViewController *)topViewController {
    UINavigationController *navigationViewController = (UINavigationController *)[UIApplication sharedApplication].keyWindow.rootViewController;
    return [[navigationViewController viewControllers] lastObject];
}

@end
