//
//  RequestErrorHandler.m
//  Gauranga
//
//  Created by a on 8/4/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import "RequestErrorHandler.h"

static const NSString *kMESSAGE_KEY = @"message";

@implementation RequestErrorHandler

+ (void)handleError:(NSError *)error {
    NSString *errorMessage = [self getErrorMessageWithKey:error];
    [self showAllerWithTitle:errorMessage];
}

+ (NSString *)getErrorMessageWithKey:(NSError *)error{
    if(![[error.userInfo allKeys] containsObject:kMESSAGE_KEY]){
        //Because this is a system error
        return [error localizedDescription];
    }
    
    NSString *key = [error.userInfo objectForKey:kMESSAGE_KEY];
    NSDictionary *errorKeys =  @{
                                 @"USER_NEED_ACTIVATION"                      : @"userNotActive",
                                 @"ACTIVATION_FAILED_KEY_MISSMATCH_OR_EXPIRE" : @"activationKeyFail",
                                 @"USER_PHONE_INUSE"                          : @"emailError",
                                 @"LOGIN_FAILD"                               : @"errorAuth",
                                 @"LOGIN_FAILD2"                              : @"errorAuth",
                                 @"USER_EMAIL_INUSE"                          : @"userEmailInUse"

                                 };
    if([[errorKeys allKeys] containsObject:key]){
        return [errorKeys objectForKey:key];
    }
    return @"Unknown_Error";
}

+ (void)showAllerWithTitle:(NSString *)title{
    [MBProgressHUD hideHUDForView:[WireFrame getView].view animated:YES];
    [KVAlert alertWithTitle:nil message:NSLocalizedString(title, @"")
             preferredStyle:UIAlertControllerStyleAlert completion:^{}];
}

@end
