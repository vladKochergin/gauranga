//
//  UIView+Addons.h
//  Tamaranga
//
//  Created by Maks on 6/4/16.
//  Copyright © 2016 Maks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Addons)

@property (nonatomic) IBInspectable CGFloat bottomBorderWidth;
@property (nonatomic) IBInspectable CGFloat topBorderWidth;

@end
