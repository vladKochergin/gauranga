//
//  main.m
//  Gauranga
//
//  Created by yvp on 2/7/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
