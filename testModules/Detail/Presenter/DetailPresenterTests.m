//
//  DetailPresenterTests.m
//  Gauranga
//
//  Created by Vlad Kochergin on 15/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "DetailPresenter.h"

#import "DetailViewInput.h"
#import "DetailInteractorInput.h"
#import "DetailRouterInput.h"

@interface DetailPresenterTests : XCTestCase

@property (nonatomic, strong) DetailPresenter *presenter;

@property (nonatomic, strong) id mockInteractor;
@property (nonatomic, strong) id mockRouter;
@property (nonatomic, strong) id mockView;

@end

@implementation DetailPresenterTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.presenter = [[DetailPresenter alloc] init];

    self.mockInteractor = OCMProtocolMock(@protocol(DetailInteractorInput));
    self.mockRouter = OCMProtocolMock(@protocol(DetailRouterInput));
    self.mockView = OCMProtocolMock(@protocol(DetailViewInput));

    self.presenter.interactor = self.mockInteractor;
    self.presenter.router = self.mockRouter;
    self.presenter.view = self.mockView;
}

- (void)tearDown {
    self.presenter = nil;

    self.mockView = nil;
    self.mockRouter = nil;
    self.mockInteractor = nil;

    [super tearDown];
}

#pragma mark - Тестирование методов DetailModuleInput

#pragma mark - Тестирование методов DetailViewOutput

- (void)testThatPresenterHandlesViewReadyEvent {
    // given


    // when
    [self.presenter didTriggerViewReadyEvent];

    // then
    OCMVerify([self.mockView setupInitialState]);
}

#pragma mark - Тестирование методов DetailInteractorOutput

@end
