//
//  DetailAssembly_Testable.h
//  Gauranga
//
//  Created by Vlad Kochergin on 15/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "DetailAssembly.h"

@class DetailViewController;
@class DetailInteractor;
@class DetailPresenter;
@class DetailRouter;

/**
 @author Vlad Kochergin

 Extension, который делает приватные методы фабрики открытыми для тестирования. 
 */
@interface DetailAssembly ()

- (DetailViewController *)viewDetail;
- (DetailPresenter *)presenterDetail;
- (DetailInteractor *)interactorDetail;
- (DetailRouter *)routerDetail;

@end
