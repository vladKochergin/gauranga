//
//  DetailInteractorTests.m
//  Gauranga
//
//  Created by Vlad Kochergin on 15/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "DetailInteractor.h"

#import "DetailInteractorOutput.h"

@interface DetailInteractorTests : XCTestCase

@property (nonatomic, strong) DetailInteractor *interactor;

@property (nonatomic, strong) id mockOutput;

@end

@implementation DetailInteractorTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.interactor = [[DetailInteractor alloc] init];

    self.mockOutput = OCMProtocolMock(@protocol(DetailInteractorOutput));

    self.interactor.output = self.mockOutput;
}

- (void)tearDown {
    self.interactor = nil;

    self.mockOutput = nil;

    [super tearDown];
}

#pragma mark - Тестирование методов DetailInteractorInput

@end
