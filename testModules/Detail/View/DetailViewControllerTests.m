//
//  DetailViewControllerTests.m
//  Gauranga
//
//  Created by Vlad Kochergin on 15/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "DetailViewController.h"

#import "DetailViewOutput.h"

@interface DetailViewControllerTests : XCTestCase

@property (nonatomic, strong) DetailViewController *controller;

@property (nonatomic, strong) id mockOutput;

@end

@implementation DetailViewControllerTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.controller = [[DetailViewController alloc] init];

    self.mockOutput = OCMProtocolMock(@protocol(DetailViewOutput));

    self.controller.output = self.mockOutput;
}

- (void)tearDown {
    self.controller = nil;

    self.mockOutput = nil;

    [super tearDown];
}

#pragma mark - Тестирование жизненного цикла

- (void)testThatControllerNotifiesPresenterOnDidLoad {
	// given

	// when
	[self.controller viewDidLoad];

	// then
	OCMVerify([self.mockOutput didTriggerViewReadyEvent]);
}

#pragma mark - Тестирование методов интерфейса

#pragma mark - Тестирование методов DetailViewInput

@end
