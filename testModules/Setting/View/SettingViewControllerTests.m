//
//  SettingViewControllerTests.m
//  Gauranga
//
//  Created by Vlad Kochergin on 09/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "SettingViewController.h"

#import "SettingViewOutput.h"

@interface SettingViewControllerTests : XCTestCase

@property (nonatomic, strong) SettingViewController *controller;

@property (nonatomic, strong) id mockOutput;

@end

@implementation SettingViewControllerTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.controller = [[SettingViewController alloc] init];

    self.mockOutput = OCMProtocolMock(@protocol(SettingViewOutput));

    self.controller.output = self.mockOutput;
}

- (void)tearDown {
    self.controller = nil;

    self.mockOutput = nil;

    [super tearDown];
}

#pragma mark - Тестирование жизненного цикла

- (void)testThatControllerNotifiesPresenterOnDidLoad {
	// given

	// when
	[self.controller viewDidLoad];

	// then
	OCMVerify([self.mockOutput didTriggerViewReadyEvent]);
}

#pragma mark - Тестирование методов интерфейса

#pragma mark - Тестирование методов SettingViewInput

@end
