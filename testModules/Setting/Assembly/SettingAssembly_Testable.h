//
//  SettingAssembly_Testable.h
//  Gauranga
//
//  Created by Vlad Kochergin on 09/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "SettingAssembly.h"

@class SettingViewController;
@class SettingInteractor;
@class SettingPresenter;
@class SettingRouter;

/**
 @author Vlad Kochergin

 Extension, который делает приватные методы фабрики открытыми для тестирования. 
 */
@interface SettingAssembly ()

- (SettingViewController *)viewSetting;
- (SettingPresenter *)presenterSetting;
- (SettingInteractor *)interactorSetting;
- (SettingRouter *)routerSetting;

@end
