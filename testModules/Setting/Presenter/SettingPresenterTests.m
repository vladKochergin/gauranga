//
//  SettingPresenterTests.m
//  Gauranga
//
//  Created by Vlad Kochergin on 09/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "SettingPresenter.h"

#import "SettingViewInput.h"
#import "SettingInteractorInput.h"
#import "SettingRouterInput.h"

@interface SettingPresenterTests : XCTestCase

@property (nonatomic, strong) SettingPresenter *presenter;

@property (nonatomic, strong) id mockInteractor;
@property (nonatomic, strong) id mockRouter;
@property (nonatomic, strong) id mockView;

@end

@implementation SettingPresenterTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.presenter = [[SettingPresenter alloc] init];

    self.mockInteractor = OCMProtocolMock(@protocol(SettingInteractorInput));
    self.mockRouter = OCMProtocolMock(@protocol(SettingRouterInput));
    self.mockView = OCMProtocolMock(@protocol(SettingViewInput));

    self.presenter.interactor = self.mockInteractor;
    self.presenter.router = self.mockRouter;
    self.presenter.view = self.mockView;
}

- (void)tearDown {
    self.presenter = nil;

    self.mockView = nil;
    self.mockRouter = nil;
    self.mockInteractor = nil;

    [super tearDown];
}

#pragma mark - Тестирование методов SettingModuleInput

#pragma mark - Тестирование методов SettingViewOutput

- (void)testThatPresenterHandlesViewReadyEvent {
    // given


    // when
    [self.presenter didTriggerViewReadyEvent];

    // then
    OCMVerify([self.mockView setupInitialState]);
}

#pragma mark - Тестирование методов SettingInteractorOutput

@end
