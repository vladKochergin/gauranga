//
//  SettingInteractorTests.m
//  Gauranga
//
//  Created by Vlad Kochergin on 09/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "SettingInteractor.h"

#import "SettingInteractorOutput.h"

@interface SettingInteractorTests : XCTestCase

@property (nonatomic, strong) SettingInteractor *interactor;

@property (nonatomic, strong) id mockOutput;

@end

@implementation SettingInteractorTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.interactor = [[SettingInteractor alloc] init];

    self.mockOutput = OCMProtocolMock(@protocol(SettingInteractorOutput));

    self.interactor.output = self.mockOutput;
}

- (void)tearDown {
    self.interactor = nil;

    self.mockOutput = nil;

    [super tearDown];
}

#pragma mark - Тестирование методов SettingInteractorInput

@end
