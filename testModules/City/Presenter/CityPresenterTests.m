//
//  CityPresenterTests.m
//  Gauranga
//
//  Created by Vlad Kochergin on 14/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "CityPresenter.h"

#import "CityViewInput.h"
#import "CityInteractorInput.h"
#import "CityRouterInput.h"

@interface CityPresenterTests : XCTestCase

@property (nonatomic, strong) CityPresenter *presenter;

@property (nonatomic, strong) id mockInteractor;
@property (nonatomic, strong) id mockRouter;
@property (nonatomic, strong) id mockView;

@end

@implementation CityPresenterTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.presenter = [[CityPresenter alloc] init];

    self.mockInteractor = OCMProtocolMock(@protocol(CityInteractorInput));
    self.mockRouter = OCMProtocolMock(@protocol(CityRouterInput));
    self.mockView = OCMProtocolMock(@protocol(CityViewInput));

    self.presenter.interactor = self.mockInteractor;
    self.presenter.router = self.mockRouter;
    self.presenter.view = self.mockView;
}

- (void)tearDown {
    self.presenter = nil;

    self.mockView = nil;
    self.mockRouter = nil;
    self.mockInteractor = nil;

    [super tearDown];
}

#pragma mark - Тестирование методов CityModuleInput

#pragma mark - Тестирование методов CityViewOutput

- (void)testThatPresenterHandlesViewReadyEvent {
    // given


    // when
    [self.presenter didTriggerViewReadyEvent];

    // then
    OCMVerify([self.mockView setupInitialState]);
}

#pragma mark - Тестирование методов CityInteractorOutput

@end
