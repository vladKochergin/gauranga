//
//  CityAssembly_Testable.h
//  Gauranga
//
//  Created by Vlad Kochergin on 14/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "CityAssembly.h"

@class CityViewController;
@class CityInteractor;
@class CityPresenter;
@class CityRouter;

/**
 @author Vlad Kochergin

 Extension, который делает приватные методы фабрики открытыми для тестирования. 
 */
@interface CityAssembly ()

- (CityViewController *)viewCity;
- (CityPresenter *)presenterCity;
- (CityInteractor *)interactorCity;
- (CityRouter *)routerCity;

@end
