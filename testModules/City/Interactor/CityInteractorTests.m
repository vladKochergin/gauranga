//
//  CityInteractorTests.m
//  Gauranga
//
//  Created by Vlad Kochergin on 14/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "CityInteractor.h"

#import "CityInteractorOutput.h"

@interface CityInteractorTests : XCTestCase

@property (nonatomic, strong) CityInteractor *interactor;

@property (nonatomic, strong) id mockOutput;

@end

@implementation CityInteractorTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.interactor = [[CityInteractor alloc] init];

    self.mockOutput = OCMProtocolMock(@protocol(CityInteractorOutput));

    self.interactor.output = self.mockOutput;
}

- (void)tearDown {
    self.interactor = nil;

    self.mockOutput = nil;

    [super tearDown];
}

#pragma mark - Тестирование методов CityInteractorInput

@end
