//
//  RegistrationRouterTests.m
//  Gauranga
//
//  Created by egor on 20/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "RegistrationRouter.h"

@interface RegistrationRouterTests : XCTestCase

@property (nonatomic, strong) RegistrationRouter *router;

@end

@implementation RegistrationRouterTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.router = [[RegistrationRouter alloc] init];
}

- (void)tearDown {
    self.router = nil;

    [super tearDown];
}

@end
