//
//  RegistrationPresenterTests.m
//  Gauranga
//
//  Created by egor on 20/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "RegistrationPresenter.h"

#import "RegistrationViewInput.h"
#import "RegistrationInteractorInput.h"
#import "RegistrationRouterInput.h"

@interface RegistrationPresenterTests : XCTestCase

@property (nonatomic, strong) RegistrationPresenter *presenter;

@property (nonatomic, strong) id mockInteractor;
@property (nonatomic, strong) id mockRouter;
@property (nonatomic, strong) id mockView;

@end

@implementation RegistrationPresenterTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.presenter = [[RegistrationPresenter alloc] init];

    self.mockInteractor = OCMProtocolMock(@protocol(RegistrationInteractorInput));
    self.mockRouter = OCMProtocolMock(@protocol(RegistrationRouterInput));
    self.mockView = OCMProtocolMock(@protocol(RegistrationViewInput));

    self.presenter.interactor = self.mockInteractor;
    self.presenter.router = self.mockRouter;
    self.presenter.view = self.mockView;
}

- (void)tearDown {
    self.presenter = nil;

    self.mockView = nil;
    self.mockRouter = nil;
    self.mockInteractor = nil;

    [super tearDown];
}

#pragma mark - Тестирование методов RegistrationModuleInput

#pragma mark - Тестирование методов RegistrationViewOutput

- (void)testThatPresenterHandlesViewReadyEvent {
    // given


    // when
    [self.presenter didTriggerViewReadyEvent];

    // then
    OCMVerify([self.mockView setupInitialState]);
}

#pragma mark - Тестирование методов RegistrationInteractorOutput

@end
