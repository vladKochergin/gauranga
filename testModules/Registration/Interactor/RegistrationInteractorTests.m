//
//  RegistrationInteractorTests.m
//  Gauranga
//
//  Created by egor on 20/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "RegistrationInteractor.h"

#import "RegistrationInteractorOutput.h"

@interface RegistrationInteractorTests : XCTestCase

@property (nonatomic, strong) RegistrationInteractor *interactor;

@property (nonatomic, strong) id mockOutput;

@end

@implementation RegistrationInteractorTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.interactor = [[RegistrationInteractor alloc] init];

    self.mockOutput = OCMProtocolMock(@protocol(RegistrationInteractorOutput));

    self.interactor.output = self.mockOutput;
}

- (void)tearDown {
    self.interactor = nil;

    self.mockOutput = nil;

    [super tearDown];
}

#pragma mark - Тестирование методов RegistrationInteractorInput

@end
