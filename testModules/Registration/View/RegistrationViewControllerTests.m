//
//  RegistrationViewControllerTests.m
//  Gauranga
//
//  Created by egor on 20/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "RegistrationViewController.h"

#import "RegistrationViewOutput.h"

@interface RegistrationViewControllerTests : XCTestCase

@property (nonatomic, strong) RegistrationViewController *controller;

@property (nonatomic, strong) id mockOutput;

@end

@implementation RegistrationViewControllerTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.controller = [[RegistrationViewController alloc] init];

    self.mockOutput = OCMProtocolMock(@protocol(RegistrationViewOutput));

    self.controller.output = self.mockOutput;
}

- (void)tearDown {
    self.controller = nil;

    self.mockOutput = nil;

    [super tearDown];
}

#pragma mark - Тестирование жизненного цикла

- (void)testThatControllerNotifiesPresenterOnDidLoad {
	// given

	// when
	[self.controller viewDidLoad];

	// then
	OCMVerify([self.mockOutput didTriggerViewReadyEvent]);
}

#pragma mark - Тестирование методов интерфейса

#pragma mark - Тестирование методов RegistrationViewInput

@end
