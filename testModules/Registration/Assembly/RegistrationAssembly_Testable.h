//
//  RegistrationAssembly_Testable.h
//  Gauranga
//
//  Created by egor on 20/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "RegistrationAssembly.h"

@class RegistrationViewController;
@class RegistrationInteractor;
@class RegistrationPresenter;
@class RegistrationRouter;

/**
 @author egor

 Extension, который делает приватные методы фабрики открытыми для тестирования. 
 */
@interface RegistrationAssembly ()

- (RegistrationViewController *)viewRegistration;
- (RegistrationPresenter *)presenterRegistration;
- (RegistrationInteractor *)interactorRegistration;
- (RegistrationRouter *)routerRegistration;

@end
