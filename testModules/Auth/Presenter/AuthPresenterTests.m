//
//  AuthPresenterTests.m
//  Gauranga
//
//  Created by Vlad Kochergin on 09/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "AuthPresenter.h"

#import "AuthViewInput.h"
#import "AuthInteractorInput.h"
#import "AuthRouterInput.h"

@interface AuthPresenterTests : XCTestCase

@property (nonatomic, strong) AuthPresenter *presenter;

@property (nonatomic, strong) id mockInteractor;
@property (nonatomic, strong) id mockRouter;
@property (nonatomic, strong) id mockView;

@end

@implementation AuthPresenterTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.presenter = [[AuthPresenter alloc] init];

    self.mockInteractor = OCMProtocolMock(@protocol(AuthInteractorInput));
    self.mockRouter = OCMProtocolMock(@protocol(AuthRouterInput));
    self.mockView = OCMProtocolMock(@protocol(AuthViewInput));

    self.presenter.interactor = self.mockInteractor;
    self.presenter.router = self.mockRouter;
    self.presenter.view = self.mockView;
}

- (void)tearDown {
    self.presenter = nil;

    self.mockView = nil;
    self.mockRouter = nil;
    self.mockInteractor = nil;

    [super tearDown];
}

#pragma mark - Тестирование методов AuthModuleInput

#pragma mark - Тестирование методов AuthViewOutput

- (void)testThatPresenterHandlesViewReadyEvent {
    // given


    // when
    [self.presenter didTriggerViewReadyEvent];

    // then
    OCMVerify([self.mockView setupInitialState]);
}

#pragma mark - Тестирование методов AuthInteractorOutput

@end
