//
//  AuthInteractorTests.m
//  Gauranga
//
//  Created by Vlad Kochergin on 09/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "AuthInteractor.h"

#import "AuthInteractorOutput.h"

@interface AuthInteractorTests : XCTestCase

@property (nonatomic, strong) AuthInteractor *interactor;

@property (nonatomic, strong) id mockOutput;

@end

@implementation AuthInteractorTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.interactor = [[AuthInteractor alloc] init];

    self.mockOutput = OCMProtocolMock(@protocol(AuthInteractorOutput));

    self.interactor.output = self.mockOutput;
}

- (void)tearDown {
    self.interactor = nil;

    self.mockOutput = nil;

    [super tearDown];
}

#pragma mark - Тестирование методов AuthInteractorInput

@end
