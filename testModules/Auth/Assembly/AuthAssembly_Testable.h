//
//  AuthAssembly_Testable.h
//  Gauranga
//
//  Created by Vlad Kochergin on 09/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "AuthAssembly.h"

@class AuthViewController;
@class AuthInteractor;
@class AuthPresenter;
@class AuthRouter;

/**
 @author Vlad Kochergin

 Extension, который делает приватные методы фабрики открытыми для тестирования. 
 */
@interface AuthAssembly ()

- (AuthViewController *)viewAuth;
- (AuthPresenter *)presenterAuth;
- (AuthInteractor *)interactorAuth;
- (AuthRouter *)routerAuth;

@end
