//
//  AddAdvertisingAssembly_Testable.h
//  Gauranga
//
//  Created by Vlad Kochergin on 10/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "AddAdvertisingAssembly.h"

@class AddAdvertisingViewController;
@class AddAdvertisingInteractor;
@class AddAdvertisingPresenter;
@class AddAdvertisingRouter;

/**
 @author Vlad Kochergin

 Extension, который делает приватные методы фабрики открытыми для тестирования. 
 */
@interface AddAdvertisingAssembly ()

- (AddAdvertisingViewController *)viewAddAdvertising;
- (AddAdvertisingPresenter *)presenterAddAdvertising;
- (AddAdvertisingInteractor *)interactorAddAdvertising;
- (AddAdvertisingRouter *)routerAddAdvertising;

@end
