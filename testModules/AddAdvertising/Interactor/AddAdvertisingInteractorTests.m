//
//  AddAdvertisingInteractorTests.m
//  Gauranga
//
//  Created by Vlad Kochergin on 10/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "AddAdvertisingInteractor.h"

#import "AddAdvertisingInteractorOutput.h"

@interface AddAdvertisingInteractorTests : XCTestCase

@property (nonatomic, strong) AddAdvertisingInteractor *interactor;

@property (nonatomic, strong) id mockOutput;

@end

@implementation AddAdvertisingInteractorTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.interactor = [[AddAdvertisingInteractor alloc] init];

    self.mockOutput = OCMProtocolMock(@protocol(AddAdvertisingInteractorOutput));

    self.interactor.output = self.mockOutput;
}

- (void)tearDown {
    self.interactor = nil;

    self.mockOutput = nil;

    [super tearDown];
}

#pragma mark - Тестирование методов AddAdvertisingInteractorInput

@end
