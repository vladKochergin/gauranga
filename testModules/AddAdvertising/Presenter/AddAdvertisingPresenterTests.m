//
//  AddAdvertisingPresenterTests.m
//  Gauranga
//
//  Created by Vlad Kochergin on 10/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "AddAdvertisingPresenter.h"

#import "AddAdvertisingViewInput.h"
#import "AddAdvertisingInteractorInput.h"
#import "AddAdvertisingRouterInput.h"

@interface AddAdvertisingPresenterTests : XCTestCase

@property (nonatomic, strong) AddAdvertisingPresenter *presenter;

@property (nonatomic, strong) id mockInteractor;
@property (nonatomic, strong) id mockRouter;
@property (nonatomic, strong) id mockView;

@end

@implementation AddAdvertisingPresenterTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.presenter = [[AddAdvertisingPresenter alloc] init];

    self.mockInteractor = OCMProtocolMock(@protocol(AddAdvertisingInteractorInput));
    self.mockRouter = OCMProtocolMock(@protocol(AddAdvertisingRouterInput));
    self.mockView = OCMProtocolMock(@protocol(AddAdvertisingViewInput));

    self.presenter.interactor = self.mockInteractor;
    self.presenter.router = self.mockRouter;
    self.presenter.view = self.mockView;
}

- (void)tearDown {
    self.presenter = nil;

    self.mockView = nil;
    self.mockRouter = nil;
    self.mockInteractor = nil;

    [super tearDown];
}

#pragma mark - Тестирование методов AddAdvertisingModuleInput

#pragma mark - Тестирование методов AddAdvertisingViewOutput

- (void)testThatPresenterHandlesViewReadyEvent {
    // given


    // when
    [self.presenter didTriggerViewReadyEvent];

    // then
    OCMVerify([self.mockView setupInitialState]);
}

#pragma mark - Тестирование методов AddAdvertisingInteractorOutput

@end
