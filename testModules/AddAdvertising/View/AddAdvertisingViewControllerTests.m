//
//  AddAdvertisingViewControllerTests.m
//  Gauranga
//
//  Created by Vlad Kochergin on 10/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "AddAdvertisingViewController.h"

#import "AddAdvertisingViewOutput.h"

@interface AddAdvertisingViewControllerTests : XCTestCase

@property (nonatomic, strong) AddAdvertisingViewController *controller;

@property (nonatomic, strong) id mockOutput;

@end

@implementation AddAdvertisingViewControllerTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.controller = [[AddAdvertisingViewController alloc] init];

    self.mockOutput = OCMProtocolMock(@protocol(AddAdvertisingViewOutput));

    self.controller.output = self.mockOutput;
}

- (void)tearDown {
    self.controller = nil;

    self.mockOutput = nil;

    [super tearDown];
}

#pragma mark - Тестирование жизненного цикла

- (void)testThatControllerNotifiesPresenterOnDidLoad {
	// given

	// when
	[self.controller viewDidLoad];

	// then
	OCMVerify([self.mockOutput didTriggerViewReadyEvent]);
}

#pragma mark - Тестирование методов интерфейса

#pragma mark - Тестирование методов AddAdvertisingViewInput

@end
