//
//  AddAdvertisingRouterTests.m
//  Gauranga
//
//  Created by Vlad Kochergin on 10/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "AddAdvertisingRouter.h"

@interface AddAdvertisingRouterTests : XCTestCase

@property (nonatomic, strong) AddAdvertisingRouter *router;

@end

@implementation AddAdvertisingRouterTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.router = [[AddAdvertisingRouter alloc] init];
}

- (void)tearDown {
    self.router = nil;

    [super tearDown];
}

@end
