//
//  ChatRouterTests.m
//  Gauranga
//
//  Created by Vlad Kochergin on 22/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "ChatRouter.h"

@interface ChatRouterTests : XCTestCase

@property (nonatomic, strong) ChatRouter *router;

@end

@implementation ChatRouterTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.router = [[ChatRouter alloc] init];
}

- (void)tearDown {
    self.router = nil;

    [super tearDown];
}

@end
