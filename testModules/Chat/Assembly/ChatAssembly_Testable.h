//
//  ChatAssembly_Testable.h
//  Gauranga
//
//  Created by Vlad Kochergin on 22/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "ChatAssembly.h"

@class ChatViewController;
@class ChatInteractor;
@class ChatPresenter;
@class ChatRouter;

/**
 @author Vlad Kochergin

 Extension, который делает приватные методы фабрики открытыми для тестирования. 
 */
@interface ChatAssembly ()

- (ChatViewController *)viewChat;
- (ChatPresenter *)presenterChat;
- (ChatInteractor *)interactorChat;
- (ChatRouter *)routerChat;

@end
