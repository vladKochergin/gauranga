//
//  ChatInteractorTests.m
//  Gauranga
//
//  Created by Vlad Kochergin on 22/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "ChatInteractor.h"

#import "ChatInteractorOutput.h"

@interface ChatInteractorTests : XCTestCase

@property (nonatomic, strong) ChatInteractor *interactor;

@property (nonatomic, strong) id mockOutput;

@end

@implementation ChatInteractorTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.interactor = [[ChatInteractor alloc] init];

    self.mockOutput = OCMProtocolMock(@protocol(ChatInteractorOutput));

    self.interactor.output = self.mockOutput;
}

- (void)tearDown {
    self.interactor = nil;

    self.mockOutput = nil;

    [super tearDown];
}

#pragma mark - Тестирование методов ChatInteractorInput

@end
