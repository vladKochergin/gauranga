//
//  ChatPresenterTests.m
//  Gauranga
//
//  Created by Vlad Kochergin on 22/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "ChatPresenter.h"

#import "ChatViewInput.h"
#import "ChatInteractorInput.h"
#import "ChatRouterInput.h"

@interface ChatPresenterTests : XCTestCase

@property (nonatomic, strong) ChatPresenter *presenter;

@property (nonatomic, strong) id mockInteractor;
@property (nonatomic, strong) id mockRouter;
@property (nonatomic, strong) id mockView;

@end

@implementation ChatPresenterTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.presenter = [[ChatPresenter alloc] init];

    self.mockInteractor = OCMProtocolMock(@protocol(ChatInteractorInput));
    self.mockRouter = OCMProtocolMock(@protocol(ChatRouterInput));
    self.mockView = OCMProtocolMock(@protocol(ChatViewInput));

    self.presenter.interactor = self.mockInteractor;
    self.presenter.router = self.mockRouter;
    self.presenter.view = self.mockView;
}

- (void)tearDown {
    self.presenter = nil;

    self.mockView = nil;
    self.mockRouter = nil;
    self.mockInteractor = nil;

    [super tearDown];
}

#pragma mark - Тестирование методов ChatModuleInput

#pragma mark - Тестирование методов ChatViewOutput

- (void)testThatPresenterHandlesViewReadyEvent {
    // given


    // when
    [self.presenter didTriggerViewReadyEvent];

    // then
    OCMVerify([self.mockView setupInitialState]);
}

#pragma mark - Тестирование методов ChatInteractorOutput

@end
