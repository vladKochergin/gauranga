//
//  MapAssembly_Testable.h
//  Gauranga
//
//  Created by Vlad Kochergin on 16/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "MapAssembly.h"

@class MapViewController;
@class MapInteractor;
@class MapPresenter;
@class MapRouter;

/**
 @author Vlad Kochergin

 Extension, который делает приватные методы фабрики открытыми для тестирования. 
 */
@interface MapAssembly ()

- (MapViewController *)viewMap;
- (MapPresenter *)presenterMap;
- (MapInteractor *)interactorMap;
- (MapRouter *)routerMap;

@end
