//
//  MapPresenterTests.m
//  Gauranga
//
//  Created by Vlad Kochergin on 16/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "MapPresenter.h"

#import "MapViewInput.h"
#import "MapInteractorInput.h"
#import "MapRouterInput.h"

@interface MapPresenterTests : XCTestCase

@property (nonatomic, strong) MapPresenter *presenter;

@property (nonatomic, strong) id mockInteractor;
@property (nonatomic, strong) id mockRouter;
@property (nonatomic, strong) id mockView;

@end

@implementation MapPresenterTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.presenter = [[MapPresenter alloc] init];

    self.mockInteractor = OCMProtocolMock(@protocol(MapInteractorInput));
    self.mockRouter = OCMProtocolMock(@protocol(MapRouterInput));
    self.mockView = OCMProtocolMock(@protocol(MapViewInput));

    self.presenter.interactor = self.mockInteractor;
    self.presenter.router = self.mockRouter;
    self.presenter.view = self.mockView;
}

- (void)tearDown {
    self.presenter = nil;

    self.mockView = nil;
    self.mockRouter = nil;
    self.mockInteractor = nil;

    [super tearDown];
}

#pragma mark - Тестирование методов MapModuleInput

#pragma mark - Тестирование методов MapViewOutput

- (void)testThatPresenterHandlesViewReadyEvent {
    // given


    // when
    [self.presenter didTriggerViewReadyEvent];

    // then
    OCMVerify([self.mockView setupInitialState]);
}

#pragma mark - Тестирование методов MapInteractorOutput

@end
