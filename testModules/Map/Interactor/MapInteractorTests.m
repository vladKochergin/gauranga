//
//  MapInteractorTests.m
//  Gauranga
//
//  Created by Vlad Kochergin on 16/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "MapInteractor.h"

#import "MapInteractorOutput.h"

@interface MapInteractorTests : XCTestCase

@property (nonatomic, strong) MapInteractor *interactor;

@property (nonatomic, strong) id mockOutput;

@end

@implementation MapInteractorTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.interactor = [[MapInteractor alloc] init];

    self.mockOutput = OCMProtocolMock(@protocol(MapInteractorOutput));

    self.interactor.output = self.mockOutput;
}

- (void)tearDown {
    self.interactor = nil;

    self.mockOutput = nil;

    [super tearDown];
}

#pragma mark - Тестирование методов MapInteractorInput

@end
