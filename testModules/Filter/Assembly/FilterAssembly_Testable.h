//
//  FilterAssembly_Testable.h
//  Gauranga
//
//  Created by Vlad Kochergin on 21/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "FilterAssembly.h"

@class FilterViewController;
@class FilterInteractor;
@class FilterPresenter;
@class FilterRouter;

/**
 @author Vlad Kochergin

 Extension, который делает приватные методы фабрики открытыми для тестирования. 
 */
@interface FilterAssembly ()

- (FilterViewController *)viewFilter;
- (FilterPresenter *)presenterFilter;
- (FilterInteractor *)interactorFilter;
- (FilterRouter *)routerFilter;

@end
