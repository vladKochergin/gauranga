//
//  FilterPresenterTests.m
//  Gauranga
//
//  Created by Vlad Kochergin on 21/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "FilterPresenter.h"

#import "FilterViewInput.h"
#import "FilterInteractorInput.h"
#import "FilterRouterInput.h"

@interface FilterPresenterTests : XCTestCase

@property (nonatomic, strong) FilterPresenter *presenter;

@property (nonatomic, strong) id mockInteractor;
@property (nonatomic, strong) id mockRouter;
@property (nonatomic, strong) id mockView;

@end

@implementation FilterPresenterTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.presenter = [[FilterPresenter alloc] init];

    self.mockInteractor = OCMProtocolMock(@protocol(FilterInteractorInput));
    self.mockRouter = OCMProtocolMock(@protocol(FilterRouterInput));
    self.mockView = OCMProtocolMock(@protocol(FilterViewInput));

    self.presenter.interactor = self.mockInteractor;
    self.presenter.router = self.mockRouter;
    self.presenter.view = self.mockView;
}

- (void)tearDown {
    self.presenter = nil;

    self.mockView = nil;
    self.mockRouter = nil;
    self.mockInteractor = nil;

    [super tearDown];
}

#pragma mark - Тестирование методов FilterModuleInput

#pragma mark - Тестирование методов FilterViewOutput

- (void)testThatPresenterHandlesViewReadyEvent {
    // given


    // when
    [self.presenter didTriggerViewReadyEvent];

    // then
    OCMVerify([self.mockView setupInitialState]);
}

#pragma mark - Тестирование методов FilterInteractorOutput

@end
