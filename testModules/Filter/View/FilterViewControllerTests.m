//
//  FilterViewControllerTests.m
//  Gauranga
//
//  Created by Vlad Kochergin on 21/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "FilterViewController.h"

#import "FilterViewOutput.h"

@interface FilterViewControllerTests : XCTestCase

@property (nonatomic, strong) FilterViewController *controller;

@property (nonatomic, strong) id mockOutput;

@end

@implementation FilterViewControllerTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.controller = [[FilterViewController alloc] init];

    self.mockOutput = OCMProtocolMock(@protocol(FilterViewOutput));

    self.controller.output = self.mockOutput;
}

- (void)tearDown {
    self.controller = nil;

    self.mockOutput = nil;

    [super tearDown];
}

#pragma mark - Тестирование жизненного цикла

- (void)testThatControllerNotifiesPresenterOnDidLoad {
	// given

	// when
	[self.controller viewDidLoad];

	// then
	OCMVerify([self.mockOutput didTriggerViewReadyEvent]);
}

#pragma mark - Тестирование методов интерфейса

#pragma mark - Тестирование методов FilterViewInput

@end
