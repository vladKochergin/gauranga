//
//  FilterInteractorTests.m
//  Gauranga
//
//  Created by Vlad Kochergin on 21/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "FilterInteractor.h"

#import "FilterInteractorOutput.h"

@interface FilterInteractorTests : XCTestCase

@property (nonatomic, strong) FilterInteractor *interactor;

@property (nonatomic, strong) id mockOutput;

@end

@implementation FilterInteractorTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.interactor = [[FilterInteractor alloc] init];

    self.mockOutput = OCMProtocolMock(@protocol(FilterInteractorOutput));

    self.interactor.output = self.mockOutput;
}

- (void)tearDown {
    self.interactor = nil;

    self.mockOutput = nil;

    [super tearDown];
}

#pragma mark - Тестирование методов FilterInteractorInput

@end
