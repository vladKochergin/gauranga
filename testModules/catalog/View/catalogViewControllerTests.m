//
//  CatalogViewControllerTests.m
//  Gauranga
//
//  Created by Vlad Kochergin on 07/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "CatalogViewController.h"

#import "CatalogViewOutput.h"

@interface CatalogViewControllerTests : XCTestCase

@property (nonatomic, strong) CatalogViewController *controller;

@property (nonatomic, strong) id mockOutput;

@end

@implementation CatalogViewControllerTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.controller = [[CatalogViewController alloc] init];

    self.mockOutput = OCMProtocolMock(@protocol(CatalogViewOutput));

    self.controller.output = self.mockOutput;
}

- (void)tearDown {
    self.controller = nil;

    self.mockOutput = nil;

    [super tearDown];
}

#pragma mark - Тестирование жизненного цикла

- (void)testThatControllerNotifiesPresenterOnDidLoad {
	// given

	// when
	[self.controller viewDidLoad];

	// then
	OCMVerify([self.mockOutput didTriggerViewReadyEvent]);
}

#pragma mark - Тестирование методов интерфейса

#pragma mark - Тестирование методов CatalogViewInput

@end
