//
//  CatalogRouterTests.m
//  Gauranga
//
//  Created by Vlad Kochergin on 07/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "CatalogRouter.h"

@interface CatalogRouterTests : XCTestCase

@property (nonatomic, strong) CatalogRouter *router;

@end

@implementation CatalogRouterTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.router = [[CatalogRouter alloc] init];
}

- (void)tearDown {
    self.router = nil;

    [super tearDown];
}

@end
