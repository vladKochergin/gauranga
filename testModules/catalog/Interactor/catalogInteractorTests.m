//
//  CatalogInteractorTests.m
//  Gauranga
//
//  Created by Vlad Kochergin on 07/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "CatalogInteractor.h"

#import "CatalogInteractorOutput.h"

@interface CatalogInteractorTests : XCTestCase

@property (nonatomic, strong) CatalogInteractor *interactor;

@property (nonatomic, strong) id mockOutput;

@end

@implementation CatalogInteractorTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.interactor = [[CatalogInteractor alloc] init];

    self.mockOutput = OCMProtocolMock(@protocol(CatalogInteractorOutput));

    self.interactor.output = self.mockOutput;
}

- (void)tearDown {
    self.interactor = nil;

    self.mockOutput = nil;

    [super tearDown];
}

#pragma mark - Тестирование методов CatalogInteractorInput

@end
