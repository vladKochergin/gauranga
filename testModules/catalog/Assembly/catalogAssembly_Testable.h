//
//  CatalogAssembly_Testable.h
//  Gauranga
//
//  Created by Vlad Kochergin on 07/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "CatalogAssembly.h"

@class CatalogViewController;
@class CatalogInteractor;
@class CatalogPresenter;
@class CatalogRouter;

/**
 @author Vlad Kochergin

 Extension, который делает приватные методы фабрики открытыми для тестирования. 
 */
@interface CatalogAssembly ()

- (CatalogViewController *)viewCatalog;
- (CatalogPresenter *)presenterCatalog;
- (CatalogInteractor *)interactorCatalog;
- (CatalogRouter *)routerCatalog;

@end
