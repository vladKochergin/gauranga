//
//  CatalogPresenterTests.m
//  Gauranga
//
//  Created by Vlad Kochergin on 07/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "CatalogPresenter.h"

#import "CatalogViewInput.h"
#import "CatalogInteractorInput.h"
#import "CatalogRouterInput.h"

@interface CatalogPresenterTests : XCTestCase

@property (nonatomic, strong) CatalogPresenter *presenter;

@property (nonatomic, strong) id mockInteractor;
@property (nonatomic, strong) id mockRouter;
@property (nonatomic, strong) id mockView;

@end

@implementation CatalogPresenterTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.presenter = [[CatalogPresenter alloc] init];

    self.mockInteractor = OCMProtocolMock(@protocol(CatalogInteractorInput));
    self.mockRouter = OCMProtocolMock(@protocol(CatalogRouterInput));
    self.mockView = OCMProtocolMock(@protocol(CatalogViewInput));

    self.presenter.interactor = self.mockInteractor;
    self.presenter.router = self.mockRouter;
    self.presenter.view = self.mockView;
}

- (void)tearDown {
    self.presenter = nil;

    self.mockView = nil;
    self.mockRouter = nil;
    self.mockInteractor = nil;

    [super tearDown];
}

#pragma mark - Тестирование методов CatalogModuleInput

#pragma mark - Тестирование методов CatalogViewOutput

- (void)testThatPresenterHandlesViewReadyEvent {
    // given


    // when
    [self.presenter didTriggerViewReadyEvent];

    // then
    OCMVerify([self.mockView setupInitialState]);
}

#pragma mark - Тестирование методов CatalogInteractorOutput

@end
