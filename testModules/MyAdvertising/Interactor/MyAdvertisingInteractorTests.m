//
//  MyAdvertisingInteractorTests.m
//  Gauranga
//
//  Created by Vlad Kochergin on 20/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "MyAdvertisingInteractor.h"

#import "MyAdvertisingInteractorOutput.h"

@interface MyAdvertisingInteractorTests : XCTestCase

@property (nonatomic, strong) MyAdvertisingInteractor *interactor;

@property (nonatomic, strong) id mockOutput;

@end

@implementation MyAdvertisingInteractorTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.interactor = [[MyAdvertisingInteractor alloc] init];

    self.mockOutput = OCMProtocolMock(@protocol(MyAdvertisingInteractorOutput));

    self.interactor.output = self.mockOutput;
}

- (void)tearDown {
    self.interactor = nil;

    self.mockOutput = nil;

    [super tearDown];
}

#pragma mark - Тестирование методов MyAdvertisingInteractorInput

@end
