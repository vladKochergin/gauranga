//
//  MyAdvertisingViewControllerTests.m
//  Gauranga
//
//  Created by Vlad Kochergin on 20/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "MyAdvertisingViewController.h"

#import "MyAdvertisingViewOutput.h"

@interface MyAdvertisingViewControllerTests : XCTestCase

@property (nonatomic, strong) MyAdvertisingViewController *controller;

@property (nonatomic, strong) id mockOutput;

@end

@implementation MyAdvertisingViewControllerTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.controller = [[MyAdvertisingViewController alloc] init];

    self.mockOutput = OCMProtocolMock(@protocol(MyAdvertisingViewOutput));

    self.controller.output = self.mockOutput;
}

- (void)tearDown {
    self.controller = nil;

    self.mockOutput = nil;

    [super tearDown];
}

#pragma mark - Тестирование жизненного цикла

- (void)testThatControllerNotifiesPresenterOnDidLoad {
	// given

	// when
	[self.controller viewDidLoad];

	// then
	OCMVerify([self.mockOutput didTriggerViewReadyEvent]);
}

#pragma mark - Тестирование методов интерфейса

#pragma mark - Тестирование методов MyAdvertisingViewInput

@end
