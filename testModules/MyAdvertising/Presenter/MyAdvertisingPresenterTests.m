//
//  MyAdvertisingPresenterTests.m
//  Gauranga
//
//  Created by Vlad Kochergin on 20/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "MyAdvertisingPresenter.h"

#import "MyAdvertisingViewInput.h"
#import "MyAdvertisingInteractorInput.h"
#import "MyAdvertisingRouterInput.h"

@interface MyAdvertisingPresenterTests : XCTestCase

@property (nonatomic, strong) MyAdvertisingPresenter *presenter;

@property (nonatomic, strong) id mockInteractor;
@property (nonatomic, strong) id mockRouter;
@property (nonatomic, strong) id mockView;

@end

@implementation MyAdvertisingPresenterTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.presenter = [[MyAdvertisingPresenter alloc] init];

    self.mockInteractor = OCMProtocolMock(@protocol(MyAdvertisingInteractorInput));
    self.mockRouter = OCMProtocolMock(@protocol(MyAdvertisingRouterInput));
    self.mockView = OCMProtocolMock(@protocol(MyAdvertisingViewInput));

    self.presenter.interactor = self.mockInteractor;
    self.presenter.router = self.mockRouter;
    self.presenter.view = self.mockView;
}

- (void)tearDown {
    self.presenter = nil;

    self.mockView = nil;
    self.mockRouter = nil;
    self.mockInteractor = nil;

    [super tearDown];
}

#pragma mark - Тестирование методов MyAdvertisingModuleInput

#pragma mark - Тестирование методов MyAdvertisingViewOutput

- (void)testThatPresenterHandlesViewReadyEvent {
    // given


    // when
    [self.presenter didTriggerViewReadyEvent];

    // then
    OCMVerify([self.mockView setupInitialState]);
}

#pragma mark - Тестирование методов MyAdvertisingInteractorOutput

@end
