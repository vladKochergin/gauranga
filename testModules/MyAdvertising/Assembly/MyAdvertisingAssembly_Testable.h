//
//  MyAdvertisingAssembly_Testable.h
//  Gauranga
//
//  Created by Vlad Kochergin on 20/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "MyAdvertisingAssembly.h"

@class MyAdvertisingViewController;
@class MyAdvertisingInteractor;
@class MyAdvertisingPresenter;
@class MyAdvertisingRouter;

/**
 @author Vlad Kochergin

 Extension, который делает приватные методы фабрики открытыми для тестирования. 
 */
@interface MyAdvertisingAssembly ()

- (MyAdvertisingViewController *)viewMyAdvertising;
- (MyAdvertisingPresenter *)presenterMyAdvertising;
- (MyAdvertisingInteractor *)interactorMyAdvertising;
- (MyAdvertisingRouter *)routerMyAdvertising;

@end
