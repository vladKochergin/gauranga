//
//  MyAdvertisingRouterTests.m
//  Gauranga
//
//  Created by Vlad Kochergin on 20/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "MyAdvertisingRouter.h"

@interface MyAdvertisingRouterTests : XCTestCase

@property (nonatomic, strong) MyAdvertisingRouter *router;

@end

@implementation MyAdvertisingRouterTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.router = [[MyAdvertisingRouter alloc] init];
}

- (void)tearDown {
    self.router = nil;

    [super tearDown];
}

@end
