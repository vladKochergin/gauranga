//
//  CategoryInteractorTests.m
//  Gauranga
//
//  Created by Vlad Kochergin on 08/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "CategoryInteractor.h"

#import "CategoryInteractorOutput.h"

@interface CategoryInteractorTests : XCTestCase

@property (nonatomic, strong) CategoryInteractor *interactor;

@property (nonatomic, strong) id mockOutput;

@end

@implementation CategoryInteractorTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.interactor = [[CategoryInteractor alloc] init];

    self.mockOutput = OCMProtocolMock(@protocol(CategoryInteractorOutput));

    self.interactor.output = self.mockOutput;
}

- (void)tearDown {
    self.interactor = nil;

    self.mockOutput = nil;

    [super tearDown];
}

#pragma mark - Тестирование методов CategoryInteractorInput

@end
