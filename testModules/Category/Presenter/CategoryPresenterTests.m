//
//  CategoryPresenterTests.m
//  Gauranga
//
//  Created by Vlad Kochergin on 08/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "CategoryPresenter.h"

#import "CategoryViewInput.h"
#import "CategoryInteractorInput.h"
#import "CategoryRouterInput.h"

@interface CategoryPresenterTests : XCTestCase

@property (nonatomic, strong) CategoryPresenter *presenter;

@property (nonatomic, strong) id mockInteractor;
@property (nonatomic, strong) id mockRouter;
@property (nonatomic, strong) id mockView;

@end

@implementation CategoryPresenterTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.presenter = [[CategoryPresenter alloc] init];

    self.mockInteractor = OCMProtocolMock(@protocol(CategoryInteractorInput));
    self.mockRouter = OCMProtocolMock(@protocol(CategoryRouterInput));
    self.mockView = OCMProtocolMock(@protocol(CategoryViewInput));

    self.presenter.interactor = self.mockInteractor;
    self.presenter.router = self.mockRouter;
    self.presenter.view = self.mockView;
}

- (void)tearDown {
    self.presenter = nil;

    self.mockView = nil;
    self.mockRouter = nil;
    self.mockInteractor = nil;

    [super tearDown];
}

#pragma mark - Тестирование методов CategoryModuleInput

#pragma mark - Тестирование методов CategoryViewOutput

- (void)testThatPresenterHandlesViewReadyEvent {
    // given


    // when
    [self.presenter didTriggerViewReadyEvent];

    // then
    OCMVerify([self.mockView setupInitialState]);
}

#pragma mark - Тестирование методов CategoryInteractorOutput

@end
