//
//  CategoryAssembly_Testable.h
//  Gauranga
//
//  Created by Vlad Kochergin on 08/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "CategoryAssembly.h"

@class CategoryViewController;
@class CategoryInteractor;
@class CategoryPresenter;
@class CategoryRouter;

/**
 @author Vlad Kochergin

 Extension, который делает приватные методы фабрики открытыми для тестирования. 
 */
@interface CategoryAssembly ()

- (CategoryViewController *)viewCategory;
- (CategoryPresenter *)presenterCategory;
- (CategoryInteractor *)interactorCategory;
- (CategoryRouter *)routerCategory;

@end
