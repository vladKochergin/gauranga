//
//  FavoritePresenterTests.m
//  Gauranga
//
//  Created by Vlad Kochergin on 09/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "FavoritePresenter.h"

#import "FavoriteViewInput.h"
#import "FavoriteInteractorInput.h"
#import "FavoriteRouterInput.h"

@interface FavoritePresenterTests : XCTestCase

@property (nonatomic, strong) FavoritePresenter *presenter;

@property (nonatomic, strong) id mockInteractor;
@property (nonatomic, strong) id mockRouter;
@property (nonatomic, strong) id mockView;

@end

@implementation FavoritePresenterTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.presenter = [[FavoritePresenter alloc] init];

    self.mockInteractor = OCMProtocolMock(@protocol(FavoriteInteractorInput));
    self.mockRouter = OCMProtocolMock(@protocol(FavoriteRouterInput));
    self.mockView = OCMProtocolMock(@protocol(FavoriteViewInput));

    self.presenter.interactor = self.mockInteractor;
    self.presenter.router = self.mockRouter;
    self.presenter.view = self.mockView;
}

- (void)tearDown {
    self.presenter = nil;

    self.mockView = nil;
    self.mockRouter = nil;
    self.mockInteractor = nil;

    [super tearDown];
}

#pragma mark - Тестирование методов FavoriteModuleInput

#pragma mark - Тестирование методов FavoriteViewOutput

- (void)testThatPresenterHandlesViewReadyEvent {
    // given


    // when
    [self.presenter didTriggerViewReadyEvent];

    // then
    OCMVerify([self.mockView setupInitialState]);
}

#pragma mark - Тестирование методов FavoriteInteractorOutput

@end
