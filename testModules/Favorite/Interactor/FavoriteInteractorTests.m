//
//  FavoriteInteractorTests.m
//  Gauranga
//
//  Created by Vlad Kochergin on 09/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "FavoriteInteractor.h"

#import "FavoriteInteractorOutput.h"

@interface FavoriteInteractorTests : XCTestCase

@property (nonatomic, strong) FavoriteInteractor *interactor;

@property (nonatomic, strong) id mockOutput;

@end

@implementation FavoriteInteractorTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.interactor = [[FavoriteInteractor alloc] init];

    self.mockOutput = OCMProtocolMock(@protocol(FavoriteInteractorOutput));

    self.interactor.output = self.mockOutput;
}

- (void)tearDown {
    self.interactor = nil;

    self.mockOutput = nil;

    [super tearDown];
}

#pragma mark - Тестирование методов FavoriteInteractorInput

@end
