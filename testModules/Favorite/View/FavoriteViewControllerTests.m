//
//  FavoriteViewControllerTests.m
//  Gauranga
//
//  Created by Vlad Kochergin on 09/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "FavoriteViewController.h"

#import "FavoriteViewOutput.h"

@interface FavoriteViewControllerTests : XCTestCase

@property (nonatomic, strong) FavoriteViewController *controller;

@property (nonatomic, strong) id mockOutput;

@end

@implementation FavoriteViewControllerTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.controller = [[FavoriteViewController alloc] init];

    self.mockOutput = OCMProtocolMock(@protocol(FavoriteViewOutput));

    self.controller.output = self.mockOutput;
}

- (void)tearDown {
    self.controller = nil;

    self.mockOutput = nil;

    [super tearDown];
}

#pragma mark - Тестирование жизненного цикла

- (void)testThatControllerNotifiesPresenterOnDidLoad {
	// given

	// when
	[self.controller viewDidLoad];

	// then
	OCMVerify([self.mockOutput didTriggerViewReadyEvent]);
}

#pragma mark - Тестирование методов интерфейса

#pragma mark - Тестирование методов FavoriteViewInput

@end
