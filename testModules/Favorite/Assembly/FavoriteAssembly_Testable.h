//
//  FavoriteAssembly_Testable.h
//  Gauranga
//
//  Created by Vlad Kochergin on 09/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "FavoriteAssembly.h"

@class FavoriteViewController;
@class FavoriteInteractor;
@class FavoritePresenter;
@class FavoriteRouter;

/**
 @author Vlad Kochergin

 Extension, который делает приватные методы фабрики открытыми для тестирования. 
 */
@interface FavoriteAssembly ()

- (FavoriteViewController *)viewFavorite;
- (FavoritePresenter *)presenterFavorite;
- (FavoriteInteractor *)interactorFavorite;
- (FavoriteRouter *)routerFavorite;

@end
