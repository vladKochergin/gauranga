//
//  FullScreenPresenterTests.m
//  Gauranga
//
//  Created by Vlad Kochergin on 16/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "FullScreenPresenter.h"

#import "FullScreenViewInput.h"
#import "FullScreenInteractorInput.h"
#import "FullScreenRouterInput.h"

@interface FullScreenPresenterTests : XCTestCase

@property (nonatomic, strong) FullScreenPresenter *presenter;

@property (nonatomic, strong) id mockInteractor;
@property (nonatomic, strong) id mockRouter;
@property (nonatomic, strong) id mockView;

@end

@implementation FullScreenPresenterTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.presenter = [[FullScreenPresenter alloc] init];

    self.mockInteractor = OCMProtocolMock(@protocol(FullScreenInteractorInput));
    self.mockRouter = OCMProtocolMock(@protocol(FullScreenRouterInput));
    self.mockView = OCMProtocolMock(@protocol(FullScreenViewInput));

    self.presenter.interactor = self.mockInteractor;
    self.presenter.router = self.mockRouter;
    self.presenter.view = self.mockView;
}

- (void)tearDown {
    self.presenter = nil;

    self.mockView = nil;
    self.mockRouter = nil;
    self.mockInteractor = nil;

    [super tearDown];
}

#pragma mark - Тестирование методов FullScreenModuleInput

#pragma mark - Тестирование методов FullScreenViewOutput

- (void)testThatPresenterHandlesViewReadyEvent {
    // given


    // when
    [self.presenter didTriggerViewReadyEvent];

    // then
    OCMVerify([self.mockView setupInitialState]);
}

#pragma mark - Тестирование методов FullScreenInteractorOutput

@end
