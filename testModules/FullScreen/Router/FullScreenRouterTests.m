//
//  FullScreenRouterTests.m
//  Gauranga
//
//  Created by Vlad Kochergin on 16/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "FullScreenRouter.h"

@interface FullScreenRouterTests : XCTestCase

@property (nonatomic, strong) FullScreenRouter *router;

@end

@implementation FullScreenRouterTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.router = [[FullScreenRouter alloc] init];
}

- (void)tearDown {
    self.router = nil;

    [super tearDown];
}

@end
