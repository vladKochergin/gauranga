//
//  FullScreenAssembly_Testable.h
//  Gauranga
//
//  Created by Vlad Kochergin on 16/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "FullScreenAssembly.h"

@class FullScreenViewController;
@class FullScreenInteractor;
@class FullScreenPresenter;
@class FullScreenRouter;

/**
 @author Vlad Kochergin

 Extension, который делает приватные методы фабрики открытыми для тестирования. 
 */
@interface FullScreenAssembly ()

- (FullScreenViewController *)viewFullScreen;
- (FullScreenPresenter *)presenterFullScreen;
- (FullScreenInteractor *)interactorFullScreen;
- (FullScreenRouter *)routerFullScreen;

@end
