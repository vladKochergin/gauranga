//
//  FullScreenViewControllerTests.m
//  Gauranga
//
//  Created by Vlad Kochergin on 16/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "FullScreenViewController.h"

#import "FullScreenViewOutput.h"

@interface FullScreenViewControllerTests : XCTestCase

@property (nonatomic, strong) FullScreenViewController *controller;

@property (nonatomic, strong) id mockOutput;

@end

@implementation FullScreenViewControllerTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.controller = [[FullScreenViewController alloc] init];

    self.mockOutput = OCMProtocolMock(@protocol(FullScreenViewOutput));

    self.controller.output = self.mockOutput;
}

- (void)tearDown {
    self.controller = nil;

    self.mockOutput = nil;

    [super tearDown];
}

#pragma mark - Тестирование жизненного цикла

- (void)testThatControllerNotifiesPresenterOnDidLoad {
	// given

	// when
	[self.controller viewDidLoad];

	// then
	OCMVerify([self.mockOutput didTriggerViewReadyEvent]);
}

#pragma mark - Тестирование методов интерфейса

#pragma mark - Тестирование методов FullScreenViewInput

@end
