//
//  FullScreenInteractorTests.m
//  Gauranga
//
//  Created by Vlad Kochergin on 16/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "FullScreenInteractor.h"

#import "FullScreenInteractorOutput.h"

@interface FullScreenInteractorTests : XCTestCase

@property (nonatomic, strong) FullScreenInteractor *interactor;

@property (nonatomic, strong) id mockOutput;

@end

@implementation FullScreenInteractorTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.interactor = [[FullScreenInteractor alloc] init];

    self.mockOutput = OCMProtocolMock(@protocol(FullScreenInteractorOutput));

    self.interactor.output = self.mockOutput;
}

- (void)tearDown {
    self.interactor = nil;

    self.mockOutput = nil;

    [super tearDown];
}

#pragma mark - Тестирование методов FullScreenInteractorInput

@end
