//
//  DialogAssembly_Testable.h
//  Gauranga
//
//  Created by Vlad Kochergin on 27/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "DialogAssembly.h"

@class DialogViewController;
@class DialogInteractor;
@class DialogPresenter;
@class DialogRouter;

/**
 @author Vlad Kochergin

 Extension, который делает приватные методы фабрики открытыми для тестирования. 
 */
@interface DialogAssembly ()

- (DialogViewController *)viewDialog;
- (DialogPresenter *)presenterDialog;
- (DialogInteractor *)interactorDialog;
- (DialogRouter *)routerDialog;

@end
