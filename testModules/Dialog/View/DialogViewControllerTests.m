//
//  DialogViewControllerTests.m
//  Gauranga
//
//  Created by Vlad Kochergin on 27/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "DialogViewController.h"

#import "DialogViewOutput.h"

@interface DialogViewControllerTests : XCTestCase

@property (nonatomic, strong) DialogViewController *controller;

@property (nonatomic, strong) id mockOutput;

@end

@implementation DialogViewControllerTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.controller = [[DialogViewController alloc] init];

    self.mockOutput = OCMProtocolMock(@protocol(DialogViewOutput));

    self.controller.output = self.mockOutput;
}

- (void)tearDown {
    self.controller = nil;

    self.mockOutput = nil;

    [super tearDown];
}

#pragma mark - Тестирование жизненного цикла

- (void)testThatControllerNotifiesPresenterOnDidLoad {
	// given

	// when
	[self.controller viewDidLoad];

	// then
	OCMVerify([self.mockOutput didTriggerViewReadyEvent]);
}

#pragma mark - Тестирование методов интерфейса

#pragma mark - Тестирование методов DialogViewInput

@end
