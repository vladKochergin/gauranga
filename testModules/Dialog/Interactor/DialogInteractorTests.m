//
//  DialogInteractorTests.m
//  Gauranga
//
//  Created by Vlad Kochergin on 27/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "DialogInteractor.h"

#import "DialogInteractorOutput.h"

@interface DialogInteractorTests : XCTestCase

@property (nonatomic, strong) DialogInteractor *interactor;

@property (nonatomic, strong) id mockOutput;

@end

@implementation DialogInteractorTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.interactor = [[DialogInteractor alloc] init];

    self.mockOutput = OCMProtocolMock(@protocol(DialogInteractorOutput));

    self.interactor.output = self.mockOutput;
}

- (void)tearDown {
    self.interactor = nil;

    self.mockOutput = nil;

    [super tearDown];
}

#pragma mark - Тестирование методов DialogInteractorInput

@end
