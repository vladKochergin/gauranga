//
//  DialogPresenterTests.m
//  Gauranga
//
//  Created by Vlad Kochergin on 27/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "DialogPresenter.h"

#import "DialogViewInput.h"
#import "DialogInteractorInput.h"
#import "DialogRouterInput.h"

@interface DialogPresenterTests : XCTestCase

@property (nonatomic, strong) DialogPresenter *presenter;

@property (nonatomic, strong) id mockInteractor;
@property (nonatomic, strong) id mockRouter;
@property (nonatomic, strong) id mockView;

@end

@implementation DialogPresenterTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.presenter = [[DialogPresenter alloc] init];

    self.mockInteractor = OCMProtocolMock(@protocol(DialogInteractorInput));
    self.mockRouter = OCMProtocolMock(@protocol(DialogRouterInput));
    self.mockView = OCMProtocolMock(@protocol(DialogViewInput));

    self.presenter.interactor = self.mockInteractor;
    self.presenter.router = self.mockRouter;
    self.presenter.view = self.mockView;
}

- (void)tearDown {
    self.presenter = nil;

    self.mockView = nil;
    self.mockRouter = nil;
    self.mockInteractor = nil;

    [super tearDown];
}

#pragma mark - Тестирование методов DialogModuleInput

#pragma mark - Тестирование методов DialogViewOutput

- (void)testThatPresenterHandlesViewReadyEvent {
    // given


    // when
    [self.presenter didTriggerViewReadyEvent];

    // then
    OCMVerify([self.mockView setupInitialState]);
}

#pragma mark - Тестирование методов DialogInteractorOutput

@end
