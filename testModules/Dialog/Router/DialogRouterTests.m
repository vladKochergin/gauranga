//
//  DialogRouterTests.m
//  Gauranga
//
//  Created by Vlad Kochergin on 27/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "DialogRouter.h"

@interface DialogRouterTests : XCTestCase

@property (nonatomic, strong) DialogRouter *router;

@end

@implementation DialogRouterTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.router = [[DialogRouter alloc] init];
}

- (void)tearDown {
    self.router = nil;

    [super tearDown];
}

@end
