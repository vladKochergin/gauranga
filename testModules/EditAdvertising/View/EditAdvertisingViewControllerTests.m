//
//  EditAdvertisingViewControllerTests.m
//  Gauranga
//
//  Created by egor on 04/05/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "EditAdvertisingViewController.h"

#import "EditAdvertisingViewOutput.h"

@interface EditAdvertisingViewControllerTests : XCTestCase

@property (nonatomic, strong) EditAdvertisingViewController *controller;

@property (nonatomic, strong) id mockOutput;

@end

@implementation EditAdvertisingViewControllerTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.controller = [[EditAdvertisingViewController alloc] init];

    self.mockOutput = OCMProtocolMock(@protocol(EditAdvertisingViewOutput));

    self.controller.output = self.mockOutput;
}

- (void)tearDown {
    self.controller = nil;

    self.mockOutput = nil;

    [super tearDown];
}

#pragma mark - Тестирование жизненного цикла

- (void)testThatControllerNotifiesPresenterOnDidLoad {
	// given

	// when
	[self.controller viewDidLoad];

	// then
	OCMVerify([self.mockOutput didTriggerViewReadyEvent]);
}

#pragma mark - Тестирование методов интерфейса

#pragma mark - Тестирование методов EditAdvertisingViewInput

@end
