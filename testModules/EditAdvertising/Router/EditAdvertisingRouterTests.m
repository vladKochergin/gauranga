//
//  EditAdvertisingRouterTests.m
//  Gauranga
//
//  Created by egor on 04/05/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "EditAdvertisingRouter.h"

@interface EditAdvertisingRouterTests : XCTestCase

@property (nonatomic, strong) EditAdvertisingRouter *router;

@end

@implementation EditAdvertisingRouterTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.router = [[EditAdvertisingRouter alloc] init];
}

- (void)tearDown {
    self.router = nil;

    [super tearDown];
}

@end
