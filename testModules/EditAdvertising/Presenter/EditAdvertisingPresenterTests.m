//
//  EditAdvertisingPresenterTests.m
//  Gauranga
//
//  Created by egor on 04/05/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "EditAdvertisingPresenter.h"

#import "EditAdvertisingViewInput.h"
#import "EditAdvertisingInteractorInput.h"
#import "EditAdvertisingRouterInput.h"

@interface EditAdvertisingPresenterTests : XCTestCase

@property (nonatomic, strong) EditAdvertisingPresenter *presenter;

@property (nonatomic, strong) id mockInteractor;
@property (nonatomic, strong) id mockRouter;
@property (nonatomic, strong) id mockView;

@end

@implementation EditAdvertisingPresenterTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.presenter = [[EditAdvertisingPresenter alloc] init];

    self.mockInteractor = OCMProtocolMock(@protocol(EditAdvertisingInteractorInput));
    self.mockRouter = OCMProtocolMock(@protocol(EditAdvertisingRouterInput));
    self.mockView = OCMProtocolMock(@protocol(EditAdvertisingViewInput));

    self.presenter.interactor = self.mockInteractor;
    self.presenter.router = self.mockRouter;
    self.presenter.view = self.mockView;
}

- (void)tearDown {
    self.presenter = nil;

    self.mockView = nil;
    self.mockRouter = nil;
    self.mockInteractor = nil;

    [super tearDown];
}

#pragma mark - Тестирование методов EditAdvertisingModuleInput

#pragma mark - Тестирование методов EditAdvertisingViewOutput

- (void)testThatPresenterHandlesViewReadyEvent {
    // given


    // when
    [self.presenter didTriggerViewReadyEvent];

    // then
    OCMVerify([self.mockView setupInitialState]);
}

#pragma mark - Тестирование методов EditAdvertisingInteractorOutput

@end
