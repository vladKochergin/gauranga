//
//  EditAdvertisingInteractorTests.m
//  Gauranga
//
//  Created by egor on 04/05/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "EditAdvertisingInteractor.h"

#import "EditAdvertisingInteractorOutput.h"

@interface EditAdvertisingInteractorTests : XCTestCase

@property (nonatomic, strong) EditAdvertisingInteractor *interactor;

@property (nonatomic, strong) id mockOutput;

@end

@implementation EditAdvertisingInteractorTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.interactor = [[EditAdvertisingInteractor alloc] init];

    self.mockOutput = OCMProtocolMock(@protocol(EditAdvertisingInteractorOutput));

    self.interactor.output = self.mockOutput;
}

- (void)tearDown {
    self.interactor = nil;

    self.mockOutput = nil;

    [super tearDown];
}

#pragma mark - Тестирование методов EditAdvertisingInteractorInput

@end
