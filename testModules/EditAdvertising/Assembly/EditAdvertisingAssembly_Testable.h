//
//  EditAdvertisingAssembly_Testable.h
//  Gauranga
//
//  Created by egor on 04/05/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "EditAdvertisingAssembly.h"

@class EditAdvertisingViewController;
@class EditAdvertisingInteractor;
@class EditAdvertisingPresenter;
@class EditAdvertisingRouter;

/**
 @author egor

 Extension, который делает приватные методы фабрики открытыми для тестирования. 
 */
@interface EditAdvertisingAssembly ()

- (EditAdvertisingViewController *)viewEditAdvertising;
- (EditAdvertisingPresenter *)presenterEditAdvertising;
- (EditAdvertisingInteractor *)interactorEditAdvertising;
- (EditAdvertisingRouter *)routerEditAdvertising;

@end
