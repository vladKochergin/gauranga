//
//  TableViewForCatalogInteractorTests.m
//  Gauranga
//
//  Created by Vlad Kochergin on 01/03/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "TableViewForCatalogInteractor.h"

#import "TableViewForCatalogInteractorOutput.h"

@interface TableViewForCatalogInteractorTests : XCTestCase

@property (nonatomic, strong) TableViewForCatalogInteractor *interactor;

@property (nonatomic, strong) id mockOutput;

@end

@implementation TableViewForCatalogInteractorTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.interactor = [[TableViewForCatalogInteractor alloc] init];

    self.mockOutput = OCMProtocolMock(@protocol(TableViewForCatalogInteractorOutput));

    self.interactor.output = self.mockOutput;
}

- (void)tearDown {
    self.interactor = nil;

    self.mockOutput = nil;

    [super tearDown];
}

#pragma mark - Тестирование методов TableViewForCatalogInteractorInput

@end
