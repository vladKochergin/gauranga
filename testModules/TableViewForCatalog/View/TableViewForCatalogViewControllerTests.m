//
//  TableViewForCatalogViewControllerTests.m
//  Gauranga
//
//  Created by Vlad Kochergin on 01/03/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "TableViewForCatalogViewController.h"

#import "TableViewForCatalogViewOutput.h"

@interface TableViewForCatalogViewControllerTests : XCTestCase

@property (nonatomic, strong) TableViewForCatalogViewController *controller;

@property (nonatomic, strong) id mockOutput;

@end

@implementation TableViewForCatalogViewControllerTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.controller = [[TableViewForCatalogViewController alloc] init];

    self.mockOutput = OCMProtocolMock(@protocol(TableViewForCatalogViewOutput));

    self.controller.output = self.mockOutput;
}

- (void)tearDown {
    self.controller = nil;

    self.mockOutput = nil;

    [super tearDown];
}

#pragma mark - Тестирование жизненного цикла

- (void)testThatControllerNotifiesPresenterOnDidLoad {
	// given

	// when
	[self.controller viewDidLoad];

	// then
	OCMVerify([self.mockOutput didTriggerViewReadyEvent]);
}

#pragma mark - Тестирование методов интерфейса

#pragma mark - Тестирование методов TableViewForCatalogViewInput

@end
