//
//  TableViewForCatalogPresenterTests.m
//  Gauranga
//
//  Created by Vlad Kochergin on 01/03/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "TableViewForCatalogPresenter.h"

#import "TableViewForCatalogViewInput.h"
#import "TableViewForCatalogInteractorInput.h"
#import "TableViewForCatalogRouterInput.h"

@interface TableViewForCatalogPresenterTests : XCTestCase

@property (nonatomic, strong) TableViewForCatalogPresenter *presenter;

@property (nonatomic, strong) id mockInteractor;
@property (nonatomic, strong) id mockRouter;
@property (nonatomic, strong) id mockView;

@end

@implementation TableViewForCatalogPresenterTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.presenter = [[TableViewForCatalogPresenter alloc] init];

    self.mockInteractor = OCMProtocolMock(@protocol(TableViewForCatalogInteractorInput));
    self.mockRouter = OCMProtocolMock(@protocol(TableViewForCatalogRouterInput));
    self.mockView = OCMProtocolMock(@protocol(TableViewForCatalogViewInput));

    self.presenter.interactor = self.mockInteractor;
    self.presenter.router = self.mockRouter;
    self.presenter.view = self.mockView;
}

- (void)tearDown {
    self.presenter = nil;

    self.mockView = nil;
    self.mockRouter = nil;
    self.mockInteractor = nil;

    [super tearDown];
}

#pragma mark - Тестирование методов TableViewForCatalogModuleInput

#pragma mark - Тестирование методов TableViewForCatalogViewOutput

- (void)testThatPresenterHandlesViewReadyEvent {
    // given


    // when
    [self.presenter didTriggerViewReadyEvent];

    // then
    OCMVerify([self.mockView setupInitialState]);
}

#pragma mark - Тестирование методов TableViewForCatalogInteractorOutput

@end
