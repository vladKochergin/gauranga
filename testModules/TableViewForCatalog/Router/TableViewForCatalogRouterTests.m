//
//  TableViewForCatalogRouterTests.m
//  Gauranga
//
//  Created by Vlad Kochergin on 01/03/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "TableViewForCatalogRouter.h"

@interface TableViewForCatalogRouterTests : XCTestCase

@property (nonatomic, strong) TableViewForCatalogRouter *router;

@end

@implementation TableViewForCatalogRouterTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.router = [[TableViewForCatalogRouter alloc] init];
}

- (void)tearDown {
    self.router = nil;

    [super tearDown];
}

@end
