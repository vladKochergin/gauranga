//
//  TableViewForCatalogAssembly_Testable.h
//  Gauranga
//
//  Created by Vlad Kochergin on 01/03/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "TableViewForCatalogAssembly.h"

@class TableViewForCatalogViewController;
@class TableViewForCatalogInteractor;
@class TableViewForCatalogPresenter;
@class TableViewForCatalogRouter;

/**
 @author Vlad Kochergin

 Extension, который делает приватные методы фабрики открытыми для тестирования. 
 */
@interface TableViewForCatalogAssembly ()

- (TableViewForCatalogViewController *)viewTableViewForCatalog;
- (TableViewForCatalogPresenter *)presenterTableViewForCatalog;
- (TableViewForCatalogInteractor *)interactorTableViewForCatalog;
- (TableViewForCatalogRouter *)routerTableViewForCatalog;

@end
