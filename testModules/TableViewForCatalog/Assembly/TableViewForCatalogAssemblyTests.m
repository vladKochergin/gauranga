//
//  TableViewForCatalogAssemblyTests.m
//  Gauranga
//
//  Created by Vlad Kochergin on 01/03/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <RamblerTyphoonUtils/AssemblyTesting.h>
#import <Typhoon/Typhoon.h>

#import "TableViewForCatalogAssembly.h"
#import "TableViewForCatalogAssembly_Testable.h"

#import "TableViewForCatalogViewController.h"
#import "TableViewForCatalogPresenter.h"
#import "TableViewForCatalogInteractor.h"
#import "TableViewForCatalogRouter.h"

@interface TableViewForCatalogAssemblyTests : RamblerTyphoonAssemblyTests

@property (nonatomic, strong) TableViewForCatalogAssembly *assembly;

@end

@implementation TableViewForCatalogAssemblyTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.assembly = [[TableViewForCatalogAssembly alloc] init];
    [self.assembly activate];
}

- (void)tearDown {
    self.assembly = nil;

    [super tearDown];
}

#pragma mark - Тестирование создания элементов модуля

- (void)testThatAssemblyCreatesViewController {
    // given
    Class targetClass = [TableViewForCatalogViewController class];
    NSArray *protocols = @[
                           @protocol(TableViewForCatalogViewInput)
                           ];
    NSArray *dependencies = @[
                              RamblerSelector(output)
                              ];
    RamblerTyphoonAssemblyTestsTypeDescriptor *descriptor = [RamblerTyphoonAssemblyTestsTypeDescriptor descriptorWithClass:targetClass
                                                                                                              andProtocols:protocols];

    // when
    id result = [self.assembly viewTableViewForCatalog];

    // then
    [self verifyTargetDependency:result
                  withDescriptor:descriptor
                    dependencies:dependencies];
}

- (void)testThatAssemblyCreatesPresenter {
    // given
    Class targetClass = [TableViewForCatalogPresenter class];
    NSArray *protocols = @[
                           @protocol(TableViewForCatalogModuleInput),
                           @protocol(TableViewForCatalogViewOutput),
                           @protocol(TableViewForCatalogInteractorOutput)
                           ];
    NSArray *dependencies = @[
                              RamblerSelector(interactor),
                              RamblerSelector(view),
                              RamblerSelector(router)
                              ];
    RamblerTyphoonAssemblyTestsTypeDescriptor *descriptor = [RamblerTyphoonAssemblyTestsTypeDescriptor descriptorWithClass:targetClass
                                                                                                              andProtocols:protocols];

    // when
    id result = [self.assembly presenterTableViewForCatalog];

    // then
    [self verifyTargetDependency:result
                  withDescriptor:descriptor
                    dependencies:dependencies];
}

- (void)testThatAssemblyCreatesInteractor {
    // given
    Class targetClass = [TableViewForCatalogInteractor class];
    NSArray *protocols = @[
                           @protocol(TableViewForCatalogInteractorInput)
                           ];
    NSArray *dependencies = @[
                              RamblerSelector(output)
                              ];
    RamblerTyphoonAssemblyTestsTypeDescriptor *descriptor = [RamblerTyphoonAssemblyTestsTypeDescriptor descriptorWithClass:targetClass
                                                                                                              andProtocols:protocols];
													      
    // when
    id result = [self.assembly interactorTableViewForCatalog];

    // then
    [self verifyTargetDependency:result
                  withDescriptor:descriptor
                    dependencies:dependencies];
}

- (void)testThatAssemblyCreatesRouter {
    // given
    Class targetClass = [TableViewForCatalogRouter class];
    NSArray *protocols = @[
                           @protocol(TableViewForCatalogRouterInput)
                           ];
    NSArray *dependencies = @[
                              RamblerSelector(transitionHandler)
                              ];
    RamblerTyphoonAssemblyTestsTypeDescriptor *descriptor = [RamblerTyphoonAssemblyTestsTypeDescriptor descriptorWithClass:targetClass
                                                                                                              andProtocols:protocols];

    // when
    id result = [self.assembly routerTableViewForCatalog];

    // then
    [self verifyTargetDependency:result
                  withDescriptor:descriptor
                    dependencies:dependencies];
}

@end
