//
//  SettingAssembly.m
//  Gauranga
//
//  Created by Vlad Kochergin on 09/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "SettingAssembly.h"

#import "SettingViewController.h"
#import "SettingInteractor.h"
#import "SettingPresenter.h"
#import "SettingRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation SettingAssembly

- (SettingViewController *)viewSetting {
    return [TyphoonDefinition withClass:[SettingViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSetting]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterSetting]];
                          }];
}

- (SettingInteractor *)interactorSetting {
    return [TyphoonDefinition withClass:[SettingInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSetting]];
                          }];
}

- (SettingPresenter *)presenterSetting{
    return [TyphoonDefinition withClass:[SettingPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewSetting]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorSetting]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerSetting]];
                          }];
}

- (SettingRouter *)routerSetting{
    return [TyphoonDefinition withClass:[SettingRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewSetting]];
                          }];
}

@end
