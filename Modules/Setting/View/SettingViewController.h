//
//  SettingViewController.h
//  Gauranga
//
//  Created by Vlad Kochergin on 09/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SettingViewInput.h"

@protocol SettingViewOutput;

@interface SettingViewController : UITableViewController <SettingViewInput>

@property (nonatomic, strong) id<SettingViewOutput> output;

@end
