//
//  SettingViewController.m
//  Gauranga
//
//  Created by Vlad Kochergin on 09/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "SettingViewController.h"

#import <AVFoundation/AVFoundation.h>
#import <Photos/Photos.h>

#import "SettingViewOutput.h"
#import "UserModel.h"
#import "CurrencyModel.h"
#import "DataManager.h"

#import "AKNumericFormatter.h"
#import "UITextField+AKNumericFormatter.h"
#import "NSString+AKNumericFormatter.h"


@interface SettingViewController()<UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UIButton *avatarButton;
@property (weak, nonatomic) IBOutlet UITextField *userName;
@property (weak, nonatomic) IBOutlet UITextField *userPhone;
@property (weak, nonatomic) IBOutlet UILabel *exitLabel;
@property (weak, nonatomic) IBOutlet UILabel *balanceLabel;

@property (strong, nonatomic) NSData *avatarImage;
@property (strong, nonatomic) UIImagePickerController *imagePickerController;

@property (weak, nonatomic) IBOutlet UIButton *changeButton;

@end

@implementation SettingViewController

#pragma mark - ProgressHUD

- (void)hideProgressHUD{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];

	[self.output didTriggerViewReadyEvent];
}

#pragma mark - Методы SettingViewInput

- (void)setupInitialState {
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(signOutAction:)];
    [tapGestureRecognizer setNumberOfTapsRequired:1];
    [self.exitLabel addGestureRecognizer:tapGestureRecognizer];
    [self.changeButton setBackgroundColor:[UIColor mainColor]];
    self.userPhone.numericFormatter = [AKNumericFormatter formatterWithMask:@"(***) ***-****"
                                                            placeholderCharacter:'*'];
    
    self.navigationItem.title = NSLocalizedString(@"navProfile", @"Russian: Профиль");
    dispatch_async(dispatch_get_main_queue(), ^{
        [self createImagePickerController];
    });
    
    [self.avatarButton.layer setCornerRadius:CGRectGetWidth(self.avatarButton.frame)/2];
    [self.avatarButton.layer setMasksToBounds:YES];
    
    [self setup];
}

- (void)setup {
    UserModel *user = [[DataManager sharedInstance] getUserProfile];
    if(user){
        self.userName.text = user.name;
        self.userPhone.text = user.phoneNumber;
        NSDictionary *currency = [[DataManager sharedInstance] getCurrency];
        CurrencyModel *model = currency[user.currency.stringValue];
        self.balanceLabel.text = [NSString stringWithFormat:@"%@ %@",@(user.balance).stringValue, model.titleShort];
        if(user.avatar.length) {
            UIImageView *imageAva = [UIImageView new];
            [imageAva sd_setImageWithURL:[NSURL URLWithString:user.avatar] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                [self.avatarButton setImage:image forState:UIControlStateNormal];
            }];
        }
    }
}

- (IBAction)changeProfile:(UIButton *)sender {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.avatarImage = self.avatarImage ? self.avatarImage : UIImageJPEGRepresentation(self.avatarButton.imageView.image, 0.6);
    [self.output setupChangeWith:self.userName.text phone:self.userPhone.text imagDate:self.avatarImage];
}

- (void)signOutAction:(UITapGestureRecognizer *)gesture {
    if([[WebService sharedService] checkToken]){
        [[DataManager sharedInstance] delUserProfile];
        [[WebService sharedService] delAToken];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - ImagePickerView

- (void)createImagePickerController {
    _imagePickerController = [[UIImagePickerController alloc] init];
    [_imagePickerController setDelegate:self];
}

- (void)fromGallery{
    _imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
    
    if (status == PHAuthorizationStatusAuthorized) {
        [self presentImagePickerVC];
    }
    else if (status == PHAuthorizationStatusDenied) {
        [KVAlert alertWithTitle:@"Приложение не имеет доступа к Вашей галерее" message:@"Вы можете включить доступ в настройках приватности" buttons:@[@"Настройки"] preferredStyle:UIAlertControllerStyleAlert completion:^(NSUInteger buttonPresed) {
            if (!buttonPresed) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
            }
        }];
    }
    else if (status == PHAuthorizationStatusNotDetermined) {
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            
            if (status == PHAuthorizationStatusAuthorized) {
                [self presentImagePickerVC];
            }
            else {
            }
        }];
    }
    else if (status == PHAuthorizationStatusRestricted) {
    }
}

- (void)fromCamera {
    _imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
        
        if(status == AVAuthorizationStatusAuthorized) {
            [self presentImagePickerVC];
        } else if(status == AVAuthorizationStatusDenied){
            [KVAlert alertWithTitle:@"Приложение не имеет доступа к Вашей камере" message:@"Вы можете включить доступ в настройках приватности" buttons:@[@"Настройки"] preferredStyle:UIAlertControllerStyleAlert completion:^(NSUInteger buttonPresed) {
                if (!buttonPresed) {
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                }
            }];
        } else if(status == AVAuthorizationStatusRestricted){
        } else if(status == AVAuthorizationStatusNotDetermined){
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                if(granted){
                    [self presentImagePickerVC];
                    return;
                }
                NSLog(@"Not granted access");
            }];
        }
    }
}

- (void)presentImagePickerVC {
    [self presentViewController:_imagePickerController animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
    NSData *imageData = UIImageJPEGRepresentation(image, 0.6);
    if (image.size.width/image.size.height > 3 && imageData.length > 1000000) {
        [KVAlert alertWithTitle:@"Ошибка" message:@"Недопустимый формат изображения" preferredStyle:UIAlertControllerStyleAlert completion:nil];
        return;
    }
    self.avatarImage = imageData;
    [picker dismissViewControllerAnimated:YES completion:^{
        [self.avatarButton setImage:[UIImage imageWithData:self.avatarImage] forState:UIControlStateNormal];
    }];
}

- (IBAction)addImage{
    UIAlertController *view = [UIAlertController alertControllerWithTitle:nil
                                                                  message:nil
                                                           preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *firstSection = [UIAlertAction actionWithTitle:NSLocalizedString(@"camera", @"Russian: Камера")
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action) {
                                                             if (([UIImagePickerController isSourceTypeAvailable:
                                                                   UIImagePickerControllerSourceTypeCamera] == NO)) {
                                                                 NSLog(@"Камера");
                                                             } else {
                                                                 [self fromCamera];
                                                             }
                                                             
                                                             [view dismissViewControllerAnimated:YES completion:nil];
                                                         }];
    UIAlertAction *secondSection = [UIAlertAction actionWithTitle:NSLocalizedString(@"Gal", @"Russian: Фотопленка")
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              
                                                              [self fromGallery];
                                                              [view dismissViewControllerAnimated:YES completion:nil];
                                                          }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"Russian: Отмена")
                                                     style:UIAlertActionStyleCancel
                                                   handler:^(UIAlertAction * action) {
                                                       
                                                       [view dismissViewControllerAnimated:YES completion:nil];
                                                   }];
    
    [view addAction:cancel];
    [view addAction:firstSection];
    [view addAction:secondSection];
    [self presentViewController:view animated:YES completion:nil];
}

@end
