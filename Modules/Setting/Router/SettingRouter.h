//
//  SettingRouter.h
//  Gauranga
//
//  Created by Vlad Kochergin on 09/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "SettingRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface SettingRouter : NSObject <SettingRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
