//
//  SettingPresenter.h
//  Gauranga
//
//  Created by Vlad Kochergin on 09/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "SettingViewOutput.h"
#import "SettingInteractorOutput.h"
#import "SettingModuleInput.h"

@protocol SettingViewInput;
@protocol SettingInteractorInput;
@protocol SettingRouterInput;

@interface SettingPresenter : NSObject <SettingModuleInput, SettingViewOutput, SettingInteractorOutput>

@property (nonatomic, weak) id<SettingViewInput> view;
@property (nonatomic, strong) id<SettingInteractorInput> interactor;
@property (nonatomic, strong) id<SettingRouterInput> router;

@end
