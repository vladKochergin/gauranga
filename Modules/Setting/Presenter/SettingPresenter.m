//
//  SettingPresenter.m
//  Gauranga
//
//  Created by Vlad Kochergin on 09/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "SettingPresenter.h"

#import "SettingViewInput.h"
#import "SettingInteractorInput.h"
#import "SettingRouterInput.h"

@implementation SettingPresenter

#pragma mark - Методы SettingModuleInput

- (void)configureModule {
    // Стартовая конфигурация модуля, не привязанная к состоянию view
}

#pragma mark - Методы SettingViewOutput

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
}

- (void)setupChangeWith:(NSString *)name phone:(NSString *)phone imagDate:(NSData *)imageData{
    [self.interactor setupChangeWith:name phone:phone imagDate:imageData];
}

#pragma mark - Методы SettingInteractorOutput

- (void)hideProgressHUD{
    [self.view hideProgressHUD];
}

@end
