//
//  SettingInteractor.h
//  Gauranga
//
//  Created by Vlad Kochergin on 09/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "SettingInteractorInput.h"

@protocol SettingInteractorOutput;

@interface SettingInteractor : NSObject <SettingInteractorInput>

@property (nonatomic, weak) id<SettingInteractorOutput> output;

@end
