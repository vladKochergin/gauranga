//
//  SettingInteractorInput.h
//  Gauranga
//
//  Created by Vlad Kochergin on 09/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SettingInteractorInput <NSObject>

- (void)setupChangeWith:(NSString *)name phone:(NSString *)phone imagDate:(NSData *)imageData;

@end
