//
//  SettingInteractor.m
//  Gauranga
//
//  Created by Vlad Kochergin on 09/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "SettingInteractor.h"

#import "SettingInteractorOutput.h"
#import "RequestMapManager.h"

@implementation SettingInteractor

#pragma mark - Методы SettingInteractorInput

- (void)setupChangeWith:(NSString *)name phone:(NSString *)phone imagDate:(NSData *)imageData{
    [RequestMapManager editProfileWithName:name phone:phone image:[self encode:imageData] completion:^(NSArray *array, NSDictionary *dictionary, id object) {
        [self.output hideProgressHUD];
        [KVAlert alertWithTitle:NSLocalizedString(@"changeProfile", @"Russian: Профиль изменен") message:nil preferredStyle:UIAlertControllerStyleAlert completion:nil];
    }];
}

#pragma mark - Encode 64base

#define ArrayLength(x) (sizeof(x)/sizeof(*(x)))

static char encodingTable[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

- (NSString*) encode:(const uint8_t*) input length:(NSInteger) length {
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    for (NSInteger i = 0; i < length; i += 3) {
        NSInteger value = 0;
        for (NSInteger j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger index = (i / 3) * 4;
        output[index + 0] =                    encodingTable[(value >> 18) & 0x3F];
        output[index + 1] =                    encodingTable[(value >> 12) & 0x3F];
        output[index + 2] = (i + 1) < length ? encodingTable[(value >> 6)  & 0x3F] : '=';
        output[index + 3] = (i + 2) < length ? encodingTable[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:data
                                 encoding:NSASCIIStringEncoding];
}

- (NSString*) encode:(NSData*) rawBytes {
    return [self encode:(const uint8_t*) rawBytes.bytes length:rawBytes.length];
}

@end
