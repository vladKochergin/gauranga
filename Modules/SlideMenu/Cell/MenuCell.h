//
//  MenuCell.h
//  Gauranga
//
//  Created by yvp on 2/7/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UIButton *iconImage;

@end
