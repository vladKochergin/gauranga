//
//  MenuWithProfileCell.m
//  Gauranga
//
//  Created by yvp on 2/7/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import "MenuWithProfileCell.h"

@implementation MenuWithProfileCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self setupColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Additions

- (void)setupColor{
    [self.signInOrUpButton setBackgroundColor:[UIColor mainColor]];
}

@end
