//
//  SlideLeftMenuTVC.m
//  jobLocal
//
//  Created by PInta on 6/8/16.
//  Copyright © 2016 PInta. All rights reserved.
//

#import "SlideMenu.h"

#import "SlideNavigationContorllerAnimatorFade.h"
#import "SlideNavigationContorllerAnimatorSlide.h"
#import "SlideNavigationContorllerAnimatorScale.h"
#import "SlideNavigationContorllerAnimatorScaleAndFade.h"
#import "SlideNavigationContorllerAnimatorSlideAndFade.h"

#import <SDWebImage/UIImageView+WebCache.h>
#import "MenuCell.h"
#import "MenuWithProfileCell.h"

#import "UserModel.h"
#import "DataManager.h"

#define CELL_IDENTIFIER @"leftMenuCellId"
#define USER_CELL_IDENTIFIER @"leftMenuUserCellId"
#define GREEN_COLOR [UIColor colorWithRed:95/255.0 green:190/255.0 blue:16/255.0 alpha:1.0] //[UIColor colorWithRed:123/255.0 green:186/255.0 blue:38/255.0 alpha:1.0]
#define GRAY_COLOR [UIColor colorWithRed:107/255.0 green:107/255.0 blue:107/255.0 alpha:1.0]

@interface SlideMenu ()<UITableViewDelegate, UITableViewDataSource, SlideNavigationControllerDelegate>
{
    NSInteger currentSection;
}

@property (nonatomic, strong) NSArray *dataSource;
@property (nonatomic, strong) NSArray *imageArray;
@property (nonatomic, strong) NSArray *imageNonActiveArray;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) UIButton *avatarButton;
@property (strong, nonatomic) UIButton *signInButton;

@property (strong, nonatomic) UIImageView *loadImageView;

@property (strong, nonatomic) UIViewController *catalogViewController;
@property (strong, nonatomic) UIViewController *favoriteViewController;

@end

@implementation SlideMenu

#pragma Setters/Getters

-(NSArray *)getMenuImage{
    return @[@"isNotFavoriteStar", @"searchMenu", @"plus"/*, @"advertising"*/, @"plus"];
}

-(NSArray *)getMenuName{
    return @[
             @"ЛИЧНЫЙ КАБИНЕТ",
             NSLocalizedString(@"adv",@"Russian: ПРОСМОТР ОБЪЯВЛЕНИЙ"),
             // NSLocalizedString(@"myFavorite",@"Russian: ИЗБРАННОЕ"),
             // NSLocalizedString(@"myAdv",@"Russian: МОИ ОБЪЯВЛЕНИЯ"),
             // @"ЧАТ",
             NSLocalizedString(@"addAdv",@"Russian: ПОДАТЬ ОБЪЯВЛЕНИЕ")
             ];
}

#pragma mark viewDidLoad

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"SlideNavigationControllerDidOpen" object:nil];
}

- (void)viewDidLoad{
    [super viewDidLoad];
    
    currentSection = 1;
    [self setupView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadAvatar)
                                                 name:@"SlideNavigationControllerDidOpen"
                                               object:nil];
    [self.signInButton setBackgroundColor:[UIColor mainColor]];
}

- (void)loadAvatar{
    
    if([[WebService sharedService] checkToken]){
        [self.signInButton setTitle:NSLocalizedString(@"exit",@"Russian: ВЫЙТИ") forState:UIControlStateNormal];
    }else{
        [self.signInButton setTitle:NSLocalizedString(@"signUpIn",@"Russian: ВОЙТИ / РЕГИСТРАЦИЯ ") forState:UIControlStateNormal];
    }
    
    [self.avatarButton setImage:[UIImage imageNamed:@"avatarPlaceholder"] forState:UIControlStateNormal];
    self.loadImageView = [UIImageView new];
    UserModel *user = [[DataManager sharedInstance] getUserProfile];
    if(user){
        if(user.avatar.length){
            [self.loadImageView sd_setImageWithURL:[NSURL URLWithString:user.avatar] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                if(image)
                    [self.avatarButton setImage:image forState:UIControlStateNormal];
            }];
        }
    }
}

- (void)setupView{
    [[SlideNavigationController sharedInstance] setEnableShadow:YES];
}

#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    
    if(!indexPath.row){
        MenuWithProfileCell *cells = [tableView dequeueReusableCellWithIdentifier:USER_CELL_IDENTIFIER];
        [cells.signInOrUpButton setTitle:NSLocalizedString(@"signUpIn",@"Russian: ВОЙТИ / РЕГИСТРАЦИЯ ") forState:UIControlStateNormal];
        [cells.avatarButton.layer setCornerRadius:cells.avatarButton.frame.size.height/2];
        [cells.avatarButton.layer setMasksToBounds:YES];
        self.avatarButton = cells.avatarButton;
        self.signInButton = cells.signInOrUpButton;
        if([[WebService sharedService] checkToken]){
            [cells.signInOrUpButton setTitle:NSLocalizedString(@"exit",@"Russian: ВЫЙТИ") forState:UIControlStateNormal];
        }
        
        cell = cells;
    }else{
        MenuCell *cells = (MenuCell *) [tableView dequeueReusableCellWithIdentifier:CELL_IDENTIFIER];
        cells.title.text = [[self getMenuName] objectAtIndex:indexPath.row-1];
        [cells.iconImage setImage:[UIImage imageNamed:[[self getMenuImage] objectAtIndex:indexPath.row-1]] forState:UIControlStateNormal];
        cell = cells;
    }
    return cell;
}

#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    __weak UIViewController *vc;
    switch (indexPath.section) {
        case 0:
            if (indexPath.row == 2) vc = [self.storyboard instantiateViewControllerWithIdentifier: @"catalogViewController"];
            
            if (indexPath.row == 1){
                if([[WebService sharedService] checkToken]){
                    vc = [self.storyboard instantiateViewControllerWithIdentifier: @"settingViewController"];
                }else{
                    [KVAlert alertWithTitle:@""
                                    message:NSLocalizedString(@"needAuth",@"Russian: Необходима авторизация")
                             preferredStyle:UIAlertControllerStyleAlert completion:nil];
                }
            }
            
            //            if (indexPath.row == 3){
            //                if([[WebService sharedService] checkToken]){
            //                    vc = [self.storyboard instantiateViewControllerWithIdentifier: @"myAdvertisingViewController"];
            //                }else{
            //                    [KVAlert alertWithTitle:NSLocalizedString(@"myAdv",@"Russian: МОИ ОБЪЯВЛЕНИЯ")
            //                                    message:NSLocalizedString(@"needAuth",@"Russian: Необходима авторизация")
            //                             preferredStyle:UIAlertControllerStyleAlert completion:nil];
            //                }
            //            }
            
            //            if (indexPath.row == 3){
            //                if([[WebService sharedService] checkToken]){
            //                    vc = [self.storyboard instantiateViewControllerWithIdentifier: @"dialogViewController"];
            //                }else{
            //                    [KVAlert alertWithTitle:@"ЧАТ"
            //                                    message:NSLocalizedString(@"needAuth",@"Russian: Необходима авторизация")
            //                             preferredStyle:UIAlertControllerStyleAlert completion:nil];
            //                }
            //            }
            
            if (indexPath.row == 3){
                if([[WebService sharedService] checkToken]){
                    vc = [self.storyboard instantiateViewControllerWithIdentifier: @"addAdvertising"];
                }else{
                    [KVAlert alertWithTitle:NSLocalizedString(@"addAdv",@"Russian: ПОДАТЬ ОБЪЯВЛЕНИЕ")
                                    message:NSLocalizedString(@"needAuth",@"Russian: Необходима авторизация")
                             preferredStyle:UIAlertControllerStyleAlert completion:nil];
                }
            }
            /*if (indexPath.row == 4) vc = [self.storyboard instantiateViewControllerWithIdentifier: @"usersViewControllers"];*/
            
            break;
        default:
            break;
    }
    if (vc) {
        [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController: vc
                                                                 withSlideOutAnimation:NO
                                                                         andCompletion:nil];
    }
    [[SlideNavigationController sharedInstance] closeMenuWithCompletion:nil];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat height = 90;
    if (indexPath.row != 0) { height = 50; }
    return height;
}

- (IBAction)avatarAction:(UIButton *)sender {
    if([[WebService sharedService] checkToken]){
        UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"settingViewController"];
        //[[SlideNavigationController sharedInstance] presentViewController:vc animated:YES completion:nil];
        [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController: vc
                                                                 withSlideOutAnimation:NO
                                                                         andCompletion:nil];
        [[SlideNavigationController sharedInstance] closeMenuWithCompletion:nil];
        return;
    }
    [KVAlert alertWithTitle:NSLocalizedString(@"needAuth",@"Russian: Необходима авторизация") message:nil preferredStyle:UIAlertControllerStyleAlert completion:nil];
}

- (IBAction)siginAction:(UIButton *)sender {
    if([[WebService sharedService] checkToken]) {
        [[WebService sharedService] signOutWithCompletion:^(NSError *error, NSArray *array, NSDictionary *dictionary) {
            if (error) {
                [KVAlert alertWithTitle:nil message:NSLocalizedString(@"networkError", @"Russian: Ошибка связи") preferredStyle:UIAlertControllerStyleAlert completion:nil];
            } else {
                [[DataManager sharedInstance] delUserProfile];
                [[WebService sharedService] delAToken];
                [[WebService sharedService] deleteAPNSToken];
                [self loadAvatar];
            }
        }];
        return;
    }
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"authView"];

    [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:vc
                                                             withSlideOutAnimation:NO
                                                                     andCompletion:nil];
    [[SlideNavigationController sharedInstance] closeMenuWithCompletion:nil];

}

- (void)deselectCellsAfterLogout
{
    [self.tableView reloadData];
}

@end
