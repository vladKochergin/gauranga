//
//  FavoriteInteractor.m
//  Gauranga
//
//  Created by Vlad Kochergin on 09/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "FavoriteInteractor.h"

#import "FavoriteInteractorOutput.h"
#import "RequestMapManager.h"
#import "UserModel.h"
#import "DataManager.h"
#import "CatalogModel.h"

@interface FavoriteInteractor()

@property (nonatomic) NSInteger pageNumber;
@property (nonatomic, strong) NSMutableArray *dateSource;
@property (nonatomic) BOOL endLoad;

@end

@implementation FavoriteInteractor

#pragma mark - Методы FavoriteInteractorInput

- (void)getData:(BOOL)nextPage{
    
    if(nextPage && !self.endLoad){
        self.pageNumber++;
    }else{
        self.endLoad = NO;
        self.pageNumber = 0;
        self.dateSource = [NSMutableArray new];
    }
    
    if(self.endLoad){
        [self.output hideAllLoaders];
        return;
    }
    
    UserModel *user = [[DataManager sharedInstance] getUserProfile];
    [RequestMapManager getFavoritesWithUserId:user.userId page:self.pageNumber completion:^(NSArray *array, NSDictionary *dictionary, id object) {
        if(nextPage && ![array count]){
            self.endLoad = YES;
            [self.output hideAllLoaders];
            return;
        }
        [self.dateSource addObjectsFromArray:array];
        [self.output reloadData:self.dateSource];
    }];
}

- (void)addToFavoriteWithItemId:(NSInteger)itemId action:(NSString *)action{
    [[WebService sharedService] addToFavoriteWithItemId:itemId action:action completion:^(NSError *error, NSArray *array, NSDictionary *dictionary) {
        [self removeModelWithId:itemId];
        NSLog(@"ADD TO FAVORITE");
    }];
}

- (void)removeModelWithId:(NSInteger)itemId{
    for(CatalogModel *model in self.dateSource){
        if(model.itemId == itemId){
            [self.dateSource removeObject:model];
            [self.output reloadData:self.dateSource];
            break;
        }
    }
}

@end
