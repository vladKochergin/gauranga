//
//  FavoriteInteractor.h
//  Gauranga
//
//  Created by Vlad Kochergin on 09/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "FavoriteInteractorInput.h"

@protocol FavoriteInteractorOutput;

@interface FavoriteInteractor : NSObject <FavoriteInteractorInput>

@property (nonatomic, weak) id<FavoriteInteractorOutput> output;

@end
