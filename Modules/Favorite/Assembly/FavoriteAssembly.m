//
//  FavoriteAssembly.m
//  Gauranga
//
//  Created by Vlad Kochergin on 09/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "FavoriteAssembly.h"

#import "FavoriteViewController.h"
#import "FavoriteInteractor.h"
#import "FavoritePresenter.h"
#import "FavoriteRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation FavoriteAssembly

- (FavoriteViewController *)viewFavorite {
    return [TyphoonDefinition withClass:[FavoriteViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterFavorite]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterFavorite]];
                          }];
}

- (FavoriteInteractor *)interactorFavorite {
    return [TyphoonDefinition withClass:[FavoriteInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterFavorite]];
                          }];
}

- (FavoritePresenter *)presenterFavorite{
    return [TyphoonDefinition withClass:[FavoritePresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewFavorite]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorFavorite]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerFavorite]];
                          }];
}

- (FavoriteRouter *)routerFavorite{
    return [TyphoonDefinition withClass:[FavoriteRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewFavorite]];
                          }];
}

@end
