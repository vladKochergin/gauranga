//
//  FavoriteAssembly.h
//  Gauranga
//
//  Created by Vlad Kochergin on 09/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>

/**
 @author Vlad Kochergin

 Favorite module
 */
@interface FavoriteAssembly : TyphoonAssembly <RamblerInitialAssembly>

@end
