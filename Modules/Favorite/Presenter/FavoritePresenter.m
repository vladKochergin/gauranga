//
//  FavoritePresenter.m
//  Gauranga
//
//  Created by Vlad Kochergin on 09/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "FavoritePresenter.h"

#import "FavoriteViewInput.h"
#import "FavoriteInteractorInput.h"
#import "FavoriteRouterInput.h"

@implementation FavoritePresenter

#pragma mark - Методы FavoriteModuleInput

- (void)configureModule {
    // Стартовая конфигурация модуля, не привязанная к состоянию view
}

#pragma mark - Методы FavoriteViewOutput

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
}

- (void)getData:(BOOL)nextPage{
    [self.interactor getData:nextPage];
}

- (void)addToFavoriteWithItemId:(NSInteger)itemId action:(NSString *)action{
    [self.interactor addToFavoriteWithItemId:itemId action:action];
}

- (void)showDetailAdvertising:(NSInteger)selectAdvId isFavorite:(BOOL)isFavorite{
    [self.router showDetailAdvertising:selectAdvId isFavorite:isFavorite];
}

#pragma mark - Методы FavoriteInteractorOutput

- (void)reloadData:(NSArray *)itemArray{
    [self.view reloadData:itemArray];
}

- (void)hideAllLoaders{
    [self.view hideAllLoaders];
}

@end
