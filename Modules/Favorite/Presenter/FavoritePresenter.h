//
//  FavoritePresenter.h
//  Gauranga
//
//  Created by Vlad Kochergin on 09/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "FavoriteViewOutput.h"
#import "FavoriteInteractorOutput.h"
#import "FavoriteModuleInput.h"

@protocol FavoriteViewInput;
@protocol FavoriteInteractorInput;
@protocol FavoriteRouterInput;

@interface FavoritePresenter : NSObject <FavoriteModuleInput, FavoriteViewOutput, FavoriteInteractorOutput>

@property (nonatomic, weak) id<FavoriteViewInput> view;
@property (nonatomic, strong) id<FavoriteInteractorInput> interactor;
@property (nonatomic, strong) id<FavoriteRouterInput> router;

@end
