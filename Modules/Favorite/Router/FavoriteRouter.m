//
//  FavoriteRouter.m
//  Gauranga
//
//  Created by Vlad Kochergin on 09/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "FavoriteRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

#import "DetailModuleInput.h"

@implementation FavoriteRouter

#pragma mark - Методы FavoriteRouterInput

- (void)showDetailAdvertising:(NSInteger)selectAdvId isFavorite:(BOOL)isFavorite{
    [[self.transitionHandler openModuleUsingSegue:@"detailSegue"] thenChainUsingBlock:^id<RamblerViperModuleOutput>(id<DetailModuleInput> moduleInput) {
        [moduleInput configureModuleWithAdvId:selectAdvId isFavorite:isFavorite isMyAdv:NO];
        return nil;
    }];
}

@end
