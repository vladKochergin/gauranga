//
//  FavoriteRouter.h
//  Gauranga
//
//  Created by Vlad Kochergin on 09/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "FavoriteRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface FavoriteRouter : NSObject <FavoriteRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
