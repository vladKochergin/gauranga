//
//  FavoriteViewInput.h
//  Gauranga
//
//  Created by Vlad Kochergin on 09/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FavoriteViewInput <NSObject>

/**
 @author Vlad Kochergin

 Метод настраивает начальный стейт view
 */
- (void)hideAllLoaders;
- (void)setupInitialState;
- (void)reloadData:(NSArray *)itemArray;

@end
