//
//  FavoriteViewController.h
//  Gauranga
//
//  Created by Vlad Kochergin on 09/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FavoriteViewInput.h"

@protocol FavoriteViewOutput;

@interface FavoriteViewController : UIViewController <FavoriteViewInput>

@property (nonatomic, strong) id<FavoriteViewOutput> output;

@end
