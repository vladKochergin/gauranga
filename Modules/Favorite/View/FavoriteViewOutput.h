//
//  FavoriteViewOutput.h
//  Gauranga
//
//  Created by Vlad Kochergin on 09/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FavoriteViewOutput <NSObject>

/**
 @author Vlad Kochergin

 Метод сообщает презентеру о том, что view готова к работе
 */
- (void)didTriggerViewReadyEvent;
- (void)getData:(BOOL)nextPage;
- (void)addToFavoriteWithItemId:(NSInteger)itemId action:(NSString *)action;
- (void)showDetailAdvertising:(NSInteger)selectAdvId isFavorite:(BOOL)isFavorite;

@end
