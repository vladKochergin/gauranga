//
//  FavoriteViewController.m
//  Gauranga
//
//  Created by Vlad Kochergin on 09/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "FavoriteViewController.h"

#import "FavoriteViewOutput.h"

#import "CatalogModel.h"
#import "CatalogCell.h"
#import "CityNavigationBar.h"
#import "DataManager.h"
#import "CurrencyModel.h"

@interface FavoriteViewController()<UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property(nonatomic, strong) NSMutableArray<CatalogModel *> *dataSource;
@property(nonatomic, strong) NSDictionary *currencyDic;

@property(nonatomic, strong) NSDictionary *categoryDic;
@property(nonatomic, strong) NSString *sort;
@property(nonatomic) BOOL pagLoad;

@end

@implementation FavoriteViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.output didTriggerViewReadyEvent];
}

- (void)dealloc{
    //[[SDImageCache sharedImageCache] clearMemory];
    //[[SDImageCache sharedImageCache] clearDisk];
}

#pragma mark - Методы FavoriteViewInput

- (void)setupInitialState {
    self.navigationItem.title = NSLocalizedString(@"navFavorite", @"Russian: Избранное");
    
    self.dataSource = [NSMutableArray new];
    self.currencyDic = [[DataManager sharedInstance] getCurrency];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"CatalogCell" bundle:nil] forCellReuseIdentifier:@"catalogCell"];
    [self.tableView addRefreshControlWithAction:@selector(getDataPull) view:self];
    [self.tableView showRefreshControl];
    [self getDataLoadNextPage:NO];
    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.tableView.bounds.size.width, 0.01f)];
}

#pragma mark - Data

- (void)hideAllLoaders{
    [self.tableView hiddenRefreshControl];
    [self.tableView hiddenPagingSpinner];
}

- (void)getDataPull{
    [self getDataLoadNextPage:NO];
}

- (void)getDataLoadNextPage:(BOOL)nextPage{
    self.pagLoad = nextPage;
    [self.output getData:nextPage];
}

- (void)reloadData:(NSArray *)itemArray{
    [self.tableView hiddenRefreshControl];
    self.dataSource = [itemArray mutableCopy];
    [self.tableView reloadDataWithCheck];
}

#pragma mark - UITableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.dataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CatalogCell *cell = [tableView dequeueReusableCellWithIdentifier:@"catalogCell"];
    
    CatalogModel *model = [self.dataSource objectAtIndex:indexPath.row];
    [cell setupCellWithModel:model hideStatus:YES hideFavoriteButton:NO];
    if(!cell.favoriteButton.isHidden){
        [cell.favoriteButton addTarget:self action:@selector(addToFavorite:) forControlEvents:UIControlEventTouchUpInside];
    }
    [cell.favoriteButton addTarget:self action:@selector(addToFavorite:) forControlEvents:UIControlEventTouchUpInside];
    
    if((indexPath.row == [self.dataSource count] - 2) && [self.dataSource count] > 9){
        [self getDataLoadNextPage:YES];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    CatalogModel *model = [self.dataSource objectAtIndex:indexPath.row];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.output showDetailAdvertising:model.itemId isFavorite:model.isFavorite];
}

#pragma mark - Action

- (void)addToFavorite:(UIButton *)button{
    NSString *action = @"items_add";
    
    if([button.restorationIdentifier integerValue]){
        [button setRestorationIdentifier:@"0"];
        [button setImage:[UIImage imageNamed:@"isNotFavoriteStar"] forState:UIControlStateNormal];
        action = @"items_del";
    }else{
        [button setRestorationIdentifier:@"1"];
        [button setImage:[UIImage imageNamed:@"isFavoriteStar"] forState:UIControlStateNormal];
    }
    
    [self.output addToFavoriteWithItemId:button.tag action:action];
}

//#pragma mark - SlideNavigationController
//
//- (BOOL)slideNavigationControllerShouldDisplayLeftMenu{
//    return YES;
//}

@end
