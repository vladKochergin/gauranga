//
//  CatalogPresenter.m
//  Gauranga
//
//  Created by Vlad Kochergin on 07/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "CatalogPresenter.h"

#import "CatalogViewInput.h"
#import "CatalogInteractorInput.h"
#import "CatalogRouterInput.h"
#import "FilterModel.h"

@class FilterModel;

@interface CatalogPresenter()

@property(nonatomic, strong) NSString *searchText;
@property(nonatomic, strong) FilterModel *filter;

@end

@implementation CatalogPresenter

#pragma mark - Методы CatalogModuleInput

- (void)configureModuleWithSearcText:(NSString *)text filer:(FilterModel *)filter {
    self.searchText = text;
    self.filter = filter;
}

#pragma mark - Методы CatalogViewOutput

- (void)didTriggerViewReadyEvent {
    [self.view setupInitialStateWithSearchText:self.searchText filer:self.filter];
}

- (void)getDataWithSort:(NSString *)sort
                 search:(NSString *)searchText
               category:(NSDictionary *)category
                   city:(NSDictionary *)city
            dopProperty:(NSArray *)dopProperty
         isLoadNextPage:(BOOL)nextPage {
    
    self.searchText = searchText;
    
    self.filter.category = category;
    self.filter.city = city;
    self.filter.dynamicproperty = dopProperty;
    
    [self.interactor getDataWithSort:sort search:searchText category:category city:city dopProperty:dopProperty isLoadNextPage:nextPage];
}

- (void)selectCategoryAction{
    [self.router selectCategoryView];
}

- (void)addToFavoriteWithItemId:(NSInteger)itemId action:(NSString *)action{
    [self.interactor addToFavoriteWithItemId:itemId action:action];
}

- (void)searh:(NSString *)searchText{
    [self.router searh:searchText];
}

- (void)showDetailAdvertising:(NSInteger)selectAdvId model:(id)model isFavorite:(BOOL)isFavorite{
    [self.router showDetailAdvertising:selectAdvId model:model isFavorite:isFavorite];
}

- (void)showFilter:(FilterModel *)filter{
    [self.router showFilter:filter];
}

- (void)applyFilter:(FilterModel *)filter{
    [self.router applyFilter:filter];
}

#pragma mark - Методы CatalogInteractorOutput

- (void)reloadData:(NSArray *)itemArray{
    [self.view reloadData:itemArray];
}

- (void)hideAllLoaders{
    [self.view hideAllLoaders];
}

@end
