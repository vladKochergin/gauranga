//
//  CatalogPresenter.h
//  Gauranga
//
//  Created by Vlad Kochergin on 07/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "CatalogViewOutput.h"
#import "CatalogInteractorOutput.h"
#import "CatalogModuleInput.h"

@protocol CatalogViewInput;
@protocol CatalogInteractorInput;
@protocol CatalogRouterInput;

@interface CatalogPresenter : NSObject <CatalogModuleInput, CatalogViewOutput, CatalogInteractorOutput>

@property (nonatomic, weak) id<CatalogViewInput> view;
@property (nonatomic, strong) id<CatalogInteractorInput> interactor;
@property (nonatomic, strong) id<CatalogRouterInput> router;

@end
