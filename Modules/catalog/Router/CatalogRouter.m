//
//  CatalogRouter.m
//  Gauranga
//
//  Created by Vlad Kochergin on 07/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "CatalogRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>
#import "CategoryViewController.h"

#import "CatalogPresenter.h"
#import "CategoryModuleInput.h"
#import "DetailPresenter.h"
#import "FilterViewController.h"
#import "FilterModuleInput.h"

@implementation CatalogRouter

#pragma mark - Методы CatalogRouterInput

- (UIStoryboard *)storyboard{
    return [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
}

- (RamblerViperModuleFactory *)transitionModuleFactory:(NSString *)viewName{
    RamblerViperModuleFactory *factory = [[RamblerViperModuleFactory alloc]initWithStoryboard:[self storyboard] andRestorationId:viewName];
    return factory;
}

- (void)selectCategoryView{
    RamblerViperModuleFactory *factory = [self transitionModuleFactory:@"categoryView"];
    [[self.transitionHandler openModuleUsingFactory:factory withTransitionBlock:^(id<RamblerViperModuleTransitionHandlerProtocol> sourceModuleTransitionHandler, id<RamblerViperModuleTransitionHandlerProtocol> destinationModuleTransitionHandler) {
        
        UIViewController<CategoryViewDelegate> *source = (id) sourceModuleTransitionHandler;
        CategoryViewController *destinationViewController = (id) destinationModuleTransitionHandler;
        [destinationViewController setDelegate:source];
        [source.navigationController pushViewController:destinationViewController animated:YES];
        
        
    }] thenChainUsingBlock:^id<RamblerViperModuleOutput>(id<CategoryModuleInput> moduleInput) {
        [moduleInput configureModuleWithModel:nil isAddAdvertising:NO];
        return nil;
    }];
}

- (void)searh:(NSString *)searchText{
    RamblerViperModuleFactory *factory = [self transitionModuleFactory:@"catalogViewController"];
    [[self.transitionHandler openModuleUsingFactory:factory withTransitionBlock:^(id<RamblerViperModuleTransitionHandlerProtocol> sourceModuleTransitionHandler, id<RamblerViperModuleTransitionHandlerProtocol> destinationModuleTransitionHandler) {
        
        UIViewController *source = (id) sourceModuleTransitionHandler;
        UIViewController *destinationViewController = (id) destinationModuleTransitionHandler;
        [source.navigationController pushViewController:destinationViewController animated:YES];
        
    }] thenChainUsingBlock:^id<RamblerViperModuleOutput>(id<RamblerViperModuleInput> moduleInput) {
                CatalogPresenter *viewPresenter = (CatalogPresenter *)moduleInput;
                [viewPresenter configureModuleWithSearcText:searchText filer:nil];
        return nil;
    }];
}

- (void)showDetailAdvertising:(NSInteger)selectAdvId model:(id)model isFavorite:(BOOL)isFavorite{
    
    RamblerViperModuleFactory *factory = [self transitionModuleFactory:@"detailViewController"];
    [[self.transitionHandler openModuleUsingFactory:factory withTransitionBlock:^(id<RamblerViperModuleTransitionHandlerProtocol> sourceModuleTransitionHandler, id<RamblerViperModuleTransitionHandlerProtocol> destinationModuleTransitionHandler) {
        
        UIViewController *source = (id) sourceModuleTransitionHandler;
        UIViewController *destinationViewController = (id) destinationModuleTransitionHandler;
        source.navigationItem.backBarButtonItem.title = @"Назад";

        [source.navigationController pushViewController:destinationViewController animated:YES];
        
    }] thenChainUsingBlock:^id<RamblerViperModuleOutput>(id<RamblerViperModuleInput> moduleInput) {
        [(DetailPresenter *)moduleInput configureModuleWithAdvId:selectAdvId model:model isFavorite:isFavorite isMyAdv:NO];
        return nil;
    }];
}

- (void)showFilter:(FilterModel *)filter{
    RamblerViperModuleFactory *factory = [self transitionModuleFactory:@"filterViewController"];
    [[self.transitionHandler openModuleUsingFactory:factory withTransitionBlock:^(id<RamblerViperModuleTransitionHandlerProtocol> sourceModuleTransitionHandler, id<RamblerViperModuleTransitionHandlerProtocol> destinationModuleTransitionHandler) {
        
        UIViewController<FilterDelegate> *source = (id) sourceModuleTransitionHandler;
        FilterViewController *destinationViewController = (id) destinationModuleTransitionHandler;
        [destinationViewController setDelegate:source];
        [source.navigationController presentViewController:destinationViewController animated:YES completion:nil];
        
    }] thenChainUsingBlock:^id<RamblerViperModuleOutput>(id<FilterModuleInput> moduleInput) {
        [moduleInput configureModuleWithFilterModel:filter];
        return nil;
    }];
}

- (void)applyFilter:(FilterModel *)filter{
    RamblerViperModuleFactory *factory = [self transitionModuleFactory:@"catalogViewController"];
    [[self.transitionHandler openModuleUsingFactory:factory withTransitionBlock:^(id<RamblerViperModuleTransitionHandlerProtocol> sourceModuleTransitionHandler, id<RamblerViperModuleTransitionHandlerProtocol> destinationModuleTransitionHandler) {
        
        UIViewController *source = (id) sourceModuleTransitionHandler;
        UIViewController *destinationViewController = (id) destinationModuleTransitionHandler;
        [source.navigationController pushViewController:destinationViewController animated:YES];
        
    }] thenChainUsingBlock:^id<RamblerViperModuleOutput>(id<RamblerViperModuleInput> moduleInput) {
        CatalogPresenter *viewPresenter = (CatalogPresenter *)moduleInput;
        [viewPresenter configureModuleWithSearcText:@"" filer:filter];
        return nil;
    }];
}

@end
