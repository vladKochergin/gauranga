//
//  CatalogRouter.h
//  Gauranga
//
//  Created by Vlad Kochergin on 07/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "CatalogRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface CatalogRouter : NSObject <CatalogRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
