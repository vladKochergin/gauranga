//
//  CatalogRouterInput.h
//  Gauranga
//
//  Created by Vlad Kochergin on 07/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FilterModel;

@protocol CatalogRouterInput <NSObject>

- (void)selectCategoryView;
- (void)searh:(NSString *)searchText;
- (void)showDetailAdvertising:(NSInteger)selectAdvId model:(id)model isFavorite:(BOOL)isFavorite;
- (void)showFilter:(FilterModel *)filter;
- (void)applyFilter:(FilterModel *)filter;

@end
