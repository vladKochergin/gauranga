//
//  CatalogAssembly.m
//  Gauranga
//
//  Created by Vlad Kochergin on 07/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "CatalogAssembly.h"

#import "CatalogViewController.h"
#import "CatalogInteractor.h"
#import "CatalogPresenter.h"
#import "CatalogRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation CatalogAssembly

- (CatalogViewController *)viewCatalog {
    return [TyphoonDefinition withClass:[CatalogViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterCatalog]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterCatalog]];
                          }];
}

- (CatalogInteractor *)interactorCatalog {
    return [TyphoonDefinition withClass:[CatalogInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterCatalog]];
                          }];
}

- (CatalogPresenter *)presenterCatalog{
    return [TyphoonDefinition withClass:[CatalogPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewCatalog]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorCatalog]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerCatalog]];
                          }];
}

- (CatalogRouter *)routerCatalog{
    return [TyphoonDefinition withClass:[CatalogRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewCatalog]];
                          }];
}

@end
