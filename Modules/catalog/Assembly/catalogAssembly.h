//
//  CatalogAssembly.h
//  Gauranga
//
//  Created by Vlad Kochergin on 07/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>

/**
 @author Vlad Kochergin

 Catalog module
 */
@interface CatalogAssembly : TyphoonAssembly <RamblerInitialAssembly>

@end
