//
//  CatalogInteractor.h
//  Gauranga
//
//  Created by Vlad Kochergin on 07/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "CatalogInteractorInput.h"

@protocol CatalogInteractorOutput;

@interface CatalogInteractor : NSObject <CatalogInteractorInput>

@property (nonatomic, weak) id<CatalogInteractorOutput> output;

@end
