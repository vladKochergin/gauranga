//
//  CatalogInteractorInput.h
//  Gauranga
//
//  Created by Vlad Kochergin on 07/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CatalogInteractorInput <NSObject>

- (void)getDataWithSort:(NSString *)sort
                 search:(NSString *)searchText
               category:(NSDictionary *)category
                   city:(NSDictionary *)city
            dopProperty:(NSArray *)dopProperty
         isLoadNextPage:(BOOL)nextPage;

- (void)addToFavoriteWithItemId:(NSInteger)itemId action:(NSString *)action;
//- (void)searh:(NSString *)searchText;

@end
