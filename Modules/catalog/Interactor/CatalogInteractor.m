//
//  CatalogInteractor.m
//  Gauranga
//
//  Created by Vlad Kochergin on 07/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "CatalogInteractor.h"

#import "CatalogInteractorOutput.h"

#import "RequestMapManager.h"
#import "UserModel.h"
#import "DataManager.h"
#import "CatalogModel.h"
#import "CatalogViewController.h"
#import "CurrencyModel.h"

@interface CatalogInteractor()

@property(nonatomic) NSInteger pageNumber;
@property (nonatomic, strong) NSMutableArray<CatalogModel *> *dateSource;
@property (nonatomic) BOOL endLoad;

@end

@implementation CatalogInteractor

#pragma mark - Методы CatalogInteractorInput

- (void)getDataWithSort:(NSString *)sort
                 search:(NSString *)searchText
               category:(NSDictionary *)category
                   city:(NSDictionary *)city
            dopProperty:(NSArray *)dopProperty
         isLoadNextPage:(BOOL)nextPage
{
    
    if(nextPage && !self.endLoad){
        self.pageNumber++;
    }else if(!nextPage){
        self.endLoad = NO;
        self.pageNumber = 0;
        self.dateSource = [NSMutableArray new];
    }
    
    if(self.endLoad){
        [self.output hideAllLoaders];
        return;
    }
    
    NSMutableDictionary *param = [NSMutableDictionary new];
    param = [@{
               @"page" : [NSString stringWithFormat:@"%li", (long)self.pageNumber],
               @"onpage" : @"10i",
               @"sort" : sort
               } mutableCopy];
    
    if([[category allKeys] count]){
        for (NSString *key in [category allKeys]) {
            if (![key isEqualToString:@"title"]) {
                [param setValue:[category objectForKey:key] forKey:key];
                break;
            }
        }
    }
    
    if([[city allKeys] count]){
        for (NSString *key in [city allKeys]) {
            if (![key isEqualToString:@"title"]) {
                [param setValue:[city objectForKey:key] forKey:key];
                break;
            }
        }
    }
    
    if(searchText.length){
        [param setValue:searchText forKey:@"title"];
    }
    
    if([dopProperty count]){
        [param setValue:dopProperty forKey:@"props"];
    }
    
    UserModel *user = [[DataManager sharedInstance] getUserProfile];
    
    [RequestMapManager getCatalogWithDictionaryParam:param userId:user.userId completion:^(NSArray *array, NSDictionary *dictionary, id object) {
        if(nextPage && ![array count]){
            self.endLoad = YES;
            [self.output hideAllLoaders];
            return;
        }
        [self.dateSource addObjectsFromArray:array];
        [self.output reloadData:self.dateSource];
    }];
}

- (void)addToFavoriteWithItemId:(NSInteger)itemId action:(NSString *)action{
    [[WebService sharedService] addToFavoriteWithItemId:itemId action:action completion:^(NSError *error, NSArray *array, NSDictionary *dictionary) {
        [self editModelWithId:itemId typeAction:action];
    }];
}

#pragma mark - Additional Method

- (void)editModelWithId:(NSInteger)itemId typeAction:(NSString *)typeAction{
    NSInteger index = 0;
    BOOL action = NO;
    
    if([typeAction isEqualToString:@"items_add"]){
        action = YES;
    }
    
    for(CatalogModel *model in self.dateSource){
        if(model.itemId == itemId){
            [self.dateSource objectAtIndex:index].isFavorite = action;
            [self.output reloadData:self.dateSource];
            break;
        }
        index++;
    }
}

@end
