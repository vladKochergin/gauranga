//
//  CatalogInteractorOutput.h
//  Gauranga
//
//  Created by Vlad Kochergin on 07/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CatalogInteractorOutput <NSObject>

- (void)reloadData:(NSArray *)itemArray;
//- (void)openSearchViewWithText:(NSString *)string;
- (void)hideAllLoaders;

@end
