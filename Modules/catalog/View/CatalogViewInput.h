//
//  CatalogViewInput.h
//  Gauranga
//
//  Created by Vlad Kochergin on 07/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FilterModel;

@protocol CatalogViewInput <NSObject>

/**
 @author Vlad Kochergin

 Метод настраивает начальный стейт view
 */
- (void)hideAllLoaders;
- (void)setupInitialStateWithSearchText:(NSString *)searchText filer:(FilterModel *)filter;
- (void)reloadData:(NSArray *)itemArray;

@end
