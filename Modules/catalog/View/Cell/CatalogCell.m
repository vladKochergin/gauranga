//
//  CatalogCell.m
//  Gauranga
//
//  Created by yvp on 2/7/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import "CatalogCell.h"
#import "CatalogModel.h"

@interface CatalogCell()

@property (weak, nonatomic) IBOutlet UIButton *tagButton;

@end

@implementation CatalogCell

- (void)setupCellWithModel:(CatalogModel *)model hideStatus:(BOOL)hideStatus hideFavoriteButton:(BOOL)hideFavoriteButotn{
    [self setupFavoriteButton:hideFavoriteButotn model:model];
    [self setupdStatusLabel:hideStatus model:model];
    
    self.title.text = model.title;
    [self.image sd_setImageWithURL:[NSURL URLWithString:model.imageMedium] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    self.price.text = model.priceDisplay;
    self.shortInfo.text = [NSString stringWithFormat:@"%@\n%@", model.city, model.date];
}

- (void)setupdStatusLabel:(BOOL)hideStatusLabel model:(CatalogModel *)model{
    if(hideStatusLabel){
        [self.statusLabel setHidden:YES];
        return;
    }
    [self.statusLabel setHidden:hideStatusLabel];
    switch (model.status.integerValue) {
        case 1:
            self.statusLabel.text = NSLocalizedString(@"nonActive", @"Russian: Не активировано");
            break;
        case 4:
            self.statusLabel.text = NSLocalizedString(@"expired", @"Russian: Снято с публикации");
            break;
        case 5:
            self.statusLabel.text = NSLocalizedString(@"blocked", @"Russian: Заблокированное");
            break;
        default:
            [self.statusLabel setHidden:YES];
            break;
    }
    self.statusLabel.layer.masksToBounds = YES;
    self.statusLabel.layer.cornerRadius = CGRectGetHeight(self.statusLabel.frame)/2;
}

- (void)setupFavoriteButton:(BOOL)hideFavoriteButotn model:(CatalogModel *)model{
    if(hideFavoriteButotn){
        [self.favoriteButton setHidden:YES];
        return;
    }
    [self.favoriteButton setHidden:![[WebService sharedService] checkToken]];
    [self.favoriteButton setTag:model.itemId];
    if(!self.favoriteButton.hidden){
        if(model.isFavorite){
            [self.favoriteButton setImage:[UIImage imageNamed:@"isFavoriteStar"] forState:UIControlStateNormal];
            [self.favoriteButton setRestorationIdentifier:@"1"];
        }
    }
}

- (void)prepareForReuse{
    [self.statusLabel setHidden:YES];
    [self.favoriteButton setHidden:YES];
    [self.favoriteButton setImage:[UIImage imageNamed:@"isNotFavoriteStar"] forState:UIControlStateNormal];
    [self.favoriteButton setTag:0];
    [self.favoriteButton setRestorationIdentifier:@"0"];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

#pragma mark - Additions

- (void)setupColor{
    [self.tagButton setTintColor:[UIColor mainColor]];
    [self.price setTextColor:[UIColor mainColor]];
    [self.favoriteButton setTintColor:[UIColor mainColor]];
}

@end
