//
//  CatalogCell.h
//  Gauranga
//
//  Created by yvp on 2/7/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CatalogCell : UITableViewCell

- (void)setupCellWithModel:(id)model hideStatus:(BOOL)hideStatus hideFavoriteButton:(BOOL)hideFavoriteButotn;

@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UIButton *favoriteButton;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UILabel *shortInfo;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;

@end
