//
//  CatalogViewController.m
//  Gauranga
//
//  Created by Vlad Kochergin on 07/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "CatalogViewController.h"

#import "CatalogViewOutput.h"

#import "CatalogModel.h"
#import "CatalogCell.h"
#import "CityNavigationBar.h"
#import "DataManager.h"
#import "CurrencyModel.h"
#import "FilterModel.h"

#import "SimilarDetailCollectionCell.h"

typedef enum : NSInteger {
    TableStyle,
    CollectionStyle,
} CatalogVisibleStyle;

@interface CatalogViewController()<UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchbar;
@property (weak, nonatomic) IBOutlet UIButton *categoryButton;

@property (strong, nonatomic) UIBarButtonItem *filterBarButton;
@property (strong, nonatomic) UIBarButtonItem *sortBarButton;

@property (nonatomic, strong) NSMutableArray *dataSource;

@property (nonatomic, strong) NSDictionary *categoryDic;
@property (nonatomic, strong) NSDictionary *cityDic;
@property (nonatomic, strong) NSArray *dopPropteryArray;

@property (nonatomic, strong) NSString *sort;

@property (nonatomic) BOOL isSearchView;
@property (nonatomic) CatalogVisibleStyle catalogVisibleStyle;
@property (nonatomic, strong) FilterModel *filterDataSource;
@property (nonatomic, strong) CatalogModel *selectedModel;

@end

@implementation CatalogViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.output didTriggerViewReadyEvent];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    if (self.selectedModel) {
        NSInteger i;
        for (i = 0; i < self.dataSource.count; i++) {
            CatalogModel *model = self.dataSource[i];
            if (model.itemId == self.selectedModel.itemId) {
                break;
            }
        }
        if (self.dataSource.count) {
            [self.tableView beginUpdates];
            [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            [self.tableView endUpdates];
        }
    }
}

#pragma mark - Методы CatalogViewInput

- (void)setupInitialStateWithSearchText:(NSString *)searchText filer:(FilterModel *)filter{
    self.catalogVisibleStyle = TableStyle;

    [self.tableView setHidden:NO];
    
    self.categoryDic = [NSDictionary new];
    self.cityDic = [NSDictionary new];
    self.dopPropteryArray = [NSArray new];
    self.dataSource = [NSMutableArray new];
    
    if(searchText.length || filter || self.filterDataSource){
        self.isSearchView = YES;
        if(filter){
            self.filterDataSource = filter ? filter : self.filterDataSource;
            self.categoryDic = self.filterDataSource.category;
            self.cityDic = self.filterDataSource.city;
            self.dopPropteryArray = self.filterDataSource.dynamicproperty;
        }
    }
    
    [self setupNavigationBarWithSearchText:searchText];
    
    self.filterBarButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"filterName", @"Russian: Фильтр")
                                                            style:UIBarButtonItemStylePlain
                                                           target:self
                                                           action:@selector(showFiler)];
    
    self.sortBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"sort"]
                                                          style:UIBarButtonItemStylePlain
                                                         target:self
                                                         action:@selector(sortTypeAction:)];
    
    self.navigationItem.rightBarButtonItems = @[self.sortBarButton, self.filterBarButton];
    self.sort = @"publicated_less";
    
    [self setupTableView];
    [self setupCollectionView];
}

#pragma mark - Setup

- (void)setupTableView{
    [self.tableView registerNib:[UINib nibWithNibName:@"CatalogCell" bundle:nil] forCellReuseIdentifier:@"catalogCell"];
    [self.tableView addRefreshControlWithAction:@selector(getDataPull) view:self];
    [self.tableView showRefreshControl];
    [self getDataLoadNextPage:NO];
    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.tableView.bounds.size.width, 0.01f)];
}

- (void)setupCollectionView{
    [self.collectionView addRefreshControlWithAction:@selector(getDataPull) view:self];
}

- (void)setupNavigationBarWithSearchText:(NSString *)searchText{
    CityNavigationBar *navTitle = [CityNavigationBar new];
    [navTitle.searchBar setDelegate:self];
    self.searchbar = navTitle.searchBar;
    self.searchbar.text = searchText;
    self.navigationItem.titleView = navTitle;
    self.navigationItem.titleView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width-44, 30);
}

#pragma mark - Data

- (void)hideAllLoaders{
    [self.tableView hiddenRefreshControl];
    [self.tableView hiddenPagingSpinner];
    
    [self.collectionView hiddenRefreshControl];
    [self.collectionView hiddenPagingSpinner];
}

- (void)getDataPull{
    [self getDataLoadNextPage:NO];
}

- (void)getDataLoadNextPage:(BOOL)nextPage{
    if(!nextPage && ![self.tableView isSpinnerShow] && ![self.collectionView isSpinnerShow]){
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
    
    if(nextPage){
        if(self.catalogVisibleStyle == TableStyle) {
            [self.tableView showPagingSpinner];
        }
    }
    
    [self.output getDataWithSort:self.sort
                          search:self.searchbar.text
                        category:self.categoryDic
                            city:self.cityDic
                     dopProperty:self.dopPropteryArray
                  isLoadNextPage:nextPage];
}

- (void)reloadData:(NSArray *)itemArray{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [self.tableView hiddenRefreshControl];
    [self.collectionView hiddenRefreshControl];
    
    self.dataSource = [itemArray mutableCopy];
    
    if(self.catalogVisibleStyle == TableStyle){
        [self.tableView reloadDataWithCheck];
    }else{
        [self.collectionView reloadData];
    }
}

#pragma mark - UITableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(self.catalogVisibleStyle == TableStyle)
        return [self.dataSource count];
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CatalogCell *cell = [tableView dequeueReusableCellWithIdentifier:@"catalogCell"];
    
    CatalogModel *model = [self.dataSource objectAtIndex:indexPath.row];
    [cell setupCellWithModel:model hideStatus:YES hideFavoriteButton:NO];
    if(!cell.favoriteButton.isHidden){
        [cell.favoriteButton addTarget:self action:@selector(addToFavorite:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    if(indexPath.row == [self.dataSource count] - 2){
        [self getDataLoadNextPage:YES];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self selectItemWithIndexPath:indexPath];
}

#pragma mark - UICollectionView

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if(self.catalogVisibleStyle == CollectionStyle)
        return [self.dataSource count];
    return 0;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    SimilarDetailCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"catalogCollectionCell" forIndexPath:indexPath];
    
    CatalogModel *model = [self.dataSource objectAtIndex:indexPath.row];

    [cell.image sd_setImageWithURL:[NSURL URLWithString:model.imageMedium] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    cell.title.text = model.title;
    
    [cell.price setHidden:YES];
    if([model.priceDisplay length]){
        [cell.price setHidden:NO];
        cell.price.text = model.priceDisplay;
    }
    
    cell.info.text = [NSString stringWithFormat:@"%@\n%@", model.city, model.date];
    [cell.layer setCornerRadius:5];
    [cell.layer setMasksToBounds:YES];
    
    if(indexPath.row == [self.dataSource count] - 2){
        [self getDataLoadNextPage:YES];
    }
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGSize size = CGSizeMake((collectionView.frame.size.width-5)/2, 250);
    
    return size;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    [self selectItemWithIndexPath:indexPath];
}

#pragma mark - CategoryDelegate

- (void)selectCategory:(NSDictionary *)dic{
    self.categoryDic = dic;
    [self getDataLoadNextPage:NO];
}

- (IBAction)selectCategoryAction:(UIButton *)sender {
    [self.output selectCategoryAction];
}

#pragma mark - Filter Delegate

- (void)applyFilter:(FilterModel *)filter{
    self.filterDataSource = filter;
    if(self.isSearchView){
        self.categoryDic = filter.category;
        self.cityDic = filter.city;
        self.dopPropteryArray = filter.dynamicproperty;
        [self getDataLoadNextPage:NO];
        return;
    }
    [self.output applyFilter:filter];
}

#pragma mark - Search

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    [self showWhiteView];
    [self.searchbar setShowsCancelButton:YES animated:YES];
}

- (void)showWhiteView{
    static UIView *whiteView;

    for(UIView *view in self.view.subviews){
        if([view.restorationIdentifier isEqualToString:@"WhiteView"]){
            return;
        }
    }
    whiteView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [whiteView setRestorationIdentifier:@"WhiteView"];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(disableWhiteView)];
    [whiteView setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.7f]];
    [whiteView addGestureRecognizer:tap];
    
    self.navigationItem.rightBarButtonItems = nil;
    self.navigationItem.titleView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 44);
    
    [self.view addSubview:whiteView];
}

- (void)disableWhiteView{
    [self.searchbar resignFirstResponder];
    if(!self.isSearchView)
        self.searchbar.text = @"";
    [self.searchbar setShowsCancelButton:NO animated:YES];
    self.navigationItem.rightBarButtonItems = @[self.sortBarButton, self.filterBarButton];
    for(UIView *view in self.view.subviews){
        if([view.restorationIdentifier isEqualToString:@"WhiteView"]){
            [view removeFromSuperview];
            return;
        }
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    if(!self.isSearchView){
        [self.output searh:searchBar.text];
        [self disableWhiteView];
        return;
    }
    [self disableWhiteView];
    [self getDataLoadNextPage:NO];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [self disableWhiteView];
}

#pragma mark - Action

- (void)selectItemWithIndexPath:(NSIndexPath *)indexPath {
    CatalogModel *model = [self.dataSource objectAtIndex:indexPath.row];
    self.selectedModel = model;
    [self.output showDetailAdvertising:model.itemId model:model isFavorite:model.isFavorite];
}
- (void)addToFavorite:(UIButton *)button{
    NSString *action = @"items_add";
    
    if([button.restorationIdentifier integerValue])
        action = @"items_del";
    
    [self.output addToFavoriteWithItemId:button.tag action:action];
}

- (void)showFiler{
    [self.output showFilter:self.filterDataSource];
}

- (IBAction)sortTypeAction:(UIButton *)sender {
    [KVAlert alertWithTitle:NSLocalizedString(@"sort", @"Russian: Сортировка")
                    message:NSLocalizedString(@"sortType", @"Russian: Тип Сортировки")
                    buttons:@[
                              NSLocalizedString(@"sortLess", @"Russian: Самые новые"),
                              NSLocalizedString(@"sortMore", @"Russian: Самые старые"),
                              NSLocalizedString(@"sortPLess", @"Russian: Самые дорогие"),
                              NSLocalizedString(@"sortPMore", @"Russian: Самые дешевые")]
             preferredStyle:UIAlertControllerStyleAlert completion:^(NSUInteger buttonPresed) {
                 switch (buttonPresed) {
                     case 0:
                         self.sort = @"publicated_less";
                         break;
                     case 1:
                         self.sort = @"publicated_more";
                         break;
                     case 2:
                         self.sort = @"price_less";
                         break;
                     case 3:
                         self.sort = @"price_more";
                         break;
                 }
                 [self getDataLoadNextPage:NO];
             }];
}

#pragma mark - SlideNavigationController

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu{
    return !self.isSearchView;
}

- (IBAction)testCcontainer:(id)sender {
    [self.collectionView setHidden:!self.collectionView.hidden];
    [self.tableView setHidden:!self.tableView.hidden];
    
    if(self.catalogVisibleStyle == TableStyle){
        self.catalogVisibleStyle = CollectionStyle;
        [self.collectionView reloadDataWithCheck];
        return;
    }
    self.catalogVisibleStyle = TableStyle;
    [self.tableView reloadData];
}

@end
