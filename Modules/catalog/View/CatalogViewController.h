//
//  CatalogViewController.h
//  Gauranga
//
//  Created by Vlad Kochergin on 07/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CatalogViewInput.h"

#import "CategoryViewController.h"

@protocol CatalogViewOutput;



@interface CatalogViewController : UIViewController <CatalogViewInput, CategoryViewDelegate>

@property (nonatomic, strong) id<CatalogViewOutput> output;

@property (weak, nonatomic) IBOutlet UIView *container;

@end
