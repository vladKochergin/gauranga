//
//  CatalogViewOutput.h
//  Gauranga
//
//  Created by Vlad Kochergin on 07/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FilterModel;

@protocol CatalogViewOutput <NSObject>

/**
 @author Vlad Kochergin

 Метод сообщает презентеру о том, что view готова к работе
 */

- (void)didTriggerViewReadyEvent;

- (void)getDataWithSort:(NSString *)sort search:(NSString *)searchText
               category:(NSDictionary *)category
                   city:(NSDictionary *)city
             dopProperty:(NSArray *)dopProperty
         isLoadNextPage:(BOOL)nextPage;

- (void)selectCategoryAction;
- (void)addToFavoriteWithItemId:(NSInteger)itemId action:(NSString *)action;
- (void)searh:(NSString *)searchText;
- (void)showDetailAdvertising:(NSInteger)selectAdvId model:(id)model isFavorite:(BOOL)isFavorite;
- (void)showFilter:(FilterModel *)filter;
- (void)applyFilter:(FilterModel *)filter;

@end
