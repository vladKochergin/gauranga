//
//  AddAdvertisingInteractor.h
//  Gauranga
//
//  Created by Vlad Kochergin on 10/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "AddAdvertisingInteractorInput.h"

@protocol AddAdvertisingInteractorOutput;

@interface AddAdvertisingInteractor : NSObject <AddAdvertisingInteractorInput>

@property (nonatomic, weak) id<AddAdvertisingInteractorOutput> output;

@end
