//
//  AddAdvertisingInteractor.m
//  Gauranga
//
//  Created by Vlad Kochergin on 10/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "AddAdvertisingInteractor.h"

#import "AddAdvertisingInteractorOutput.h"

#import "RequestMapManager.h"

#import "DPTextField.h"
#import "CurrencyModel.h"
#import "DataManager.h"

@implementation AddAdvertisingInteractor

#pragma mark - Методы AddAdvertisingInteractorInput

- (void)getDynamicPropertyWithCategoryId:(NSString *)catId{
    [RequestMapManager getDynamicPropertyWithCategoryId:catId completion:^(NSArray *array, NSDictionary *dictionary, id object) {
        [self.output reloadDataWithDynamicProperty:array];
    }];
}

- (void)sendAdvertisingWith:(UITextField *)title category:(NSDictionary *)category desc:(UITextView *)desc priceType:(DPTextField *)priceType city:(NSDictionary *)city name:(UITextField *)name email:(NSString *)email phone:(UITextField *)phone price:(UITextField *)price dynamProp:(NSArray *)dynamProp imageData:(NSArray *)imageData{
    if([self validProperty:title category:category desc:desc priceType:priceType city:city name:name email:email price:price]){
        
        NSMutableArray *arrayDataBase64 = [NSMutableArray new];
        for(NSData *data in imageData){
            UIImage *image = [UIImage imageWithData:data];
            NSData *imageData = UIImageJPEGRepresentation(image, 0.3f);
            [arrayDataBase64 addObject:[self encode:imageData]];
        }
        
        NSDictionary *curDic = [[DataManager sharedInstance] getCurrency];
        CurrencyModel *curModel = [curDic objectForKey:[[curDic allKeys] objectAtIndex:priceType.selectedIndex]];
        NSDictionary *dicParam = @{
                                   @"cat_id": [category objectForKey:@"cat_up"],
                                   @"title": title.text,
                                   @"price": price.text,
                                   @"price_curr": [NSString stringWithFormat:@"%li",(long)curModel.currencyId],
                                   @"descr": desc.text,
                                   @"name": name.text,
                                   @"dop": dynamProp,
                                   @"phones": @[phone.text],
                                   @"city_id": [city objectForKey:@"city_id"],
                                   @"images" : arrayDataBase64
                                   };
        
        [[WebService sharedService] sendAdvertising:dicParam completion:^(NSError *error, NSArray *array, NSDictionary *dictionary) {
            [KVAlert alertWithTitle:@"" message:@"Объявление создано успешно" preferredStyle:UIAlertControllerStyleAlert completion:^{
                [self.output hideProgressHUD];
                [self.output closeCurrentController];
            }];
        }];
        return;
    }
    [self.output hideProgressHUD];
    [KVAlert alertWithTitle:@"Ошибка" message:@"Не все поля заполнены" preferredStyle:UIAlertControllerStyleAlert completion:nil];
}

#pragma mark - Validation

- (BOOL)validProperty:(UITextField *)title category:(NSDictionary *)category desc:(UITextView *)desc priceType:(DPTextField *)priceType city:(NSDictionary *)city name:(UITextField *)name email:(NSString *)email price:(UITextField *)price{
    
    if(title.text.length > 4 &&
       [[category allKeys] count] &&
       desc.text.length >= 12 &&
       price.text.length &&
       priceType.text.length &&
       [[city allKeys] count] &&
       name.text.length &&
       email.length)
        return YES;
    return NO;
}

#pragma mark - Encode 64base

#define ArrayLength(x) (sizeof(x)/sizeof(*(x)))

static char encodingTable[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
//static char decodingTable[128];

- (NSString*) encode:(const uint8_t*) input length:(NSInteger) length {
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    for (NSInteger i = 0; i < length; i += 3) {
        NSInteger value = 0;
        for (NSInteger j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger index = (i / 3) * 4;
        output[index + 0] =                    encodingTable[(value >> 18) & 0x3F];
        output[index + 1] =                    encodingTable[(value >> 12) & 0x3F];
        output[index + 2] = (i + 1) < length ? encodingTable[(value >> 6)  & 0x3F] : '=';
        output[index + 3] = (i + 2) < length ? encodingTable[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:data
                                 encoding:NSASCIIStringEncoding];
}

- (NSString*) encode:(NSData*) rawBytes {
    return [self encode:(const uint8_t*) rawBytes.bytes length:rawBytes.length];
}

#pragma mark - UIImageResize

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

@end
