//
//  AddAdvertisingInteractorInput.h
//  Gauranga
//
//  Created by Vlad Kochergin on 10/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DPTextField;

@protocol AddAdvertisingInteractorInput <NSObject>

- (void)getDynamicPropertyWithCategoryId:(NSString *)catId;
- (void)sendAdvertisingWith:(UITextField *)title category:(NSDictionary *)category desc:(UITextView *)desc priceType:(DPTextField *)priceType city:(NSDictionary *)city name:(UITextField *)name email:(NSString *)email phone:(UITextField *)phone price:(UITextField *)price dynamProp:(NSArray *)dynamProp imageData:(NSArray *)imageData;

@end
