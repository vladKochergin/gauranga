//
//  AddAdvertisingAssembly.h
//  Gauranga
//
//  Created by Vlad Kochergin on 10/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>

/**
 @author Vlad Kochergin

 AddAdvertising module
 */
@interface AddAdvertisingAssembly : TyphoonAssembly <RamblerInitialAssembly>

@end
