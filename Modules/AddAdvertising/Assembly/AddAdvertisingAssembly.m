//
//  AddAdvertisingAssembly.m
//  Gauranga
//
//  Created by Vlad Kochergin on 10/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "AddAdvertisingAssembly.h"

#import "AddAdvertisingViewController.h"
#import "AddAdvertisingInteractor.h"
#import "AddAdvertisingPresenter.h"
#import "AddAdvertisingRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation AddAdvertisingAssembly

- (AddAdvertisingViewController *)viewAddAdvertising {
    return [TyphoonDefinition withClass:[AddAdvertisingViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterAddAdvertising]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterAddAdvertising]];
                          }];
}

- (AddAdvertisingInteractor *)interactorAddAdvertising {
    return [TyphoonDefinition withClass:[AddAdvertisingInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterAddAdvertising]];
                          }];
}

- (AddAdvertisingPresenter *)presenterAddAdvertising{
    return [TyphoonDefinition withClass:[AddAdvertisingPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewAddAdvertising]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorAddAdvertising]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerAddAdvertising]];
                          }];
}

- (AddAdvertisingRouter *)routerAddAdvertising{
    return [TyphoonDefinition withClass:[AddAdvertisingRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewAddAdvertising]];
                          }];
}

@end
