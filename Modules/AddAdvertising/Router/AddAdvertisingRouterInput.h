//
//  AddAdvertisingRouterInput.h
//  Gauranga
//
//  Created by Vlad Kochergin on 10/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol AddAdvertisingRouterInput <NSObject>

- (void)selectCategoryView;
- (void)selectRegionView;
- (void)closeCurrentController;

@end
