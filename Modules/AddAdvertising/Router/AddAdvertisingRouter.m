//
//  AddAdvertisingRouter.m
//  Gauranga
//
//  Created by Vlad Kochergin on 10/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "AddAdvertisingRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

#import "CategoryViewController.h"
#import "CityViewController.h"
#import "CategoryModuleInput.h"
#import "CityModuleInput.h"

@implementation AddAdvertisingRouter

- (UIStoryboard *)storyboard{
    return [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
}

- (RamblerViperModuleFactory *)transitionModuleFactory{
    
    NSString *indfViewController = @"categoryView";
    RamblerViperModuleFactory *factory = [[RamblerViperModuleFactory alloc]initWithStoryboard:[self storyboard] andRestorationId:indfViewController];
    return factory;
}

- (RamblerViperModuleFactory *)transitionModuleFactoryRegion{
    NSString *indfViewController = @"regionView";
    RamblerViperModuleFactory *factory = [[RamblerViperModuleFactory alloc]initWithStoryboard:[self storyboard] andRestorationId:indfViewController];
    return factory;
}

#pragma mark - Методы AddAdvertisingRouterInput

- (void)selectCategoryView{
    RamblerViperModuleFactory *factory = [self transitionModuleFactory];
    [[self.transitionHandler openModuleUsingFactory:factory withTransitionBlock:^(id<RamblerViperModuleTransitionHandlerProtocol> sourceModuleTransitionHandler, id<RamblerViperModuleTransitionHandlerProtocol> destinationModuleTransitionHandler) {
        
        UIViewController<CategoryViewDelegate> *source = (id) sourceModuleTransitionHandler;
        CategoryViewController *destinationViewController = (id) destinationModuleTransitionHandler;
        [destinationViewController setDelegate:source];
        [source.navigationController pushViewController:destinationViewController animated:YES];
        
    }] thenChainUsingBlock:^id<RamblerViperModuleOutput>(id<CategoryModuleInput> moduleInput) {
        [moduleInput configureModuleWithModel:nil isAddAdvertising:YES];
        return nil;
    }];
}

- (void)selectRegionView{
    RamblerViperModuleFactory *factory = [self transitionModuleFactoryRegion];
    [[self.transitionHandler openModuleUsingFactory:factory withTransitionBlock:^(id<RamblerViperModuleTransitionHandlerProtocol> sourceModuleTransitionHandler, id<RamblerViperModuleTransitionHandlerProtocol> destinationModuleTransitionHandler) {
        
        UIViewController<RegionViewDelegate> *source = (id) sourceModuleTransitionHandler;
        CityViewController *destinationViewController = (id) destinationModuleTransitionHandler;
        [destinationViewController setDelegate:source];
        [source.navigationController pushViewController:destinationViewController animated:YES];
        
    }] thenChainUsingBlock:^id<RamblerViperModuleOutput>(id<CityModuleInput> moduleInput) {
        [moduleInput configureModuleWithModel:nil isAddAdvertising:YES];
        return nil;
    }];
}

- (void)closeCurrentController {
    [self.transitionHandler closeCurrentModule:YES];
}

@end
