//
//  AddAdvertisingRouter.h
//  Gauranga
//
//  Created by Vlad Kochergin on 10/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "AddAdvertisingRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface AddAdvertisingRouter : NSObject <AddAdvertisingRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
