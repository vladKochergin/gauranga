//
//  AddAdvertisingPresenter.h
//  Gauranga
//
//  Created by Vlad Kochergin on 10/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "AddAdvertisingViewOutput.h"
#import "AddAdvertisingInteractorOutput.h"
#import "AddAdvertisingModuleInput.h"

@protocol AddAdvertisingViewInput;
@protocol AddAdvertisingInteractorInput;
@protocol AddAdvertisingRouterInput;

@interface AddAdvertisingPresenter : NSObject <AddAdvertisingModuleInput, AddAdvertisingViewOutput, AddAdvertisingInteractorOutput>

@property (nonatomic, weak) id<AddAdvertisingViewInput> view;
@property (nonatomic, strong) id<AddAdvertisingInteractorInput> interactor;
@property (nonatomic, strong) id<AddAdvertisingRouterInput> router;

@end
