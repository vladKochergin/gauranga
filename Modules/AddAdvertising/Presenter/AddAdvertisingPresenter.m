//
//  AddAdvertisingPresenter.m
//  Gauranga
//
//  Created by Vlad Kochergin on 10/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "AddAdvertisingPresenter.h"

#import "AddAdvertisingViewInput.h"
#import "AddAdvertisingInteractorInput.h"
#import "AddAdvertisingRouterInput.h"

@implementation AddAdvertisingPresenter

#pragma mark - Методы AddAdvertisingModuleInput

- (void)configureModule {
    // Стартовая конфигурация модуля, не привязанная к состоянию view
}

#pragma mark - Методы AddAdvertisingViewOutput

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
}

- (void)selectCategoryAction{
    [self.router selectCategoryView];
}

- (void)getDynamicPropertyWithCategoryId:(NSString *)catId{
    [self.interactor getDynamicPropertyWithCategoryId:catId];
}

- (void)selectRegionViewAction{
    [self.router selectRegionView];
}

- (void)sendAdvertisingWith:(UITextField *)title category:(NSDictionary *)category desc:(UITextView *)desc priceType:(DPTextField *)priceType city:(NSDictionary *)city name:(UITextField *)name email:(NSString *)email phone:(UITextField *)phone price:(UITextField *)price dynamProp:(NSArray *)dynamProp imageData:(NSArray *)imageData{
    
    [self.interactor sendAdvertisingWith:title category:category desc:desc priceType:priceType city:city name:name email:email phone:phone price:price dynamProp:dynamProp imageData:imageData];
}

#pragma mark - Методы AddAdvertisingInteractorOutput

- (void)reloadDataWithDynamicProperty:(NSArray *)dynamicProperty{
    [self.view reloadDataWithDynamicProperty:dynamicProperty];
}
- (void)hideProgressHUD{
    [self.view hideProgressHUD];
}
- (void)closeCurrentController {
    [self.router closeCurrentController];
}

@end
