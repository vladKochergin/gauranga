//
//  SendCell.m
//  Gauranga
//
//  Created by yvp on 2/14/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import "SendCell.h"

@implementation SendCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self setupColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

#pragma mark - Additions

- (void)setupColor{
    [self.sendButton setBackgroundColor:[UIColor mainColor]];
}

@end
