//
//  AddAdvertisingViewController.m
//  Gauranga
//
//  Created by Vlad Kochergin on 10/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "AddAdvertisingViewController.h"

#import "AddAdvertisingViewOutput.h"

#import "imageCell.h"
#import "ButtonCell.h"
#import "TextCell.h"
#import "TextViewCell.h"
#import "TextWithPickerCell.h"
#import "MultiSelectView.h"
#import "MultiSelectCell.h"

#import "DataManager.h"
#import "CurrencyModel.h"
#import "UserModel.h"

#import "DescriptionCell.h"
#import "PriceCell.h"
#import "FioCell.h"
#import "SendCell.h"

#import "TPKeyboardAvoidingTableView.h"
#import "DPTextField.h"

#define gayColor [UIColor colorWithRed:(199.f/255) green:(199.f/255) blue:(205.f/255) alpha:1]


@interface AddAdvertisingViewController()<UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingTableView *tableView;
@property(nonatomic)int tagIndex;
@property(nonatomic, strong)UserModel *userProfile;

@property(nonatomic, strong) NSDictionary *categoryDic;
@property(nonatomic, strong) NSDictionary *regionDic;

@property(nonatomic, strong) UITextField *advTitle;
@property(nonatomic, strong) UIButton *advCategory;
@property(nonatomic, strong) UITextView *advDescriptions;

@property(nonatomic, strong) DPTextField *advPriceType;
@property(nonatomic, strong) UITextField *advPrice;

@property(nonatomic, strong) UIButton *advCity;
@property(nonatomic, strong) UITextField *advName;
@property(nonatomic, strong) UITextField *advEmail;
@property(nonatomic, strong) UITextField *advPhone;

@property(nonatomic, strong) NSMutableDictionary *cellArray;
@property(nonatomic, strong) NSMutableArray *dynamicPropertyArray;

@property(nonatomic, strong)  UIImageView *validDesc;
@property(nonatomic, strong) UILabel *categoryPlaceholder;

@end

@implementation AddAdvertisingViewController

#pragma mark - ProgressHUD

- (void)hideProgressHUD{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.output didTriggerViewReadyEvent];
}

#pragma mark - Методы AddAdvertisingViewInput

- (void)setupInitialState {
    self.navigationItem.title = NSLocalizedString(@"added",@"Russian: Добавление");
    self.cellArray = [NSMutableDictionary new];
    self.dynamicPropertyArray = [NSMutableArray new];
    self.tagIndex = 0;
    self.userProfile = [[DataManager sharedInstance]getUserProfile];
    self.tableView.estimatedRowHeight = 100.0;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
}

#pragma mark - UITableView

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    NSString *indfCell = [self getTypeFieldForSection:indexPath.section];
    if(indexPath.section != 2){
        if([indfCell isEqualToString:@"imageCell"]){
            imageCell *Cell = [tableView dequeueReusableCellWithIdentifier:indfCell];
            if(![self.imageButtonArray count]){
                for(UIButton *btn in Cell.buttonImageArray){
                    [[btn imageView] setContentMode: UIViewContentModeScaleAspectFill];
                    [btn addTarget:self action:@selector(addImage:) forControlEvents:UIControlEventTouchUpInside];
                }
                self.imageButtonArray = Cell.buttonImageArray;
                return Cell;
            }
            return Cell;
        }
        if(indexPath.section == 1){
            DescriptionCell *Cell = [tableView dequeueReusableCellWithIdentifier:indfCell];
            if(!self.advTitle){
                [Cell.category addTarget:self action:@selector(selectCategoryAction:) forControlEvents:UIControlEventTouchUpInside];
                [Cell.category setTitleColor:gayColor forState:UIControlStateNormal];
                self.advTitle = Cell.title;
                self.advCategory = Cell.category;
                self.advDescriptions = Cell.descriptionAdv;
                [Cell.descriptionAdv setDelegate:self];
                self.validDesc = [Cell.validImageArray objectAtIndex:2];
                return Cell;
            }
            
            if(self.categoryDic){
                
                if (!self.categoryPlaceholder) {
                    UILabel *placeholder = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(Cell.category.frame) + 2, CGRectGetMinY(Cell.category.frame) + 2, CGRectGetWidth(Cell.category.frame) / 2, 10)];
                    placeholder.font = [UIFont systemFontOfSize:10];
                    placeholder.textColor = [UIColor mainColor];
                    placeholder.text = Cell.category.titleLabel.text;
                    [Cell addSubview:placeholder];
                    self.categoryPlaceholder = placeholder;
                }
                [[Cell.validImageArray objectAtIndex:1] setImage:[UIImage imageNamed:@"validTrue"]];
                [Cell.category setTitle:[self.categoryDic objectForKey:@"title"] forState:UIControlStateNormal];
                [Cell.category setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            }
            self.validDesc = [Cell.validImageArray objectAtIndex:2];
            Cell.title = self.advTitle;
            Cell.category = self.advCategory;
            Cell.descriptionAdv = self.advDescriptions;
            
            
            return Cell;
        }
        if(indexPath.section == 3){
            PriceCell *Cell = [tableView dequeueReusableCellWithIdentifier:indfCell];
            if(!self.advPrice){
                [Cell.priceType setDataSource:[self getCurrencyArray]];
                self.advPrice = Cell.price;
                self.advPriceType = Cell.priceType;
                return Cell;
            }
            Cell.priceType = self.advPriceType;
            Cell.price = self.advPrice;
            
            return Cell;
        }
        if(indexPath.section == 4){
            FioCell *Cell = [tableView dequeueReusableCellWithIdentifier:indfCell];
            if(!self.advCity){
                [Cell.city addTarget:self action:@selector(selectRegionAction:) forControlEvents:UIControlEventTouchUpInside];
                [Cell.city setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
                Cell.name.text = self.userProfile.name;
                Cell.phone.text = self.userProfile.phoneNumber;
                self.advCity = Cell.city;
                self.advName = Cell.name;
                self.advPhone = Cell.phone;
                [Cell textFieldChange:Cell.name];
                [Cell textFieldChange:Cell.phone];
                return Cell;
            }
            
            if(self.regionDic){
                UILabel *placeholder = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(Cell.city.frame) + 2, CGRectGetMinY(Cell.city.frame) + 2, CGRectGetWidth(Cell.city.frame) / 2, 10)];
                placeholder.font = [UIFont systemFontOfSize:10];
                placeholder.textColor = [UIColor mainColor];
                placeholder.text = Cell.city.titleLabel.text;
                [Cell addSubview:placeholder];
                [[Cell.validImageArray objectAtIndex:0] setImage:[UIImage imageNamed:@"validTrue"]];
                [Cell.city setTitle:[self.regionDic objectForKey:@"title"] forState:UIControlStateNormal];
                [Cell.city setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            }
            
            Cell.city = self.advCity;
            Cell.name = self.advName;
            Cell.phone = self.advPhone;
            
            return Cell;
        }
        if(indexPath.section == 5){
            SendCell *Cell = [tableView dequeueReusableCellWithIdentifier:indfCell];
            [Cell.sendButton addTarget:self action:@selector(sendAdvertising) forControlEvents:UIControlEventTouchUpInside];
            return Cell;
        }
    }else{
        cell = [self tableView:tableView dynamicPropertyCellForRowAtIndexPath:indexPath];
        return cell;
    }
    return cell;
}


- (UITableViewCell *)tableView:(UITableView *)tableView dynamicPropertyCellForRowAtIndexPath:(NSIndexPath *)indexPath{
    DynamicPropertyModel *model =  [self.dynamicPropertyDataSourсe objectAtIndex:indexPath.row];
    UITableViewCell *cell;
    
    switch ([model.type integerValue]) {
        case 1:
        case 2:{
            TextCell *Cell = (TextCell*)[self createCellWithIndexPath:indexPath tableView:tableView reuserIndfCell:@"textCellDOP"];
            TextCell *CellNew = [tableView dequeueReusableCellWithIdentifier:@"textCellDOP"];
            if(!Cell){
                [CellNew.textField setTag:model.dynPropID];
                [CellNew.textField setPlaceholder:model.title];
                [self saveCell:CellNew indexPath:indexPath];
                [self.dynamicPropertyArray addObject:CellNew.textField];
                return CellNew;
            }
            CellNew.textField = Cell.textField;
            cell = CellNew;
        }
            break;
        case 4:{
            TextWithPickerCell *Cell = (TextWithPickerCell*)[self createCellWithIndexPath:indexPath tableView:tableView reuserIndfCell:@"textWithPickerCellDOP"];
            TextWithPickerCell *CellNew = [tableView dequeueReusableCellWithIdentifier:@"textWithPickerCellDOP"];
            if(!Cell){
                [CellNew.textPicker setDataSource:@[@"NO", @"YES"]];
                [CellNew.textPicker setTag:model.dynPropID];
                [CellNew.textPicker setPlaceholder:model.title];
                [self saveCell:CellNew indexPath:indexPath];
                [self.dynamicPropertyArray addObject:CellNew.textPicker];
                return CellNew;
            }
            CellNew.textPicker = Cell.textPicker;
            cell = CellNew;
        }
            break;
        case 9:
        case 7:{
            MultiSelectCell *Cell = (MultiSelectCell*)[self createCellWithIndexPath:indexPath tableView:tableView reuserIndfCell:@"multiSelectCellDOP"];
            MultiSelectCell *CellNew = [tableView dequeueReusableCellWithIdentifier:@"multiSelectCellDOP"];
            if(!Cell){
                [CellNew.textPicker setDataSource:model.list];
                [CellNew.textPicker setTag:model.dynPropID];
                [CellNew.textPicker setPlaceholder:model.title];
                [self saveCell:CellNew indexPath:indexPath];
                [self.dynamicPropertyArray addObject:CellNew.textPicker];
                return CellNew;
            }
            CellNew.textPicker = Cell.textPicker;
            cell = CellNew;
        }
            break;
        case 8:
        case 6:{
            TextWithPickerCell *Cell = (TextWithPickerCell*)[self createCellWithIndexPath:indexPath tableView:tableView reuserIndfCell:@"textWithPickerCellDOP"];
            TextWithPickerCell *CellNew =  [tableView dequeueReusableCellWithIdentifier:@"textWithPickerCellDOP"];
            if(!Cell){
                [CellNew.textPicker setDataSource:model.list];
                [CellNew.textPicker setTag:model.dynPropID];
                [CellNew.textPicker setPlaceholder:model.title];
                [self saveCell:CellNew indexPath:indexPath];
                [self.dynamicPropertyArray addObject:CellNew.textPicker];
                return CellNew;
            }
            CellNew.textPicker = Cell.textPicker;
            cell = CellNew;
        }
            break;
        case 10:{
            TextCell *Cell = (TextCell*)[self createCellWithIndexPath:indexPath tableView:tableView reuserIndfCell:@"textCellDOP"];
            TextCell *CellNew = [tableView dequeueReusableCellWithIdentifier:@"textCellDOP"];
            if(!Cell){
                [CellNew.textField setKeyboardType:UIKeyboardTypeNumberPad];
                [CellNew.textField setTag:model.dynPropID ];
                [CellNew.textField setPlaceholder:model.title];
                [self saveCell:CellNew indexPath:indexPath];
                [self.dynamicPropertyArray addObject:CellNew.textField];
                return CellNew;
            }
            CellNew.textField = Cell.textField;
            cell = CellNew;
        }
            break;
        case 11:{
            TextWithPickerCell *Cell = (TextWithPickerCell*)[self createCellWithIndexPath:indexPath tableView:tableView reuserIndfCell:@"textWithPickerCellDOP"];
            TextWithPickerCell *CellNew =  [tableView dequeueReusableCellWithIdentifier:@"textWithPickerCellDOP"];
            if(!Cell){
                [CellNew.textPicker setDataSource:model.list];
                [CellNew.textPicker setTag:model.dynPropID];
                [CellNew.textPicker setPlaceholder:model.title];
                [self saveCell:CellNew indexPath:indexPath];
                [self.dynamicPropertyArray addObject:CellNew.textPicker];
                return CellNew;
            }
            CellNew.textPicker = Cell.textPicker;
            cell = CellNew;
        }
            break;
        default:{
            TextCell *Cell = (TextCell*)[self createCellWithIndexPath:indexPath tableView:tableView reuserIndfCell:@"textCellDOP"];
            TextCell *CellNew = [tableView dequeueReusableCellWithIdentifier:@"textCellDOP"];
            if(!Cell){
                CellNew.accessoryView = nil;
                [CellNew.textField setTag:model.dynPropID];
                [CellNew.textField setPlaceholder:model.title];
                [self saveCell:CellNew indexPath:indexPath];
                [self.dynamicPropertyArray addObject:CellNew.textField];
                return CellNew;
            }
            CellNew.textField = Cell.textField;
            cell = CellNew;
        }
            break;
    }
    return cell;
}

#pragma mark - TextViewDelegate

- (void)textViewDidChange:(UITextView *)textView{
    [self.tableView beginUpdates];
    [self setDescHeight:textView.contentSize.height + 90];
    [self.validDesc setImage:[UIImage imageNamed:@"validFalse"]];
    if(textView.text.length >= 12){
        [self.validDesc setImage:[UIImage imageNamed:@"validTrue"]];
    }
    [self.tableView endUpdates];
}

#pragma mark - Dynamic Property

- (UITableViewCell *)createCellWithIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView reuserIndfCell:(NSString *)reuseCell{
    NSString *key = [NSString stringWithFormat:@"%li:%li", (long)indexPath.section,(long)indexPath.row];
    if([[self.cellArray allKeys] containsObject:key]){
        return [self.cellArray objectForKey:key];
    }
    return nil;
}

- (void)saveCell:(UITableViewCell *)cell indexPath:(NSIndexPath *)indexPath{
    NSString *key = [NSString stringWithFormat:@"%li:%li", (long)indexPath.section,(long)indexPath.row];
    [self.cellArray setValue:cell forKey:key];
}

- (NSArray *)getCurrencyArray{
    NSMutableArray *currencyArray = [NSMutableArray new];
    NSDictionary *dicCurr = [[DataManager sharedInstance] getCurrency];
    for(NSString *key in [dicCurr allKeys]){
        CurrencyModel *curModel = [dicCurr objectForKey:key];
        [currencyArray addObject:curModel.title];
    }
    return currencyArray;
}

- (NSArray *)getDynamicProperty{
    NSMutableArray *tempArray = [NSMutableArray new];
    for(UITextField *textField in self.dynamicPropertyArray){
        
        if([textField isKindOfClass:[MultiSelectView class]]){
            MultiSelectView *textfield = (MultiSelectView *)textField;
            NSMutableArray *result = [NSMutableArray new];
            
            for (NSString *number in textfield.selectItemsID){
                [result addObject:[NSString stringWithFormat:@"%li", (long)pow(2, [number integerValue])]];
            }
            
            NSDictionary *dic = @{
                                  [NSString stringWithFormat:@"%li", (long)textField.tag] : result
                                  };
            
            [tempArray addObject:dic];
            
        }else if([textField isKindOfClass:[DPTextField class]]){
            DPTextField *textfield = (DPTextField *)textField;
            NSDictionary *dic = @{
                                  [NSString stringWithFormat:@"%li", (long)textField.tag] : [NSString stringWithFormat:@"%li", (long)textfield.selectedIndex]
                                  };
            [tempArray addObject:dic];
        }else{
            NSDictionary *dic = @{
                                  [NSString stringWithFormat:@"%li", (long)textField.tag] : textField.text
                                  };
            [tempArray addObject:dic];
        }
    }
    
    return tempArray;
}

#pragma mark - CategoryDelegate

- (void)selectCategory:(NSDictionary *)dictionary {
    self.categoryDic = dictionary;
    NSString *categoryId = [dictionary objectForKey:[[dictionary allKeys] lastObject]];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self.output getDynamicPropertyWithCategoryId:categoryId];
}

- (void)selectRegion:(NSDictionary *)dictionary{
    self.regionDic = dictionary;
    [self.tableView reloadData];
}

- (void)selectCategoryAction:(UIButton *)sender {
    [self.output selectCategoryAction];
}

- (void)selectRegionAction:(UIButton *)sender {
    [self.output selectRegionViewAction];
}

#pragma mark - DataSaving

- (void)saveDetailInfo {
    
}

#pragma mark - Data

- (void)reloadDataWithDynamicProperty:(NSArray *)dynamicProperty{
    self.dynamicPropertyDataSourсe = [NSArray new];
    self.dynamicPropertyArray = [NSMutableArray new];
    self.cellArray = [NSMutableDictionary new];
    [self.tableView reloadData];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    self.dynamicPropertyDataSourсe = [dynamicProperty copy];
    [self.tableView reloadData];
}

#pragma mark - Action

- (void)sendAdvertising{
    NSArray *array = [self getDynamicProperty];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self.output sendAdvertisingWith:self.advTitle category:self.categoryDic desc:self.advDescriptions priceType:self.advPriceType city:self.regionDic name:self.advName email:self.userProfile.email phone:self.advPhone price:self.advPrice dynamProp:array imageData:self.imageDataArray];
}

#pragma mark - SlideNavigationController

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu{
    return YES;
}

@end

