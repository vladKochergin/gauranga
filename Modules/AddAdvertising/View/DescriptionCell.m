//
//  DescriptionCell.m
//  Gauranga
//
//  Created by yvp on 2/14/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import "DescriptionCell.h"

@interface DescriptionCell()<UITextViewDelegate>



@end

@implementation DescriptionCell


- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.title addTarget:self action:@selector(textFieldChange:) forControlEvents:UIControlEventEditingChanged | UIControlEventEditingDidEnd];
    self.descriptionAdv.textContainerInset = UIEdgeInsetsMake(14, 0, 2, 0);
    self.descriptionAdv.textContainer.lineFragmentPadding = 0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)textFieldChange:(UITextField *)textField{
    [[self.validImageArray objectAtIndex:textField.tag] setImage:[UIImage imageNamed:@"validFalse"]];
    switch (textField.tag) {
        case 0:
            if(textField.text.length > 4){
                [[self.validImageArray objectAtIndex:textField.tag] setImage:[UIImage imageNamed:@"validTrue"]];
            }
            break;
        case 2:
            if(textField.text.length >= 12){
                [[self.validImageArray objectAtIndex:textField.tag] setImage:[UIImage imageNamed:@"validTrue"]];
            }
            break;
    }
}

@end
