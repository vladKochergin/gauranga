//
//  RootAddAdvertisingViewController.m
//  Gauranga
//
//  Created by yvp on 2/10/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import "RootAddAdvertisingViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <Photos/Photos.h>

@interface RootAddAdvertisingViewController ()<UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, UITextViewDelegate>

@property (strong, nonatomic) UIImagePickerController *imagePickerController;

@end

@implementation RootAddAdvertisingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.imageDataArray = [NSMutableArray new];
    self.descHeight = 123;
    dispatch_async(dispatch_get_main_queue(),^{
        [self createImagePickerController];
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSString *)getTypeFieldForSection:(NSInteger)section{
    switch (section) {
        case 0:
            return @"imageCell";
            break;
        case 1:
            return @"descriptionCell";
            break;
        case 2:
            return nil;
            break;
        case 3:
            return @"priceCell";
            break;
        case 4:
            return @"fioCell";
            break;
        default:
            return @"sendCell";
            break;
    }
}

- (NSArray *)getPlaceholdeForSection:(NSInteger)section{
    NSArray *placeholderAdvInfo = @[
                                    @"Заголовок",
                                    @"Категория",
                                    @"Описание"
                                    ];
    NSArray *placeholderPrice = @[
                                  @"Тип цены",
                                  @"Цена"
                                  ];
    NSArray *placeholderUserInfo = @[
                                     @"Город",
                                     @"Контактное лицо",
                                     @"Номер телефона"
                                     ];
    switch (section) {
        case 1:
            return placeholderAdvInfo;
            break;
        case 2:
            return nil;
            break;
        case 3:
            return placeholderPrice;
            break;
        case 4:
            return placeholderUserInfo;
            break;
    }
    return nil;
}

- (NSArray *)getIndexForField:(NSInteger)section{
    switch (section) {
        case 1:
            return @[@"1",@"2",@"3",@"4"];
            break;
        case 2:
            return @[@"0",@"0",@"0",@"0",@"0"];
            break;
        case 3:
            return @[@"3",@"3",@"3",@"3",@"3"];
            break;
        case 4:
            return @[@"4",@"5",@"6",@"7"];
            break;
    }
    return @[@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10"];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 6;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSInteger countRow = 1;
    if(section == 2)
        countRow = [self.dynamicPropertyDataSourсe count];
    return countRow;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat countRow = 1;
    
    switch (indexPath.section) {
        case 0:
            countRow = 200;
            break;
        case 1:
            countRow = self.descHeight;
            break;
        case 2:
            countRow = 50;
            break;
        case 3:
            countRow = 88;
            break;
        case 4:
            countRow = 129;
            break;
        case 5:
            countRow = 40;
            break;
    }
    return countRow;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if ([textField isFirstResponder]) {
        if ([[[textField textInputMode] primaryLanguage] isEqualToString:@"emoji"] || ![[textField textInputMode] primaryLanguage]) {
            return NO;
        }
    }
    return YES;
}

#pragma mark - UITextViewDelegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if ([textView isFirstResponder]) {
        if ([[[textView textInputMode] primaryLanguage] isEqualToString:@"emoji"] || ![[textView textInputMode] primaryLanguage]) {
            return NO;
        }
    }
    return YES;
}

#pragma mark - ImagePickerView

- (void)createImagePickerController {
    _imagePickerController = [[UIImagePickerController alloc] init];
    [_imagePickerController setDelegate:self];
}

- (void)fromGallery{
    _imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
    
    if (status == PHAuthorizationStatusAuthorized) {
        [self presentImagePickerVC];
    }
    else if (status == PHAuthorizationStatusDenied) {
        [KVAlert alertWithTitle:@"Приложение не имеет доступа к Вашей галерее" message:@"Вы можете включить доступ в настройках приватности" buttons:@[@"Настройки"] preferredStyle:UIAlertControllerStyleAlert completion:^(NSUInteger buttonPresed) {
            if (!buttonPresed) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
            }
        }];
    }
    else if (status == PHAuthorizationStatusNotDetermined) {
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            
            if (status == PHAuthorizationStatusAuthorized) {
                [self presentImagePickerVC];
            }
            else {
            }
        }];
    }
    else if (status == PHAuthorizationStatusRestricted) {
    }
}

- (void)fromCamera {
    _imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
        
        if(status == AVAuthorizationStatusAuthorized) {
            [self presentImagePickerVC];
        } else if(status == AVAuthorizationStatusDenied){
            [KVAlert alertWithTitle:@"Приложение не имеет доступа к Вашей камере" message:@"Вы можете включить доступ в настройках приватности" buttons:@[@"Настройки"] preferredStyle:UIAlertControllerStyleAlert completion:^(NSUInteger buttonPresed) {
                if (!buttonPresed) {
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                }
            }];
        } else if(status == AVAuthorizationStatusRestricted){
        } else if(status == AVAuthorizationStatusNotDetermined){
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                if(granted){
                    [self presentImagePickerVC];
                    return;
                }
                NSLog(@"Not granted access");
            }];
        }
    }
}

- (void)presentImagePickerVC {
    [self presentViewController:_imagePickerController animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
    NSData *imageData = UIImageJPEGRepresentation(image, 0.6f);
    if (image.size.width/image.size.height > 3 && imageData.length > 1000000) {
        [KVAlert alertWithTitle:@"Ошибка" message:@"Недопустимый формат изображения" preferredStyle:UIAlertControllerStyleAlert completion:nil];
        return;
    }
    [self.imageDataArray addObject:imageData];
    [picker dismissViewControllerAnimated:YES completion:^{
        [self setupImage];
    }];
}

- (IBAction)addImage:(UIButton *)btn{
    if([btn.restorationIdentifier integerValue]){
        [KVAlert alertWithTitle:NSLocalizedString(@"picture",@"Russian: Изображение")
                        message:nil buttons:@[NSLocalizedString(@"delete",@"Russian: Удалить")]
                 preferredStyle:(UIAlertControllerStyleAlert)
                     completion:^(NSUInteger buttonPresed) {
                         [self.imageDataArray removeObjectAtIndex:btn.tag];
                         [self setupImage];
                     }];
        return;
    }
    
    UIAlertController *view = [UIAlertController alertControllerWithTitle:nil
                                                                  message:nil
                                                           preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *firstSection = [UIAlertAction actionWithTitle:NSLocalizedString(@"camera",@"Russian: Камера")
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action) {
                                                             if (([UIImagePickerController isSourceTypeAvailable:
                                                                   UIImagePickerControllerSourceTypeCamera] == NO)) {
                                                                 NSLog(@"Камера");
                                                             } else {
                                                                 [self fromCamera];
                                                             }
                                                             
                                                             [view dismissViewControllerAnimated:YES completion:nil];
                                                         }];
    UIAlertAction *secondSection = [UIAlertAction actionWithTitle:NSLocalizedString(@"Gal",@"Russian: Фотопленка")
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              
                                                              [self fromGallery];
                                                              [view dismissViewControllerAnimated:YES completion:nil];
                                                          }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel",@"Russian: Отмена")
                                                     style:UIAlertActionStyleCancel
                                                   handler:^(UIAlertAction * action) {
                                        
                                                       [view dismissViewControllerAnimated:YES completion:nil];
                                                   }];
    
    [view addAction:cancel];
    [view addAction:firstSection];
    [view addAction:secondSection];
    [self presentViewController:view animated:YES completion:nil];
}

- (void)setupImage{
    NSInteger index = 0;
    for(UIButton *btn in self.imageButtonArray){
        [btn setBackgroundImage:nil forState:UIControlStateNormal];
        [btn setRestorationIdentifier:@"0"];
        
        if(![self.imageDataArray count] || [self.imageDataArray count] - 1 < index) {
            return;
        }
        
        [btn setBackgroundImage:[UIImage imageWithData:[self.imageDataArray objectAtIndex:index]] forState:UIControlStateNormal];
        [btn setTag:index];
        [btn setRestorationIdentifier:@"1"];
        index++;
    }
}
@end
