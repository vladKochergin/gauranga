//
//  TextWithPickerCell.h
//  Gauranga
//
//  Created by yvp on 2/13/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DPTextField.h"

@interface TextWithPickerCell : UITableViewCell

@property (weak, nonatomic) IBOutlet DPTextField *textPicker;

@end
