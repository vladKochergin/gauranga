//
//  PriceCell.h
//  Gauranga
//
//  Created by yvp on 2/14/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
@class DPTextField;

@interface PriceCell : UITableViewCell

@property (weak, nonatomic) IBOutlet DPTextField *priceType;
@property (weak, nonatomic) IBOutlet UITextField *price;

@end
