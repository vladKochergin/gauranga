//
//  MultiSelectCell.m
//  Gauranga
//
//  Created by yvp on 3/3/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import "MultiSelectCell.h"

@interface MultiSelectCell ()

@end

@implementation MultiSelectCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
