//
//  FioCell.h
//  Gauranga
//
//  Created by yvp on 2/14/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FioCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *city;
@property (weak, nonatomic) IBOutlet UITextField *name;
@property (weak, nonatomic) IBOutlet UITextField *phone;

@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *validImageArray;

- (void)textFieldChange:(UITextField *)textField;

@end
