//
//  PriceCell.m
//  Gauranga
//
//  Created by yvp on 2/14/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import "PriceCell.h"
#import "DPTextField.h"

@interface PriceCell()

@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *validImageArray;

@end

@implementation PriceCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.price addTarget:self action:@selector(textFieldChange:) forControlEvents:UIControlEventEditingChanged];
    [self.priceType addTarget:self action:@selector(textFieldChange:) forControlEvents:UIControlEventEditingChanged | UIControlEventEditingDidEnd];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)textFieldChange:(UITextField *)textField{
    [[self.validImageArray objectAtIndex:textField.tag] setImage:[UIImage imageNamed:@"validFalse"]];
    switch (textField.tag) {
        case 0:
            if(textField.text.length > 0){
                [[self.validImageArray objectAtIndex:textField.tag] setImage:[UIImage imageNamed:@"validTrue"]];
            }
            break;
        case 1:
            if(textField.text.length > 0){
                [[self.validImageArray objectAtIndex:textField.tag] setImage:[UIImage imageNamed:@"validTrue"]];
            }
            break;
    }
}

@end
