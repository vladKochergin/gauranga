//
//  DescriptionCell.h
//  Gauranga
//
//  Created by yvp on 2/14/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@class cTextField;

@interface DescriptionCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *title;
@property (weak, nonatomic) IBOutlet UIButton *category;
@property (weak, nonatomic) IBOutlet UITextView *descriptionAdv;

@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *validImageArray;

@end
