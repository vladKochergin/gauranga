//
//  RootAddAdvertisingViewController.h
//  Gauranga
//
//  Created by yvp on 2/10/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DynamicPropertyModel.h"

@interface RootAddAdvertisingViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property(nonatomic) CGFloat descHeight;
@property (nonatomic, strong) NSArray *dynamicPropertyDataSourсe;
@property(nonatomic, strong) NSArray<UIButton *> *imageButtonArray;
@property(nonatomic, strong) NSMutableArray *imageDataArray;

- (IBAction)addImage:(UIButton *)btn;
- (NSString *)getTypeFieldForSection:(NSInteger)section;
- (NSArray *)getPlaceholdeForSection:(NSInteger)section;
- (NSArray *)getIndexForField:(NSInteger)section;

@end
