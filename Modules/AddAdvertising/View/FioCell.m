//
//  FioCell.m
//  Gauranga
//
//  Created by yvp on 2/14/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import "FioCell.h"

@interface FioCell()

@end

@implementation FioCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.name addTarget:self action:@selector(textFieldChange:) forControlEvents:UIControlEventEditingChanged];
    [self.phone addTarget:self action:@selector(textFieldChange:) forControlEvents:UIControlEventEditingChanged];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)textFieldChange:(UITextField *)textField{
    [[self.validImageArray objectAtIndex:textField.tag] setImage:[UIImage imageNamed:@"validFalse"]];
    switch (textField.tag) {
        case 1:
            if(textField.text.length > 0){
                [[self.validImageArray objectAtIndex:textField.tag] setImage:[UIImage imageNamed:@"validTrue"]];
            }
            break;
        case 2:
            if(textField.text.length > 0){
                [[self.validImageArray objectAtIndex:textField.tag] setImage:[UIImage imageNamed:@"validTrue"]];
            }
        case 3:
            if(textField.text.length > 0){
                [[self.validImageArray objectAtIndex:textField.tag] setImage:[UIImage imageNamed:@"validTrue"]];
            }
            break;
    }
}

@end
