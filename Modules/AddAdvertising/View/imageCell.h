//
//  imageCell.h
//  Gauranga
//
//  Created by yvp on 2/10/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface imageCell : UITableViewCell

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttonImageArray;

@end
