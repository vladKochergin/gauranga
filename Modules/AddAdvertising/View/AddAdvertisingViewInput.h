//
//  AddAdvertisingViewInput.h
//  Gauranga
//
//  Created by Vlad Kochergin on 10/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol AddAdvertisingViewInput <NSObject>

/**
 @author Vlad Kochergin

 Метод настраивает начальный стейт view
 */
- (void)setupInitialState;
- (void)reloadDataWithDynamicProperty:(NSArray *)dynamicProperty;
- (void)hideProgressHUD;

@end
