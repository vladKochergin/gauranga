//
//  ButtonCell.m
//  Gauranga
//
//  Created by yvp on 2/10/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import "ButtonCell.h"

@implementation ButtonCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Additions

- (void)setupColor{
    [self.button setBackgroundColor:[UIColor mainColor]];
}

@end
