//
//  AddAdvertisingViewController.h
//  Gauranga
//
//  Created by Vlad Kochergin on 10/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AddAdvertisingViewInput.h"
#import "RootAddAdvertisingViewController.h"

@protocol AddAdvertisingViewOutput;

@interface AddAdvertisingViewController : RootAddAdvertisingViewController <AddAdvertisingViewInput>

@property (nonatomic, strong) id<AddAdvertisingViewOutput> output;

@end
