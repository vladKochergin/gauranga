//
//  SendCell.h
//  Gauranga
//
//  Created by yvp on 2/14/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SendCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *sendButton;

@end
