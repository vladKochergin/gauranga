//
//  MultiSelectCell.h
//  Gauranga
//
//  Created by yvp on 3/3/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MultiSelectView.h"

@interface MultiSelectCell : UITableViewCell

@property (weak, nonatomic) IBOutlet MultiSelectView *textPicker;

@end
