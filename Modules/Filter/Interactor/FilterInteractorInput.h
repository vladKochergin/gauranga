//
//  FilterInteractorInput.h
//  Gauranga
//
//  Created by Vlad Kochergin on 21/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FilterInteractorInput <NSObject>

- (void)getDynamicPropertyWithCategoryId:(NSString *)catId;

@end
