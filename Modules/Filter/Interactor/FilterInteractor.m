//
//  FilterInteractor.m
//  Gauranga
//
//  Created by Vlad Kochergin on 21/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "FilterInteractor.h"

#import "FilterInteractorOutput.h"

#import "RequestMapManager.h"

@implementation FilterInteractor

#pragma mark - Методы FilterInteractorInput

- (void)getDynamicPropertyWithCategoryId:(NSString *)catId{
    [RequestMapManager getDynamicPropertyWithCategoryId:catId completion:^(NSArray *array, NSDictionary *dictionary, id object) {
        [self.output reloadDataWithDynamicProperty:array];
    }];
}

@end
