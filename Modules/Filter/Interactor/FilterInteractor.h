//
//  FilterInteractor.h
//  Gauranga
//
//  Created by Vlad Kochergin on 21/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "FilterInteractorInput.h"

@protocol FilterInteractorOutput;

@interface FilterInteractor : NSObject <FilterInteractorInput>

@property (nonatomic, weak) id<FilterInteractorOutput> output;

@end
