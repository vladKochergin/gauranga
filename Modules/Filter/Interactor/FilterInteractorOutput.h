//
//  FilterInteractorOutput.h
//  Gauranga
//
//  Created by Vlad Kochergin on 21/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FilterInteractorOutput <NSObject>

- (void)reloadDataWithDynamicProperty:(NSArray *)array;

@end
