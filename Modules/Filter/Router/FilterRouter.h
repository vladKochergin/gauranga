//
//  FilterRouter.h
//  Gauranga
//
//  Created by Vlad Kochergin on 21/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "FilterRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface FilterRouter : NSObject <FilterRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
