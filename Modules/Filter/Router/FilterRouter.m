//
//  FilterRouter.m
//  Gauranga
//
//  Created by Vlad Kochergin on 21/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "FilterRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

#import "CategoryViewController.h"
#import "CategoryModuleInput.h"
#import "CityViewController.h"
#import "CityModuleInput.h"

#import "CityModuleInput.h"
#import "SlideNavigationController.h"

@implementation FilterRouter

- (UIStoryboard *)storyboard{
    return [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
}

- (RamblerViperModuleFactory *)transitionModuleFactory:(NSString *)viewName{
    RamblerViperModuleFactory *factory = [[RamblerViperModuleFactory alloc]initWithStoryboard:[self storyboard] andRestorationId:viewName];
    return factory;
}

#pragma mark - Методы FilterRouterInput

- (void)selectCategory{
    RamblerViperModuleFactory *factory = [self transitionModuleFactory:@"categoryView"];
    [[self.transitionHandler openModuleUsingFactory:factory withTransitionBlock:^(id<RamblerViperModuleTransitionHandlerProtocol> sourceModuleTransitionHandler, id<RamblerViperModuleTransitionHandlerProtocol> destinationModuleTransitionHandler) {
        
        UIViewController<CategoryViewDelegate> *source = (id) sourceModuleTransitionHandler;
        CategoryViewController *destinationViewController = (id) destinationModuleTransitionHandler;
        [destinationViewController setDelegate:source];
        
        UINavigationController *navigationControllerss = [[UINavigationController alloc] initWithRootViewController:destinationViewController];
        [source presentViewController:navigationControllerss animated:YES completion:nil];
        
        
    }] thenChainUsingBlock:^id<RamblerViperModuleOutput>(id<CategoryModuleInput> moduleInput) {
        [moduleInput configureModuleWithModel:nil isAddAdvertising:NO];
        return nil;
    }];
}

- (void)selectRegion{
    RamblerViperModuleFactory *factory = [self transitionModuleFactory:@"regionView"];
    [[self.transitionHandler openModuleUsingFactory:factory withTransitionBlock:^(id<RamblerViperModuleTransitionHandlerProtocol> sourceModuleTransitionHandler, id<RamblerViperModuleTransitionHandlerProtocol> destinationModuleTransitionHandler) {
        
        UIViewController<RegionViewDelegate> *source = (id) sourceModuleTransitionHandler;
        CityViewController *destinationViewController = (id) destinationModuleTransitionHandler;
        [destinationViewController setDelegate:source];
        
        UINavigationController *navigationControllerss = [[UINavigationController alloc] initWithRootViewController:destinationViewController];
        [source presentViewController:navigationControllerss animated:YES completion:nil];
        
    }] thenChainUsingBlock:^id<RamblerViperModuleOutput>(id<CityModuleInput> moduleInput) {
        [moduleInput configureModuleWithModel:nil isAddAdvertising:NO];
        return nil;
    }];
}

@end
