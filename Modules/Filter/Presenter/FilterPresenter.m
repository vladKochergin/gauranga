//
//  FilterPresenter.m
//  Gauranga
//
//  Created by Vlad Kochergin on 21/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "FilterPresenter.h"

#import "FilterViewInput.h"
#import "FilterInteractorInput.h"
#import "FilterRouterInput.h"

@implementation FilterPresenter

#pragma mark - Методы FilterModuleInput

- (void)configureModuleWithFilterModel:(FilterModel *)filter {
    [self.view setFilter:filter];
}

#pragma mark - Методы FilterViewOutput

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
}

- (void)selectCategory{
    [self.router selectCategory];
}

- (void)selectRegion{
    [self.router selectRegion];
}

- (void)getDynamicPropertyWithCategoryId:(NSString *)catId{
    [self.interactor getDynamicPropertyWithCategoryId:catId];
}

#pragma mark - Методы FilterInteractorOutput

- (void)reloadDataWithDynamicProperty:(NSArray *)array{
    [self.view reloadDataWithDynamicProperty:array];
}

@end
