//
//  FilterPresenter.h
//  Gauranga
//
//  Created by Vlad Kochergin on 21/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "FilterViewOutput.h"
#import "FilterInteractorOutput.h"
#import "FilterModuleInput.h"

@protocol FilterViewInput;
@protocol FilterInteractorInput;
@protocol FilterRouterInput;

@interface FilterPresenter : NSObject <FilterModuleInput, FilterViewOutput, FilterInteractorOutput>

@property (nonatomic, weak) id<FilterViewInput> view;
@property (nonatomic, strong) id<FilterInteractorInput> interactor;
@property (nonatomic, strong) id<FilterRouterInput> router;

@end
