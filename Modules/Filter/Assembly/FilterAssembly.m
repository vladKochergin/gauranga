//
//  FilterAssembly.m
//  Gauranga
//
//  Created by Vlad Kochergin on 21/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "FilterAssembly.h"

#import "FilterViewController.h"
#import "FilterInteractor.h"
#import "FilterPresenter.h"
#import "FilterRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation FilterAssembly

- (FilterViewController *)viewFilter {
    return [TyphoonDefinition withClass:[FilterViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterFilter]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterFilter]];
                          }];
}

- (FilterInteractor *)interactorFilter {
    return [TyphoonDefinition withClass:[FilterInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterFilter]];
                          }];
}

- (FilterPresenter *)presenterFilter{
    return [TyphoonDefinition withClass:[FilterPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewFilter]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorFilter]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerFilter]];
                          }];
}

- (FilterRouter *)routerFilter{
    return [TyphoonDefinition withClass:[FilterRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewFilter]];
                          }];
}

@end
