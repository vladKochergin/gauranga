//
//  FilterViewController.m
//  Gauranga
//
//  Created by Vlad Kochergin on 21/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "FilterViewController.h"

#import "FilterViewOutput.h"
#import "CategoryViewController.h"
#import "CityViewController.h"

#import "DPTextField.h"
#import "MultiSelectView.h"
#import "MultiSelectCell.h"
#import "TextCell.h"
#import "TextWithPickerCell.h"
#import "DataManager.h"
#import "CurrencyModel.h"
#import "DynamicPropertyModel.h"
#import "FilterModel.h"

@interface FilterViewController()<UITableViewDataSource, UITableViewDelegate, CategoryViewDelegate, RegionViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UINavigationBar *navBar;

@property (strong, nonatomic) NSArray *dynamicPropertyDataSoure;
@property(nonatomic, strong) NSMutableDictionary *dynamicPropertyArrayForCell;

@property(nonatomic, strong) NSMutableDictionary *cellArray;

@property (strong, nonatomic) NSDictionary *categoryDic;
@property (strong, nonatomic) NSDictionary *cityDic;

@end

@implementation FilterViewController

@synthesize filter;

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.output didTriggerViewReadyEvent];
}

#pragma mark - Методы FilterViewInput

- (void)setupInitialState {
    self.cellArray = [NSMutableDictionary new];
    self.cityDic = [NSDictionary new];
    self.categoryDic = [NSDictionary new];
    self.dynamicPropertyArrayForCell = [NSMutableDictionary new];
    
    if(self.filter){
        self.cityDic = filter.city;
        self.categoryDic = filter.category;
        
        if([filter.category allKeys]){
            self.categoryDic = filter.category;
        }
        
        if(filter.dynamicPropertyFields){
            self.dynamicPropertyArrayForCell =  filter.dynamicPropertyFields;
        }
    }
    
    [self setupColor];
}

#pragma mark - Data

- (void)reloadDataWithDynamicProperty:(NSArray *)array{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    self.dynamicPropertyDataSoure = array;
    [self.tableView reloadData];
}

#pragma mark - UITableView

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60.0f;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    switch (section) {
        case 0:
            return 2;
            break;
            
        case 1:{
            NSInteger rowsInSection = [[self.dynamicPropertyArrayForCell allKeys] count];
            if([self.dynamicPropertyDataSoure count])
                rowsInSection = [self.dynamicPropertyDataSoure count];
            return rowsInSection;
        }
            break;
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"filterCell"];
    
    switch (indexPath.section) {
        case 0:{
            if(indexPath.row == 0){
                cell.textLabel.text = NSLocalizedString(@"category",@"Russian: Категория");
                [cell.imageView setImage:[UIImage imageNamed:@"category"]];
                if([[self.categoryDic allKeys] count]){
                    cell.detailTextLabel.text = [self.categoryDic objectForKey:@"title"];
                    return cell;
                }
                cell.detailTextLabel.text = NSLocalizedString(@"selecCatFilter",@"Russian: Выбрать категорию");
                return cell;
            }
            if(indexPath.row == 1){
                cell.textLabel.text = NSLocalizedString(@"region",@"Russian: Регион");
                [cell.imageView setImage:[UIImage imageNamed:@"location"]];
                if([[self.cityDic allKeys] count]){
                    cell.detailTextLabel.text = [self.cityDic objectForKey:@"title"];
                    return cell;
                }
                cell.detailTextLabel.text = NSLocalizedString(@"selectCityFilter",@"Russian: Выбрать регион");
                return cell;
            }
            break;
        }
        case 1:{
            cell = [self tableView:tableView dynamicPropertyCellForRowAtIndexPath:indexPath];
        }
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.section) {
        case 0:{
            if(indexPath.row == 0){
                [self.output selectCategory];
                return;
            }
            if(indexPath.row == 1){
                [self.output selectRegion];
                return;
            }
        }
            break;
    }
}

#pragma mark - Dynamic Property

- (UITableViewCell *)tableView:(UITableView *)tableView dynamicPropertyCellForRowAtIndexPath:(NSIndexPath *)indexPath{
    DynamicPropertyModel *model =  [self.dynamicPropertyDataSoure objectAtIndex:indexPath.row];
    UITableViewCell *cell;
    
    UITextField *textField = [[self getTextFieldDynamicPropertyWithIndexPath:indexPath] objectAtIndex:1];
    NSInteger type = [model.type integerValue];
    
    if(textField){
        type = [[[self getTextFieldDynamicPropertyWithIndexPath:indexPath] objectAtIndex:0] integerValue];
    }
    
    switch (type) {
        case 1:
        case 2:{
            TextCell *Cell = [tableView dequeueReusableCellWithIdentifier:@"textCellDOP"];
            if(!textField){
                [Cell.textField setTag:model.dynPropID];
                [Cell.textField setPlaceholder:model.title];
                Cell.textField.text = @"";
                [self saveDynamicPropertyWithIndexPath:indexPath textField:Cell.textField type:type];
                return Cell;
            }
            Cell.textField.tag = textField.tag;
            Cell.textField.text = textField.text;
            Cell.textField.placeholder = textField.placeholder;
            [self saveDynamicPropertyWithIndexPath:indexPath textField:Cell.textField type:type];
            cell = Cell;
        }
            break;
        case 4:{
            TextWithPickerCell *Cell = [tableView dequeueReusableCellWithIdentifier:@"textWithPickerCellDOP"];
            if(!textField){
                Cell.textPicker.text = @"";
                Cell.textPicker.selected = 0;
                [Cell.textPicker setDataSource:@[@"NO", @"YES"]];
                [Cell.textPicker setTag:model.dynPropID];
                [Cell.textPicker setPlaceholder:model.title];
                [self saveDynamicPropertyWithIndexPath:indexPath textField:Cell.textPicker type:type];
                return Cell;
            }
            Cell.textPicker.tag = textField.tag;
            Cell.textPicker.dataSource = (NSArray *)[textField performSelector:@selector(dataSource)];
            Cell.textPicker.text = textField.text;
            Cell.textPicker.placeholder = textField.placeholder;
            Cell.textPicker.selectedIndex = (NSInteger)[textField performSelector:@selector(selectedIndex)];
            [self saveDynamicPropertyWithIndexPath:indexPath textField:Cell.textPicker type:type];
            cell = Cell;
        }
            break;
        case 9:
        case 7:{
            MultiSelectCell *Cell = [tableView dequeueReusableCellWithIdentifier:@"multiSelectCellDOP"];
            if(!textField){
                [Cell.textPicker setDataSource:model.list];
                [Cell.textPicker setTag:model.dynPropID];
                [Cell.textPicker setPlaceholder:model.title];
                [self saveDynamicPropertyWithIndexPath:indexPath textField:Cell.textPicker type:type];
                return Cell;
            }
            Cell.textPicker.tag = textField.tag;
            Cell.textPicker.dataSource = (NSArray *)[textField performSelector:@selector(dataSource)];
            Cell.textPicker.text = textField.text;
            Cell.textPicker.placeholder = textField.placeholder;
            Cell.textPicker.selectItemsID = [textField performSelector:@selector(selectItemsID)];
            Cell.textPicker.selectItemsString = [textField performSelector:@selector(selectItemsString)];
            [self saveDynamicPropertyWithIndexPath:indexPath textField:Cell.textPicker type:type];
            cell = Cell;
        }
            break;
        case 8:
        case 6:{
            TextWithPickerCell *Cell =  [tableView dequeueReusableCellWithIdentifier:@"textWithPickerCellDOP"];
            if(!textField){
                Cell.textPicker.text = @"";
                Cell.textPicker.selected = 0;
                [Cell.textPicker setDataSource:model.list];
                [Cell.textPicker setTag:model.dynPropID];
                [Cell.textPicker setPlaceholder:model.title];
                [self saveDynamicPropertyWithIndexPath:indexPath textField:Cell.textPicker type:type];
                return Cell;
            }
            Cell.textPicker.tag = textField.tag;
            Cell.textPicker.dataSource = (NSArray *)[textField performSelector:@selector(dataSource)];
            Cell.textPicker.text = textField.text;
            Cell.textPicker.placeholder = textField.placeholder;
            Cell.textPicker.selectedIndex = (NSInteger)[textField performSelector:@selector(selectedIndex)];
            [self saveDynamicPropertyWithIndexPath:indexPath textField:Cell.textPicker type:type];
            cell = Cell;
        }
            break;
        case 10:{
            TextCell *Cell = [tableView dequeueReusableCellWithIdentifier:@"textCellDOP"];
            if(!textField){
                Cell.textField.text = @"";
                [Cell.textField setKeyboardType:UIKeyboardTypeNumberPad];
                [Cell.textField setTag:model.dynPropID ];
                [Cell.textField setPlaceholder:model.title];
                [self saveDynamicPropertyWithIndexPath:indexPath textField:Cell.textField type:type];
                return Cell;
            }
            [Cell.textField setKeyboardType:UIKeyboardTypeNumberPad];
            Cell.textField.tag = textField.tag;
            Cell.textField.text = textField.text;
            Cell.textField.placeholder = textField.placeholder;
            [self saveDynamicPropertyWithIndexPath:indexPath textField:Cell.textField type:type];
            cell = Cell;
        }
            break;
        default:{
            TextCell *Cell = [tableView dequeueReusableCellWithIdentifier:@"textCellDOP"];
            if(!textField){
                Cell.accessoryView = nil;
                [Cell.textField setTag:model.dynPropID];
                [Cell.textField setPlaceholder:model.title];
                [self saveDynamicPropertyWithIndexPath:indexPath textField:Cell.textField type:type];
                return Cell;
            }
            Cell.textField.tag = textField.tag;
            Cell.textField.text = textField.text;
            Cell.textField.placeholder = textField.placeholder;
            [self saveDynamicPropertyWithIndexPath:indexPath textField:Cell.textField type:type];
            cell = Cell;
        }
            break;
    }
    return cell;
}

- (NSArray *)getTextFieldDynamicPropertyWithIndexPath:(NSIndexPath *)indexPath{
    NSString *key = [NSString stringWithFormat:@"%li:%li", (long)indexPath.section,(long)indexPath.row];
    if([[self.dynamicPropertyArrayForCell allKeys] containsObject:key]){
        return [self.dynamicPropertyArrayForCell objectForKey:key];
    }
    return nil;
}

- (void)saveDynamicPropertyWithIndexPath:(NSIndexPath *)indexPath textField:(UITextField *)textField type:(NSInteger)type{
    NSString *key = [NSString stringWithFormat:@"%li:%li", (long)indexPath.section,(long)indexPath.row];
    NSArray *array = @[[NSString stringWithFormat:@"%li",(long)type],textField];
    [self.dynamicPropertyArrayForCell setValue:array forKey:key];
}

- (NSArray *)getDynamicProperty{
    NSMutableArray *tempArray = [NSMutableArray new];
    for(NSString *key in [self.dynamicPropertyArrayForCell allKeys]){
        UITextField *textField = [[self.dynamicPropertyArrayForCell objectForKey:key] lastObject];
        if(textField.text.length > 0){
            if([textField isKindOfClass:[MultiSelectView class]]){
                MultiSelectView *textfield = (MultiSelectView *)textField;
                //NSMutableArray *result = [NSMutableArray new];
                NSInteger result = 0;
                for (NSString *number in textfield.selectItemsID){
                    //[result addObject:[NSString stringWithFormat:@"%li", (long)pow(2, [number integerValue])]];
                    result += pow(2, [number integerValue]);
                }
                NSDictionary *dic = @{
                                      @"n" : [NSString stringWithFormat:@"%li", (long)textField.tag],
                                      @"v" : [NSString stringWithFormat:@"%li", (long)result]
                                      };
                
                [tempArray addObject:dic];
                
            }else if([textField isKindOfClass:[DPTextField class]]){
                DPTextField *textfield = (DPTextField *)textField;
                NSDictionary *dic = @{
                                      @"n" : [NSString stringWithFormat:@"%li", (long)textField.tag],
                                      @"v" : [NSString stringWithFormat:@"%li", (long)textfield.selectedIndex]
                                      };
                
                [tempArray addObject:dic];
            }else{
                NSDictionary *dic = @{
                                      @"n" : [NSString stringWithFormat:@"%li", (long)textField.tag],
                                      @"v" : textField.text
                                      };
                [tempArray addObject:dic];
            }
        }
    }
    return tempArray;
}

#pragma mark - Action

- (IBAction)cancelButton:(UIBarButtonItem *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)applyFilter:(UIBarButtonItem *)sender {
    [self applyFilter];
}

#pragma mark - Category Delegate

- (void)selectCategory:(NSDictionary *)dic{
    self.categoryDic = dic;
    [self.cellArray removeAllObjects];
    self.dynamicPropertyArrayForCell = [NSMutableDictionary new];
    
    if(!dic){
        self.dynamicPropertyDataSoure = [NSArray new];
        return;
    }
    
    NSString *categoryId = [dic objectForKey:[[dic allKeys] lastObject]];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self.output getDynamicPropertyWithCategoryId:categoryId];
}

#pragma mark - Region Delegate

- (void)selectRegion:(NSDictionary *)dic{
    self.cityDic = dic;
    [self.tableView reloadData];
}

#pragma mark - Apply Filter

- (void)applyFilter{
    FilterModel *filterModel = [FilterModel new];
    filterModel.category = self.categoryDic;
    filterModel.city = self.cityDic;
    filterModel.dynamicproperty = [self getDynamicProperty];
    
    NSMutableDictionary *test = [NSMutableDictionary new];
    for(NSString *key in [self.dynamicPropertyArrayForCell allKeys]){
        NSString *type = [[self.dynamicPropertyArrayForCell objectForKey:key] firstObject];
        UITextField *textFieldCopy = [[self.dynamicPropertyArrayForCell objectForKey:key] lastObject];
        NSArray *array = @[type,textFieldCopy];
        [test setValue:[array mutableCopy] forKey:key];
    }
    
    
    //[self.dynamicPropertyArrayForCell setValue:array forKey:key];
    filterModel.dynamicPropertyFields = [NSMutableDictionary new];
    filterModel.dynamicPropertyFields = self.dynamicPropertyArrayForCell;//[test copy];
    
    if(self.delegate){
        [self.delegate applyFilter:filterModel];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Additions

- (void)setupColor{
    [self.view setBackgroundColor:[UIColor mainColor]];
    [self.navBar setBarTintColor:[UIColor mainColor]];
}

@end
