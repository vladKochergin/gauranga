//
//  FilterViewInput.h
//  Gauranga
//
//  Created by Vlad Kochergin on 21/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FilterModel;

@protocol FilterViewInput <NSObject>

/**
 @author Vlad Kochergin

 Метод настраивает начальный стейт view
 */

@property(nonatomic, strong) FilterModel *filter;

- (void)setupInitialState;
- (void)reloadDataWithDynamicProperty:(NSArray *)array;

@end
