//
//  FilterViewController.h
//  Gauranga
//
//  Created by Vlad Kochergin on 21/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FilterViewInput.h"

@class FilterModel;

@protocol FilterViewOutput;

@protocol FilterDelegate

- (void)applyFilter:(FilterModel *)filter;

@end

@interface FilterViewController : UIViewController <FilterViewInput>

@property (nonatomic, strong) id<FilterViewOutput> output;
@property (nonatomic, weak) id<FilterDelegate> delegate;

@end
