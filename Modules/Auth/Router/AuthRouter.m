//
//  AuthRouter.m
//  Gauranga
//
//  Created by Vlad Kochergin on 09/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "AuthRouter.h"
#import "AuthViewController.h"
#import "RegistrationViewController.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation AuthRouter

#pragma mark - Методы AuthRouterInput

-(void)showRegistrationViewContoller {
        
    RamblerViperModuleFactory *factory = [self transitionModuleFactory];
    [[self.transitionHandler openModuleUsingFactory:factory withTransitionBlock:^(id<RamblerViperModuleTransitionHandlerProtocol> sourceModuleTransitionHandler, id<RamblerViperModuleTransitionHandlerProtocol> destinationModuleTransitionHandler) {
        
        AuthViewController *source = (id) sourceModuleTransitionHandler;
        RegistrationViewController *destinationViewController = (id) destinationModuleTransitionHandler;
        [source.navigationController pushViewController:destinationViewController animated:YES];
        
    }] thenChainUsingBlock:^id<RamblerViperModuleOutput>(id<RamblerViperModuleInput> moduleInput) {
        return nil;
    }];
}

- (RamblerViperModuleFactory *)transitionModuleFactory{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    NSString *indfViewController = @"RegistrationViewController";
    RamblerViperModuleFactory *factory = [[RamblerViperModuleFactory alloc]initWithStoryboard:storyBoard andRestorationId:indfViewController];
    return factory;
}

@end
