//
//  AuthRouter.h
//  Gauranga
//
//  Created by Vlad Kochergin on 09/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "AuthRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface AuthRouter : NSObject <AuthRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
