//
//  AuthViewController.h
//  Gauranga
//
//  Created by Vlad Kochergin on 09/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AuthViewInput.h"

@protocol AuthViewOutput;

@interface AuthViewController : UIViewController <AuthViewInput>

@property (nonatomic, strong) id<AuthViewOutput> output;

@end
