//
//  AuthViewController.m
//  Gauranga
//
//  Created by Vlad Kochergin on 09/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "AuthViewController.h"

#import "AuthViewOutput.h"

@interface AuthViewController()

@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UITextField *password;

@property (weak, nonatomic) IBOutlet UIButton *signInButton;
@property (weak, nonatomic) IBOutlet UIButton *signUpButton;
@property (weak, nonatomic) IBOutlet UIButton *forgotPassword;

@end

@implementation AuthViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.output didTriggerViewReadyEvent];
}

#pragma mark - Методы AuthViewInput

- (void)setupInitialState {
    [self setupColor];
    self.navigationItem.title = NSLocalizedString(@"login", @"Russian: Вход");
}

- (void)sigInCompletion {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)signInFailedComletion {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}


#pragma mark - Action

- (IBAction)signIn:(UIButton *)sender {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self.output sigInWithUserName:self.email.text password:self.password.text];
}

- (IBAction)signUp:(UIButton *)sender {
    [self.output showRegistrationViewController];
}

- (IBAction)forgotPassword:(UIButton *)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:kForgotPasswordURL]];
}

#pragma mark - Additions

- (void)setupColor{
    [self.view setBackgroundColor:[UIColor mainColor]];
    [self.signInButton setBackgroundColor:[UIColor mainColor]];
    [self.signUpButton setBackgroundColor:[UIColor mainColor]];
    [self.forgotPassword setTitleColor:[UIColor mainColor] forState:UIControlStateNormal];
}

@end
