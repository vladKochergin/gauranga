//
//  AuthAssembly.m
//  Gauranga
//
//  Created by Vlad Kochergin on 09/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "AuthAssembly.h"

#import "AuthViewController.h"
#import "AuthInteractor.h"
#import "AuthPresenter.h"
#import "AuthRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation AuthAssembly

- (AuthViewController *)viewAuth {
    return [TyphoonDefinition withClass:[AuthViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterAuth]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterAuth]];
                          }];
}

- (AuthInteractor *)interactorAuth {
    return [TyphoonDefinition withClass:[AuthInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterAuth]];
                          }];
}

- (AuthPresenter *)presenterAuth{
    return [TyphoonDefinition withClass:[AuthPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewAuth]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorAuth]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerAuth]];
                          }];
}

- (AuthRouter *)routerAuth{
    return [TyphoonDefinition withClass:[AuthRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewAuth]];
                          }];
}

@end
