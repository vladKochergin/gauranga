//
//  AuthInteractorOutput.h
//  Gauranga
//
//  Created by Vlad Kochergin on 09/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol AuthInteractorOutput <NSObject>

- (void)sigInCompletion;
- (void)signInFailedComletion;

@end
