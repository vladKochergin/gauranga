//
//  AuthInteractorInput.h
//  Gauranga
//
//  Created by Vlad Kochergin on 09/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol AuthInteractorInput <NSObject>

- (void)sigInWithUserName:(NSString *)userName password:(NSString *)password;

@end
