//
//  AuthInteractor.m
//  Gauranga
//
//  Created by Vlad Kochergin on 09/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "AuthInteractor.h"

#import "AuthInteractorOutput.h"
#import "Validation.h"
#import "RequestMapManager.h"

@implementation AuthInteractor

#pragma mark - Методы AuthInteractorInput

- (void)sigInWithUserName:(NSString *)userName password:(NSString *)password{
    if([self validation:userName password:password]){
        [RequestMapManager loginWitnEmail:userName password:password completion:^(NSArray *array, NSDictionary *dictionary, id object) {
            if(!object){
                [self error];
                return;
            }
            [self.output sigInCompletion];
        }];
    } else {
        [self.output signInFailedComletion];
    }
}

- (BOOL)validation:(NSString *)userName password:(NSString *)password{
    if([Validation isValidEmail:userName] &&
       password.length > 5)
        return YES;
    [self error];
    return NO;
}

- (void)error{
    [MBProgressHUD hideHUDForView:[WireFrame getView].view animated:YES];
    [KVAlert alertWithTitle:nil message:NSLocalizedString(@"errorAuth",@"Russian: Неверный email или пароль")
             preferredStyle:UIAlertControllerStyleAlert completion:nil];
}

@end
