//
//  AuthInteractor.h
//  Gauranga
//
//  Created by Vlad Kochergin on 09/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "AuthInteractorInput.h"

@protocol AuthInteractorOutput;

@interface AuthInteractor : NSObject <AuthInteractorInput>

@property (nonatomic, weak) id<AuthInteractorOutput> output;

@end
