//
//  AuthModuleInput.h
//  Gauranga
//
//  Created by Vlad Kochergin on 09/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>

@protocol AuthModuleInput <RamblerViperModuleInput>

/**
 @author Vlad Kochergin

 Метод инициирует стартовую конфигурацию текущего модуля
 */
- (void)configureModule;

@end
