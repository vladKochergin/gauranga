//
//  AuthPresenter.h
//  Gauranga
//
//  Created by Vlad Kochergin on 09/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "AuthViewOutput.h"
#import "AuthInteractorOutput.h"
#import "AuthModuleInput.h"

@protocol AuthViewInput;
@protocol AuthInteractorInput;
@protocol AuthRouterInput;

@interface AuthPresenter : NSObject <AuthModuleInput, AuthViewOutput, AuthInteractorOutput>

@property (nonatomic, weak) id<AuthViewInput> view;
@property (nonatomic, strong) id<AuthInteractorInput> interactor;
@property (nonatomic, strong) id<AuthRouterInput> router;

@end
