//
//  AuthPresenter.m
//  Gauranga
//
//  Created by Vlad Kochergin on 09/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "AuthPresenter.h"

#import "AuthViewInput.h"
#import "AuthInteractorInput.h"
#import "AuthRouterInput.h"

@implementation AuthPresenter

#pragma mark - Методы AuthModuleInput

- (void)configureModule {
    // Стартовая конфигурация модуля, не привязанная к состоянию view
}

#pragma mark - Методы AuthViewOutput

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
}

- (void)sigInWithUserName:(NSString *)userName password:(NSString *)password{
    [self.interactor sigInWithUserName:userName password:password];
}

#pragma mark - Методы AuthInteractorOutput

- (void)sigInCompletion{
    [self.view sigInCompletion];
}

- (void)signInFailedComletion {
    [self.view signInFailedComletion];
}

- (void)showRegistrationViewController {
    [self.router showRegistrationViewContoller];
}

@end
