//
//  CategoryViewController.m
//  Gauranga
//
//  Created by Vlad Kochergin on 08/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "CategoryViewController.h"

#import "CategoryViewOutput.h"
#import "CategoryModel.h"
#import "CatalogViewController.h"
#import "AddAdvertisingViewController.h"
#import "FilterViewController.h"
#import "EditAdvertisingViewController.h"

@interface CategoryViewController()<UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *dataSource;
@property (nonatomic) NSInteger selectCategory;
@property (nonatomic) NSInteger mIndex;

@end

@implementation CategoryViewController

@synthesize preModel;
@synthesize isAddAdvertising;

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.output didTriggerViewReadyEvent];
}

#pragma mark - Методы CategoryViewInput

- (void)setupInitialState {
    
    if(self.isAddAdvertising)
        self.mIndex = 0;
    else
        self.mIndex = 1;
    
    [self.output getData:nil];
    self.navigationItem.title = NSLocalizedString(@"navSelectCategory", @"Russian: Выбор категории");
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

#pragma mark - Data

- (void)reloadData:(NSArray *)categoryArray{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    self.dataSource = categoryArray;
    if([categoryArray count] == 1){
        [self endSelectCaregory];
    }
    [self.tableView reloadData];
}

#pragma mark - UITableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.dataSource count]+self.mIndex;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"categoryCell"];
    
    
    if(indexPath.row == 0 && !self.isAddAdvertising){
        NSString *title;
        NSString *subtitle = @"";
        if(self.preModel){
            title = [NSString stringWithFormat:NSLocalizedString(@"allInCategory%@",
                                                                 @"Russian: Все в категории: %@"), self.preModel.title];
            subtitle = [NSString stringWithFormat:NSLocalizedString(@"itemsInCat",
                                                                    @"Russian: Объявлений в категории: %li"),(long)self.preModel.itemsCount];
        }else{
            title = NSLocalizedString(@"allCategory", @"Russian: Все категории");
        }
        cell.textLabel.text = title;
        cell.detailTextLabel.text = subtitle;
        return cell;
    }
    
    CategoryModel *model = [self.dataSource objectAtIndex:indexPath.row-self.mIndex];
    cell.textLabel.text = model.title;
    cell.detailTextLabel.text = [NSString stringWithFormat:NSLocalizedString(@"itemsInCat",
                                                                             @"Russian: Объявлений в категории: %li"),(long)model.itemsCount];
    if(model.image.length>0){
        NSURL *imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%lio%@", model.imagePath, model.categoryID, model.image]];
        [cell.imageView sd_setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"placeholder"]];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(!self.isAddAdvertising && indexPath.row == 0){
        CategoryModel *catModel = self.preModel;
        if(!self.preModel){
            catModel = [self getFakeModelForAllCategory];
        }
        self.selectCategory = 0;
        self.dataSource = @[catModel];
        [self endSelectCaregory];
        return;
    }
    
    self.selectCategory = indexPath.row-self.mIndex;
    CategoryModel *model = [self.dataSource objectAtIndex:self.selectCategory];
    if(model.subCategoryCount){
        [self.output showNextSubCategoryWithModel:[self.dataSource objectAtIndex:indexPath.row-self.mIndex]];
        return;
    }
    [self endSelectCaregory];
}

- (void)endSelectCaregory{
    NSDictionary *dic = [self getDictionaryForCategory];
    [self.delegate selectCategory:dic];
    for(NSInteger i = [self.navigationController.viewControllers count]-1; i >= 0; i--){
        UIViewController *view = [self.navigationController.viewControllers objectAtIndex:i];
        if([view isKindOfClass:[EditAdvertisingViewController class]] ||
           [view isKindOfClass:[CatalogViewController class]] ||
           [view isKindOfClass:[AddAdvertisingViewController class]] ||
           [view isKindOfClass:[FilterViewController class]]){
            [self.navigationController popToViewController:view animated:YES];
            return;
        }
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
}

- (NSDictionary *)getDictionaryForCategory{
    CategoryModel *model = [self.dataSource objectAtIndex:self.selectCategory];
    if(model.levelNesting != 999){
        NSMutableDictionary *categoryDic = [NSMutableDictionary new];
        if(model.levelNesting >= 1){
            [categoryDic setValue:[NSString stringWithFormat:@"%li", (long)model.categoryID] forKey:@"cat_up"];
        }else{
            [categoryDic setValue:[NSString stringWithFormat:@"%li", (long)model.categoryID] forKey:@"cats_id"];
        }
        [categoryDic setValue:model.title forKey:@"title"];
        return categoryDic;
    }
    return nil;
}

- (CategoryModel *)getFakeModelForAllCategory{
    CategoryModel *model = [CategoryModel new];
    model.title = NSLocalizedString(@"allCategory", @"Russian: Все категории");
    model.levelNesting = 999;
    model.categoryID = 0;
    
    return model;
}

@end
