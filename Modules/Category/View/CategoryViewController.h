//
//  CategoryViewController.h
//  Gauranga
//
//  Created by Vlad Kochergin on 08/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CategoryViewInput.h"

@class CategoryModel;

@protocol CategoryViewDelegate

- (void)selectCategory:(NSDictionary *)dic;

@end

@protocol CategoryViewOutput;

@interface CategoryViewController : UIViewController <CategoryViewInput>

@property (nonatomic, strong) id<CategoryViewOutput> output;
@property (nonatomic, weak) id <CategoryViewDelegate> delegate;

@end
