//
//  CategoryRouter.h
//  Gauranga
//
//  Created by Vlad Kochergin on 08/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "CategoryRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface CategoryRouter : NSObject <CategoryRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
