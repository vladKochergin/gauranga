//
//  CategoryRouterInput.h
//  Gauranga
//
//  Created by Vlad Kochergin on 08/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CategoryModel;
@protocol CategoryRouterInput <NSObject>

- (void)openNextSubCategoryWithModel:(CategoryModel *)model isAddAvertising:(BOOL)isAddAvertising;

@end
