//
//  CategoryRouter.m
//  Gauranga
//
//  Created by Vlad Kochergin on 08/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "CategoryRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

#import "CategoryViewController.h"
#import "CategoryPresenter.h"
@class CategoryModel;

@implementation CategoryRouter

#pragma mark - Методы CategoryRouterInput

- (RamblerViperModuleFactory *)transitionModuleFactory{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    NSString *indfViewController = @"categoryView";
    RamblerViperModuleFactory *factory = [[RamblerViperModuleFactory alloc]initWithStoryboard:storyBoard andRestorationId:indfViewController];
    return factory;
}

- (void)openNextSubCategoryWithModel:(CategoryModel *)model isAddAvertising:(BOOL)isAddAvertising{
    RamblerViperModuleFactory *factory = [self transitionModuleFactory];
    [[self.transitionHandler openModuleUsingFactory:factory withTransitionBlock:^(id<RamblerViperModuleTransitionHandlerProtocol> sourceModuleTransitionHandler, id<RamblerViperModuleTransitionHandlerProtocol> destinationModuleTransitionHandler) {
        
        CategoryViewController *source = (id) sourceModuleTransitionHandler;
        CategoryViewController *destinationViewController = (id) destinationModuleTransitionHandler;
        source.self.navigationItem.backBarButtonItem.title = @" ";
        [destinationViewController setDelegate:source.delegate];
        [source.navigationController pushViewController:destinationViewController animated:YES];
        
        
    }] thenChainUsingBlock:^id<RamblerViperModuleOutput>(id<RamblerViperModuleInput> moduleInput) {
        CategoryPresenter *viewPresenter = (CategoryPresenter *)moduleInput;
        [viewPresenter configureModuleWithModel:model isAddAdvertising:isAddAvertising];
        return nil;
    }];
}


@end
