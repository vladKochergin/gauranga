//
//  CategoryAssembly.m
//  Gauranga
//
//  Created by Vlad Kochergin on 08/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "CategoryAssembly.h"

#import "CategoryViewController.h"
#import "CategoryInteractor.h"
#import "CategoryPresenter.h"
#import "CategoryRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation CategoryAssembly

- (CategoryViewController *)viewCategory {
    return [TyphoonDefinition withClass:[CategoryViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterCategory]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterCategory]];
                          }];
}

- (CategoryInteractor *)interactorCategory {
    return [TyphoonDefinition withClass:[CategoryInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterCategory]];
                          }];
}

- (CategoryPresenter *)presenterCategory{
    return [TyphoonDefinition withClass:[CategoryPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewCategory]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorCategory]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerCategory]];
                          }];
}

- (CategoryRouter *)routerCategory{
    return [TyphoonDefinition withClass:[CategoryRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewCategory]];
                          }];
}

@end
