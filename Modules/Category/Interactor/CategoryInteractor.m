//
//  CategoryInteractor.m
//  Gauranga
//
//  Created by Vlad Kochergin on 08/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "CategoryInteractor.h"

#import "CategoryInteractorOutput.h"
#import "CategoryModel.h"
#import "RequestMapManager.h"

@implementation CategoryInteractor

@synthesize model;

#pragma mark - Методы CategoryInteractorInput
- (void)getData:(CategoryModel *)models{
    self.model = models ? models : self.model;
    if(self.model){
        [RequestMapManager getCategoryWithLVL:self.model.levelNesting+1 pID:self.model.categoryID completion:^(NSArray *array, NSDictionary *dictionary, id object) {
            if(![array count]){
                [self.output reloaData:@[model]];
                return;
            }
            [self.output reloaData:array];
        }];
        return;
    }
    [RequestMapManager getCategoryWithLVL:1 pID:0 completion:^(NSArray *array, NSDictionary *dictionary, id object) {
        [self.output reloaData:array];
    }];
}

@end
