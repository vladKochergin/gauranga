//
//  CategoryInteractorInput.h
//  Gauranga
//
//  Created by Vlad Kochergin on 08/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CategoryModel;

@protocol CategoryInteractorInput <NSObject>

- (void)getData:(CategoryModel *)models;

@property (strong, nonatomic) CategoryModel *model;

@end
