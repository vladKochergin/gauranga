//
//  CategoryInteractor.h
//  Gauranga
//
//  Created by Vlad Kochergin on 08/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "CategoryInteractorInput.h"

@class CategoryModel;

@protocol CategoryInteractorOutput;

@interface CategoryInteractor : NSObject <CategoryInteractorInput>

@property (nonatomic, weak) id<CategoryInteractorOutput> output;

@end
