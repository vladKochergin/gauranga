//
//  CategoryPresenter.h
//  Gauranga
//
//  Created by Vlad Kochergin on 08/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "CategoryViewOutput.h"
#import "CategoryInteractorOutput.h"
#import "CategoryModuleInput.h"

@protocol CategoryViewInput;
@protocol CategoryInteractorInput;
@protocol CategoryRouterInput;

@interface CategoryPresenter : NSObject <CategoryModuleInput, CategoryViewOutput, CategoryInteractorOutput>

@property (nonatomic, weak) id<CategoryViewInput> view;
@property (nonatomic, strong) id<CategoryInteractorInput> interactor;
@property (nonatomic, strong) id<CategoryRouterInput> router;

@end
