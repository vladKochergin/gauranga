//
//  CategoryPresenter.m
//  Gauranga
//
//  Created by Vlad Kochergin on 08/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "CategoryPresenter.h"

#import "CategoryViewInput.h"
#import "CategoryInteractorInput.h"
#import "CategoryRouterInput.h"

@implementation CategoryPresenter

#pragma mark - Методы CategoryModuleInput

- (void)configureModuleWithModel:(id)model isAddAdvertising:(BOOL)isAddAdvertising{
    [self.interactor setModel:model];
    [self.view setPreModel:model];
    [self.view setIsAddAdvertising:isAddAdvertising];
}

#pragma mark - Методы CategoryViewOutput

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
}

- (void)getData:(id)model{
    [self.interactor getData:model];
}

- (void)showNextSubCategoryWithModel:(id)model{
    [self.router openNextSubCategoryWithModel:model isAddAvertising:[self.view isAddAdvertising]];
}

#pragma mark - Методы CategoryInteractorOutput

- (void)reloaData:(NSArray *)categoryArray{
    [self.view reloadData:categoryArray];
}

@end
