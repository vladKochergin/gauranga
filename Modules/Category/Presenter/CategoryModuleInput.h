//
//  CategoryModuleInput.h
//  Gauranga
//
//  Created by Vlad Kochergin on 08/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>

@protocol CategoryModuleInput <RamblerViperModuleInput>

/**
 @author Vlad Kochergin

 Метод инициирует стартовую конфигурацию текущего модуля
 */
- (void)configureModuleWithModel:(id)model isAddAdvertising:(BOOL)isAddAdvertising;

@end
