//
//  EditAdvertisingRouter.h
//  Gauranga
//
//  Created by egor on 04/05/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "EditAdvertisingRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface EditAdvertisingRouter : NSObject <EditAdvertisingRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
