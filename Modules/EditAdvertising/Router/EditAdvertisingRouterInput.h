//
//  EditAdvertisingRouterInput.h
//  Gauranga
//
//  Created by egor on 24/04/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol EditAdvertisingRouterInput <NSObject>

- (void)selectCategoryView;
- (void)selectRegionView;

@end
