//
//  EditAdvertisingInteractorInput.h
//  Gauranga
//
//  Created by egor on 24/04/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DPTextField;
@class CatalogModel;
@class DetailAdvertisingModel;

@protocol EditAdvertisingInteractorInput <NSObject>

- (void)getDynamicPropertyWithCategoryId:(NSString *)catId;
- (void)updateAdvertisingWithId:(NSInteger)identifier title:(UITextField *)title category:(NSDictionary *)category desc:(UITextView *)desc priceType:(DPTextField *)priceType city:(NSDictionary *)city name:(UITextField *)name email:(NSString *)email phone:(UITextField *)phone price:(UITextField *)price dynamProp:(NSArray *)dynamProp imageData:(NSArray *)imageData;
//- (void)setAdvId:(NSInteger)advId;
- (void)getData;

@property(nonatomic) CatalogModel *catalogModel;
@property(nonatomic) DetailAdvertisingModel *detailModel;

@end
