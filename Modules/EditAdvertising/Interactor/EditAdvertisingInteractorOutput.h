//
//  EditAdvertisingInteractorOutput.h
//  Gauranga
//
//  Created by egor on 24/04/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol EditAdvertisingInteractorOutput <NSObject>

- (void)reloadDataWithModel:(id)model;
- (void)reloadDataWithDynamicProperty:(NSArray *)dynamicProperty;
- (void)hideProgressHUD;
- (void)showMessage;

@end
