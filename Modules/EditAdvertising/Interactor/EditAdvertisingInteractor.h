//
//  EditAdvertisingInteractor.h
//  Gauranga
//
//  Created by egor on 04/05/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "EditAdvertisingInteractorInput.h"

@protocol EditAdvertisingInteractorOutput;

@interface EditAdvertisingInteractor : NSObject <EditAdvertisingInteractorInput>

@property (nonatomic, weak) id<EditAdvertisingInteractorOutput> output;

@end
