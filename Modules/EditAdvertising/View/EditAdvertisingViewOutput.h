//
//  EditAdvertisingViewOutput.h
//  Gauranga
//
//  Created by egor on 04/05/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DPTextField;

@protocol EditAdvertisingViewOutput <NSObject>

/**
 @author egor

 Метод сообщает презентеру о том, что view готова к работе
 */
- (void)didTriggerViewReadyEvent;
- (void)getData;
- (void)selectCategoryAction;
- (void)selectRegionViewAction;
- (void)getDynamicPropertyWithCategoryId:(NSString *)catId;
- (void)updateAdvertisingWithId:(NSInteger)identifier title:(UITextField *)title category:(NSDictionary *)category desc:(UITextView *)desc priceType:(DPTextField *)priceType city:(NSDictionary *)city name:(UITextField *)name email:(NSString *)email phone:(UITextField *)phone price:(UITextField *)price dynamProp:(NSArray *)dynamProp imageData:(NSArray *)imageData;


@end
