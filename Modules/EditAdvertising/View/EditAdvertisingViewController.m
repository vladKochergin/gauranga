//
//  EditAdvertisingViewController.m
//  Gauranga
//
//  Created by egor on 24/04/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "EditAdvertisingViewController.h"

#import "EditAdvertisingViewOutput.h"

#import "DetailAdvertisingModel.h"
#import "CategoryModel.h"

#import "imageCell.h"
#import "ButtonCell.h"
#import "TextCell.h"
#import "TextViewCell.h"
#import "TextWithPickerCell.h"
#import "MultiSelectCell.h"

#import "DataManager.h"
#import "CurrencyModel.h"
#import "UserModel.h"

#import "DescriptionCell.h"
#import "PriceCell.h"
#import "FioCell.h"
#import "SendCell.h"

#import "TPKeyboardAvoidingTableView.h"
#import "DPTextField.h"

@interface EditAdvertisingViewController () <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UITextViewDelegate>

@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingTableView *tableView;
@property (nonatomic) int tagIndex;
@property (nonatomic, strong) UserModel *userProfile;
@property (nonatomic, strong) DetailAdvertisingModel *dataSource;
@property (nonatomic, strong) NSString *tempPhoneNumber;

@property (nonatomic, strong) NSDictionary *categoryDic;
@property (nonatomic, strong) NSDictionary *regionDic;

@property (nonatomic, strong) UITextField *advTitle;
@property (nonatomic, strong) UIButton *advCategory;
@property (nonatomic, strong) UITextView *advDescriptions;

@property (nonatomic, strong) DPTextField *advPriceType;
@property (nonatomic, strong) UITextField *advPrice;

@property (nonatomic, strong) UIButton *advCity;
@property (nonatomic, strong) UITextField *advName;
@property (nonatomic, strong) UITextField *advEmail;
@property (nonatomic, strong) UITextField *advPhone;

@property (nonatomic, strong) NSMutableDictionary *cellArray;
@property (nonatomic, strong) NSMutableArray *dynamicPropertyArray;
@property (nonatomic, strong) NSMutableArray *imageArray;

@property (nonatomic, strong)  UIImageView *validDesc;
@property (nonatomic, strong) UILabel *textViewPlaceholder;
@property (nonatomic, strong) UILabel *categoryPlaceholder;

@end

@implementation EditAdvertisingViewController

#pragma mark - Методы жизненного цикла

//- (void)viewWillAppear:(BOOL)animated{
//    [super viewWillAppear:YES];
//    
//    [self.tableView reloadData];
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.output didTriggerViewReadyEvent];
}

#pragma mark - Методы EditAdvertisingViewInput

- (void)setupInitialState {
    self.navigationItem.title = @"Редактирование";
    self.cellArray = [NSMutableDictionary new];
    self.dynamicPropertyArray = [NSMutableArray new];
    self.tagIndex = 0;
    self.userProfile = [[DataManager sharedInstance]getUserProfile];
    self.tableView.estimatedRowHeight = 100.0;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    [self.output getData];
}

#pragma mark - ProgressHUD

- (void)hideProgressHUD{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

#pragma mark - UITableView

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    NSString *indfCell = [self getTypeFieldForSection:indexPath.section];
    if(indexPath.section != 2){
        if([indfCell isEqualToString:@"imageCell"]){
            imageCell *Cell = [tableView dequeueReusableCellWithIdentifier:indfCell];
            for (int i=0; i<[self.imageDataArray count]; i++){
                UIButton *button = [Cell.buttonImageArray objectAtIndex:i];
                
                [button setBackgroundImage:[UIImage imageWithData:self.imageDataArray[i]] forState:UIControlStateNormal];
                [button setRestorationIdentifier:@"1"];
                [button setTag:i];
            }
            
            if(![self.imageButtonArray count]){
                for(UIButton *btn in Cell.buttonImageArray){
                    [[btn imageView] setContentMode: UIViewContentModeScaleAspectFill];
                    [btn addTarget:self action:@selector(addImage:) forControlEvents:UIControlEventTouchUpInside];
                }
                self.imageButtonArray = Cell.buttonImageArray;
                return Cell;
            }
            return Cell;
        }
        if(indexPath.section == 1){
            DescriptionCell *Cell = [tableView dequeueReusableCellWithIdentifier:indfCell];
            if(!self.advTitle){
                [Cell.category addTarget:self action:@selector(selectCategoryAction:) forControlEvents:UIControlEventTouchUpInside];
                [Cell.category setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
                self.advTitle = Cell.title;
                self.advCategory = Cell.category;
                self.advDescriptions = Cell.descriptionAdv;
                [Cell.descriptionAdv setDelegate:self];
                self.validDesc = [Cell.validImageArray objectAtIndex:2];
                
                return Cell;
            }
            
            Cell.title.text = self.dataSource.title;
            Cell.descriptionAdv.text = self.dataSource.descriptionItem;
            [Cell performSelector:@selector(textFieldChange:) withObject:Cell.title];
            [[Cell.validImageArray objectAtIndex:2] setImage:[UIImage imageNamed:@"validTrue"]];
            
            if(self.categoryDic){
                if (!self.categoryPlaceholder) {
                    UILabel *placeholder = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(Cell.category.frame) + 2, CGRectGetMinY(Cell.category.frame), CGRectGetWidth(Cell.category.frame) / 2, 10)];
                    placeholder.font = [UIFont systemFontOfSize:10];
                    placeholder.textColor = [UIColor mainColor];
                    placeholder.text = Cell.category.titleLabel.text;
                    [Cell addSubview:placeholder];
                    self.categoryPlaceholder = placeholder;
                }
                [[Cell.validImageArray objectAtIndex:1] setImage:[UIImage imageNamed:@"validTrue"]];
                [Cell.category setTitle:[self.categoryDic objectForKey:@"title"] forState:UIControlStateNormal];
                [Cell.category setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            }
            self.validDesc = [Cell.validImageArray objectAtIndex:2];
            Cell.title = self.advTitle;
            Cell.category = self.advCategory;
            Cell.descriptionAdv = self.advDescriptions;
            
            return Cell;
        }
        if(indexPath.section == 3){
            PriceCell *Cell = [tableView dequeueReusableCellWithIdentifier:indfCell];
            if(!self.advPrice){
                [Cell.priceType setDataSource:[self getCurrencyArray]];
                self.advPrice = Cell.price;
                self.advPriceType = Cell.priceType;
                return Cell;
            }
            Cell.price.text = self.dataSource.price;
            Cell.priceType.text = [self getCurrencyArray][self.dataSource.curency - 1];
            [Cell performSelector:@selector(textFieldChange:) withObject:Cell.priceType];
            [Cell performSelector:@selector(textFieldChange:) withObject:Cell.price];
            Cell.priceType = self.advPriceType;
            Cell.price = self.advPrice;
            
            return Cell;
        }
        if(indexPath.section == 4) {
            FioCell *Cell = [tableView dequeueReusableCellWithIdentifier:indfCell];
            
            if(!self.advCity) {
                [Cell.city setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
                if (self.dataSource) {
                    UILabel *placeholder = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(Cell.city.frame) + 2, CGRectGetMinY(Cell.city.frame), CGRectGetWidth(Cell.city.frame) / 2, 10)];
                    placeholder.font = [UIFont systemFontOfSize:10];
                    placeholder.textColor = [UIColor mainColor];
                    placeholder.text = Cell.city.titleLabel.text;
                    [Cell addSubview:placeholder];
                    self.advCity = Cell.city;
                    
                    [[Cell.validImageArray objectAtIndex:0] setImage:[UIImage imageNamed:@"validTrue"]];
                    [Cell.city setTitleColor:[UIColor blackColor]forState:UIControlStateNormal];
                    [Cell.city setTitle:self.dataSource.city forState:UIControlStateNormal];
                    NSMutableString *phones = [NSMutableString new];
                    for (NSString *phone in self.dataSource.phones) {
                        [phones appendString:phone];
                        if (![phone isEqual:self.dataSource.phones.lastObject]) {
                            [phones appendString:@", "];
                        }
                    }
                    Cell.phone.text = phones;
                }
                [Cell.city addTarget:self action:@selector(selectRegionAction:) forControlEvents:UIControlEventTouchUpInside];
                Cell.name.text = self.userProfile.name;
                self.advName = Cell.name;
                self.advPhone = Cell.phone;
                [Cell textFieldChange:Cell.name];
                [Cell textFieldChange:Cell.phone];
                return Cell;
            }
            
            if(self.regionDic){
                [[Cell.validImageArray objectAtIndex:0] setImage:[UIImage imageNamed:@"validTrue"]];
                [Cell.city setTitle:[self.regionDic objectForKey:@"title"] forState:UIControlStateNormal];
                [Cell.city setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            }

            [Cell.city addTarget:self action:@selector(selectRegionAction:) forControlEvents:UIControlEventTouchUpInside];
            Cell.name.text = self.userProfile.name;
            [Cell textFieldChange:Cell.name];
//            if (self.dataSource.phones.count) {
//                NSMutableString *phones = [@"" mutableCopy];
//                for (NSString *phone in self.dataSource.phones) {
//                    [phones appendString:phone];
//                    if (![phone isEqual:self.dataSource.phones.lastObject]) {
//                        [phones appendString:@", "];
//                    }
//                }
//                Cell.phone.text = phones;
//            }
            if (Cell.phone.text.length) {
                [Cell textFieldChange:Cell.phone];
            }
            Cell.city = self.advCity;
            Cell.name = self.advName;
            self.advPhone = Cell.phone;
            if (self.tempPhoneNumber.length) {
                Cell.phone.text = self.tempPhoneNumber;
                self.advPhone.text = self.tempPhoneNumber;
            }
            
            return Cell;
        }
        if(indexPath.section == 5){
            SendCell *Cell = [tableView dequeueReusableCellWithIdentifier:indfCell];
            [Cell.sendButton addTarget:self action:@selector(updateAdvertising) forControlEvents:UIControlEventTouchUpInside];
            return Cell;
        }
    }else{
        cell = [self tableView:tableView dynamicPropertyCellForRowAtIndexPath:indexPath];
        return cell;
    }
    return cell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView dynamicPropertyCellForRowAtIndexPath:(NSIndexPath *)indexPath{
    DynamicPropertyModel *model =  [self.dynamicPropertyDataSourсe objectAtIndex:indexPath.row];
    UITableViewCell *cell;
    
    switch ([model.type integerValue]) {
        case 1:
        case 2:{
            TextCell *Cell = (TextCell*)[self createCellWithIndexPath:indexPath tableView:tableView reuserIndfCell:@"textCellDOP"];
            if(!Cell){
                Cell = [tableView dequeueReusableCellWithIdentifier:@"textCellDOP"];
                [Cell.textField setTag:model.dynPropID];
                [Cell.textField setPlaceholder:model.title];
                if (![model.value isEqualToString:@""]) {
                    [Cell.textField setText:model.value];
                }
                [Cell.textField addTarget:self action:@selector(dopPropertyDidEndEditing:) forControlEvents:UIControlEventEditingDidEnd];
                [self saveCell:Cell indexPath:indexPath];
            }
            cell = Cell;
        }
            break;
        case 4:{
            TextWithPickerCell *Cell = (TextWithPickerCell*)[self createCellWithIndexPath:indexPath tableView:tableView reuserIndfCell:@"textWithPickerCellDOP"];
            if(!Cell){
                Cell = [tableView dequeueReusableCellWithIdentifier:@"textWithPickerCellDOP"];
                [Cell.textPicker addTarget:self action:@selector(dopPropertyDidEndEditing:) forControlEvents:UIControlEventEditingDidEnd];
                [Cell.textPicker setDataSource:@[@"NO", @"YES"]];
                [Cell.textPicker setTag:model.dynPropID];
                [Cell.textPicker setPlaceholder:model.title];
                [Cell.textPicker setText:model.value];
                
                [self saveCell:Cell indexPath:indexPath];
            }
            cell = Cell;
        }
            break;
        case 9:
        case 7:{
            MultiSelectCell *Cell = (MultiSelectCell*)[self createCellWithIndexPath:indexPath tableView:tableView reuserIndfCell:@"multiSelectCellDOP"];
            MultiSelectCell *CellNew = [tableView dequeueReusableCellWithIdentifier:@"multiSelectCellDOP"];
            if(!Cell){
                NSMutableArray *selectedItemsIds = [NSMutableArray new];
                NSArray *valuesArray = [model.value componentsSeparatedByString:@", "];
                for (NSString *value in model.list) {
                    for (NSString *selectedValue in valuesArray) {
                        if ([selectedValue isEqualToString:value]) {
                            [selectedItemsIds addObject:[NSString stringWithFormat:@"%li", [model.list indexOfObject:value]]];
                        }
                    }
                }
                [CellNew.textPicker setSelectItemsID:[selectedItemsIds mutableCopy]];
                [CellNew.textPicker setSelectItemsString:[valuesArray mutableCopy]];
                [CellNew.textPicker setText];
                [CellNew.textPicker setDataSource:model.list];
                [CellNew.textPicker setTag:model.dynPropID];
                [CellNew.textPicker setPlaceholder:model.title];
                [self saveCell:CellNew indexPath:indexPath];
                return CellNew;
            }
            CellNew.textPicker = Cell.textPicker;
            cell = CellNew;
        }
            break;
        case 8:
        case 6:{
            TextWithPickerCell *Cell = (TextWithPickerCell*)[self createCellWithIndexPath:indexPath tableView:tableView reuserIndfCell:@"textWithPickerCellDOP"];
            if(!Cell){
                Cell =  [tableView dequeueReusableCellWithIdentifier:@"textWithPickerCellDOP"];
                [Cell.textPicker addTarget:self action:@selector(dopPropertyDidEndEditing:) forControlEvents:UIControlEventEditingDidEnd];
                [Cell.textPicker setDataSource:model.list];
                [Cell.textPicker setTag:model.dynPropID];
                [Cell.textPicker setPlaceholder:model.title];
                [Cell.textPicker setText:model.value];
                [self saveCell:Cell indexPath:indexPath];
            }
            cell = Cell;
        }
            break;
        case 10:{
            TextCell *Cell = (TextCell*)[self createCellWithIndexPath:indexPath tableView:tableView reuserIndfCell:@"textCellDOP"];
            if(!Cell){
                Cell = [tableView dequeueReusableCellWithIdentifier:@"textCellDOP"];
                [Cell.textField addTarget:self action:@selector(dopPropertyDidEndEditing:) forControlEvents:UIControlEventEditingDidEnd];
                [Cell.textField setKeyboardType:UIKeyboardTypeNumberPad];
                [Cell.textField setTag:model.dynPropID ];
                [Cell.textField setPlaceholder:model.title];
                if (![model.value isEqualToString:@""]) {
                    [Cell.textField setText:model.value];
                }
                [self saveCell:Cell indexPath:indexPath];
            }
            cell = Cell;
        }
            break;
        default:{
            TextCell *Cell = (TextCell*)[self createCellWithIndexPath:indexPath tableView:tableView reuserIndfCell:@"textCellDOP"];
            if(!Cell){
                Cell = [tableView dequeueReusableCellWithIdentifier:@"textCellDOP"];
                [Cell.textField addTarget:self action:@selector(dopPropertyDidEndEditing:) forControlEvents:UIControlEventEditingDidEnd];
                Cell.accessoryView = nil;
                [Cell.textField setTag:model.dynPropID];
                [Cell.textField setPlaceholder:model.title];
                if (![model.value isEqualToString:@""]) {
                    [Cell.textField setText:model.value];
                }
                [self saveCell:Cell indexPath:indexPath];
            }
            cell = Cell;
        }
            break;
    }
    return cell;
}

#pragma mark - TextViewDelegate

- (void)textViewDidChange:(UITextView *)textView{
    [self.tableView beginUpdates];
    if (!textView.text.length) {
        [UIView animateWithDuration:0.3 animations:^{
            self.textViewPlaceholder.alpha = 0;
        }];
    } else {
        self.textViewPlaceholder.alpha = 1;
    }
    [self setDescHeight:textView.contentSize.height + 105];
    [self.validDesc setImage:[UIImage imageNamed:@"validFalse"]];
    if(textView.text.length > 11){
        [self.validDesc setImage:[UIImage imageNamed:@"validTrue"]];
    }
    [self.tableView endUpdates];
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    [self.tableView beginUpdates];
    if (!textView.text.length) {
        [UIView animateWithDuration:0.3 animations:^{
            [self.textViewPlaceholder setAlpha:0];
        }];
    }
    [self.tableView endUpdates];
}


#pragma mark - Dynamic Property

- (UITableViewCell *)createCellWithIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView reuserIndfCell:(NSString *)reuseCell{
    NSString *key = [NSString stringWithFormat:@"%li:%li", (long)indexPath.section,(long)indexPath.row];
    if([[self.cellArray allKeys] containsObject:key]){
        return [self.cellArray objectForKey:key];
    }
    return nil;
}

- (void)saveCell:(UITableViewCell *)cell indexPath:(NSIndexPath *)indexPath{
    NSString *key = [NSString stringWithFormat:@"%li:%li", (long)indexPath.section,(long)indexPath.row];
    [self.cellArray setValue:cell forKey:key];
}

- (NSArray *)getCurrencyArray{
    NSMutableArray *currencyArray = [NSMutableArray new];
    NSDictionary *currensyDict = [[DataManager sharedInstance] getCurrency];
    for(int i = 1; i <= [currensyDict allValues].count; i++){
        CurrencyModel *curModel = [currensyDict objectForKey:@(i).stringValue];
        [currencyArray addObject:curModel.title];
    }
    return currencyArray;
}

- (void)dopPropertyDidEndEditing:(UITextField *)textField{
    NSMutableArray *tempArray = [NSMutableArray new];
    if([textField isKindOfClass:[DPTextField class]]){
        DPTextField *textfield = (DPTextField *)textField;
        NSDictionary *dict = @{
                               [NSString stringWithFormat:@"%li", (long)textField.tag] : [NSString stringWithFormat:@"%li", (long)textfield.selectedIndex]
                               };
        
        for(NSDictionary *dicTwo in self.dynamicPropertyArray){
            if ([[dicTwo allKeys] containsObject:[NSString stringWithFormat:@"%li", (long)textField.tag]]){
                [tempArray addObject:dict];
            } else {
                [tempArray addObject:dicTwo];
            }
        }
        self.dynamicPropertyArray = [tempArray mutableCopy];
        return;
    }
    NSDictionary *dic = @{
                          [NSString stringWithFormat:@"%li", (long)textField.tag] : textField.text
                          };
    for(NSDictionary *dicTwo in self.dynamicPropertyArray){
        if ([[dicTwo allKeys] containsObject:[NSString stringWithFormat:@"%li", (long)textField.tag]]){
            [tempArray addObject:dic];
        } else {
            [tempArray addObject:dicTwo];
        }
    }
    self.dynamicPropertyArray = [tempArray mutableCopy];
}

#pragma mark - CategoryDelegate

- (void)selectCategory:(NSDictionary *)dictionary {
    self.categoryDic = dictionary;
    NSString *categoryId = [dictionary objectForKey:[[dictionary allKeys] lastObject]];
    [self.output getDynamicPropertyWithCategoryId:categoryId];
}

- (void)selectRegion:(NSDictionary *)dictionary {
    self.regionDic = dictionary;
    NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:4];
    [self.tableView beginUpdates];
    [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
    [self.tableView endUpdates];
}

- (void)selectCategoryAction:(UIButton *)sender {
    [self.output selectCategoryAction];
}

- (void)selectRegionAction:(UIButton *)sender {
    [self savePhoneNumber];
    [self.output selectRegionViewAction];
}

#pragma marl - Data

- (void)showMessage {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [KVAlert alertWithTitle:nil message:@"Cохранено" preferredStyle:UIAlertControllerStyleAlert completion:^{
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

- (void)reloadDataWithModel:(id)model {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (model) {
            self.dataSource = model;
            NSMutableArray *dataArray = [NSMutableArray new];
            for (NSString *urlString in self.dataSource.images) {
                NSURL *url = [NSURL URLWithString:urlString];
                UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
                [img sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"avatarPlaceholder"]];
                [img setContentMode:UIViewContentModeScaleAspectFill];
                NSData *imageData = UIImageJPEGRepresentation(img.image, 1.0);
                [dataArray addObject:imageData];
            }
            CategoryModel *category = [self.dataSource.categories lastObject];
            NSMutableDictionary *categoryDic = [@{@"title":category.title, @"cat_up":@(category.categoryID)} mutableCopy];
            NSMutableDictionary *regionDic = [@{@"title":self.dataSource.city, @"city_id":@(self.dataSource.cityId)} mutableCopy];
            [self reloadDataWithDynamicProperty: self.dataSource.dynamicPropertyArray];
            
            NSMutableArray *temporaryArray = [NSMutableArray new];
            for (DynamicPropertyModel *dynPropModel in self.dataSource.dynamicPropertyArray) {
                NSDictionary *dic = @{
                                      [NSString stringWithFormat:@"%li", (long)dynPropModel.dynPropID] : dynPropModel.value
                                      };
                [temporaryArray addObject:dic];
            }
            self.dynamicPropertyArray = [temporaryArray mutableCopy];
            
            self.regionDic = regionDic;
            self.categoryDic = categoryDic;
            self.imageDataArray = dataArray;
            
            [self.tableView reloadData];
        }
    });
}

- (void)reloadDataWithDynamicProperty:(NSArray *)dynamicProperty {
    self.dynamicPropertyDataSourсe = dynamicProperty;
    [self.tableView reloadData];
}

- (void)addMultiselectProperty {
    for(UITableViewCell *cell in [self.cellArray allValues]) {
        
        if([cell isKindOfClass:[MultiSelectCell class]]) {
            MultiSelectCell *multiSelectCell = (MultiSelectCell *)cell;
            NSMutableArray *result = [NSMutableArray new];
            
            for (NSString *number in multiSelectCell.textPicker.selectItemsID){
                [result addObject:[NSString stringWithFormat:@"%li", (long)pow(2, [number integerValue])]];
            }
            NSDictionary *dic = @{
                                  [NSString stringWithFormat:@"%li", (long)multiSelectCell.textPicker.tag] : result
                                  };
            for(NSDictionary *dicTwo in self.dynamicPropertyArray) {
                if ([[dicTwo allKeys] containsObject:[NSString stringWithFormat:@"%li", (long)multiSelectCell.textPicker.tag]]){
                    [self.dynamicPropertyArray removeObject:dicTwo];
                    break;
                }
            }
            [self.dynamicPropertyArray addObject:dic];
        }
    }
}

- (void)savePhoneNumber {
    FioCell *fioCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:4]];
    NSString *phoneNumber = fioCell.phone.text;
    self.tempPhoneNumber = phoneNumber;
}

- (void)didFinishPhoneNumber {
    
    FioCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0
                                                                             inSection:4]];
    if ([cell isKindOfClass:[FioCell class]]) {
        cell.phone.delegate = self;
        [cell.phone.delegate textFieldDidEndEditing:cell.phone];
    }
}

- (void)updateAdvertising {
    [self addMultiselectProperty];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self.output updateAdvertisingWithId:self.dataSource.advId title:self.advTitle category:self.categoryDic desc:self.advDescriptions priceType:self.advPriceType city:self.regionDic name:self.advName email:self.userProfile.email phone:self.advPhone price:self.advPrice dynamProp:self.dynamicPropertyArray imageData:self.imageDataArray];
}

@end
