//
//  EditAdvertisingViewController.h
//  Gauranga
//
//  Created by egor on 04/05/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "EditAdvertisingViewInput.h"
#import "RootAddAdvertisingViewController.h"

@protocol EditAdvertisingViewOutput;

@interface EditAdvertisingViewController : RootAddAdvertisingViewController <EditAdvertisingViewInput>

@property (nonatomic, strong) id<EditAdvertisingViewOutput> output;

@end
