//
//  EditAdvertisingViewInput.h
//  Gauranga
//
//  Created by egor on 04/05/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol EditAdvertisingViewInput <NSObject>

/**
 @author egor

 Метод настраивает начальный стейт view
 */
- (void)setupInitialState;
- (void)reloadDataWithModel:(id)model;
- (void)reloadDataWithDynamicProperty:(NSArray *)dynamicProperty;
- (void)hideProgressHUD;
- (void)showMessage;

@end
