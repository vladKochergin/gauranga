//
//  EditAdvertisingAssembly.m
//  Gauranga
//
//  Created by egor on 04/05/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "EditAdvertisingAssembly.h"

#import "EditAdvertisingViewController.h"
#import "EditAdvertisingInteractor.h"
#import "EditAdvertisingPresenter.h"
#import "EditAdvertisingRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation EditAdvertisingAssembly

- (EditAdvertisingViewController *)viewEditAdvertising {
    return [TyphoonDefinition withClass:[EditAdvertisingViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterEditAdvertising]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterEditAdvertising]];
                          }];
}

- (EditAdvertisingInteractor *)interactorEditAdvertising {
    return [TyphoonDefinition withClass:[EditAdvertisingInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterEditAdvertising]];
                          }];
}

- (EditAdvertisingPresenter *)presenterEditAdvertising{
    return [TyphoonDefinition withClass:[EditAdvertisingPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewEditAdvertising]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorEditAdvertising]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerEditAdvertising]];
                          }];
}

- (EditAdvertisingRouter *)routerEditAdvertising{
    return [TyphoonDefinition withClass:[EditAdvertisingRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewEditAdvertising]];
                          }];
}

@end
