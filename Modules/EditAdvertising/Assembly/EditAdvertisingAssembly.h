//
//  EditAdvertisingAssembly.h
//  Gauranga
//
//  Created by egor on 04/05/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>

/**
 @author egor

 EditAdvertising module
 */
@interface EditAdvertisingAssembly : TyphoonAssembly <RamblerInitialAssembly>

@end
