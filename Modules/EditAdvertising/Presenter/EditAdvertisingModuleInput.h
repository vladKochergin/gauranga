//
//  EditAdvertisingModuleInput.h
//  Gauranga
//
//  Created by egor on 04/05/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>

@class CatalogModel;
@class DetailAdvertisingModel;

@protocol EditAdvertisingModuleInput <RamblerViperModuleInput>

/**
 @author egor

 Метод инициирует стартовую конфигурацию текущего модуля
 */
- (void)configureModuleWithCatalogModel:(CatalogModel *)catalogModel detailModel:(DetailAdvertisingModel *)detailModel;

@end
