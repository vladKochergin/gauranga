//
//  EditAdvertisingPresenter.m
//  Gauranga
//
//  Created by egor on 24/04/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "EditAdvertisingPresenter.h"

#import "EditAdvertisingViewInput.h"
#import "EditAdvertisingInteractorInput.h"
#import "EditAdvertisingRouterInput.h"

@implementation EditAdvertisingPresenter

#pragma mark - Методы EditAdvertisingModuleInput

- (void)configureModuleWithCatalogModel:(CatalogModel *)catalogModel detailModel:(DetailAdvertisingModel *)detailModel {
    [self.interactor setCatalogModel:catalogModel];
    [self.interactor setDetailModel:detailModel];
}

#pragma mark - Методы EditAdvertisingViewOutput

- (void)didTriggerViewReadyEvent {
    [self.view setupInitialState];
}

#pragma mark - Методы EditAdvertisingInteractorOutput

- (void)reloadDataWithModel:(id)model{
    [self.view reloadDataWithModel:model];
}

- (void)getData{
    [self.interactor getData];
}

- (void)selectCategoryAction{
    [self.router selectCategoryView];
}

- (void)getDynamicPropertyWithCategoryId:(NSString *)catId{
    [self.interactor getDynamicPropertyWithCategoryId:catId];
}

- (void)showMessage{
    [self.view showMessage];
}

- (void)selectRegionViewAction{
    [self.router selectRegionView];
}

- (void)updateAdvertisingWithId:(NSInteger)identifier title:(UITextField *)title category:(NSDictionary *)category desc:(UITextView *)desc priceType:(DPTextField *)priceType city:(NSDictionary *)city name:(UITextField *)name email:(NSString *)email phone:(UITextField *)phone price:(UITextField *)price dynamProp:(NSArray *)dynamProp imageData:(NSArray *)imageData{
    
    [self.interactor updateAdvertisingWithId:identifier title:title category:category desc:desc priceType:priceType city:city name:name email:email phone:phone price:price dynamProp:dynamProp imageData:imageData];
}

- (void)hideProgressHUD{
    [self.view hideProgressHUD];
}

- (void)reloadDataWithDynamicProperty:(NSArray *)dynamicProperty{
    [self.view reloadDataWithDynamicProperty:dynamicProperty];
}

@end
