//
//  EditAdvertisingPresenter.h
//  Gauranga
//
//  Created by egor on 04/05/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "EditAdvertisingViewOutput.h"
#import "EditAdvertisingInteractorOutput.h"
#import "EditAdvertisingModuleInput.h"

@protocol EditAdvertisingViewInput;
@protocol EditAdvertisingInteractorInput;
@protocol EditAdvertisingRouterInput;

@interface EditAdvertisingPresenter : NSObject <EditAdvertisingModuleInput, EditAdvertisingViewOutput, EditAdvertisingInteractorOutput>

@property (nonatomic, weak) id<EditAdvertisingViewInput> view;
@property (nonatomic, strong) id<EditAdvertisingInteractorInput> interactor;
@property (nonatomic, strong) id<EditAdvertisingRouterInput> router;

@end
