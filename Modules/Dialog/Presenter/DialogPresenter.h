//
//  DialogPresenter.h
//  Gauranga
//
//  Created by Vlad Kochergin on 27/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "DialogViewOutput.h"
#import "DialogInteractorOutput.h"
#import "DialogModuleInput.h"

@protocol DialogViewInput;
@protocol DialogInteractorInput;
@protocol DialogRouterInput;

@interface DialogPresenter : NSObject <DialogModuleInput, DialogViewOutput, DialogInteractorOutput>

@property (nonatomic, weak) id<DialogViewInput> view;
@property (nonatomic, strong) id<DialogInteractorInput> interactor;
@property (nonatomic, strong) id<DialogRouterInput> router;

@end
