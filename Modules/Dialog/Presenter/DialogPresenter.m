//
//  DialogPresenter.m
//  Gauranga
//
//  Created by Vlad Kochergin on 27/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "DialogPresenter.h"

#import "DialogViewInput.h"
#import "DialogInteractorInput.h"
#import "DialogRouterInput.h"

@implementation DialogPresenter

#pragma mark - Методы DialogModuleInput

- (void)configureModule {
    // Стартовая конфигурация модуля, не привязанная к состоянию view
}

#pragma mark - Методы DialogViewOutput

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
}

- (void)getData{
    [self.interactor getData];
}

- (void)selectDialogWithInterlocutorId:(NSInteger)interlocutorId partnerName:(NSString *)partnerName{
    [self.router selectDialogWithInterlocutorId:interlocutorId partnerName:partnerName];
}

#pragma mark - Методы DialogInteractorOutput

- (void)reloadData:(NSArray *)messageArray{
    [self.view reloadData:messageArray];
}

@end
