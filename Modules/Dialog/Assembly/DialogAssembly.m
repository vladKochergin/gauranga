//
//  DialogAssembly.m
//  Gauranga
//
//  Created by Vlad Kochergin on 27/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "DialogAssembly.h"

#import "DialogViewController.h"
#import "DialogInteractor.h"
#import "DialogPresenter.h"
#import "DialogRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation DialogAssembly

- (DialogViewController *)viewDialog {
    return [TyphoonDefinition withClass:[DialogViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterDialog]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterDialog]];
                          }];
}

- (DialogInteractor *)interactorDialog {
    return [TyphoonDefinition withClass:[DialogInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterDialog]];
                          }];
}

- (DialogPresenter *)presenterDialog{
    return [TyphoonDefinition withClass:[DialogPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewDialog]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorDialog]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerDialog]];
                          }];
}

- (DialogRouter *)routerDialog{
    return [TyphoonDefinition withClass:[DialogRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewDialog]];
                          }];
}

@end
