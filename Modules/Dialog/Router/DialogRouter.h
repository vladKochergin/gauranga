//
//  DialogRouter.h
//  Gauranga
//
//  Created by Vlad Kochergin on 27/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "DialogRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface DialogRouter : NSObject <DialogRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
