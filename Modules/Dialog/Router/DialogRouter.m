//
//  DialogRouter.m
//  Gauranga
//
//  Created by Vlad Kochergin on 27/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "DialogRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

#import "ChatModuleInput.h"

@implementation DialogRouter

#pragma mark - Методы DialogRouterInput

- (void)selectDialogWithInterlocutorId:(NSInteger)interlocutorId partnerName:(NSString *)partnerName{
    [[self.transitionHandler openModuleUsingSegue:@"showChatViewController"] thenChainUsingBlock:^id<RamblerViperModuleOutput>(id<ChatModuleInput> moduleInput) {
        [moduleInput configureModuleWithInterlocutorId:interlocutorId partnerName:partnerName];
        return nil;
    }];
}
@end
