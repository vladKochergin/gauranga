//
//  DialogRouterInput.h
//  Gauranga
//
//  Created by Vlad Kochergin on 27/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DialogRouterInput <NSObject>

- (void)selectDialogWithInterlocutorId:(NSInteger)interlocutorId partnerName:(NSString *)partnerName;

@end
