//
//  DialogViewController.m
//  Gauranga
//
//  Created by Vlad Kochergin on 27/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "DialogViewController.h"

#import "DialogViewOutput.h"

#import "DialogCell.h"
#import "DialogModel.h"

@interface DialogViewController()<UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) NSArray *dataSource;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSDateFormatter *dateFormatter;

@end

@implementation DialogViewController


#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getData) name:@"ReceiveNewMessageNotification" object:nil];

	[self.output didTriggerViewReadyEvent];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Методы DialogViewInput

- (void)setupInitialState {
    self.navigationItem.title = @"Чат";
    [self.tableView addRefreshControlWithAction:@selector(getData) view:self];
    [self.tableView showRefreshControl];
    [self getData];
    self.dateFormatter = [NSDateFormatter new];
}

#pragma mark - Data

- (void)getData {
    [self.output getData];
}

- (void)reloadData:(NSArray *)messageArray {
    self.dataSource = messageArray;
    [self.tableView reloadDataWithCheck];
}

#pragma mark - UITableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.dataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    DialogCell *cell = [tableView dequeueReusableCellWithIdentifier:@"dialogCell"];
    DialogModel *dialogModel = [self.dataSource objectAtIndex:indexPath.row];
    
    NSString *dialogTitle = [self getDialogTitle:dialogModel];
    
    NSURL *avatarURL = [NSURL URLWithString:dialogModel.avatarUrl];
    
    [cell.image sd_setImageWithURL:avatarURL placeholderImage:[UIImage imageNamed:@"placeholder"]];
    cell.image.layer.cornerRadius = CGRectGetHeight(cell.image.frame)/2;

    cell.title.text = dialogTitle;
    cell.lastMessageDate.text = [self convertDate:dialogModel.lastMessageDate];
    
    if (dialogModel.lastSenderID) {
        cell.author.text = @"Вы";
    } else {
        cell.author.text = @"";
    }
    cell.lastMessage.text = dialogModel.message;

    [cell.unReadMessageCount setHidden:YES];
    if (dialogModel.messageNew.integerValue) {
        cell.unReadMessageCount.text = dialogModel.messageNew;
        cell.unReadMessageCount.layer.cornerRadius = CGRectGetHeight(cell.unReadMessageCount.frame)/2;
        [cell.unReadMessageCount setHidden:NO];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    DialogModel *dialogModel = [self.dataSource objectAtIndex:indexPath.row];
    dialogModel.messageNew = @"0";
    [self.tableView reloadData];
    
    NSString *dialogTitle = [self getDialogTitle:dialogModel];
    [self.output selectDialogWithInterlocutorId:dialogModel.interlocutorId partnerName:dialogTitle];
}

#pragma mark - Additional 

- (NSString *)getDialogTitle:(DialogModel *)model{
    NSString *dialogTitle = model.userName;
    if(!model.userName) {
        dialogTitle = model.userLogin;
    }
    
    if(model.shopTitle) {
        dialogTitle = model.shopTitle;
    }
    return  dialogTitle;
}

- (NSString *)convertDate:(NSString *)dateString {
    self.dateFormatter.dateFormat = @"yyyy-MM-dd HH-mm-ss";
    NSDate *date  = [self.dateFormatter dateFromString:dateString];
    NSDate *now = [NSDate date];
    NSTimeInterval timeInterval = [now timeIntervalSinceDate:date];
    if (timeInterval < 86400) {
        [self.dateFormatter setDateFormat:@"HH:mm"];
    } else {
        [self.dateFormatter setDateFormat:@"yyyy-MM-dd"];
    }
    NSString *newDate = [self.dateFormatter stringFromDate:date];
    return newDate;
}

@end
