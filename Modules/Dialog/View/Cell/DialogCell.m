//
//  DialogCell.m
//  Gauranga
//
//  Created by yvp on 2/27/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import "DialogCell.h"

@implementation DialogCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

#pragma mark - Additions

- (void)setupColor {
}

@end
