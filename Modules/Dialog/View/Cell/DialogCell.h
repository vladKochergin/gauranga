//
//  DialogCell.h
//  Gauranga
//
//  Created by yvp on 2/27/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DialogCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *author;
@property (weak, nonatomic) IBOutlet UILabel *lastMessage;
@property (weak, nonatomic) IBOutlet UILabel *unReadMessageCount;
@property (weak, nonatomic) IBOutlet UILabel *lastMessageDate;

@end
