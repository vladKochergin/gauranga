//
//  DialogViewController.h
//  Gauranga
//
//  Created by Vlad Kochergin on 27/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DialogViewInput.h"

@protocol DialogViewOutput;

@interface DialogViewController : UIViewController <DialogViewInput>

@property (nonatomic, strong) id<DialogViewOutput> output;

@end
