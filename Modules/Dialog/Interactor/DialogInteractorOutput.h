//
//  DialogInteractorOutput.h
//  Gauranga
//
//  Created by Vlad Kochergin on 27/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DialogInteractorOutput <NSObject>

- (void)reloadData:(NSArray *)messageArray;

@end
