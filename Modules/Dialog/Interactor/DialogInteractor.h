//
//  DialogInteractor.h
//  Gauranga
//
//  Created by Vlad Kochergin on 27/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "DialogInteractorInput.h"

@protocol DialogInteractorOutput;

@interface DialogInteractor : NSObject <DialogInteractorInput>

@property (nonatomic, weak) id<DialogInteractorOutput> output;

@end
