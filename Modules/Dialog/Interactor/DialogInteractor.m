//
//  DialogInteractor.m
//  Gauranga
//
//  Created by Vlad Kochergin on 27/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "DialogInteractor.h"

#import "DialogInteractorOutput.h"

#import "RequestMapManager.h"
#import "UserModel.h"
#import "DataManager.h"

@implementation DialogInteractor

#pragma mark - Методы DialogInteractorInput

- (void)getData{
    [RequestMapManager getDialogList:^(NSArray *array, NSDictionary *dictionary, id object) {
        [self.output reloadData:array];
    }];
}

@end
