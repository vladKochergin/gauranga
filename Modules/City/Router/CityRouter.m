//
//  CityRouter.m
//  Gauranga
//
//  Created by Vlad Kochergin on 14/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "CityRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

#import "CityViewController.h"
#import "CityPresenter.h"

@implementation CityRouter

#pragma mark - Методы CityRouterInput

- (RamblerViperModuleFactory *)transitionModuleFactory{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    NSString *indfViewController = @"regionView";
    RamblerViperModuleFactory *factory = [[RamblerViperModuleFactory alloc]initWithStoryboard:storyBoard andRestorationId:indfViewController];
    return factory;
}

- (void)openNextRegionWithModel:(RegionModel *)model isAddAvertising:(BOOL)isAddAvertising{
    RamblerViperModuleFactory *factory = [self transitionModuleFactory];
    [[self.transitionHandler openModuleUsingFactory:factory withTransitionBlock:^(id<RamblerViperModuleTransitionHandlerProtocol> sourceModuleTransitionHandler, id<RamblerViperModuleTransitionHandlerProtocol> destinationModuleTransitionHandler) {
        
        CityViewController *source = (id) sourceModuleTransitionHandler;
        CityViewController *destinationViewController = (id) destinationModuleTransitionHandler;
        source.self.navigationItem.backBarButtonItem.title = @" ";
        [destinationViewController setDelegate:source.delegate];
        [source.navigationController pushViewController:destinationViewController animated:YES];
        
        
    }] thenChainUsingBlock:^id<RamblerViperModuleOutput>(id<RamblerViperModuleInput> moduleInput) {
        CityPresenter *viewPresenter = (CityPresenter *)moduleInput;
        [viewPresenter configureModuleWithModel:model isAddAdvertising:isAddAvertising];
        return nil;
    }];
}

@end
