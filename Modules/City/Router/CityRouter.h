//
//  CityRouter.h
//  Gauranga
//
//  Created by Vlad Kochergin on 14/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "CityRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface CityRouter : NSObject <CityRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
