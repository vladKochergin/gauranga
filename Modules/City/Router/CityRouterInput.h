//
//  CityRouterInput.h
//  Gauranga
//
//  Created by Vlad Kochergin on 14/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RegionModel;

@protocol CityRouterInput <NSObject>

- (void)openNextRegionWithModel:(RegionModel *)model isAddAvertising:(BOOL)isAddAvertising;

@end
