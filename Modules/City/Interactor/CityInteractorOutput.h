//
//  CityInteractorOutput.h
//  Gauranga
//
//  Created by Vlad Kochergin on 14/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CityInteractorOutput <NSObject>

- (void)reloaData:(NSArray *)categoryArray;

@end
