//
//  CityInteractor.h
//  Gauranga
//
//  Created by Vlad Kochergin on 14/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "CityInteractorInput.h"

@protocol CityInteractorOutput;

@interface CityInteractor : NSObject <CityInteractorInput>

@property (nonatomic, weak) id<CityInteractorOutput> output;

@end
