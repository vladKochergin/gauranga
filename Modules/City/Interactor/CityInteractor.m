//
//  CityInteractor.m
//  Gauranga
//
//  Created by Vlad Kochergin on 14/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "CityInteractor.h"

#import "CityInteractorOutput.h"

#import "RequestMapManager.h"
#import "RegionModel.h"

@implementation CityInteractor

@synthesize model;

#pragma mark - Методы CityInteractorInput

- (void)getData:(RegionModel *)models{
    self.model = models ? models : self.model;
    NSInteger sendIndex = 0;
    if(self.model.parentID){
        sendIndex = model.parentID;
    }
    if(self.model.regionID){
        sendIndex = model.regionID;
    }
//    if(self.model.countryID > 1){
//        sendIndex = model.countryID;
//    }
    
    if(self.model){
        [RequestMapManager getRegionWithPid:sendIndex completion:^(NSArray *array, NSDictionary *dictionary, id object) {
            if(![array count]){
                [self.output reloaData:@[model]];
                return;
            }
            [self.output reloaData:array];
        }];
        return;
    }
    
    [RequestMapManager getRegionWithPid:sendIndex completion:^(NSArray *array, NSDictionary *dictionary, id object) {
        [self.output reloaData:array];
    }];
}

@end
