//
//  CityInteractorInput.h
//  Gauranga
//
//  Created by Vlad Kochergin on 14/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RegionModel;
@protocol CityInteractorInput <NSObject>

- (void)getData:(RegionModel *)models;

@property (strong, nonatomic) RegionModel *model;

@end
