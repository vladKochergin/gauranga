//
//  CityPresenter.h
//  Gauranga
//
//  Created by Vlad Kochergin on 14/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "CityViewOutput.h"
#import "CityInteractorOutput.h"
#import "CityModuleInput.h"

@protocol CityViewInput;
@protocol CityInteractorInput;
@protocol CityRouterInput;

@interface CityPresenter : NSObject <CityModuleInput, CityViewOutput, CityInteractorOutput>

@property (nonatomic, weak) id<CityViewInput> view;
@property (nonatomic, strong) id<CityInteractorInput> interactor;
@property (nonatomic, strong) id<CityRouterInput> router;

@end
