//
//  CityPresenter.m
//  Gauranga
//
//  Created by Vlad Kochergin on 14/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "CityPresenter.h"

#import "CityViewInput.h"
#import "CityInteractorInput.h"
#import "CityRouterInput.h"

@implementation CityPresenter

#pragma mark - Методы CityModuleInput

- (void)configureModuleWithModel:(id)model isAddAdvertising:(BOOL)isAddAdvertising{
    [self.interactor setModel:model];
    [self.view setPreModel:model];
    [self.view setIsAddAdvertising:isAddAdvertising];
}

#pragma mark - Методы CityViewOutput

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
}

- (void)getData:(id)model{
    [self.interactor getData:model];
}

- (void)showNextCityWithModel:(id)model{
    [self.router openNextRegionWithModel:model isAddAvertising:self.view.isAddAdvertising];
}

#pragma mark - Методы CityInteractorOutput

- (void)reloaData:(NSArray *)categoryArray{
    [self.view reloadData:categoryArray];
}

@end
