//
//  CityAssembly.m
//  Gauranga
//
//  Created by Vlad Kochergin on 14/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "CityAssembly.h"

#import "CityViewController.h"
#import "CityInteractor.h"
#import "CityPresenter.h"
#import "CityRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation CityAssembly

- (CityViewController *)viewCity {
    return [TyphoonDefinition withClass:[CityViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterCity]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterCity]];
                          }];
}

- (CityInteractor *)interactorCity {
    return [TyphoonDefinition withClass:[CityInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterCity]];
                          }];
}

- (CityPresenter *)presenterCity{
    return [TyphoonDefinition withClass:[CityPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewCity]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorCity]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerCity]];
                          }];
}

- (CityRouter *)routerCity{
    return [TyphoonDefinition withClass:[CityRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewCity]];
                          }];
}

@end
