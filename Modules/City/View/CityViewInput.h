//
//  CityViewInput.h
//  Gauranga
//
//  Created by Vlad Kochergin on 14/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RegionModel;

@protocol CityViewInput <NSObject>

/**
 @author Vlad Kochergin

 Метод настраивает начальный стейт view
 */
- (void)setupInitialState;
- (void)reloadData:(NSArray *)categoryArray;

@property (strong, nonatomic) RegionModel *preModel;
@property (nonatomic) BOOL isAddAdvertising;

@end
