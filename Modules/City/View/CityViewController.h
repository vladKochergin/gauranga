//
//  CityViewController.h
//  Gauranga
//
//  Created by Vlad Kochergin on 14/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CityViewInput.h"

@protocol CityViewOutput;

@protocol RegionViewDelegate

- (void)selectRegion:(NSDictionary *)dic;

@end

@interface CityViewController : UIViewController <CityViewInput>

@property (nonatomic, strong) id<CityViewOutput> output;
@property (nonatomic, weak) id <RegionViewDelegate> delegate;

@end
