//
//  CityViewController.m
//  Gauranga
//
//  Created by Vlad Kochergin on 14/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "CityViewController.h"

#import "CityViewOutput.h"
#import "CatalogViewController.h"
#import "AddAdvertisingViewController.h"
#import "EditAdvertisingViewController.h"

#import "RegionModel.h"

static NSInteger levelRegion = 0; //JOSKO

@interface CityViewController()<UITableViewDelegate, UITableViewDataSource, UINavigationControllerDelegate, UIViewControllerTransitioningDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *dataSource;
@property (nonatomic) NSInteger selectRegion;
@property (nonatomic) NSInteger mIndex;

@end

@implementation CityViewController

@synthesize isAddAdvertising;
@synthesize preModel;

#pragma mark - Методы жизненного цикла

- (void)dealloc{
    if(levelRegion)
        levelRegion--;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.output didTriggerViewReadyEvent];
}

#pragma mark - Методы CityViewInput

- (void)setupInitialState {
    
    [self.output getData:nil];
    if(self.isAddAdvertising)
        self.mIndex = 0;
    else
        self.mIndex = 1;
    
    self.navigationItem.title = NSLocalizedString(@"navRegion", @"Russian: Выбор региона");
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

#pragma mark - Data

- (void)reloadData:(NSArray *)categoryArray{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    self.dataSource = categoryArray;
//    if([categoryArray count] == 1){
//        [self endSelectRegion];
//    }
    [self.tableView reloadData];
}

#pragma mark - UITableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.dataSource count]+self.mIndex;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cityCell"];
    
    if(indexPath.row == 0 && !self.isAddAdvertising){
        NSString *title = [self getTextForFirstItem];
        cell.textLabel.text = title;
        return cell;
    }
    
    RegionModel *model = [self.dataSource objectAtIndex:indexPath.row-self.mIndex];
    cell.textLabel.text = model.title;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(!self.isAddAdvertising && indexPath.row == 0){
        self.selectRegion = 0;
        
        if(!self.preModel){
            self.preModel = [RegionModel new];
        }
        
        self.dataSource = @[self.preModel];
        [self endSelectRegion];
        return;
    }
    levelRegion++;
    self.selectRegion = indexPath.row-self.mIndex;
    RegionModel *model = [self.dataSource objectAtIndex:self.selectRegion];
    if(levelRegion!=3){
        [self.output showNextCityWithModel:model];
        return;
    }
    [self endSelectRegion];
    
}

- (void)endSelectRegion{
    NSDictionary *responseDic = [self getDictionaryForCity];
    [self.delegate selectRegion:responseDic];
    levelRegion = 0;
    for(NSInteger i = [self.navigationController.viewControllers count]-1; i >= 0; i--){
        UIViewController *view = [self.navigationController.viewControllers objectAtIndex:i];
        if([view isKindOfClass:[EditAdvertisingViewController class]] ||
           [view isKindOfClass:[CatalogViewController class]] ||
           [view isKindOfClass:[AddAdvertisingViewController class]]){
            [self.navigationController popToViewController:view animated:YES];
            return;
        }
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
}

- (NSDictionary *)getDictionaryForCity{
    NSMutableDictionary *regionDic = [NSMutableDictionary new];
    RegionModel *model = [self.dataSource objectAtIndex:self.selectRegion];
    if(![self.preModel.title isEqualToString:@""]){
        switch (levelRegion) {
            case 1:
                [regionDic setValue:[NSString stringWithFormat:@"%li",(long)model.regionID] forKey:@"reg1_country"];
                break;
            case 2:
                [regionDic setValue:[NSString stringWithFormat:@"%li",(long)model.regionID] forKey:@"reg2_region"];
                break;
            case 3:
                [regionDic setValue:[NSString stringWithFormat:@"%li",(long)model.regionID] forKey:@"city_id"];
                break;
        }
        [regionDic setValue:model.title forKey:@"title"];
        return regionDic;
    }
    return nil;
}

#pragma mark - Addition

- (NSString *)getTextForFirstItem{
    switch (levelRegion) {
        case 0:
            return NSLocalizedString(@"allСountry", @"Russian: Все страны");
            break;
        case 1:
            return NSLocalizedString(@"allRegion", @"Russian: Все области");
            break;
        case 2:
            return NSLocalizedString(@"allCity", @"Russian: Все города");
            break;
    }
    return nil;
}


@end
