//
//  MyAdvertisingViewController.m
//  Gauranga
//
//  Created by Vlad Kochergin on 20/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "MyAdvertisingViewController.h"

#import "MyAdvertisingViewOutput.h"

#import "DataManager.h"
#import "CurrencyModel.h"
#import "CatalogModel.h"
#import "CatalogCell.h"

@interface MyAdvertisingViewController()<UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(nonatomic, strong) NSMutableArray<CatalogModel *> *dataSource;
@property(nonatomic, strong) NSDictionary *currencyDic;

@property(nonatomic) BOOL pagLoad;

@end

@implementation MyAdvertisingViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];

	[self.output didTriggerViewReadyEvent];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    [self.tableView reloadData];
//    [self getDataLoadNextPage:NO];
}

#pragma mark - Методы MyAdvertisingViewInput

- (void)setupInitialState {
    self.navigationItem.title = NSLocalizedString(@"navMyAdvertising", @"Russian: Мои объявления");
    
    self.dataSource = [NSMutableArray new];
    self.currencyDic = [[DataManager sharedInstance] getCurrency];
    
    //self.sort = @"publicated_less";
    [self.tableView registerNib:[UINib nibWithNibName:@"CatalogCell" bundle:nil] forCellReuseIdentifier:@"catalogCell"];
    [self.tableView addRefreshControlWithAction:@selector(getDataPull) view:self];
    [self.tableView showRefreshControl];
    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.tableView.bounds.size.width, 0.01f)];
    [self getDataPull];
}

#pragma mark - Data

- (void)getDataPull{
    [self getDataLoadNextPage:NO];
}

- (void)getDataLoadNextPage:(BOOL)nextPage{
    self.pagLoad = nextPage;
    [self.output getData:nextPage];
}

- (void)reloadData:(NSArray *)itemArray{
    [self.tableView hiddenRefreshControl];
    self.dataSource = [itemArray mutableCopy];
    [self.tableView reloadDataWithCheck];
}

#pragma mark - UITableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.dataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CatalogCell *cell = [tableView dequeueReusableCellWithIdentifier:@"catalogCell"];
    
    CatalogModel *model = [self.dataSource objectAtIndex:indexPath.row];
    
    [cell setupCellWithModel:model hideStatus:NO hideFavoriteButton:YES];
    
    if((indexPath.row == [self.dataSource count] - 2) && [self.dataSource count] > 9){
        [self getDataLoadNextPage:YES];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    CatalogModel *model = [self.dataSource objectAtIndex:indexPath.row];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.output showDetailAdvertising:model isFavorite:model.isFavorite];
}

//#pragma mark - SlideNavigationController
//
//- (BOOL)slideNavigationControllerShouldDisplayLeftMenu{
//    return YES;
//}

@end
