//
//  MyAdvertisingViewController.h
//  Gauranga
//
//  Created by Vlad Kochergin on 20/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MyAdvertisingViewInput.h"

@protocol MyAdvertisingViewOutput;

@interface MyAdvertisingViewController : UIViewController <MyAdvertisingViewInput>

@property (nonatomic, strong) id<MyAdvertisingViewOutput> output;

@end
