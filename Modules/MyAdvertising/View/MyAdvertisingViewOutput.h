//
//  MyAdvertisingViewOutput.h
//  Gauranga
//
//  Created by Vlad Kochergin on 20/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MyAdvertisingViewOutput <NSObject>

/**
 @author Vlad Kochergin

 Метод сообщает презентеру о том, что view готова к работе
 */
- (void)didTriggerViewReadyEvent;
- (void)getData:(BOOL)nextPage;
- (void)showDetailAdvertising:(id)catalogModel isFavorite:(BOOL)isFavorite;

@end
