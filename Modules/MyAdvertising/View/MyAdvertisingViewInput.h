//
//  MyAdvertisingViewInput.h
//  Gauranga
//
//  Created by Vlad Kochergin on 20/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MyAdvertisingViewInput <NSObject>

/**
 @author Vlad Kochergin

 Метод настраивает начальный стейт view
 */
- (void)setupInitialState;
- (void)reloadData:(NSArray *)itemArray;

@end
