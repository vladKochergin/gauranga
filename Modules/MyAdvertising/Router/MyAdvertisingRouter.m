//
//  MyAdvertisingRouter.m
//  Gauranga
//
//  Created by Vlad Kochergin on 20/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "MyAdvertisingRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

#import "DetailModuleInput.h"
#import "CatalogModel.h"

@implementation MyAdvertisingRouter

#pragma mark - Методы MyAdvertisingRouterInput

- (void)showDetailAdvertising:(CatalogModel *)catalogModel isFavorite:(BOOL)isFavorite{
    [[self.transitionHandler openModuleUsingSegue:@"detailSegue"] thenChainUsingBlock:^id<RamblerViperModuleOutput>(id<DetailModuleInput> moduleInput) {
        [moduleInput configureModuleWithAdvId:catalogModel.itemId model:catalogModel isFavorite:isFavorite isMyAdv:YES];
        return nil;
    }];
}

@end
