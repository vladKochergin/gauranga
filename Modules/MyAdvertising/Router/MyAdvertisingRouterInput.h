//
//  MyAdvertisingRouterInput.h
//  Gauranga
//
//  Created by Vlad Kochergin on 20/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MyAdvertisingRouterInput <NSObject>

- (void)showDetailAdvertising:(id)catalogModel isFavorite:(BOOL)isFavorite;

@end
