//
//  MyAdvertisingRouter.h
//  Gauranga
//
//  Created by Vlad Kochergin on 20/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "MyAdvertisingRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface MyAdvertisingRouter : NSObject <MyAdvertisingRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
