//
//  MyAdvertisingAssembly.m
//  Gauranga
//
//  Created by Vlad Kochergin on 20/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "MyAdvertisingAssembly.h"

#import "MyAdvertisingViewController.h"
#import "MyAdvertisingInteractor.h"
#import "MyAdvertisingPresenter.h"
#import "MyAdvertisingRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation MyAdvertisingAssembly

- (MyAdvertisingViewController *)viewMyAdvertising {
    return [TyphoonDefinition withClass:[MyAdvertisingViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterMyAdvertising]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterMyAdvertising]];
                          }];
}

- (MyAdvertisingInteractor *)interactorMyAdvertising {
    return [TyphoonDefinition withClass:[MyAdvertisingInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterMyAdvertising]];
                          }];
}

- (MyAdvertisingPresenter *)presenterMyAdvertising{
    return [TyphoonDefinition withClass:[MyAdvertisingPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewMyAdvertising]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorMyAdvertising]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerMyAdvertising]];
                          }];
}

- (MyAdvertisingRouter *)routerMyAdvertising{
    return [TyphoonDefinition withClass:[MyAdvertisingRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewMyAdvertising]];
                          }];
}

@end
