//
//  MyAdvertisingPresenter.h
//  Gauranga
//
//  Created by Vlad Kochergin on 20/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "MyAdvertisingViewOutput.h"
#import "MyAdvertisingInteractorOutput.h"
#import "MyAdvertisingModuleInput.h"

@protocol MyAdvertisingViewInput;
@protocol MyAdvertisingInteractorInput;
@protocol MyAdvertisingRouterInput;

@interface MyAdvertisingPresenter : NSObject <MyAdvertisingModuleInput, MyAdvertisingViewOutput, MyAdvertisingInteractorOutput>

@property (nonatomic, weak) id<MyAdvertisingViewInput> view;
@property (nonatomic, strong) id<MyAdvertisingInteractorInput> interactor;
@property (nonatomic, strong) id<MyAdvertisingRouterInput> router;

@end
