//
//  MyAdvertisingPresenter.m
//  Gauranga
//
//  Created by Vlad Kochergin on 20/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "MyAdvertisingPresenter.h"

#import "MyAdvertisingViewInput.h"
#import "MyAdvertisingInteractorInput.h"
#import "MyAdvertisingRouterInput.h"

@implementation MyAdvertisingPresenter

#pragma mark - Методы MyAdvertisingModuleInput

- (void)configureModule {
    // Стартовая конфигурация модуля, не привязанная к состоянию view
}

#pragma mark - Методы MyAdvertisingViewOutput

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
}

- (void)getData:(BOOL)nextPage{
    [self.interactor getData:nextPage];
}

- (void)showDetailAdvertising:(id)catalogModel isFavorite:(BOOL)isFavorite{
    [self.router showDetailAdvertising:catalogModel isFavorite:isFavorite];
}

#pragma mark - Методы MyAdvertisingInteractorOutput

- (void)reloadData:(NSArray *)itemArray{
    [self.view reloadData:itemArray];
}

@end
