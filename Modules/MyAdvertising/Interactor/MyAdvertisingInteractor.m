//
//  MyAdvertisingInteractor.m
//  Gauranga
//
//  Created by Vlad Kochergin on 20/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "MyAdvertisingInteractor.h"

#import "MyAdvertisingInteractorOutput.h"
#import "UserModel.h"
#import "DataManager.h"
#import "RequestMapManager.h"

@interface MyAdvertisingInteractor()

@property (nonatomic) NSInteger pageNumber;
@property (nonatomic, strong) NSMutableArray *dateSource;
@property (nonatomic) BOOL endLoad;

@end

@implementation MyAdvertisingInteractor

#pragma mark - Методы MyAdvertisingInteractorInput

- (void)getData:(BOOL)nextPage{
    
    if(nextPage && !self.endLoad){
        self.pageNumber++;
    }else if(!nextPage){
        self.endLoad = NO;
        self.pageNumber = 0;
        self.dateSource = [NSMutableArray new];
    }
    
    if(self.endLoad)
        return;
    
    UserModel *user = [[DataManager sharedInstance] getUserProfile];
    NSDictionary *dicParam =@{
                              @"user_id":[NSString stringWithFormat:@"%li", user.userId],
                              @"page" : [NSString stringWithFormat:@"%li", (long)self.pageNumber],
                              @"onpage" : @"10i",
                              @"sort" : @"publicated_less"
                              };
    
    [RequestMapManager getCatalogWithDictionaryParam:dicParam userId:user.userId completion:^(NSArray *array, NSDictionary *dictionary, id object) {
        if(nextPage && ![array count]){
            self.endLoad = YES;
            return;
        }
        [self.dateSource addObjectsFromArray:array];
        [self.output reloadData:self.dateSource];
    }];
}

@end
