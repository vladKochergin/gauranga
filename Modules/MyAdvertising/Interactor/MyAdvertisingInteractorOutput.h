//
//  MyAdvertisingInteractorOutput.h
//  Gauranga
//
//  Created by Vlad Kochergin on 20/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MyAdvertisingInteractorOutput <NSObject>

- (void)reloadData:(NSArray *)itemArray;

@end
