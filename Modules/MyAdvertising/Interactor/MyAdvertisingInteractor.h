//
//  MyAdvertisingInteractor.h
//  Gauranga
//
//  Created by Vlad Kochergin on 20/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "MyAdvertisingInteractorInput.h"

@protocol MyAdvertisingInteractorOutput;

@interface MyAdvertisingInteractor : NSObject <MyAdvertisingInteractorInput>

@property (nonatomic, weak) id<MyAdvertisingInteractorOutput> output;

@end
