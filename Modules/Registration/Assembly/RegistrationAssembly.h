//
//  RegistrationAssembly.h
//  Gauranga
//
//  Created by egor on 20/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>

/**
 @author egor

 Registration module
 */
@interface RegistrationAssembly : TyphoonAssembly <RamblerInitialAssembly>

@end
