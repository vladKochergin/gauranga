//
//  RegistrationAssembly.m
//  Gauranga
//
//  Created by egor on 20/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "RegistrationAssembly.h"

#import "RegistrationViewController.h"
#import "RegistrationInteractor.h"
#import "RegistrationPresenter.h"
#import "RegistrationRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation RegistrationAssembly

- (RegistrationViewController *)viewRegistration {
    return [TyphoonDefinition withClass:[RegistrationViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterRegistration]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterRegistration]];
                          }];
}

- (RegistrationInteractor *)interactorRegistration {
    return [TyphoonDefinition withClass:[RegistrationInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterRegistration]];
                          }];
}

- (RegistrationPresenter *)presenterRegistration{
    return [TyphoonDefinition withClass:[RegistrationPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewRegistration]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorRegistration]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerRegistration]];
                          }];
}

- (RegistrationRouter *)routerRegistration{
    return [TyphoonDefinition withClass:[RegistrationRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewRegistration]];
                          }];
}

@end
