//
//  RegistrationModuleInput.h
//  Gauranga
//
//  Created by egor on 20/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>

@protocol RegistrationModuleInput <RamblerViperModuleInput>

/**
 @author egor

 Метод инициирует стартовую конфигурацию текущего модуля
 */
- (void)configureModule;

@end
