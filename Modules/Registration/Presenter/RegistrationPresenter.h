//
//  RegistrationPresenter.h
//  Gauranga
//
//  Created by egor on 20/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "RegistrationViewOutput.h"
#import "RegistrationInteractorOutput.h"
#import "RegistrationModuleInput.h"

@protocol RegistrationViewInput;
@protocol RegistrationInteractorInput;
@protocol RegistrationRouterInput;

@interface RegistrationPresenter : NSObject <RegistrationModuleInput, RegistrationViewOutput, RegistrationInteractorOutput>

@property (nonatomic, weak) id<RegistrationViewInput> view;
@property (nonatomic, strong) id<RegistrationInteractorInput> interactor;
@property (nonatomic, strong) id<RegistrationRouterInput> router;

@end
