//
//  RegistrationPresenter.m
//  Gauranga
//
//  Created by egor on 20/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "RegistrationPresenter.h"

#import "RegistrationViewInput.h"
#import "RegistrationInteractorInput.h"
#import "RegistrationRouterInput.h"

@implementation RegistrationPresenter

#pragma mark - Методы RegistrationModuleInput

- (void)configureModule {
}

#pragma mark - Методы RegistrationViewOutput

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
}

- (void)showAgreement{
    [self.interactor showAgreement];
}

- (void)signUpWithUserName:(NSString *)email
                  password:(NSString *)password
           confirmPassword:(NSString *)confirmPass
                 agreement:(BOOL)value {
    
    [self.interactor signUpWithUserName:email
                               password:password
                        confirmPassword:confirmPass
                              agreement:value];
}

#pragma mark - Методы RegistrationInteractorOutput

- (void)signUpCompletion {
    [self.view signUpCompletion];
}

- (void)signUpFailedComletion {
    [self.view signUpFailedCompletion];
}


@end
