//
//  RegistrationViewController.m
//  Gauranga
//
//  Created by egor on 20/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "RegistrationViewController.h"

#import "RegistrationViewOutput.h"

#import "cTextField.h"

#import "MBProgressHUD.h"

@interface RegistrationViewController ()

@property (weak, nonatomic) IBOutlet cTextField *emailTextField;
@property (weak, nonatomic) IBOutlet cTextField *passwordTextField;

@property (weak, nonatomic) IBOutlet UISwitch *confirmSwitch;
@property (weak, nonatomic) IBOutlet cTextField *confirmTextField;

@property (weak, nonatomic) IBOutlet UIButton *LCbutton;
@property (weak, nonatomic) IBOutlet UIButton *signUpButton;

@end

@implementation RegistrationViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];

	[self.output didTriggerViewReadyEvent];
}

#pragma mark - Методы RegistrationViewInput

- (void)setupInitialState {
    [self setupColor];
    self.navigationItem.title = NSLocalizedString(@"signup", @"Russian: Регистрация");
}

- (void)signUpCompletion {
    [KVAlert alertWithTitle:nil
                    message:NSLocalizedString(@"regSuc",@"Russian: Регистрация прошла успешно")
             preferredStyle:UIAlertControllerStyleAlert
                 completion:^(void) {
                     [self.navigationController popToRootViewControllerAnimated:YES];
                 }];
}

- (void)signUpFailedCompletion {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

#pragma mark - IBActions

- (IBAction)signUp:(id)sender {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self.output signUpWithUserName:self.emailTextField.text
                           password:self.passwordTextField.text
                    confirmPassword:self.confirmTextField.text
                          agreement:self.confirmSwitch.isOn];
}

- (IBAction)showAgrementAction:(id)sender {
    [self.output showAgreement];
}

#pragma mark - Additions

- (void)setupColor{
    [self.view setBackgroundColor:[UIColor mainColor]];
    [self.signUpButton setBackgroundColor:[UIColor mainColor]];
    [self.LCbutton setTitleColor:[UIColor mainColor] forState:UIControlStateNormal];
}

@end
