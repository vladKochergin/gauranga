//
//  RegistrationViewInput.h
//  Gauranga
//
//  Created by egor on 20/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RegistrationViewInput <NSObject>

/**
 @author egor

 Метод настраивает начальный стейт view
 */
- (void)setupInitialState;

- (void)signUpCompletion;
- (void)signUpFailedCompletion;

@end
