//
//  RegistrationViewController.h
//  Gauranga
//
//  Created by egor on 20/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RegistrationViewInput.h"

@protocol RegistrationViewOutput;

@interface RegistrationViewController : UIViewController <RegistrationViewInput>

@property (nonatomic, strong) id<RegistrationViewOutput> output;

@end
