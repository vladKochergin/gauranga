//
//  RegistrationRouter.h
//  Gauranga
//
//  Created by egor on 20/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "RegistrationRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface RegistrationRouter : NSObject <RegistrationRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
