//
//  RegistrationInteractorInput.h
//  Gauranga
//
//  Created by egor on 20/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RegistrationInteractorInput <NSObject>

- (void)showAgreement;

- (void)signUpWithUserName:(NSString *)email
                  password:(NSString *)password
           confirmPassword:(NSString *)confirmPass
                 agreement:(BOOL)value;

@end
