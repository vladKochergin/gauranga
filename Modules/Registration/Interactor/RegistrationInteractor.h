//
//  RegistrationInteractor.h
//  Gauranga
//
//  Created by egor on 20/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "RegistrationInteractorInput.h"

@protocol RegistrationInteractorOutput;

@interface RegistrationInteractor : NSObject <RegistrationInteractorInput>

@property (nonatomic, weak) id<RegistrationInteractorOutput> output;

@end
