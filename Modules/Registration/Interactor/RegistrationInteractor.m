//
//  RegistrationInteractor.m
//  Gauranga
//
//  Created by egor on 20/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "RegistrationInteractor.h"

#import "RegistrationInteractorOutput.h"

#import "Validation.h"
#import "RequestMapManager.h"

@implementation RegistrationInteractor

#pragma mark - Методы RegistrationInteractorInput

- (void)showAgreement {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:kAgreementURL]];
}

- (void)signUpWithUserName:(NSString *)email
                  password:(NSString *)password
           confirmPassword:(NSString *)confirmPass
                 agreement:(BOOL)value {
    
    if([self validation:email password:password confirmPass:confirmPass agreement:value]){
        [RequestMapManager signUpWitnEmail:email password:password completion:^(NSArray *array, NSDictionary *dictionary, id object) {
            if(!object){
                [self error: NSLocalizedString(@"emailError",@"Russian: Пользователь с указанным email уже существует")];
                return;
            }
            [self.output signUpCompletion];
        }];
    } else {
        [self.output signUpFailedComletion];
    }
}

- (BOOL)validation:(NSString *)userName
          password:(NSString *)password
       confirmPass:(NSString *)confirmPass
         agreement:(BOOL)value {
    
    if([Validation isValidEmail:userName] &&
       password.length > 5 &&
       [password isEqualToString:confirmPass] && value) {
        return YES;
    }
    if (![Validation isValidEmail:userName]) {
        [self error:NSLocalizedString(@"emailNC",@"Russian: Введите корректный email")];
        return NO;
    }
    if (password.length < 6) {
        [self error:NSLocalizedString(@"passwordError",@"Russian: Пароль должен содержать не менее 6 символов")];
        return NO;
    }
    if (![password isEqualToString:confirmPass]) {
        [self error:NSLocalizedString(@"confirmPassError",@"Russian: Пароли не совпадают")];
        return NO;
    }
    if (!value) {
        [self error:NSLocalizedString(@"pSL",@"Russian: Примите пользовательское соглашение")];
        return NO;
    }
    return NO;
}

- (void)error:(NSString *)errorText {
    [MBProgressHUD hideHUDForView:[WireFrame getView].view animated:YES];
    [KVAlert alertWithTitle:nil
                    message:errorText
             preferredStyle:UIAlertControllerStyleAlert
                 completion:nil];
}

@end
