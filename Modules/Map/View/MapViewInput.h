//
//  MapViewInput.h
//  Gauranga
//
//  Created by Vlad Kochergin on 16/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MapViewInput <NSObject>

/**
 @author Vlad Kochergin

 Метод настраивает начальный стейт view
 */
- (void)setupInitialStateWithLat:(double)lat lng:(double)lng;

@end
