//
//  MapViewController.h
//  Gauranga
//
//  Created by Vlad Kochergin on 16/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MapViewInput.h"

@protocol MapViewOutput;

@interface MapViewController : UIViewController <MapViewInput>

@property (nonatomic, strong) id<MapViewOutput> output;

@end
