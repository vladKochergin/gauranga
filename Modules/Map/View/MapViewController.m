//
//  MapViewController.m
//  Gauranga
//
//  Created by Vlad Kochergin on 16/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "MapViewController.h"

#import "MapViewOutput.h"
#import <MapKit/MapKit.h>

@interface MapViewController()

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end

@implementation MapViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];

	[self.output didTriggerViewReadyEvent];
}

#pragma mark - Методы MapViewInput

- (void)setupInitialStateWithLat:(double)lat lng:(double)lng {
    self.navigationItem.title = NSLocalizedString(@"navMap",@"Russian: Карта");
    
    CLLocationCoordinate2D pinlocation;
    pinlocation.latitude = lat;
    pinlocation.longitude = lng;
    
    MKPointAnnotation *Pin = [[MKPointAnnotation alloc]init];
    Pin.coordinate = pinlocation;
    Pin.title = @"Annotation Title";
    Pin.subtitle = @"Annotation Subtitle";
    

    [self.mapView addAnnotation:Pin];
    MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:MKCoordinateRegionMakeWithDistance(pinlocation, 200, 200)];
    [self.mapView setRegion:adjustedRegion animated:YES];
}

#pragma mark - Map

- (MKAnnotationView *)mapView:(MKMapView *)map viewForAnnotation:(id <MKAnnotation>)annotation{
    
    static NSString *AnnotationViewID = @"annotationViewID";
    
    MKAnnotationView *annotationView = (MKAnnotationView *)[self.mapView dequeueReusableAnnotationViewWithIdentifier:AnnotationViewID];
    
    if (annotationView == nil){
        annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:AnnotationViewID];
    }
    
    annotationView.image = [UIImage imageNamed:@"location"];
    annotationView.annotation = annotation;
    
    return annotationView;
}

@end
