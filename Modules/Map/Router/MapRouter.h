//
//  MapRouter.h
//  Gauranga
//
//  Created by Vlad Kochergin on 16/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "MapRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface MapRouter : NSObject <MapRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
