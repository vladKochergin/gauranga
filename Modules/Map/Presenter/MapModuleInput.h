//
//  MapModuleInput.h
//  Gauranga
//
//  Created by Vlad Kochergin on 16/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>

@protocol MapModuleInput <RamblerViperModuleInput>

/**
 @author Vlad Kochergin

 Метод инициирует стартовую конфигурацию текущего модуля
 */
- (void)configureModuleWithLat:(double)lat lng:(double)lng;

@end
