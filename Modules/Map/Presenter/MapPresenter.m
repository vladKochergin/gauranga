//
//  MapPresenter.m
//  Gauranga
//
//  Created by Vlad Kochergin on 16/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "MapPresenter.h"

#import "MapViewInput.h"
#import "MapInteractorInput.h"
#import "MapRouterInput.h"

@class CLLocationCoordinate2D;

@interface MapPresenter()

@property(nonatomic) double lat;
@property(nonatomic) double lng;

@end

@implementation MapPresenter

#pragma mark - Методы MapModuleInput

- (void)configureModuleWithLat:(double)lat lng:(double)lng {
    self.lat = lat;
    self.lng = lng;
}

#pragma mark - Методы MapViewOutput

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialStateWithLat:self.lat lng:self.lng];
}

#pragma mark - Методы MapInteractorOutput

@end
