//
//  MapPresenter.h
//  Gauranga
//
//  Created by Vlad Kochergin on 16/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "MapViewOutput.h"
#import "MapInteractorOutput.h"
#import "MapModuleInput.h"

@protocol MapViewInput;
@protocol MapInteractorInput;
@protocol MapRouterInput;

@interface MapPresenter : NSObject <MapModuleInput, MapViewOutput, MapInteractorOutput>

@property (nonatomic, weak) id<MapViewInput> view;
@property (nonatomic, strong) id<MapInteractorInput> interactor;
@property (nonatomic, strong) id<MapRouterInput> router;

@end
