//
//  MapInteractor.h
//  Gauranga
//
//  Created by Vlad Kochergin on 16/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "MapInteractorInput.h"

@protocol MapInteractorOutput;

@interface MapInteractor : NSObject <MapInteractorInput>

@property (nonatomic, weak) id<MapInteractorOutput> output;

@end
