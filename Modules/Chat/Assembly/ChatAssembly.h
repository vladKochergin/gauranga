//
//  ChatAssembly.h
//  Gauranga
//
//  Created by Vlad Kochergin on 22/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>

/**
 @author Vlad Kochergin

 Chat module
 */
@interface ChatAssembly : TyphoonAssembly <RamblerInitialAssembly>

@end
