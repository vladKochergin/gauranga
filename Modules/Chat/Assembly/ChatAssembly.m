//
//  ChatAssembly.m
//  Gauranga
//
//  Created by Vlad Kochergin on 22/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "ChatAssembly.h"

#import "ChatViewController.h"
#import "ChatInteractor.h"
#import "ChatPresenter.h"
#import "ChatRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation ChatAssembly

- (ChatViewController *)viewChat {
    return [TyphoonDefinition withClass:[ChatViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterChat]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterChat]];
                          }];
}

- (ChatInteractor *)interactorChat {
    return [TyphoonDefinition withClass:[ChatInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterChat]];
                          }];
}

- (ChatPresenter *)presenterChat{
    return [TyphoonDefinition withClass:[ChatPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewChat]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorChat]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerChat]];
                          }];
}

- (ChatRouter *)routerChat{
    return [TyphoonDefinition withClass:[ChatRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewChat]];
                          }];
}

@end
