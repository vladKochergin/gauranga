//
//  KVInputView.h
//  Gauranga
//
//  Created by yvp on 2/24/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@class KVInputView;

@protocol KVInputDelegate

- (void)clickButtonSend:(KVInputView *)inputView;

@end

@interface KVInputView : UIView 

@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) id<KVInputDelegate> delegate;

@end
