//
//  KVInputView.m
//  Gauranga
//
//  Created by yvp on 2/24/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import "KVInputView.h"

@implementation KVInputView

//- (instancetype)initWithCoder:(NSCoder *)aDecoder {
//    if ((self = [super initWithCoder:aDecoder])) {
//    }
//    return self;
//}

//- (id)initWithFrame:(CGRect)frame
//{
//    self = [super initWithFrame:frame];
//    if (self) {
//        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"KVInputView" owner:self options:nil];
//        self = [nib objectAtIndex:0];
//        self.frame = frame;
//        //[self setupInputView];
//    }
//    return self;
//}

//- (void)setupInputView{
//    //[self.textView setDelegate:self];
//    [self.textView.layer setCornerRadius:10];
//    //[self.textView.layer setMasksToBounds:YES];
//    self.textView.contentInset = UIEdgeInsetsMake(-1,0,0,0);
//}

- (IBAction)sendClick:(UIButton *)sender {
    [self.delegate clickButtonSend:self];
    self.textView.text = @"";
}

@end
