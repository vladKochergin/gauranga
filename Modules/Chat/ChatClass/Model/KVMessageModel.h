//
//  KVMessageModel.h
//  Gauranga
//
//  Created by yvp on 2/24/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <EasyMapping/EasyMapping.h>

@interface KVMessageModel : EKObjectModel

@property(nonatomic, strong) NSString *userName;
@property(nonatomic, strong) NSString *message;
@property(nonatomic, strong) NSString *date;
@property(nonatomic) BOOL isMyMessage;

@property(nonatomic, strong) NSString *itemCity;
@property(nonatomic) NSInteger itemId;
@property(nonatomic, strong) NSString *itemImg;
@property(nonatomic, strong) NSString *itemPrice;
@property(nonatomic, strong) NSString *itemDate;
@property(nonatomic) NSInteger itemCurrId;
@property(nonatomic, strong) NSString *itemTitle;
@property(nonatomic, strong) NSString *author;

+ (instancetype)messageWithDictionary:(NSDictionary *)JSONresponse;

@end
