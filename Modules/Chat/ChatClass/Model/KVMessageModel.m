//
//  KVMessageModel.m
//  Gauranga
//
//  Created by yvp on 2/24/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import "KVMessageModel.h"

@implementation KVMessageModel

+ (EKObjectMapping *)objectMapping{
    
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapKeyPath:@"message" toProperty:@"message"];
        [mapping mapKeyPath:@"created_date" toProperty:@"date"];
        [mapping mapKeyPath:@"my" toProperty:@"isMyMessage"];
        
        [mapping mapKeyPath:@"item_city" toProperty:@"itemCity"];
        [mapping mapKeyPath:@"item_id" toProperty:@"itemId"];
        [mapping mapKeyPath:@"item_img" toProperty:@"itemImg"];
        [mapping mapKeyPath:@"item_price" toProperty:@"itemPrice"];
        [mapping mapKeyPath:@"item_publicated" toProperty:@"itemDate"];
        [mapping mapKeyPath:@"item_price_curr" toProperty:@"itemCurrId"];
        [mapping mapKeyPath:@"item_title" toProperty:@"itemTitle"];
        [mapping mapKeyPath:@"author" toProperty:@"author"];
    }];
}

+ (instancetype)messageWithDictionary:(NSDictionary *)JSONresponse{
    return [KVMessageModel objectWithProperties:JSONresponse];
}

@end
