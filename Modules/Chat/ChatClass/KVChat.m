//
//  KVChat.m
//  Gauranga
//
//  Created by yvp on 2/24/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import "KVChat.h"

#import "Message Cell/MessageCell.h"
#import "KVMessageModel.h"
#import "MessageCellWithADV.h"
#import "DataManager.h"
#import "CurrencyModel.h"
#import "UserModel.h"

@interface KVChat()<KVInputDelegate>

@property (strong, nonatomic) NSString *myName;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *inputViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *inputViewBottom;

//@property(nonatomic, strong) KVInputView *inputView;

@end

@implementation KVChat

- (void)viewDidLoad{
    [super viewDidLoad];
    
    self.dataSource = [NSMutableArray new];
    
    [self createTableView];
    [self registerNib];
    [self createInputView];
    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self selector:@selector(keyboardOnScreen:) name:UIKeyboardWillShowNotification object:nil];
    [center addObserver:self selector:@selector(keyboardHideScreen:) name:UIKeyboardWillHideNotification object:nil];
    UserModel *userModel = [DataManager sharedInstance].getUserProfile;
    self.myName = userModel.name;
    self.tableView.transform = CGAffineTransformMakeRotation(-M_PI);
}

#pragma mark - registerNib

- (void)registerNib{
    [self.tableView registerNib:[UINib nibWithNibName:@"TextMessageCell" bundle:nil] forCellReuseIdentifier:@"TextMessageCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"TextMessageWithADV" bundle:nil] forCellReuseIdentifier:@"TextMessageWithADV"];
}

#pragma mark - UITableView

- (void)createTableView{
    
    self.tableView.estimatedRowHeight = 50.0f;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    //[self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.tableView setAllowsSelection:NO];
    [self.tableView setShowsVerticalScrollIndicator:NO];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.dataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITapGestureRecognizer *hideKeyboard = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    KVMessageModel *model = [self.dataSource objectAtIndex:indexPath.row];
    
    NSString *userName = self.myName;
    if(!model.isMyMessage){
        userName = self.pName;
    }
    
    if(model.itemId){
        MessageCellWithADV *message = [tableView dequeueReusableCellWithIdentifier:@"TextMessageWithADV"];
        message.message.text = model.message;
        message.date.text = model.date;
        message.userName.text = userName;
        
        [message.priceADV setHidden:YES];
#warning Currency2
        if(model.itemPrice){
            [message.priceADV setHidden:NO];
            CurrencyModel *currModel = [[[DataManager sharedInstance] getCurrency] objectForKey:[NSString stringWithFormat:@"%li", (long)model.itemCurrId]];
            message.priceADV.text = [NSString stringWithFormat:@"%@ %@",[CurrencyModel priceInFormat:[model.itemPrice doubleValue]], currModel.titleShort];
        }
        message.placeADV.text = [NSString stringWithFormat:@"%@\n%@", model.itemCity, model.itemDate];
        
        //NSString *urlstring = [NSString stringWithFormat:@"http:%@", model.itemImg];
        NSString *urlstring = model.itemImg;

        [message.imageADV sd_setImageWithURL:[NSURL URLWithString:urlstring] placeholderImage:[UIImage imageNamed:@"avatarPlaceholder"]];
        message.titleADV.text = model.itemTitle;
        
        [message addGestureRecognizer:hideKeyboard];
        
        UserModel *user = [[DataManager sharedInstance] getUserProfile];
        if (model.author.integerValue == user.userId) {
            for (UIView *view in message.subviews)
            view.backgroundColor = [UIColor whiteColor];
        }
        
        return message;
    }
    
    MessageCell *message = [tableView dequeueReusableCellWithIdentifier:@"TextMessageCell"];
    message.userName.text = userName;
    message.message.text = model.message;
    message.date.text = model.date;
    [message addGestureRecognizer:hideKeyboard];
    return message;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}



#pragma - KVInputView

- (void)createInputView{
    //self.inputView = [[KVInputView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    //[self.inputView.textView setDelegate:self];
    [self.inputView setDelegate:self];
    //self.inputView.layer.zPosition = 300;
    //[self.view addSubview:self.inputView];
    //[self.view bringSubviewToFront:self.inputView];
}


#pragma mark - Keyboard

- (void)hideKeyboard{
    [self.view endEditing:YES];
}

-(void)keyboardOnScreen:(NSNotification *)notification{
    NSDictionary *info  = notification.userInfo;
    NSValue      *value = info[UIKeyboardFrameEndUserInfoKey];
    
    CGRect rawFrame      = [value CGRectValue];
    CGRect keyboardFrame = [self.view convertRect:rawFrame fromView:nil];
    
    self.inputViewBottom.constant = keyboardFrame.size.height;
}

-(void)keyboardHideScreen:(NSNotification *)notification{
    self.inputViewBottom.constant = 0;
}

#pragma mark - UITextView Delegate

//- (void)textViewDidBeginEditing:(UITextView *)textView{
//    [self tableUP:textView];
//}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([textView isFirstResponder]) {
        if ([[[textView textInputMode] primaryLanguage] isEqualToString:@"emoji"] || ![[textView textInputMode] primaryLanguage]) {
            return NO;
        }
    }
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView{
    CGFloat height = textView.contentSize.height + 17;
    if(height<=135)
        self.inputViewHeight.constant = height;
    //CGRect inptRect = self.inputView.frame;
    //NSLog(@"%f",textView.contentSize.height);
    //inptRect.size.height = textView.contentSize.height + 13;
    //inptRect.origin.y =(self.view.frame.size.height - 50) - ((textView.contentSize.height + 13) - 50);
    //self.inputView.frame = inptRect;
    //[self tableUP:textView];
}

//- (void)tableUP:(UITextView *)textView{
//    CGRect tableRect = self.tableView.frame;
//    if(!tableRect.origin.y)
//        tableRect.origin.y *= -1;
//    tableRect.size.height = ((self.view.frame.size.height - 50) - ((textView.contentSize.height + 13) - 50))-tableRect.origin.y;
//    self.tableView.frame = tableRect;
//}

#pragma mark - InputView

- (void)clickButtonSend:(KVInputView *)inputView{
    [self sendMessage];
    self.inputViewHeight.constant = 51;
}

- (void)sendMessage{
    //Sub Class
}

@end
