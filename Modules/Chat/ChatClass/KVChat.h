//
//  KVChat.h
//  Gauranga
//
//  Created by yvp on 2/24/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "KVInputView.h"

@interface KVChat : UIViewController<UITableViewDelegate, UITableViewDataSource, UITextViewDelegate>

@property (nonatomic) NSInteger partnerID;

@property(nonatomic, strong) NSMutableArray *dataSource;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet KVInputView *inputView;
@property (strong, nonatomic) NSString *pName;

- (void)sendMessage;

@end
