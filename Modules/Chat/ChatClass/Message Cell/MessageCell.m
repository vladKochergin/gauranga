//
//  MessageCell.m
//  Gauranga
//
//  Created by yvp on 2/24/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import "MessageCell.h"

@implementation MessageCell

- (void)awakeFromNib {
    [super awakeFromNib];

    [self.backgroundMessage.layer setCornerRadius:10];
    [self.backgroundMessage.layer setBorderWidth:0.5f];
    [self.backgroundMessage.layer setMasksToBounds:YES];
    self.transform = CGAffineTransformMakeRotation(M_PI);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
