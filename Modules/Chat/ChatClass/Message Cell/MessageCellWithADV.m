//
//  MessageCellWithADV.m
//  Gauranga
//
//  Created by yvp on 4/4/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import "MessageCellWithADV.h"

@interface MessageCellWithADV()

@property (weak, nonatomic) IBOutlet UIView *itemView;
@property (weak, nonatomic) IBOutlet UIButton *tagButton;

@end

@implementation MessageCellWithADV

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.transform = CGAffineTransformMakeRotation(M_PI);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    [self setupColor];
}

#pragma mark - Additions

- (void)setupColor{
    [self setBackgroundColor:[[UIColor mainColor] colorWithAlphaComponent:0.1]];
    [self.itemView setBackgroundColor:[[UIColor mainColor] colorWithAlphaComponent:0.1]];
    [self.priceADV setTextColor:[UIColor mainColor]];
    [self.tagButton setTintColor:[UIColor mainColor]];
}

@end
