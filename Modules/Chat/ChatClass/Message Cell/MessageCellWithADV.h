//
//  MessageCellWithADV.h
//  Gauranga
//
//  Created by yvp on 4/4/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageCellWithADV : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *message;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UILabel *date;

@property (weak, nonatomic) IBOutlet UIImageView *imageADV;
@property (weak, nonatomic) IBOutlet UILabel *titleADV;
@property (weak, nonatomic) IBOutlet UILabel *priceADV;
@property (weak, nonatomic) IBOutlet UILabel *placeADV;

@end
