//
//  ChatViewController.m
//  Gauranga
//
//  Created by Vlad Kochergin on 22/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "ChatViewController.h"

#import "ChatViewOutput.h"

#import "CityNavigationBar.h"

@interface ChatViewController()

@property (weak, nonatomic) IBOutlet UIToolbar *toolBar;

@end

@implementation ChatViewController

@synthesize partnerName;

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateMessages) name:@"ReceiveNewMessageNotification" object:nil];

	[self.output didTriggerViewReadyEvent];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Методы ChatViewInput

- (void)setupInitialState {
    [self.inputView.textView.layer setCornerRadius:10];
    [self.inputView.textView.layer setMasksToBounds:YES];
    self.pName = self.partnerName;
    self.navigationItem.title = self.pName;
    [self.tableView addRefreshControlWithAction:@selector(getDataIsNextPage:) view:self];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self getDataIsNextPage:NO];
}

#pragma mark - Data

- (void)getDataIsNextPage:(BOOL)nextPage {
    [self.output getDataIsNextPage:nextPage];
}

- (void)reloadData:(NSArray *)data {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    self.dataSource = [data mutableCopy];
    [self.tableView reloadDataWithCheck];
}

- (void)sendMessage{
    if ([self.inputView.textView.text isEmpty]) {
        return;
    }
    [self.output sendMessage:self.inputView.textView.text];
}

- (void)updateMessages {
    [self getDataIsNextPage:NO];
}

//#pragma mark - JSQMessages CollectionView DataSource
//
//- (id<JSQMessageData>)collectionView:(JSQMessagesCollectionView *)collectionView messageDataForItemAtIndexPath:(NSIndexPath *)indexPath{
//    return [self.messagesDataModel.messages objectAtIndex:indexPath.item];
//}
//
//- (id<JSQMessageAvatarImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView avatarImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    JSQMessage *message = self.messagesDataModel.messages[indexPath.item];
//    
//    return self.messagesDataModel.avatars[message.senderId];
//}
//
//- (id<JSQMessageBubbleImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView messageBubbleImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    JSQMessage *message = self.messagesDataModel.messages[indexPath.item];
//    
//    if ([message.senderId isEqualToString:self.senderId]) {
//        return self.messagesDataModel.outgoingBubbleImageData;
//    }
//    else {
//        return self.messagesDataModel.incomingBubbleImageData;
//    }
//}
//
//- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
//{
//    JSQMessage *message = [self.messagesDataModel messageAtIndex:indexPath.item];
//    NSString *date = (NSString *)message.date;
//    
//    // if date repeats, dont show
//    if ((indexPath.item < self.messagesDataModel.messages.count) && (indexPath.item != 0))
//    {
//        JSQMessage *previousMessage = [self.messagesDataModel messageAtIndex:indexPath.item - 1];
//        if ([message.date isEqual:previousMessage.date]) {
//            date = @"";
//        }
//    }
//    
//    return [[NSAttributedString alloc] initWithString:date];
//}
//
//- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath
//{
//    return nil;
//}
//
//- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath
//{
//    //    JSQMessage *message = self.messagesDataModel.messages[indexPath.item];
//    //    NSAttributedString *attribStr = [[NSAttributedString alloc] initWithString:message.senderDisplayName];
//    //
//    //    if (message.senderId == self.senderId) {
//    //        attribStr = [[NSAttributedString alloc] initWithString:@""];
//    //    }
//    
//    NSAttributedString *attribStr = [[NSAttributedString alloc] initWithString:@""];
//    
//    return attribStr;
//}

@end
