//
//  ChatViewController.h
//  Gauranga
//
//  Created by Vlad Kochergin on 22/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ChatViewInput.h"
#import "KVChat.h"


@protocol ChatViewOutput;

@interface ChatViewController : KVChat <ChatViewInput>

@property (nonatomic, strong) id<ChatViewOutput> output;

@end
