//
//  ChatViewInput.h
//  Gauranga
//
//  Created by Vlad Kochergin on 22/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ChatViewInput <NSObject>

/**
 @author Vlad Kochergin

 Метод настраивает начальный стейт view
 */
@property (strong, nonatomic) NSString *partnerName;

- (void)setupInitialState;
- (void)reloadData:(NSArray *)data;

@end
