//
//  ChatPresenter.h
//  Gauranga
//
//  Created by Vlad Kochergin on 22/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "ChatViewOutput.h"
#import "ChatInteractorOutput.h"
#import "ChatModuleInput.h"

@protocol ChatViewInput;
@protocol ChatInteractorInput;
@protocol ChatRouterInput;

@interface ChatPresenter : NSObject <ChatModuleInput, ChatViewOutput, ChatInteractorOutput>

@property (nonatomic, weak) id<ChatViewInput> view;
@property (nonatomic, strong) id<ChatInteractorInput> interactor;
@property (nonatomic, strong) id<ChatRouterInput> router;

@end
