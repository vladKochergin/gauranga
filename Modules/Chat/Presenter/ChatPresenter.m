//
//  ChatPresenter.m
//  Gauranga
//
//  Created by Vlad Kochergin on 22/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "ChatPresenter.h"

#import "ChatViewInput.h"
#import "ChatInteractorInput.h"
#import "ChatRouterInput.h"

@implementation ChatPresenter

#pragma mark - Методы ChatModuleInput

- (void)configureModuleWithInterlocutorId:(NSInteger)interlocutorId partnerName:(NSString *)partnerName {
    [self.interactor setInterlocutorId:interlocutorId];
    [self.view setPartnerName:partnerName];
}

#pragma mark - Методы ChatViewOutput

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
}

- (void)getDataIsNextPage:(BOOL)nextPage{
    [self.interactor getDataIsNextPage:nextPage];
}

- (void)sendMessage:(NSString *)message{
    [self.interactor sendMessage:message];
}

#pragma mark - Методы ChatInteractorOutput

- (void)reloadData:(NSArray *)data{
    [self.view reloadData:data];
}

@end
