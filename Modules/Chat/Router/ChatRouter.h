//
//  ChatRouter.h
//  Gauranga
//
//  Created by Vlad Kochergin on 22/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "ChatRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface ChatRouter : NSObject <ChatRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
