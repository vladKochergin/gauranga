//
//  ChatInteractorInput.h
//  Gauranga
//
//  Created by Vlad Kochergin on 22/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ChatInteractorInput <NSObject>

- (void)getDataIsNextPage:(BOOL)nextPage;
- (void)sendMessage:(NSString *)message;

@property(nonatomic) NSInteger interlocutorId;

@end
