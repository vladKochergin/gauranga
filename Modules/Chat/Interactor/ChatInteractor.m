//
//  ChatInteractor.m
//  Gauranga
//
//  Created by Vlad Kochergin on 22/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "ChatInteractor.h"

#import "ChatInteractorOutput.h"
#import "RequestMapManager.h"
#import "KVMessageModel.h"

@interface ChatInteractor()

@property (nonatomic) NSInteger pageNumber;
@property (nonatomic, strong) NSMutableArray *dateSource;
@property (nonatomic) BOOL endLoad;

@end

@implementation ChatInteractor

@synthesize interlocutorId;

#pragma mark - Методы ChatInteractorInput

- (void)getDataIsNextPage:(BOOL)nextPage{
    if(nextPage && !self.endLoad){
        self.pageNumber++;
    }else{
        self.endLoad = NO;
        self.pageNumber = 0;
        self.dateSource = [NSMutableArray new];
    }
    
    if(self.endLoad){
        //[self.output hideAllLoaders];
        return;
    }
    
    [RequestMapManager getMessageListWitInterlocutor:self.interlocutorId page:self.pageNumber completion:^(NSArray *array, NSDictionary *dictionary, id object) {
        if(nextPage && ![array count]){
            self.endLoad = YES;
            //[self.output hideAllLoaders];
            return;
        }
        [self.dateSource addObjectsFromArray:array];
        [self.output reloadData:self.dateSource];
    }];
}

- (void)sendMessage:(NSString *)message{
    [[WebService sharedService] sendMessageWithpartnerId:self.interlocutorId
                                                 message:message
                                              completion:^(NSError *error, NSArray *array, NSDictionary *dictionary) {
      if (error) {
          if([error.userInfo[@"message"] isEqualToString:@"MAIL_TIMELIMIT"]) {
              [KVAlert alertWithTitle:[NSString stringWithFormat:NSLocalizedString(@"timeLimit", @"Russian: Сообщение можно отправлять каждые %li секунд(ы)"), error.userInfo[@"dtime"]]
                              message:[NSString stringWithFormat:NSLocalizedString(@"timeLeft", @"Russian: Осталось %li секунд(ы)"), error.userInfo[@"left"]]
                       preferredStyle:UIAlertControllerStyleAlert
                           completion:^{}];
          }
      } else {
          KVMessageModel *newMessgeModel = [KVMessageModel new];
          newMessgeModel.message = message;
          NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
          [formatter setDateFormat:@"yyyy-MM-dd"];
          newMessgeModel.date = [formatter stringFromDate:[NSDate date]];
          newMessgeModel.isMyMessage = YES;
          
          NSArray *temp = [self.dateSource copy];
          [self.dateSource removeAllObjects];
          [self.dateSource addObject:newMessgeModel];
          [self.dateSource addObjectsFromArray:temp];
          [self.output reloadData:self.dateSource];
      }
    }];
}

@end
