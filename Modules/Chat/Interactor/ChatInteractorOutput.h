//
//  ChatInteractorOutput.h
//  Gauranga
//
//  Created by Vlad Kochergin on 22/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ChatInteractorOutput <NSObject>

- (void)reloadData:(NSArray *)data;

@end
