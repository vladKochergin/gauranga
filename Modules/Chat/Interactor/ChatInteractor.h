//
//  ChatInteractor.h
//  Gauranga
//
//  Created by Vlad Kochergin on 22/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "ChatInteractorInput.h"

@protocol ChatInteractorOutput;

@interface ChatInteractor : NSObject <ChatInteractorInput>

@property (nonatomic, weak) id<ChatInteractorOutput> output;

@end
