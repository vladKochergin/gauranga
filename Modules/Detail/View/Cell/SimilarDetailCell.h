//
//  SimilarDetailCell.h
//  Gauranga
//
//  Created by yvp on 2/16/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SimilarViewDelegate

- (void)didSelectSimilarItem:(id)model;

@end

@interface SimilarDetailCell : UITableViewCell

- (void)setDataSourceWithModel:(id)dataSourece;
@property (nonatomic, weak) id <SimilarViewDelegate> delegate;

@end
