//
//  ImageDetailCell.h
//  Gauranga
//
//  Created by yvp on 2/15/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UIImagePicker.h"

@interface ImageDetailCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImagePicker *imagePicker;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *price;

@end
