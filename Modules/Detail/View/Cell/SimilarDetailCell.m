//
//  SimilarDetailCell.m
//  Gauranga
//
//  Created by yvp on 2/16/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import "SimilarDetailCell.h"
#import "SimilarDetailCollectionCell.h"
#import "CatalogModel.h"
#import "DataManager.h"
#import "CurrencyModel.h"

@interface SimilarDetailCell()<UICollectionViewDelegate, UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (strong, nonatomic) NSArray *dataSource;
@property(nonatomic, strong) NSDictionary *currencyDic;
@end

@implementation SimilarDetailCell

static NSString * const reuseIdentifier = @"Cell";

- (void)awakeFromNib {
    [super awakeFromNib];
    self.currencyDic = [[DataManager sharedInstance] getCurrency];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)setDataSourceWithModel:(id)dataSourece{
    self.dataSource = dataSourece;
    [self.collectionView reloadData];
}

#pragma mark - CollectionView

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.dataSource count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    SimilarDetailCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    CatalogModel *model = [self.dataSource objectAtIndex:indexPath.row];
    
//    NSURL *urlImage = [NSURL URLWithString:[NSString stringWithFormat:@"http:%@", model.imageMedium]];
    [cell.image sd_setImageWithURL:[NSURL URLWithString:model.imageMedium] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    cell.title.text = model.title;
    [cell.price setHidden:YES];
    if([model.priceDisplay length]){
        [cell.price setHidden:NO];
//        CurrencyModel *currModel = [self.currencyDic objectForKey:[NSString stringWithFormat:@"%li",model.curency]];
        cell.price.text = model.priceDisplay;//[NSString stringWithFormat:@"%@ %@",model.price, currModel.titleShort];
    }
    cell.info.text = [NSString stringWithFormat:@"%@\n%@", model.city, model.date];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGSize size = CGSizeMake((collectionView.frame.size.width-5)/3, 200);
    
    return size;
}

#pragma mark - Delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if(self.delegate){
        [self.delegate didSelectSimilarItem:[self.dataSource objectAtIndex:indexPath.row]];
    }
}

@end
