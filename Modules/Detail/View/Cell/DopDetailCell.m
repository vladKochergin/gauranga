//
//  DopDetailCell.m
//  Gauranga
//
//  Created by yvp on 2/15/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import "DopDetailCell.h"

@implementation DopDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self setupColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Additions

- (void)setupColor{
    [self.desc setTextColor:[UIColor mainColor]];
}

@end
