//
//  CoordinateDetailCell.h
//  Gauranga
//
//  Created by yvp on 2/15/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CoordinateDetailCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *coordinateButton;
@property (weak, nonatomic) IBOutlet UIButton *coordinateImage;

@end
