//
//  ImageDetailCell.m
//  Gauranga
//
//  Created by yvp on 2/15/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import "ImageDetailCell.h"

@implementation ImageDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self setupColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Additions

- (void)setupColor{
    [self.price setTextColor:[UIColor mainColor]];
}

@end
