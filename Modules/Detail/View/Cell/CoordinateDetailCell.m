//
//  CoordinateDetailCell.m
//  Gauranga
//
//  Created by yvp on 2/15/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import "CoordinateDetailCell.h"

@implementation CoordinateDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    [self setupColor];
}

#pragma mark - Additions

- (void)setupColor{
    [self.coordinateImage setTintColor:[UIColor mainColor]];
}

@end
