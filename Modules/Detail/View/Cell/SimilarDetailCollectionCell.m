//
//  SimilarDetailCollectionCell.m
//  Gauranga
//
//  Created by yvp on 2/16/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import "SimilarDetailCollectionCell.h"

@implementation SimilarDetailCollectionCell

- (void)awakeFromNib{
    [super awakeFromNib];
    
    [self setupColor];
}

#pragma mark - Additions

- (void)setupColor{
    [self.tagButton setTintColor:[UIColor mainColor]];
    [self.price setTextColor:[UIColor mainColor]];
}

@end
