//
//  SimilarDetailCollectionCell.h
//  Gauranga
//
//  Created by yvp on 2/16/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SimilarDetailCollectionCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UILabel *info;
@property (weak, nonatomic) IBOutlet UIButton *tagButton;

@end
