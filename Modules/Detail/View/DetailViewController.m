//
//  DetailViewController.m
//  Gauranga
//
//  Created by Vlad Kochergin on 15/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "DetailViewController.h"

#import "DetailViewOutput.h"

#import "DetailAdvertisingModel.h"
#import "UserModel.h"
#import "dopParamModel.h"
#import "DataManager.h"
#import "CurrencyModel.h"
#import "CatalogModel.h"

#import "ImageDetailCell.h"
#import "CoordinateDetailCell.h"
#import "DopDetailCell.h"
#import "UserDetailCell.h"
#import "SimilarDetailCell.h"

#import <SDWebImage/SDWebImageDownloader.h>
@interface DetailViewController()<SimilarViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) DetailAdvertisingModel *dataSource;
@property(nonatomic, strong) NSDictionary *currencyDic;

@property (nonatomic, strong) UIView *placeHolderView;
@property (nonatomic, strong) UIBarButtonItem *favoriteBarButtonItem;

@end

@implementation DetailViewController

@synthesize isFavorite, isMyAdv, catalogModel;

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.output didTriggerViewReadyEvent];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    [self.tableView reloadData];
    [self.placeHolderView setBackgroundColor:[UIColor whiteColor]];
}

#pragma mark - Методы DetailViewInput

- (void)setupInitialState {
    self.currencyDic = [[DataManager sharedInstance] getCurrency];
    
    self.tableView.estimatedRowHeight = 40.0f;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.tableView.bounds.size.width, 0.01f)];
    
    self.placeHolderView = [[UIView alloc] initWithFrame:self.view.frame];
    [self getData];
}

#pragma mark - Data

- (void)getData {
    [MBProgressHUD showHUDAddedTo:self.placeHolderView animated:YES];
    [self.view addSubview:self.placeHolderView];
    [self.output getData];
}

- (void)reloadData:(id)model {
    [MBProgressHUD hideHUDForView:self.placeHolderView animated:YES];
    self.dataSource = model;
    self.dopRowCount = [self.dataSource.dopParam count];
    [self addBarButton];
    
    [self.tableView reloadData];
    [self.placeHolderView removeFromSuperview];
}

- (void)updateFaforiteIcon:(BOOL)value {
    if(self.isFavorite) {
        [self.favoriteBarButtonItem setTintColor:nil];
    }
    else {
        [self.favoriteBarButtonItem setTintColor:[UIColor yellowColor]];
    }
    self.isFavorite = !self.isFavorite;
    self.catalogModel.isFavorite = self.isFavorite;
}

#pragma mark - UITableView

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    
    switch (indexPath.section) {
        case 0:{
            if(indexPath.row == 0){
                ImageDetailCell *cCell = [tableView dequeueReusableCellWithIdentifier:@"imageCell"];
                NSString *priceType = @"";
                [cCell.price setHidden:YES];
                if(self.dataSource.price){
                    [cCell.price setHidden:NO];
#warning Currency
                    if(self.dataSource.curency){
                        CurrencyModel *currModel = [self.currencyDic objectForKey:[NSString stringWithFormat:@"%li",self.dataSource.curency]];
                        priceType = currModel.titleShort;
                    }
                    cCell.price.text = [NSString stringWithFormat:@"%@ %@", self.dataSource.price, priceType];
                }
                cCell.title.text = self.dataSource.title;
                [cCell.imagePicker setDataSource:self.dataSource.images selectedIndex:0];
//                UITapGestureRecognizer *tapOpenFullScreenImage = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openFullScreenImage:)];
//                [cCell.imagePicker addGestureRecognizer:tapOpenFullScreenImage];
                cell = cCell;
            }else if(indexPath.row == 1){
                CoordinateDetailCell *cCell = [tableView dequeueReusableCellWithIdentifier:@"coordinateCell"];
                [cCell.coordinateButton setTitle:self.dataSource.city forState:UIControlStateNormal];
                if(self.dataSource.latitude > 0.0f)
                    [cCell.coordinateButton addTarget:self action:@selector(actionMapButton) forControlEvents:UIControlEventTouchUpInside];
                cell = cCell;
            }
        }
            break;
        case 1:{
            DopDetailCell *cCell = [tableView dequeueReusableCellWithIdentifier:@"dopCell"];
            dopParamModel *dopModel = [self.dataSource.dopParam objectAtIndex:indexPath.row];
            cCell.title.text = dopModel.title;
            NSString *type = [dopModel.valueType stringByReplacingOccurrencesOfString:@"<sup>" withString:@""];
            type = [type stringByReplacingOccurrencesOfString:@"</sup>" withString:@""];
            cCell.desc.text = [NSString stringWithFormat:@"%@ %@", dopModel.value, type];
            return cCell;
        }
            break;
        case 2:{
            UITableViewCell *cCell = [tableView dequeueReusableCellWithIdentifier:@"descriptionCell"];
            cCell.textLabel.text = self.dataSource.descriptionItem;
            cell = cCell;
        }
            break;
        case 3:{
            UserDetailCell *cCell = [tableView dequeueReusableCellWithIdentifier:@"userCell"];
            cCell.name.text = self.dataSource.userName;
            NSURL *urlAvatar = [NSURL URLWithString:self.dataSource.user.avatar];
            [cCell.avatarUser sd_setImageWithURL:urlAvatar placeholderImage:[UIImage imageNamed:@"avatarPlaceholder"] options:SDWebImageCacheMemoryOnly];
            [cCell.avatarUser.layer setCornerRadius:cCell.avatarUser.frame.size.height/2];
            [cCell.avatarUser.layer setMasksToBounds:YES];
            
            UserModel *user = [[DataManager sharedInstance] getUserProfile];
            [cCell.callButton setHidden:NO];
            [cCell.postButton setHidden:NO];
            if (user.userId == self.dataSource.userID) {
                [cCell.callButton setHidden:YES];
                [cCell.postButton setHidden:YES];
            }
            [cCell.callButton addTarget:self action:@selector(callPhone) forControlEvents:UIControlEventTouchUpInside];
            [cCell.postButton addTarget:self action:@selector(sendMessage) forControlEvents:UIControlEventTouchUpInside];
            cell = cCell;
        }
            break;
        case 4:{
            SimilarDetailCell *cCell = [tableView dequeueReusableCellWithIdentifier:@"similarCell"];
            [cCell setDataSourceWithModel:self.dataSource.similarAdv];
            [cCell setDelegate:self];
            cell = cCell;
        }
            break;
    }
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section{
    NSString *info = [NSString stringWithFormat:NSLocalizedString(@"detailBottomInfo",
                                                                  @"Russian: Просмотров: %li(%li сегодня)\nID:%li\n%@"),
                      (long)self.dataSource.viewsTotal,
                      (long)self.dataSource.viewsToday,
                      (long)self.dataSource.advId,
                      self.dataSource.date];
    
    if(section == 4)
        return info;
    return @"";
}

#pragma mark - Action Button

- (void)actionMapButton{
    [self.output openMapWithLat:self.dataSource.latitude lng:self.dataSource.longitude];
}

- (void)editAdvertising{
    [self.output editAdvertisingWithCatalogModel:self.catalogModel detailModel:self.dataSource];
}

//- (void)openFullScreenImage:(UIGestureRecognizer *)imagePicker{
//    
//    UIImagePicker *imgPicker = (UIImagePicker *)imagePicker.view;
//    [self.output openFullScreenImage:self.dataSource.images selectImageIndex:imgPicker.pageControl.currentPage];
//}

- (void)callPhone{
    
    if (!self.dataSource.phones.count) {
        [KVAlert alertWithTitle:nil
                        message:NSLocalizedString(@"phoneError",@"Russian: Телефон в объявлении не указан")
                 preferredStyle:UIAlertControllerStyleAlert
                     completion:nil];
        return;
    }
    
    [KVAlert alertWithTitle:NSLocalizedString(@"actPhone",@"Russian: Действия над телефоном")
                    message:NSLocalizedString(@"phone",@"Russian: Телефон:")
                    buttons:self.dataSource.phones preferredStyle:UIAlertControllerStyleAlert completion:^(NSUInteger buttonPresed) {
                        [self willSelectPhoneNumber:[self.dataSource.phones objectAtIndex:buttonPresed]];
                    }];
    
}

- (void)willSelectPhoneNumber:(NSString *)number{
    
    NSString *call = NSLocalizedString(@"call",@"Russian: Позвонить");
    NSString *copy = NSLocalizedString(@"copy",@"Russian: Скопировать");
    
    [KVAlert alertWithTitle:number message:nil buttons:@[call, copy] preferredStyle:UIAlertControllerStyleAlert
                 completion:^(NSUInteger buttonPresed) {
                     if(buttonPresed == 0){
                         NSString *phoneURLString = [NSString stringWithFormat:@"tel:%@", number];
                         NSURL *phoneURL = [NSURL URLWithString:phoneURLString];
                         [[UIApplication sharedApplication] openURL:phoneURL];
                     }else{
                         UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
                         pasteboard.string = self.dataSource.user.phoneNumber;
                     }
                 }];
}

- (void)sendReport{
    if(![[WebService sharedService] checkToken]){
        [KVAlert alertWithTitle:NSLocalizedString(@"report",@"Russian: Пожаловаться")
                        message:NSLocalizedString(@"needAuth",@"Russian: Необходима авторизация")
                 preferredStyle:UIAlertControllerStyleAlert completion:nil];
        return;
    }
    [self showReportAlert];
}

- (void)sendMessage {
    if(![[WebService sharedService] checkToken]) {
        [KVAlert alertWithTitle:NSLocalizedString(@"writeAuthor",@"Russian: Написать автору")
                        message:NSLocalizedString(@"needAuth",@"Russian: Необходима авторизация")
                 preferredStyle:UIAlertControllerStyleAlert completion:nil];
        return;
    }
    [self sendMessageAlert];
}

- (void)addToFavorite:(UIBarButtonItem *)button {
    if(![[WebService sharedService] checkToken]) {
        [KVAlert alertWithTitle:NSLocalizedString(@"addToFavorite",@"Russian: Добавить в избранное")
                        message:NSLocalizedString(@"needAuth",@"Russian: Необходима авторизация")
                 preferredStyle:UIAlertControllerStyleAlert completion:nil];
        return;
    }
    
    NSString *action = @"items_add";
    
    if(self.isFavorite) {
        action = @"items_del";
        [button setTintColor:nil];
        self.isFavorite = NO;
    }
    else {
        [button setTintColor:[UIColor yellowColor]];
        self.isFavorite = YES;
    }
    self.catalogModel.isFavorite = self.isFavorite;
    [self.output addToFavoriteWithAction:action];
}

#pragma SimilarDelegate

- (void)didSelectSimilarItem:(id)model{
    CatalogModel *cModel = model;
    [self.output openSimilarWithId:cModel.itemId isFavorite:cModel.isFavorite];
}

#pragma mark - BarButton

- (void)addBarButton{
    
    NSMutableArray *barButtonsArray = [NSMutableArray new];
    
    if (self.isMyAdv) {
        UIBarButtonItem *editButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"edit"]
                                                                       style:UIBarButtonItemStylePlain
                                                                      target:self action:@selector(editAdvertising)];
        [barButtonsArray addObject:editButton];
        if(self.dataSource.status != 5) {
            UIBarButtonItem *changeStatusButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"settings"]
                                                                                   style:UIBarButtonItemStylePlain
                                                                                  target:self action:@selector(changeStatusAction)];
            [barButtonsArray addObject:changeStatusButton];
        }
    } else {
        UIBarButtonItem *favoriteButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"isNotFavoriteStar"]
                                                                           style:UIBarButtonItemStylePlain
                                                                          target:self action:@selector(addToFavorite:)];
        self.favoriteBarButtonItem = favoriteButton;
        [barButtonsArray addObject:favoriteButton];
        
        if(self.isFavorite) {
            [favoriteButton setTintColor:[UIColor yellowColor]];
        }
        UIBarButtonItem *repotButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"report"]
                                                                        style:UIBarButtonItemStylePlain
                                                                       target:self action:@selector(sendReport)];
        
        [barButtonsArray addObject:repotButton];
    }
    
    self.navigationItem.rightBarButtonItems = barButtonsArray;
}

#pragma mark - Alert

- (void)showUpdateStatusAlert {
    NSString *status1 = @"Опубликовать";
    NSString *status2 = @"Снять с публикации";
    NSString *status3 = @"Удалить";
    
    NSArray *buttonsArray;
    
    switch (self.dataSource.status) {
        case 4:
            buttonsArray = @[status1, status3];
            break;
        case 3:
            buttonsArray = @[status2];
            break;
        default:
            break;
    }
    
    [KVAlert alertWithTitle:@"Изменить статус объявления" message:@"Выберите статус" buttons:buttonsArray
             preferredStyle:UIAlertControllerStyleAlert completion:^(NSUInteger buttonPresed) {
                 if (self.dataSource.status == 4) {
                     if(buttonPresed == 0){
                         [self.output updateAd:self.dataSource.advId withStatus:@"publicate"];
                         return;
                     }
                     
                     if(buttonPresed == 1){
                         [self.output updateAd:self.dataSource.advId withStatus:@"delete"];
                         return;
                     }
                 }
                 if (self.dataSource.status == 3) {
                     if(buttonPresed == 0){
                         [self.output updateAd:self.dataSource.advId withStatus:@"unpublicate"];
                         return;
                     }
                 }
             }];
}


- (void)changeStatusAction {
    [self showUpdateStatusAlert];
}

- (void)showReportAlert{
    NSString *reason1 = NSLocalizedString(@"reason1",@"Russian: Неверная рубрика");
    NSString *reason2 = NSLocalizedString(@"reason2",@"Russian: Запрещенный товар/услуга");
    NSString *reason3 = NSLocalizedString(@"reason3",@"Russian: Объявление не актуально");
    NSString *reason4 = NSLocalizedString(@"reason4",@"Russian: Неверный адрес");
    NSString *reason5 = NSLocalizedString(@"reason5",@"Russian: Другое");
    
    [KVAlert alertWithTitle:@"Жалоба" message:@"Укажите причину" buttons:@[
                                                                           reason1,
                                                                           reason2,
                                                                           reason3,
                                                                           reason4,
                                                                           reason5]
             preferredStyle:UIAlertControllerStyleAlert completion:^(NSUInteger buttonPresed) {
                 if(buttonPresed == 4){
                     [self repotOther];
                     return;
                 }
                 if(buttonPresed == 0)
                     [self.output sendReportWithId:1 message:@""];
                 if(buttonPresed == 1)
                     [self.output sendReportWithId:2 message:@""];
                 if(buttonPresed == 2)
                     [self.output sendReportWithId:4 message:@""];
                 if(buttonPresed == 3)
                     [self.output sendReportWithId:8 message:@""];
             }];
}

- (void)repotOther{
    UITextField *textField = [UITextField new];
    [textField setPlaceholder:NSLocalizedString(@"reason",@"Russian: Причина")];
    
    [KVAlert alertWithTitle:NSLocalizedString(@"other",@"Russian: Другое")
                    message:NSLocalizedString(@"reasonTextOther",@"Russian: Укажите причину жалобы в текстовом виде")
                    buttons:@[NSLocalizedString(@"send",@"Russian: Отправить")]
                  textField:@[textField]
                 completion:^(NSUInteger buttonPresed, NSArray<UITextField *> *textField) {
                     [self.output sendReportWithId:1024 message:[textField firstObject].text];
                 }];
}

- (void)sendMessageAlert{
    UITextField *textField = [UITextField new];
    [textField setPlaceholder:NSLocalizedString(@"message",@"Russian: Сообщение")];
    [KVAlert alertWithTitle:NSLocalizedString(@"writeToChat",@"Russian: Написать в чат")
                    message:nil
                    buttons:@[NSLocalizedString(@"send",@"Russian: Отправить")]
                  textField:@[textField]
                 completion:^(NSUInteger buttonPresed, NSArray<UITextField *> *textField) {
                     if ([[textField firstObject].text isEmpty]) {
                         [KVAlert alertWithTitle:nil message:NSLocalizedString(@"sendError",@"Russian: Нельзя отправить пустое сообщение") preferredStyle:UIAlertControllerStyleAlert completion:nil];
                         return;
                     }
                     [self.output sendMessageWithItemId:self.dataSource.advId message:[textField firstObject].text];
                 }];
}

@end
