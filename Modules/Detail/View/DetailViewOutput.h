//
//  DetailViewOutput.h
//  Gauranga
//
//  Created by Vlad Kochergin on 15/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DetailViewOutput <NSObject>

/**
 @author Vlad Kochergin

 Метод сообщает презентеру о том, что view готова к работе
 */
- (void)didTriggerViewReadyEvent;
- (void)getData;
- (void)openMapWithLat:(double)lat lng:(double)lng;
- (void)openSimilarWithId:(NSInteger)similarId isFavorite:(BOOL)isFavorite;
//- (void)openFullScreenImage:(NSArray *)image selectImageIndex:(NSInteger)selectImageIndex;
- (void)sendReportWithId:(NSInteger)reportId message:(NSString *)message;
- (void)sendMessageWithItemId:(NSInteger)itemid message:(NSString *)message;
- (void)addToFavoriteWithAction:(NSString *)action;
- (void)editAdvertisingWithCatalogModel:(id)catalogModel detailModel:(id)detailModel;
- (void)updateAd:(NSInteger)adID withStatus:(NSString *)status;

@end
