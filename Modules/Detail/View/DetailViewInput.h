//
//  DetailViewInput.h
//  Gauranga
//
//  Created by Vlad Kochergin on 15/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CatalogModel.h"

@protocol DetailViewInput <NSObject>

/**
 @author Vlad Kochergin

 Метод настраивает начальный стейт view
 */
@property (nonatomic) BOOL isFavorite;
@property (nonatomic) BOOL isMyAdv;
@property (nonatomic, strong) CatalogModel *catalogModel;

- (void)setupInitialState;
- (void)reloadData:(id)model;
- (void)updateFaforiteIcon:(BOOL)value;
- (void)closeViewController;

@end
