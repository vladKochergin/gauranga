//
//  RootDetailView.m
//  Gauranga
//
//  Created by yvp on 2/15/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

#import "RootDetailView.h"

@interface RootDetailView ()<UITableViewDelegate, UITableViewDataSource>

@end

@implementation RootDetailView

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark - UITableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger rowCount = 0;
    
    switch (section) {
        case 0:
            rowCount = 2;
            break;
        case 1:
            rowCount = self.dopRowCount;
            break;
        case 2:
            rowCount = 1;
            break;
        case 3:
            rowCount = 1;
            break;
        case 4:
            rowCount = 1;
            break;
    }
    
    return rowCount;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if(section == 4) {
        return 30.0f;
    }
    return 1.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if(section == 4) {
        return 70.0f;
    }
    return 7.0f;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if(section == 4) {
        return NSLocalizedString(@"similarAdv",@"Russian: Похожие объявления");
    }
    return @"";
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    return cell;
}

@end
