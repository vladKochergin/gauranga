//
//  DetailViewController.h
//  Gauranga
//
//  Created by Vlad Kochergin on 15/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DetailViewInput.h"
#import "RootDetailView.h"

@protocol DetailViewOutput;

@interface DetailViewController : RootDetailView <DetailViewInput>

@property (nonatomic, strong) id<DetailViewOutput> output;

@end
