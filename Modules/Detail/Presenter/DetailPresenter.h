//
//  DetailPresenter.h
//  Gauranga
//
//  Created by Vlad Kochergin on 15/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "DetailViewOutput.h"
#import "DetailInteractorOutput.h"
#import "DetailModuleInput.h"

@protocol DetailViewInput;
@protocol DetailInteractorInput;
@protocol DetailRouterInput;

@interface DetailPresenter : NSObject <DetailModuleInput, DetailViewOutput, DetailInteractorOutput>

@property (nonatomic, weak) id<DetailViewInput> view;
@property (nonatomic, strong) id<DetailInteractorInput> interactor;
@property (nonatomic, strong) id<DetailRouterInput> router;

@end
