//
//  DetailModuleInput.h
//  Gauranga
//
//  Created by Vlad Kochergin on 15/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>

@protocol DetailModuleInput <RamblerViperModuleInput>

/**
 @author Vlad Kochergin

 Метод инициирует стартовую конфигурацию текущего модуля
 */

- (void)configureModuleWithAdvId:(NSInteger)advId model:(id)model isFavorite:(BOOL)isFavorete isMyAdv:(BOOL)isMyAdv;

- (void)configureModuleWithAdvId:(NSInteger)advId isFavorite:(BOOL)isFavorete isMyAdv:(BOOL)isMyAdv;

@end
