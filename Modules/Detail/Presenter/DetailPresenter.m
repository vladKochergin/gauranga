//
//  DetailPresenter.m
//  Gauranga
//
//  Created by Vlad Kochergin on 15/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "DetailPresenter.h"

#import "DetailViewInput.h"
#import "DetailInteractorInput.h"
#import "DetailRouterInput.h"

@implementation DetailPresenter

#pragma mark - Методы DetailModuleInput

- (void)configureModuleWithAdvId:(NSInteger)advId model:(id)model isFavorite:(BOOL)isFavorete isMyAdv:(BOOL)isMyAdv {
    [self.interactor setAdvId:advId];
    [self.view setIsMyAdv:isMyAdv];
    [self.view setIsFavorite:isFavorete];
//    [self.view setCatalogModel:model];
    [self.interactor setCatalogModel:model];
}

- (void)configureModuleWithAdvId:(NSInteger)advId isFavorite:(BOOL)isFavorete isMyAdv:(BOOL)isMyAdv {
    [self configureModuleWithAdvId:advId model:nil isFavorite:isFavorete isMyAdv:isMyAdv];
}

#pragma mark - Методы DetailViewOutput

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
}

- (void)getData{
    [self.interactor getData];
}

- (void)updateAd:(NSInteger)adID withStatus:(NSString *)status {
    [self.interactor updateAd:adID withStatus:status];
}

- (void)editAdvertisingWithCatalogModel:(id)catalogModel detailModel:(id)detailModel {
    [self.router editAdvertisingWithCatalogModel:catalogModel detailModel:detailModel];
}

- (void)openMapWithLat:(double)lat lng:(double)lng{
    [self.router openMapWithLat:lat lng:lng];
}

- (void)openSimilarWithId:(NSInteger)similarId isFavorite:(BOOL)isFavorite{
    [self.router openSimilarWithId:similarId isFavorite:isFavorite];
}

//- (void)openFullScreenImage:(NSArray *)image selectImageIndex:(NSInteger)selectImageIndex{
//    [self.router openFullScreenImage:image selectImageIndex:selectImageIndex];
//}

- (void)sendReportWithId:(NSInteger)reportId message:(NSString *)message{
    [self.interactor sendReportWithId:reportId message:message];
}

- (void)sendMessageWithItemId:(NSInteger)itemid message:(NSString *)message{
    [self.interactor sendMessageWithItemId:itemid message:message];
}

- (void)addToFavoriteWithAction:(NSString *)action{
    [self.interactor addToFavoriteWithAction:action];
}

#pragma mark - Методы DetailInteractorOutput

- (void)reloadData:(id)model catalogModel:(id)catalogModel{
    [self.view setCatalogModel:catalogModel];
    [self.view reloadData:model];
}

- (void)updateFaforiteIcon:(BOOL)value {
    [self.view updateFaforiteIcon:value];
}

- (void)closeViewController {
    [self.router closeViewController];
}

@end
