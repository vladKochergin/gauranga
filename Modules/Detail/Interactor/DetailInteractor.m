//
//  DetailInteractor.m
//  Gauranga
//
//  Created by Vlad Kochergin on 15/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "DetailInteractor.h"

#import "DetailInteractorOutput.h"

#import "RequestMapManager.h"
#import "DetailAdvertisingModel.h"
#import "CatalogModel.h"

@interface DetailInteractor()

@property (nonatomic, strong) DetailAdvertisingModel *dataSource;

@end


@implementation DetailInteractor

@synthesize advId;
@synthesize catalogModel;

#pragma mark - Методы DetailInteractorInput

- (void)getData{
    [RequestMapManager getDetailAdvertisingWithId:self.advId completion:^(NSArray *array, NSDictionary *dictionary, id object) {
        self.dataSource = object;
        [self.output reloadData:self.dataSource catalogModel:self.catalogModel];
    }];
}

- (void)sendReportWithId:(NSInteger)reportId message:(NSString *)message{
    [[WebService sharedService] sendReportWithId:reportId message:message
                                           advId:self.dataSource.advId
                                      completion:^(NSError *error, NSArray *array, NSDictionary *dictionary) {
                                          if (error) {
                                              if([error.userInfo[@"message"] isEqualToString:@"CLAIM_EMPTY_MESSAGE"]) {
                                                  
                                                  [KVAlert alertWithTitle:nil
                                                                  message:NSLocalizedString(@"characterLimit", @"Russian: Сообщение должно содержать больше 10 символов")
                                                           preferredStyle:UIAlertControllerStyleAlert
                                                               completion:^{}];
                                              }
                                          }
                                      }];
}

- (void)sendMessageWithItemId:(NSInteger)itemid message:(NSString *)message{
    [[WebService sharedService] sendMessageWithItemId:itemid
                                              message:message
                                           completion:^(NSError *error,
                                                        NSArray *array,
                                                        NSDictionary *dictionary) {
                                               if (error) {
                                                   if([error.userInfo[@"message"] isEqualToString:@"MAIL_TIMELIMIT"]) {
                                                       
                                                       [KVAlert alertWithTitle:[NSString stringWithFormat:NSLocalizedString(@"timeLimit", @"Russian: Сообщение можно отправлять каждые %@ секунд(ы)"), error.userInfo[@"dtime"]]
                                                                       message:[NSString stringWithFormat:NSLocalizedString(@"timeLeft", @"Russian: Осталось %@ секунд(ы)"), error.userInfo[@"left"]]
                                                                preferredStyle:UIAlertControllerStyleAlert
                                                                    completion:^{}];
                                                   }
                                               }
                                           }];
}

- (void)addToFavoriteWithAction:(NSString *)action{
    [[WebService sharedService] addToFavoriteWithItemId:self.dataSource.advId
                                                 action:action
                                             completion:^(NSError *error, NSArray *array, NSDictionary *dictionary) {
                                                 if (error) {
                                                     [KVAlert alertWithTitle:nil
                                                                     message:NSLocalizedString(@"networkError", @"Russian: Ошибка связи)")
                                                              preferredStyle:UIAlertControllerStyleAlert
                                                                  completion:^{
                                                                      [self.output updateFaforiteIcon:YES];
                                                                  }];
                                                 }
                                                 if (![dictionary[@"RESULT"] integerValue]) {
                                                     [self.output updateFaforiteIcon:YES];
                                                 }
                                             }];
}

- (void)updateAd:(NSInteger)adID withStatus:(NSString *)status {
    [self changeAdStatus:status];
    [RequestMapManager updateStatusAdvertising:advId status:status completion:^(NSArray *array, NSDictionary *dictionary, NSError *error) {
        if (!error) {
            if(![status isEqualToString:@"delete"])
                [self.output reloadData:self.dataSource catalogModel:self.catalogModel];
        }
    }];
}

- (void)changeAdStatus:(NSString *)status{
    if([status isEqualToString:@"unpublicate"]){
        self.catalogModel.status = [@(4) stringValue];
        self.dataSource.status = 4;
    }
    else if([status isEqualToString:@"publicate"]){
        self.catalogModel.status = [@(3) stringValue];
        self.dataSource.status = 3;
    }else if([status isEqualToString:@"delete"]){
        [self.output closeViewController];
        //        self.catalogModel = nil;
    }
}

@end
