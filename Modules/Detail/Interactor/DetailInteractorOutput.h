//
//  DetailInteractorOutput.h
//  Gauranga
//
//  Created by Vlad Kochergin on 15/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DetailInteractorOutput <NSObject>

- (void)reloadData:(id)model catalogModel:(id)catalogModel;
- (void)updateFaforiteIcon:(BOOL)value;
- (void)closeViewController;

@end
