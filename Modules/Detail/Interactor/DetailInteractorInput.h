//
//  DetailInteractorInput.h
//  Gauranga
//
//  Created by Vlad Kochergin on 15/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CatalogModel;

@protocol DetailInteractorInput <NSObject>

- (void)setAdvId:(NSInteger)advId;
- (void)getData;
- (void)sendReportWithId:(NSInteger)reportId message:(NSString *)message;
- (void)sendMessageWithItemId:(NSInteger)itemid message:(NSString *)message;
- (void)addToFavoriteWithAction:(NSString *)action;
- (void)updateAd:(NSInteger)adID withStatus:(NSString *)status;

@property(nonatomic) NSInteger advId;
@property(nonatomic) CatalogModel *catalogModel;

@end
