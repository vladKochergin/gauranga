//
//  DetailInteractor.h
//  Gauranga
//
//  Created by Vlad Kochergin on 15/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "DetailInteractorInput.h"

@protocol DetailInteractorOutput;

@interface DetailInteractor : NSObject <DetailInteractorInput>

@property (nonatomic, weak) id<DetailInteractorOutput> output;

@end
