//
//  DetailAssembly.m
//  Gauranga
//
//  Created by Vlad Kochergin on 15/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "DetailAssembly.h"

#import "DetailViewController.h"
#import "DetailInteractor.h"
#import "DetailPresenter.h"
#import "DetailRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation DetailAssembly

- (DetailViewController *)viewDetail {
    return [TyphoonDefinition withClass:[DetailViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterDetail]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterDetail]];
                          }];
}

- (DetailInteractor *)interactorDetail {
    return [TyphoonDefinition withClass:[DetailInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterDetail]];
                          }];
}

- (DetailPresenter *)presenterDetail{
    return [TyphoonDefinition withClass:[DetailPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewDetail]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorDetail]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerDetail]];
                          }];
}

- (DetailRouter *)routerDetail{
    return [TyphoonDefinition withClass:[DetailRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewDetail]];
                          }];
}

@end
