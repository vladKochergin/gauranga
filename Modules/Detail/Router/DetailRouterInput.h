//
//  DetailRouterInput.h
//  Gauranga
//
//  Created by Vlad Kochergin on 15/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DetailRouterInput <NSObject>

- (void)openMapWithLat:(double)lat lng:(double)lng;
- (void)openSimilarWithId:(NSInteger)similarId isFavorite:(BOOL)isFavorite;
//- (void)openFullScreenImage:(NSArray *)image selectImageIndex:(NSInteger)selectImageIndex;
- (void)editAdvertisingWithCatalogModel:(id)catalogModel detailModel:(id)detailModel;
- (void)closeViewController;

@end
