//
//  DetailRouter.h
//  Gauranga
//
//  Created by Vlad Kochergin on 15/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "DetailRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface DetailRouter : NSObject <DetailRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
