//
//  DetailRouter.m
//  Gauranga
//
//  Created by Vlad Kochergin on 15/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "DetailRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

#import "MapModuleInput.h"
#import "DetailModuleInput.h"
#import "FullScreenModuleInput.h"
#import "EditAdvertisingModuleInput.h"

@implementation DetailRouter

- (RamblerViperModuleFactory *)transitionModuleFactory:(NSString *)indfViewController{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    RamblerViperModuleFactory *factory = [[RamblerViperModuleFactory alloc]initWithStoryboard:storyBoard andRestorationId:indfViewController];
    return factory;
}

#pragma mark - Методы DetailRouterInput

- (void)closeViewController{
    [self.transitionHandler closeCurrentModule:YES];
}

- (void)openMapWithLat:(double)lat lng:(double)lng{
    [[self.transitionHandler openModuleUsingSegue:@"mapViewController"] thenChainUsingBlock:^id<RamblerViperModuleOutput>(id<MapModuleInput> moduleInput) {
        [moduleInput configureModuleWithLat:lat lng:lng];
        return nil;
    }];
}

- (void)editAdvertisingWithCatalogModel:(id)catalogModel detailModel:(id)detailModel{
    [[self.transitionHandler openModuleUsingSegue:@"editAdvertising"] thenChainUsingBlock:^id<RamblerViperModuleOutput>(id<EditAdvertisingModuleInput> moduleInput) {
        [moduleInput configureModuleWithCatalogModel:catalogModel detailModel:detailModel];
        return nil;
    }];
}

- (void)openSimilarWithId:(NSInteger)similarId isFavorite:(BOOL)isFavorite{
    [[self.transitionHandler openModuleUsingFactory:[self transitionModuleFactory:@"detailViewController"] withTransitionBlock:^(id<RamblerViperModuleTransitionHandlerProtocol> sourceModuleTransitionHandler, id<RamblerViperModuleTransitionHandlerProtocol> destinationModuleTransitionHandler) {
        UIViewController *source = (id)sourceModuleTransitionHandler;
        UIViewController *destin = (id)destinationModuleTransitionHandler;
        [source.navigationController pushViewController:destin animated:YES];
    }] thenChainUsingBlock:^id<RamblerViperModuleOutput>(id<DetailModuleInput> moduleInput) {
        [moduleInput configureModuleWithAdvId:similarId isFavorite:isFavorite isMyAdv:NO];
        return nil;
    }];
}

//- (void)openFullScreenImage:(NSArray *)image selectImageIndex:(NSInteger)selectImageIndex{
//    [[self.transitionHandler openModuleUsingFactory:[self transitionModuleFactory:@"fullScreenView"] withTransitionBlock:^(id<RamblerViperModuleTransitionHandlerProtocol> sourceModuleTransitionHandler, id<RamblerViperModuleTransitionHandlerProtocol> destinationModuleTransitionHandler) {
//        UIViewController *source = (id)sourceModuleTransitionHandler;
//        UIViewController *destin = (id)destinationModuleTransitionHandler;
//        
//        CATransition* transition = [CATransition animation];
//        transition.duration = 0.3;
//        transition.type = kCATransitionFade;
//        transition.subtype = kCATransitionFromBottom;
//        [source.view.window.layer addAnimation:transition forKey:kCATransition];
//        
//        [source presentViewController:destin animated:NO completion:nil];
//        
//    }] thenChainUsingBlock:^id<RamblerViperModuleOutput>(id<FullScreenModuleInput> moduleInput) {
//        [moduleInput configureModuleImageArray:image selectedImageIndex:selectImageIndex];
//        return nil;
//    }];
//}

@end
