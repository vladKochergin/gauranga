//
//  FullScreenViewInput.h
//  Gauranga
//
//  Created by Vlad Kochergin on 16/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FullScreenViewInput <NSObject>

/**
 @author Vlad Kochergin

 Метод настраивает начальный стейт view
 */
- (void)setupInitialState;

@property(nonatomic, strong) NSArray *dataSource;
@property(nonatomic) NSInteger selectedImage;

@end
