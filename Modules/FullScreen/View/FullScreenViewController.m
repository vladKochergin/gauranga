//
//  FullScreenViewController.m
//  Gauranga
//
//  Created by Vlad Kochergin on 16/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "FullScreenViewController.h"

#import "FullScreenViewOutput.h"
#import "UIImagePicker.h"

@interface FullScreenViewController()

@property (weak, nonatomic) IBOutlet UIImagePicker *imagePicker;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;

@end

@implementation FullScreenViewController

@synthesize dataSource;
@synthesize selectedImage;

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];

	[self.output didTriggerViewReadyEvent];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self.imagePicker setDataSource:self.dataSource selectedIndex:self.selectedImage];
    [self.closeButton setTitle:NSLocalizedString(@"close", @"Russian : Закрыть")
                      forState: UIControlStateNormal];
    [self.imagePicker bringSubviewToFront:self.closeButton];
}

#pragma mark - Методы FullScreenViewInput

- (void)setupInitialState {
}

- (IBAction)closeViewController:(id)sender {
    CATransition* transition = [CATransition animation];
    transition.duration = 0.3;
    transition.type = kCATransitionFade;
    transition.subtype = kCATransitionFromBottom;
    [self.view.window.layer addAnimation:transition forKey:kCATransition];
    [self dismissViewControllerAnimated:NO completion:nil];
}

@end
