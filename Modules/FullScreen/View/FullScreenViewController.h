//
//  FullScreenViewController.h
//  Gauranga
//
//  Created by Vlad Kochergin on 16/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FullScreenViewInput.h"

@protocol FullScreenViewOutput;

@interface FullScreenViewController : UIViewController <FullScreenViewInput>

@property (nonatomic, strong) id<FullScreenViewOutput> output;

@end
