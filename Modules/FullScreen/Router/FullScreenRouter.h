//
//  FullScreenRouter.h
//  Gauranga
//
//  Created by Vlad Kochergin on 16/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "FullScreenRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface FullScreenRouter : NSObject <FullScreenRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
