//
//  FullScreenAssembly.m
//  Gauranga
//
//  Created by Vlad Kochergin on 16/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "FullScreenAssembly.h"

#import "FullScreenViewController.h"
#import "FullScreenInteractor.h"
#import "FullScreenPresenter.h"
#import "FullScreenRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation FullScreenAssembly

- (FullScreenViewController *)viewFullScreen {
    return [TyphoonDefinition withClass:[FullScreenViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterFullScreen]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterFullScreen]];
                          }];
}

- (FullScreenInteractor *)interactorFullScreen {
    return [TyphoonDefinition withClass:[FullScreenInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterFullScreen]];
                          }];
}

- (FullScreenPresenter *)presenterFullScreen{
    return [TyphoonDefinition withClass:[FullScreenPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewFullScreen]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorFullScreen]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerFullScreen]];
                          }];
}

- (FullScreenRouter *)routerFullScreen{
    return [TyphoonDefinition withClass:[FullScreenRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewFullScreen]];
                          }];
}

@end
