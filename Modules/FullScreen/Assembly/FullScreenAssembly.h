//
//  FullScreenAssembly.h
//  Gauranga
//
//  Created by Vlad Kochergin on 16/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>

/**
 @author Vlad Kochergin

 FullScreen module
 */
@interface FullScreenAssembly : TyphoonAssembly <RamblerInitialAssembly>

@end
