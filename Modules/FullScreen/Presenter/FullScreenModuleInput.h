//
//  FullScreenModuleInput.h
//  Gauranga
//
//  Created by Vlad Kochergin on 16/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>

@protocol FullScreenModuleInput <RamblerViperModuleInput>

/**
 @author Vlad Kochergin

 Метод инициирует стартовую конфигурацию текущего модуля
 */
- (void)configureModuleImageArray:(NSArray *)image selectedImageIndex:(NSInteger)selectedIndex;

@end
