//
//  FullScreenPresenter.m
//  Gauranga
//
//  Created by Vlad Kochergin on 16/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "FullScreenPresenter.h"

#import "FullScreenViewInput.h"
#import "FullScreenInteractorInput.h"
#import "FullScreenRouterInput.h"

@implementation FullScreenPresenter

#pragma mark - Методы FullScreenModuleInput

- (void)configureModuleImageArray:(NSArray *)image selectedImageIndex:(NSInteger)selectedIndex {
    [self.view setDataSource:image];
    [self.view setSelectedImage:selectedIndex];
}

#pragma mark - Методы FullScreenViewOutput

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
}

#pragma mark - Методы FullScreenInteractorOutput

@end
