//
//  FullScreenPresenter.h
//  Gauranga
//
//  Created by Vlad Kochergin on 16/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "FullScreenViewOutput.h"
#import "FullScreenInteractorOutput.h"
#import "FullScreenModuleInput.h"

@protocol FullScreenViewInput;
@protocol FullScreenInteractorInput;
@protocol FullScreenRouterInput;

@interface FullScreenPresenter : NSObject <FullScreenModuleInput, FullScreenViewOutput, FullScreenInteractorOutput>

@property (nonatomic, weak) id<FullScreenViewInput> view;
@property (nonatomic, strong) id<FullScreenInteractorInput> interactor;
@property (nonatomic, strong) id<FullScreenRouterInput> router;

@end
