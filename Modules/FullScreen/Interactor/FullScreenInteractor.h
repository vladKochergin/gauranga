//
//  FullScreenInteractor.h
//  Gauranga
//
//  Created by Vlad Kochergin on 16/02/2017.
//  Copyright © 2017 Vlad Kochergin. All rights reserved.
//

#import "FullScreenInteractorInput.h"

@protocol FullScreenInteractorOutput;

@interface FullScreenInteractor : NSObject <FullScreenInteractorInput>

@property (nonatomic, weak) id<FullScreenInteractorOutput> output;

@end
